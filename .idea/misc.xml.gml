graph [
    node [
        id 0
         graphics [  w 170.0000000 ]
         label "dblp.datatools"
    ]
    node [
        id 1
         graphics [  w 170.0000000 ]
         label "code"
    ]
    node [
        id 2
         graphics [  w 170.0000000 ]
         label "dblp.datatools.test"
    ]
    node [
        id 3
         graphics [  w 170.0000000 ]
         label "dblp.datatools.main"
    ]
    edge [
        source 2
        target 3
    ]
]

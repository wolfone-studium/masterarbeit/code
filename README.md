Dieses Repository enthält den Code, der im Zuge der
Masterarbeit von Philipp Bous im Bereich Informatik an der
Universität Trier 2019 entstanden ist.

Benötigt Java 11.

Die Abhängigkeitsverwaltung findet mittels Gradle (4.10.2) statt.

Build via:

    gradle build

**Sollte Gradle aus einer IDE heraus genutzt werden, kann es vorkommen (z.B. bei Nutzung von IntelliJ), dass die genutzte JVM für Gradle separat mittels der Settings gesetzt werden muss.** 

**Sollte es gewünscht sein den Code aus einer IDE heraus auszuführen, ist es evtl. nötig Annotation Preprocessing manuell zu aktivieren.**
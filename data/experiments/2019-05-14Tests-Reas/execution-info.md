*This file contains commit hashes and needed executables for result-recreation.*

(For major semantic-changing commits for vocabulary-building, vectorization and training/test/CV-scenarios 
there exist tags that indicate the change that was made, so this may be
the easiest way to navigate the repo with a visual tool.)

If ranges for commits are given, the executable will be referring to the latest commit in this range.

## Vectorization:
* commit: f94199bb7c4c8d269b923717cc5a4b42cb6ad171 to d6ede08e33f17d3e61e1ce9ae95107c82a05b149
* executable class: bous.philipp.dblp.datatools.executables.vectorization.ConvertSingleColumnToSVMLightCVSetUnbalanced

## Training:
* commit: 2216d1f1f4b86381cd33a38188bd34ec73597724 to latest master commit
* executable class: bous.philipp.dblp.datatools.executables.modelbuilding.svmlogreg.TrainLiblinearAndWriteModelFile

## Testing:
* commit: 2216d1f1f4b86381cd33a38188bd34ec73597724 to latest master commit
* executable class: bous.philipp.dblp.datatools.executables.testing.svmlogreg.TestLiblinearModel


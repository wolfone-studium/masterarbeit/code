*This file contains commit hashes and needed executables for result-recreation.*

(For major semantic-changing commits for vocabulary-building, vectorization and training/test/CV-scenarios 
there exist tags that indicate the change that was made, so this may be
the easiest way to navigate the repo with a visual tool.)

If ranges for commits are given, the executable will be referring to the latest commit in this range.

## Vocabulary-Building:
* commit: b9d8e9e3f95923ba66e619d5e90fa27a7d2d2b46 to 0d65cc1eaddd01fcb41d58fe6c720c377e2cb5aa  
* executable class: bous.philipp.dblp.datatools.executables.vocabbuilding.BuildScoredVocabularyBalanced

## Vectorization:
* commit: f94199bb7c4c8d269b923717cc5a4b42cb6ad171 to 0d65cc1eaddd01fcb41d58fe6c720c377e2cb5aa
* executable class: bous.philipp.dblp.datatools.executables.vectorization.ConvertSingleColumnToSVMLightCVSetBalanced

## Cross-Validation:
* commit: compatible with latest master-commit
* executable class: bous.philipp.dblp.datatools.executables.modelbuilding.svmlogreg.TrainLiblinearAndDoCrossvalidation


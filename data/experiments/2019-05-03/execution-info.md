*This file contains commit hashes and needed executables for result-recreation.*

(For major semantic-changing commits for vocabulary-building, vectorization and training/test/CV-scenarios 
there exist tags that indicate the change that was made, so this may be
the easiest way to navigate the repo with a visual tool.)

If ranges for commits are given, the executable will be referring to the latest commit in this range.

## Cross-Validation:
* commit: compatible with latest master-commit
* executable class: bous.philipp.dblp.datatools.executables.modelbuilding.svmlogreg.TrainLiblinearAndDoCrossvalidation


Testing impact of feature-order-shuffling based on vocabulary:

**2019-06-27VocabDissTest4000VocabOnTotalAdvRandomness(0)**

(build on the total CV Data from arxiv)

Tests will be conducted on the balanced arxiv testset.

Subfolder-names are the respective numbers that were used as randomization
seeds in: 

**bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC.RandomizeVocabOrderInFile.main**

to shuffle the base vocabulary (latest master commit).
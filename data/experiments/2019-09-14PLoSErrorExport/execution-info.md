*This file contains commit hashes and needed executables for result-recreation.*

(For major semantic-changing commits for vocabulary-building, vectorization and training/test/CV-scenarios 
there exist tags that indicate the change that was made, so this may be
the easiest way to navigate the repo with a visual tool.)


**COMMIT FOR ALL: 65a1794086e485325ba2af03a6b797b2e2629e5d**


## Vocabulary-Building:
* executable class: bous.philipp.dblp.datatools.executables.vocabbuilding.BuildScoredVocabularyBalanced

## Vectorization (balanced):
* executable class: bous.philipp.dblp.datatools.executables.vectorization.BOWConvertSingleColumnToSVMLightCVSetBalanced

## Vectorization (unbalanced):
* executable class: bous.philipp.dblp.datatools.executables.vectorization.BOWConvertSingleColumnToSVMLightCVSetUnbalanced

## Training:
* executable class: bous.philipp.dblp.datatools.executables.modelbuilding.svmlogreg.TrainLiblinearAndWriteModelFile

## Testing:
* executable class: bous.philipp.dblp.datatools.executables.testing.svmlogreg.TestLiblinearModel


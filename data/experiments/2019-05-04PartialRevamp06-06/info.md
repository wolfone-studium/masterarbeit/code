#### Intention
*Intended to check up on the performance estimations from 2019-05-04 and previous optimization runs.*

See experiment-folders **2019-05-04** and previous.

#### Background
Until now a single vocabulary was used for every fold in CV. The newer Code-Version
supports individual vocabularies for every fold.

Until now every vocabulary was based on the entire CV-Set.

We want to check how big the impact is if the vocabulary is build only based on those
records that are NOT in the validation-part of each fold individually.

This test is done to estimate the "thread to validity" of the initial performance
estimations. There is a small chance that the painting of a "perfect world", where
every vocab is known, negatively impacts the models performance in production use.

However in our case, it is expected that the effect is absolutely marginal for following reasons:

* The effect would already have shown when evaluating the choosen models on the test-sets 
(see folder 2019-05-04Tests) as both
of them were hold-out sets that were not involved in either CV training or
vocabulary-building.

* We have access to a large amount of documents; new documents are quite unlikely to
hold much surprise when it comes to the vocabulary.

* The previous point is underlined by the fact that we look at scientific texts
where we can expect the vocabulary to change less fast than for example in
contemporary youth language.

#### Procedure

As most of the relevant trainings and tests refer to the vocabulary 2019-04-22(6) we will
conduct our revamp-test on the same setting but will build the appropriate vocabulary-set
for a 10-fold CV.
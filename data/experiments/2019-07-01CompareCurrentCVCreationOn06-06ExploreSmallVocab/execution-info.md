*This file contains commit hashes and needed executables for result-recreation.*

(For major semantic-changing commits for vocabulary-building, vectorization and training/test/CV-scenarios 
there exist tags that indicate the change that was made, so this may be
the easiest way to navigate the repo with a visual tool.)

If ranges for commits are given, the executable will be referring to the latest commit in this range.

## Vocabulary-Building:
* commit: 4d56f2853f2e2dc71780021a631f35a2bbb360d9 to 73a185106fcc98b58c7b448ba1c1657e3b3c7625
* executable class: bous.philipp.dblp.datatools.executables.vocabbuilding.BuildScoredVocabularyBalanced

## Vectorization (balanced):
* commit: 9efcc982e001a342241ea909236e439d29210c0f to latest master commit
* executable class: bous.philipp.dblp.datatools.executables.vectorization.BOWConvertSingleColumnToSVMLightCVSetBalanced

## Cross-Validation:
* commit: latest master-commit
* executable class: bous.philipp.dblp.datatools.executables.modelbuilding.svmlogreg.TrainLiblinearAndDoCrossvalidation


All vocab-sets in subfolders (except vocabBase) are manual reductions of the vocabset 2019-06-25AdvRandomnessBaseVocab done by selecting the top n terms.
The foldername indicates how many of the top vocabs by fisherscore are preserved from 
the original vocabulary-set.
 
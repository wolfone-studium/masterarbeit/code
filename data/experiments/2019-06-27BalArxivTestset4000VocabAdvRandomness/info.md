Based on model from:

**2019-06-27TestDissTest4000VocabAdvRandomness**

so only testvectors and testconfig are different.

Advanced randomness in record-drawing is used but does literally nothing in this as all entries available are drawn
and put into the same set.
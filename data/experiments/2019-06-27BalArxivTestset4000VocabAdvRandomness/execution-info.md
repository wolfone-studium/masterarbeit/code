*This file contains commit hashes and needed executables for result-recreation.*

(For major semantic-changing commits for vocabulary-building, vectorization and training/test/CV-scenarios 
there exist tags that indicate the change that was made, so this may be
the easiest way to navigate the repo with a visual tool.)

If ranges for commits are given, the executable will be referring to the latest commit in this range.

## Vectorization (balanced):
* for: 2019-06-27TestVectorsBalArxivTestset4000VocabAdvRandomness.properties
* commit: 9efcc982e001a342241ea909236e439d29210c0f to latest master commit
* executable class: bous.philipp.dblp.datatools.executables.vectorization.BOWConvertSingleColumnToSVMLightCVSetBalanced

## Testing:
* commit: 2216d1f1f4b86381cd33a38188bd34ec73597724 to latest master commit
* executable class: bous.philipp.dblp.datatools.executables.testing.svmlogreg.TestLiblinearModel


*This file contains commit hashes and needed executables for result-recreation.*

(For major semantic-changing commits for vocabulary-building, vectorization and training/test/CV-scenarios 
there exist tags that indicate the change that was made, so this may be
the easiest way to navigate the repo with a visual tool.)

If ranges for commits are given, the executable will be referring to the latest commit in this range.

## Vocabulary-Building:
* commit: 0b37075061e5df4bb6e62df6ca929fd422c8df37 to latest master commit
* executable class: bous.philipp.dblp.datatools.executables.vocabbuilding.BuildScoredVocabularyBalanced

## Vectorization (balanced):
* commit: 9efcc982e001a342241ea909236e439d29210c0f to latest master commit
* executable class: bous.philipp.dblp.datatools.executables.vectorization.BOWConvertSingleColumnToSVMLightCVSetBalanced

## Cross-Validation:
* commit: compatible with latest master-commit
* executable class: bous.philipp.dblp.datatools.executables.modelbuilding.svmlogreg.TrainLiblinearAndDoCrossvalidation


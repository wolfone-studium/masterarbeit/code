package bous.philipp.dblp.datatools.featurescoring;


import java.util.*;

/**
 * Class providing means to perform an analysis to calculate tf-idf-values for a set of terms/documents.
 * Requires the user to indicate when the currently observed document changes.
 */
public class TfIdfAnalyzer {
    Map<Integer, Integer> docNumberAndNumberOfTermsInThisDoc;
    Map<String, TermInfo> termInfos;

    private List<Map.Entry<String, TermInfo>> termInfoList; //too boost runtime; needs call of finish() to be able to unleash its power
    /**
     * An integer used to differentiate between objects; starts with 1 and is incremented with each call of nextDocument() .
     * The user is responsible for mapping these numbers back to the correct documents when calculating things like the tf-idf for a document.
     */
    int currentDocNumber;


    /**
     * Has to be called to signal to the TfidfAnalyzer that the document switched to a new one.
     */
    public void nextDocument(){
        this.currentDocNumber++;
    }

    /**
     * Standard-constructor that will initialize the first document's number to be 1 beside necessary initialization.
     */
    public TfIdfAnalyzer(){
        this.currentDocNumber = 1;
        docNumberAndNumberOfTermsInThisDoc = new HashMap<>();
        termInfos = new HashMap<>();
    }

    /**
     * Updates the analysis with a sighting of the given term for the current document.
     * @param term the term
     */
    public void updateForTerm(String term){
        //increase number of terms in the current doc
        docNumberAndNumberOfTermsInThisDoc.merge(currentDocNumber, 1, Integer::sum);
        TermInfo termInfo = termInfos.get(term);
        if (termInfo == null){
            termInfo = new TermInfo(term, currentDocNumber);
            termInfos.put(term, termInfo);
        }else{
            termInfo.addSightingInDocument(currentDocNumber);
        }
    }

    /**
     * Updates the analysis with sightings of the given terms for the current document.
     * @param terms the terms
     */
    public void updateForTerms(List<String> terms){
        for (int i = 0; i < terms.size(); i++) {
            String term = terms.get(i);
            updateForTerm(term);
        }
    }

    /**
     * Updates the analysis with sightings of the given terms for the current document.
     * @param terms the terms
     */
    public void updateForTerms(String[] terms) {
        for (int i = 0; i < terms.length; i++) {
            String term = terms[i];
            updateForTerm(term);
        }
    }


    /**
     * Returns the number of occurrences of a given term in a given document as known by this analysis meaning it
     * is not useful before the analysis has been done.
     * @param term the term in question
     * @param docNumber the document number in question
     * @return the number of occurrences of term in the document with the given number
     */
    public int getNumberOfoccurrencesOfTermInDocument(String term, int docNumber){
        if (termInfos.get(term) != null){
            return termInfos.get(term).numberOfSightingsIn(docNumber);
        }
        return 0;
    }

    /**
     * Returns information about tfidf in the form of a map. The keys are the terms for which a tf-idf is calculated;
     * the value is again a map which maps the document-number as key to the value representing the if-idf of the term
     * for this specific document.
     * @return tfidf-information
     */
    public Map<String, Map<Integer, Double>> getTfIdfAsMap(){
        Map<String, Map<Integer, Double>> tfidfs = new HashMap<>();

        Set<Map.Entry<String, TermInfo>> entries = this.termInfos.entrySet();
        List<Map.Entry<String, TermInfo>> entriesList;
        entriesList = Objects.requireNonNullElseGet(
                this.termInfoList,
                () -> new ArrayList<>(entries)
        );
        for (int i = 0; i < entriesList.size(); i++) {
            Map.Entry<String, TermInfo> stringTermInfoEntry = entriesList.get(i);
            String term = stringTermInfoEntry.getKey();
            Map<Integer, Double> docNumbersAndTfidfs = new HashMap<>();
            for (int j = 1; j <= currentDocNumber; j++){
                docNumbersAndTfidfs.put(j, tfidf(term, j));
            }
            tfidfs.put(term, docNumbersAndTfidfs);
        }
        return tfidfs;
    }

    //NOTE: das dauert sonst viel zu lange wie sich herausgestellt hat!!!
    /**
     * Calculates tf-idfs for tokens in a document with the specified document number. It will do so ONLY for the tokens
     * specified in the given array to critically boost runtime in comparison with a brute force approach.
     * @param documentNumber number of the document in question
     * @param tokens list of tokens for which the tf-idf should be calculated with respect to the specified document
     * @return the tf-idfs as values for the keys in a map that represent the terms in question
     */
    public Map<String, Double> tfIdfsForDocument(int documentNumber, String[] tokens){
        Map<String, Double> tfIdfs = new HashMap<>();
        for (int i = 0; i < tokens.length; i++) {
            String term = tokens[i];
            tfIdfs.put(term, tfidf(term, documentNumber));
        }
        return tfIdfs;
    }

    //ATTENTION: linear runtime in number of terms!!!
    /**
     * Calculates tf-idfs for tokens in a document with the specified document number.
     * This method can be very time consuming (linear in number of terms) for huge number of terms
     * known by this Analyzer-instance; whenever possible use {@link #tfIdfsForDocument(int, String[])} instead!
     * @param docNumber number of the document in question
     * @return the tf-idfs as values for the keys in a map that represent the terms in question
     */
    public Map<String, Double> tfIdfsForDocument(int docNumber){
        Map<String, Double> tfIdfs = new HashMap<>();
        Set<Map.Entry<String, TermInfo>> entries = this.termInfos.entrySet();
        List<Map.Entry<String, TermInfo>> entriesList;
        entriesList = Objects.requireNonNullElseGet(
                this.termInfoList,
                () -> new ArrayList<>(entries)
        );
        for (int i = 0; i < entriesList.size(); i++) {
            Map.Entry<String, TermInfo> stringTermInfoEntry = entriesList.get(i);
            String term = stringTermInfoEntry.getKey();
            tfIdfs.put(term, tfidf(term, docNumber));
        }
        return tfIdfs;
    }

    //should be called if all terms are added

    /**
     * Should be called if all relevant terms for the analysis have been added.
     * i.e. one of the classes {@link #updateForTerm(String)} or {@link #updateForTerms(String[])} methods
     * has been called for all relevant terms.
     */
    public void finish(){
        this.termInfoList = new ArrayList<>(this.termInfos.entrySet());
    }

    /**
     * Returns the term-frequency for a given term and document. Remark that there exist several weighting-versions to do this.
     * This method calculates: (number of sightings of the specified term in the specified document / total number of terms in this document).
     * @param term the term in question
     * @param docNumber the docNumber of the document in question; the user of this class is responsible to establish a proper mapping from docNumbers to actual documents
     * @return the th-value
     */
    public double tf(String term, int docNumber){
        TermInfo termInfo = termInfos.get(term);
        if (termInfo == null){
            return 0.0;
        }
        return (double) termInfo.numberOfSightingsIn(docNumber)/ (double) docNumberAndNumberOfTermsInThisDoc.get(docNumber);
    }

    /**
     * Returns the inverse document frequency for a given term as calculated through:
     * log_10(total number of documents/ number od documents containing this term).
     * @param term the term in question
     * @return the idf-value
     */
    public double idf(String term){
        TermInfo termInfo = termInfos.get(term);
        int numberOfDocsContainingTerm = termInfo.numberOfDocumentsAppearedIn();
        return Math.log((double) currentDocNumber / (double) numberOfDocsContainingTerm)/Math.log(10);
    }

    /**
     * Returns the tf-idf-value of a given term for a given document.
     * @param term the term in question
     * @param docNumber the docNumber specifying the document in question
     * @return the tf-idf value calculatet as product of tf and idf.
     */
    public double tfidf(String term, int docNumber) {
        return tf(term, docNumber) * idf(term);
    }
}

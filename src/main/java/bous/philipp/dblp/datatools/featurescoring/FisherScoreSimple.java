package bous.philipp.dblp.datatools.featurescoring;


import org.apache.commons.math3.stat.StatUtils;

/**
 * "Medium level of abstraction"-calculation of fisher scores for features.
 */
public class FisherScoreSimple {

    /**
     * Calculates the fisher score for a set of features (like words in a bag of words vectorization scenario) based on
     * empirical means and variance values.
     * This method is only medium high api to potentially save space and complexity.
     * Expects class-codes to be mapped into a range of 0,1,2,... as it will treat the indices of relevant parameters as class codes.
     * Time-complexity: O(#features * #classes) IMPORTANT REMARK: this does not include the fact that estimations for variances and means
     * per feature and per feature per class have to be calculated beforehand which adds significantly to the calculation of fisher scores
     * making it a rather time consuming task! Therefore: If possible recycle scores once they are calculated if possible.
     * This can maybe seen more easily when inspecting the formula for the i-th feature which is:
     * S_i=(sum(n_j(u_ij-u_i)^2))/(sum(n_j*(r_ij)^2)) with u_ij being the mean (empirical expected value) of the i_th feature in the j-th class
     * and r_ij respective variance, u_i being the mean of the i-th feature and n_j denoting the number of instances in the j-th class.
     * @param numberOfInstancesPerClass number of instances per class [#instancesClass0, #instancesClass1,...]
     * @param meansPerFeature means per feature [meanFeature0, meanFeature1,...]
     * @param meansPerFeaturePerClass similar to means per feature but calculated separately for each class; first index is feature-index, second one class-index
     * @param variancesPerFeaturePerClass similar to meansPerFeaturePerClass but containing variance-values
     * @return fisher-scores for each feature
     */
    public static double[] fisherScores(int[] numberOfInstancesPerClass, double[] meansPerFeature, double[][] meansPerFeaturePerClass, double[][] variancesPerFeaturePerClass) {
        int numberOfFeatures = meansPerFeature.length;
        double[] scores = new double[numberOfFeatures];

        for (int featureIndex = 0; featureIndex < numberOfFeatures; featureIndex++) {
            double numerator = numerator(featureIndex, numberOfInstancesPerClass, meansPerFeature, meansPerFeaturePerClass);
            double denominator = denominator(featureIndex, numberOfInstancesPerClass, variancesPerFeaturePerClass);
            if (numerator == 0 && denominator == 0){
                scores[featureIndex] = 0;
            }else{
                scores[featureIndex] =  numerator / denominator;
            }
        }

        return scores;
    }

    //zaehler
    private static double numerator(int featureIndex, int[] numberOfInstancesPerClass, double[] meansPerFeature, double[][] meansPerFeaturePerClass){
        double[] summands = new double[numberOfInstancesPerClass.length];
        for (int classIndex = 0; classIndex < summands.length; classIndex++){
            summands[classIndex] = (double)numberOfInstancesPerClass[classIndex] * Math.pow(meansPerFeaturePerClass[featureIndex][classIndex] - meansPerFeature[featureIndex],2);
        }
        return sum(summands);
    }

    //nenner
    private static double denominator(int featureIndex, int[] numberOfInstancesPerClass, double[][] variancesPerFeaturePerClass){
        double[] summands = new double[numberOfInstancesPerClass.length];
        for (int classIndex = 0; classIndex < summands.length; classIndex++) {
            summands[classIndex] = (double)numberOfInstancesPerClass[classIndex] * variancesPerFeaturePerClass[featureIndex][classIndex];
        }
        return sum(summands);
    }

    private static double sum(double[] input){
        return StatUtils.sum(input);
    }
}

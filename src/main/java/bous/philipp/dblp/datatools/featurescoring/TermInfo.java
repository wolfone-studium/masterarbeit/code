package bous.philipp.dblp.datatools.featurescoring;

import java.util.HashMap;
import java.util.Map;

class TermInfo {
    private Map<Integer, Integer> docNumberAndNumberOfTimesSeenInThisDoc;
    private String term;


    TermInfo(){
        this.docNumberAndNumberOfTimesSeenInThisDoc = new HashMap<>();
    }

    TermInfo(String term){
        this();
        this.term = term;
    }

    //while constructing we can already specify the first docnumber for the new terminfo...
    TermInfo(String term, int docNumber){
        this(term);
        docNumberAndNumberOfTimesSeenInThisDoc.put(docNumber, 1);
    }

    void addSightingInDocument(int docNumber){
        docNumberAndNumberOfTimesSeenInThisDoc.merge(docNumber, 1, Integer::sum);
    }

    int numberOfDocumentsAppearedIn(){
        return docNumberAndNumberOfTimesSeenInThisDoc.size();
    }

    int numberOfSightingsIn(int docNumber){
        if (docNumberAndNumberOfTimesSeenInThisDoc.get(docNumber) == null){
            return 0;
        }
        return docNumberAndNumberOfTimesSeenInThisDoc.get(docNumber);
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public  Map<Integer, Integer> getDocNumberAndNumberOfTimesSeenInThisDoc() {
        return docNumberAndNumberOfTimesSeenInThisDoc;
    }

    public void setDocNumberAndNumberOfTimesSeenInThisDoc( Map<Integer, Integer> docNumberAndNumberOfTimesSeenInThisDoc) {
        this.docNumberAndNumberOfTimesSeenInThisDoc = docNumberAndNumberOfTimesSeenInThisDoc;
    }

}

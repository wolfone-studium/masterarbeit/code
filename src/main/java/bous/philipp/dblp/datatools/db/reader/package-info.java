/**
 * Provides means of drawing data from a database/tables as described in the
 * corresponding thesis as well as transformation into the unified {@link bous.philipp.dblp.datatools.datamodel.RecordDocument} format.
 */
package bous.philipp.dblp.datatools.db.reader;
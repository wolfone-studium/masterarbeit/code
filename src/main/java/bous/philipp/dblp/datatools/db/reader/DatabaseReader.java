package bous.philipp.dblp.datatools.db.reader;

import bous.philipp.dblp.datatools.datamodel.CsClassification;
import bous.philipp.dblp.datatools.datamodel.Feature;
import bous.philipp.dblp.datatools.datamodel.FeatureType;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * Provides convenience methods to draw records from a database while converting them back to
 * {@link RecordDocument}s.
 */
public class DatabaseReader {
    private static final int FETCH_SIZE = Integer.MIN_VALUE;

    /**
     * Draws records from a database and returns them in form of a list of {@link RecordDocument}s.
     * Lets the user specify a where and limit-clause to constraint which records are drawn, so this method
     * is suitable to "cut out" sections of records if needed (f.e. useful for some CV-tasks).
     * @param connection database connection
     * @param excludedFeatures exclude those featuretypes from the record-document-instantiation (to conveniently keep them simple if not the full information is needed)
     * @param recordTableName name of the record table
     * @param featureTableName name of the corresponding feature-table
     * @param whereClauseRecords where-clause to contraint the selection of records (including the "WHERE"-keyword)
     * @param limitClauseRecords limit-clause to constraint the selection of records (including the "LIMIT"-keyword)
     * @param byRandomId exists for compatibility-reasons; if true; the method will expect the db-tables to contain a column "random_id" and will draw records sorted by random_id instead of id; useful if "normal" ids are connected to some form of structural ordering
     * @return the record documents as list
     */
    public static SelectionResult selectRecordsFromDB(
            Connection connection,
            EnumSet<FeatureType> excludedFeatures,
            String recordTableName,
            String featureTableName,
            String whereClauseRecords,
            String limitClauseRecords,
            boolean byRandomId
    ) {

        List<RecordDocument> records = new ArrayList<>();
        ResultSet resultSet;
        int lastId = 0;

        try {
            Object[] formatParams = new Object[]{recordTableName, featureTableName};
            StringBuilder sqlPrefab = new StringBuilder();
            sqlPrefab.append("SELECT {1}.record_id, t.original_id, t.cs_classification, {1}.id AS feature_id, {1}.type, {1}.value, {1}.original_position ");
            if (byRandomId) {
                sqlPrefab.append(", t.random_id ");
            }
            sqlPrefab.append("FROM (");
            sqlPrefab.append("SELECT * FROM {0} ");
            sqlPrefab.append(whereClauseRecords);
            sqlPrefab.append(" ").append(limitClauseRecords);
            sqlPrefab.append(") t LEFT JOIN {1} ON t.id = {1}.record_id ");
            if (byRandomId) {
                sqlPrefab.append("ORDER BY t.random_id ASC");
            }else{
                sqlPrefab.append("ORDER BY t.id ASC");
            }

            String sql = MessageFormat.format(sqlPrefab.toString(), formatParams);

            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(FETCH_SIZE);

            resultSet = statement.executeQuery(sql);

            lastId = addRecordDocumentsFromResultSet(excludedFeatures, records, resultSet, byRandomId);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (records.get(0) == null){
            records.remove(0);
        }
        return new SelectionResult(records, lastId);
    }

    /**
     * Draws records from a database and returns them in form of a list of {@link RecordDocument}s.
     * Provides a parameter to specify at which id to start reading subsequently.
     * @param connection database connection
     * @param excludedFeatures exclude those featuretypes from the record-document-instantiation (to conveniently keep them simple if not the full information is needed)
     * @param recordTableName name of the record table
     * @param featureTableName name of the corresponding feature-table
     * @param startingId id of the first record that should be read, going upwards after that (enables basic split processing of individual parts of a database; remark to maintain good shuffling BEFORE assigning ids to records)
     * @param maxNumber maximum number of records to be drawn, -1 for all
     * @param classificationConstraint specifies constraints on the class label of records to be considered
     * @param byRandomId exists for compatibility-reasons; if true; the method will expect the db-tables to contain a column "random_id" and will draw records sorted by random_id instead of id; useful if "normal" ids are connected to some form of structural ordering
     * @return the record documents as list
     */
    public static SelectionResult selectRecordsFromDB(
            Connection connection,
            EnumSet<FeatureType> excludedFeatures,
            String recordTableName,
            String featureTableName,
            int startingId,
            int maxNumber,
            CsClassification classificationConstraint,
            boolean byRandomId
    ) {

        if (connection == null) {
            throw new IllegalArgumentException("No valid DB-connection given! Check your parameters and if your DBMS is running!");
        }

        List<RecordDocument> records = new ArrayList<>();
        ResultSet resultSet;
        int lastId = 0;

        String relevantId;
        if (byRandomId) {
            relevantId = "random_id";
        } else {
            relevantId = "id";
        }

        try {
            Object[] formatParams = new Object[]{recordTableName, featureTableName};
            StringBuilder sqlPrefab = new StringBuilder();
            sqlPrefab.append("SELECT {1}.record_id, t.original_id, t.cs_classification, {1}.id AS feature_id, {1}.type, {1}.value, {1}.original_position ");
            if (byRandomId) {
                sqlPrefab.append(", t.random_id ");
            }
            sqlPrefab.append("FROM (");
            sqlPrefab.append("SELECT * FROM {0} ");
            if (classificationConstraint != CsClassification.ALL) {
                switch (classificationConstraint) {
                    case CS_ONLY:
                        sqlPrefab.append(" WHERE {0}.cs_classification = 1 ");
                        break;
                    case NON_CS_ONLY:
                        sqlPrefab.append(" WHERE {0}.cs_classification = -1 ");
                        break;
                    case CS_AND_NON_CS:
                        sqlPrefab.append(" WHERE {0}.cs_classification != 0 ");
                        break;
                    case UNKNOWN:
                        sqlPrefab.append(" WHERE {0}.cs_classification = 0 ");
                        break;
                }
                sqlPrefab.append(" AND {0}.").append(relevantId).append(" >= ").append(startingId).append(" ");
            } else {
                sqlPrefab.append(" WHERE {0}.").append(relevantId).append(" >= ").append(startingId).append(" ");
            }

            if (maxNumber != -1) {
                sqlPrefab.append("LIMIT ").append(maxNumber).append(" ");
            }
            sqlPrefab.append(") t LEFT JOIN {1} ON t.id = {1}.record_id ");
            if (byRandomId) {
                sqlPrefab.append("ORDER BY t.random_id ASC");
            }else{
                sqlPrefab.append("ORDER BY t.id ASC");
            }

            String sql = MessageFormat.format(sqlPrefab.toString(), formatParams);

            //NOTE: siehe Eintrag log 11.1.2019 für mehr Infos. Und: https://stackoverflow.com/questions/1318354/what-does-statement-setfetchsizensize-method-really-do-in-sql-server-jdbc-driv UND http://benjchristensen.com/2008/05/27/mysql-jdbc-memory-usage-on-large-resultset/
            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(FETCH_SIZE);

            resultSet = statement.executeQuery(sql);

            lastId = addRecordDocumentsFromResultSet(excludedFeatures, records, resultSet, byRandomId);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (records.get(0) == null){
            records.remove(0);
        }
        return new SelectionResult(records, lastId);
    }

    /**
     * Draws records from a database and returns them in form of a list of {@link RecordDocument}s.
     * @param connection database connection
     * @param excludedFeatures exclude those featuretypes from the record-document-instantiation (to conveniently keep them simple if not the full information is needed)
     * @param recordTableName name of the record table
     * @param featureTableName name of the corresponding feature-table
     * @param maxNumber maximum number of records to be drawn, -1 for all
     * @param classificationConstraint specifies constraints on the class label of records to be considered
     * @param byRandomId exists for compatibility-reasons; if true; the method will expect the db-tables to contain a column "random_id" and will draw records sorted by random_id instead of id; useful if "normal" ids are connected to some form of structural ordering
     * @return the record documents as list
     */
    public static SelectionResult selectRecordsFromDB(
            Connection connection,
            EnumSet<FeatureType> excludedFeatures,
            String recordTableName,
            String featureTableName,
            int maxNumber,
            CsClassification classificationConstraint,
            boolean byRandomId
    ) {
        List<RecordDocument> records = new ArrayList<>();
        ResultSet resultSet;
        int lastId = 0;

        try {
            Object[] formatParams = new Object[]{recordTableName, featureTableName};
            StringBuilder sqlPrefab = new StringBuilder();
            sqlPrefab.append("SELECT {1}.record_id, t.original_id, t.cs_classification, {1}.id AS feature_id, {1}.type, {1}.value, {1}.original_position ");
            if (byRandomId) {
                sqlPrefab.append(", t.random_id ");
            }
            sqlPrefab.append("FROM (");
            sqlPrefab.append("SELECT * FROM {0} ");
            if (classificationConstraint != CsClassification.ALL) {
                switch (classificationConstraint) {
                    case CS_ONLY:
                        sqlPrefab.append(" WHERE {0}.cs_classification = 1 ");
                        break;
                    case NON_CS_ONLY:
                        sqlPrefab.append(" WHERE {0}.cs_classification = -1 ");
                        break;
                    case CS_AND_NON_CS:
                        sqlPrefab.append(" WHERE {0}.cs_classification != 0 ");
                        break;
                    case UNKNOWN:
                        sqlPrefab.append(" WHERE {0}.cs_classification = 0 ");
                        break;
                }
            }
            if (maxNumber != -1) {
                sqlPrefab.append("LIMIT ").append(maxNumber).append(" ");
            }
            sqlPrefab.append(") t LEFT JOIN {1} ON t.id = {1}.record_id ");
            if (byRandomId) {
                sqlPrefab.append("ORDER BY t.random_id ASC");
            }else{
                sqlPrefab.append("ORDER BY t.id ASC");
            }

            String sql = MessageFormat.format(sqlPrefab.toString(), formatParams);

            //NOTE: siehe Eintrag log 11.1.2019 für mehr Infos. Und: https://stackoverflow.com/questions/1318354/what-does-statement-setfetchsizensize-method-really-do-in-sql-server-jdbc-driv UND http://benjchristensen.com/2008/05/27/mysql-jdbc-memory-usage-on-large-resultset/
            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(FETCH_SIZE);

            resultSet = statement.executeQuery(sql);

            lastId = addRecordDocumentsFromResultSet(excludedFeatures, records, resultSet, byRandomId);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new SelectionResult(records, lastId);
    }

    /**
     * Processes a result set of an SQL-Query and adds the receoived records to the given record-list.
     * @param excludedFeatures features to exclude
     * @param records list of records where the new records should be added
     * @param resultSet the result-set to be processed
     * @param byRandomId if true will return the last random id seen in the processed records
     * @return the last id of the given records; either id or random-id depending on the value of the respective parameter
     * @throws SQLException thrown if processing of resultSet fails for some reason
     */
    private static int addRecordDocumentsFromResultSet(EnumSet<FeatureType> excludedFeatures, List<RecordDocument> records, ResultSet resultSet, boolean byRandomId) throws SQLException {
        int previousId = -1;
        int currentId;

        int lastId = 0;

        RecordDocument currentRecord = null;
        FeatureType currentFeatureType;

        while (resultSet.next()) {
            currentId = resultSet.getInt(1);
            currentFeatureType = FeatureType.valueOf(resultSet.getString(5));

            if (currentId != previousId) {
                if (currentRecord != null) {
                    records.add(currentRecord);
                }
                currentRecord = new RecordDocument();
                currentRecord.setCsClassificationStatus(resultSet.getInt(3));
                currentRecord.setId(currentId);
                currentRecord.setOriginalId(resultSet.getString(2));
            }

            if (excludedFeatures == null || !excludedFeatures.contains(currentFeatureType)) {
                currentRecord.addFeature(
                        new Feature(
                                resultSet.getInt(4),
                                currentFeatureType,
                                resultSet.getString(6),
                                currentId,
                                resultSet.getString(7)));
            }
            if (byRandomId) {
                lastId = resultSet.getInt(8);
            }else {
                lastId = currentId;
            }
            previousId = currentId;
        }
        records.add(currentRecord);
        return lastId;
    }
}

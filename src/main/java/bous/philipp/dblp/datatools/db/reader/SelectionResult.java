package bous.philipp.dblp.datatools.db.reader;

import bous.philipp.dblp.datatools.datamodel.RecordDocument;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import java.sql.Connection;
import java.util.EnumSet;
import java.util.List;

/**
 * Encapsulates the result of a selection of records from a database
 * in form of a list along with an id intended to represent the id of the last
 * read record (may for example be "id" or "random_id" in {@link DatabaseReader#selectRecordsFromDB(Connection, EnumSet, String, String, String, String, boolean)}).
 */
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class SelectionResult {
    @NonNull @Getter private List<RecordDocument> records;
    @Getter private int lastId;
}

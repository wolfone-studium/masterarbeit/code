package bous.philipp.dblp.datatools.util.config;

import java.nio.file.Path;

/**
 * Interface providing some abstraction for steps in the ML-process that are configurable by external properties-files.
 */
public interface ExecutionConfigProperties {
    /**
     * Reads properties from a properties file to fill in everything that is needed
     * to make the the executing ExecutionConfigProperties-instance meaningful.
     * @param configPath absolute path to the properties-file
     * @param methodVersionId a method version id to enforce possibility of checkups between method versions of methods using this instance of ExecutionConfigProperties and the ones specified in config files. Main reason is traceability of experiments and early failure for "wrong" configuration.
     */
    void readProperties(Path configPath, String methodVersionId);
}



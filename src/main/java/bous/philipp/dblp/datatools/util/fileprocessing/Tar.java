package bous.philipp.dblp.datatools.util.fileprocessing;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.utils.IOUtils;

import java.io.*;
import java.util.Collection;

/**
 * Simplifies working with tar-archives.
 * Main credit goes to: <a href="https://memorynotfound.com/java-tar-example-compress-decompress-tar-tar-gz-files/">https://memorynotfound.com/java-tar-example-compress-decompress-tar-tar-gz-files/</a>
 * I just build on that.
 */
public class Tar {

    private Tar() {

    }

    /**
     * Compresses a collection of files.
     * @param nameIncludingPath path of the new archive
     * @param files files to be added
     * @throws IOException can be thrown during file creation
     */
    public static void compress(String nameIncludingPath, Collection<File> files) throws IOException {
        try (TarArchiveOutputStream out = getTarArchiveOutputStream(nameIncludingPath)){
            for (File file : files){
                addToArchiveCompression(out, file, ".");
            }
        }
    }
    /**
     * Compresses a collection of files.
     * @param nameIncludingPath path of the new archive
     * @param files files to be added
     * @throws IOException can be thrown during file creation
     */
    public static void compress(String nameIncludingPath, File... files) throws IOException {
        try (TarArchiveOutputStream out = getTarArchiveOutputStream(nameIncludingPath)){
            for (File file : files){
                addToArchiveCompression(out, file, ".");
            }
        }
    }

    private static TarArchiveOutputStream getTarArchiveOutputStream(String name) throws IOException {
        TarArchiveOutputStream taos = new TarArchiveOutputStream(new GzipCompressorOutputStream(new BufferedOutputStream(new FileOutputStream(name))));
            // TAR has an 8 gig file limit by default, this gets around that
            taos.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_STAR);
            // TAR originally didn't support long file names, so enable the support for it
            taos.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);
            taos.setAddPaxHeadersForNonAsciiNames(true);
            return taos;
    }

    private static void addToArchiveCompression(TarArchiveOutputStream out, File file, String dir) throws IOException {
        String entry = dir + File.separator + file.getName();
        if (file.isFile()){
            out.putArchiveEntry(new TarArchiveEntry(file, entry));
            try (FileInputStream in = new FileInputStream(file)){
                IOUtils.copy(in, out);
            }
            out.closeArchiveEntry();
        } else if (file.isDirectory()) {
            File[] children = file.listFiles();
            if (children != null){
                for (File child : children){
                    addToArchiveCompression(out, child, entry);
                }
            }
        } else {
            System.out.println(file.getName() + " is not supported");
        }
    }
}
package bous.philipp.dblp.datatools.util.config;

/**
 * An execution config that is used in context where a proper database-connection is needed.
 */
public interface DbDependentExecutionConfig extends ExecutionConfigProperties {
    String getDbPropertiesFilePath();
}

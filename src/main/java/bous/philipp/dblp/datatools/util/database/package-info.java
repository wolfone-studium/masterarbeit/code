/**
 * Provides utility classes to work with databases.
 */
package bous.philipp.dblp.datatools.util.database;
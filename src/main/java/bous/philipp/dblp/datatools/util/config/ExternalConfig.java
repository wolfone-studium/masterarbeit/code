package bous.philipp.dblp.datatools.util.config;

import bous.philipp.dblp.datatools.util.logging.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Helper to work with properties-files.
 */
public class ExternalConfig {

    /**
     * Read properties from a properties file.
     * @param path absolute path to the properties file
     * @return the read properties
     */
    public static Properties readProperties(String path) {
        Properties properties = new Properties();

        try (FileInputStream in = new FileInputStream(path)) {
            properties.load(in);
        } catch (IOException ex) {
            Logger lgr = LoggerFactory.getLogger(ExternalConfig.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }

        return properties;
    }

}

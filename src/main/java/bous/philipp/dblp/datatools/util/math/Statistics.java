package bous.philipp.dblp.datatools.util.math;

import org.apache.commons.math3.stat.StatUtils;

/**
 * Helper class providing functions for statistical calculations.
 */
public class Statistics {

    /**
     * Calculates the empirical expected value ("mean") for an array of values (based on arithmetic mean).
     * @param values the values
     * @return the empirical expected value; 0 if input array has length 0
     */
    public static double empiricalExpectedValue(double[] values){
        if (values.length == 0){
            return 0;
        }
        return StatUtils.sum(values)/values.length;
    }

    /**
     * Calculates the empirical variance/sample variance using Bessel's correction.
     * @see <a href="https://en.wikipedia.org/wiki/Variance#Sample_variance">Sample Variance</a>
     * @param values the values
     * @return empirical variance
     */
    public static double empiricalVariance(double[] values){
        if (values.length == 0){
            return 0;
        }
        double empExpectedValue = empiricalExpectedValue(values);
        double[] poweredDifferences = new double[values.length];
        for (int i = 0; i < poweredDifferences.length; i++) {
            poweredDifferences[i] = Math.pow(values[i] - empExpectedValue,2);
        }
        return StatUtils.sum(poweredDifferences)/(values.length - 1);
    }

    /**
     * Calculates the empirical expected value ("mean") for an array of values (based on arithmetic mean.
     * Does not need all observations but only those different from zero.
     * This implies the necessity to specify the total number of observations including all zeros.
     * @param totalNumberOfObservations number of observations including observed zero-values
     * @param observationsDifferentFromZero an array of all observation-values different from zero
     * @return the empirical expected value; 0 if input array has length 0
     */
    public static double empiricalExpectedValueSparse(int totalNumberOfObservations, double[] observationsDifferentFromZero){
        if (observationsDifferentFromZero.length == 0){
            return 0;
        }
        return StatUtils.sum(observationsDifferentFromZero)/(double)totalNumberOfObservations;
    }

    /**
     * Calculates the empirical variance/sample variance using Bessel's correction.
     * Does not need all observations but only those different from zero.
     * @see <a href="https://en.wikipedia.org/wiki/Variance#Sample_variance">Sample Variance</a>
     * @param totalNumberOfObservations number of observations including observed zero-values
     * @param observationsDifferentFromZero an array of all observation-values different from zero
     * @return empirical variance
     */
    public static double empiricalVarianceSparse(int totalNumberOfObservations, double[] observationsDifferentFromZero){
        if (observationsDifferentFromZero.length == 0 || totalNumberOfObservations == 1){
            return 0;
        }
        double empExpectedValue = empiricalExpectedValueSparse(totalNumberOfObservations, observationsDifferentFromZero);
        double[] poweredDifferences = new double[observationsDifferentFromZero.length];
        for (int i = 0; i < poweredDifferences.length; i++) {
            poweredDifferences[i] = Math.pow(observationsDifferentFromZero[i] - empExpectedValue,2);
        }
        int countZeros = totalNumberOfObservations - observationsDifferentFromZero.length;
        return (  StatUtils.sum(poweredDifferences) + (countZeros * Math.pow(empExpectedValue,2))  )  /  (totalNumberOfObservations - 1);
    }

    /**
     * Calculates empirical expected value and variance.
     * @see #empiricalExpectedValueSparse(int, double[])
     * @see #empiricalVarianceSparse(int, double[])
     * @param totalNumberOfExperiments number of observations including observed zero-values
     * @param observationsDifferentFromZero an array of all observation-values different from zero
     * @return an array of length 2 where the first value is the empirical expected value and the second one the variance
     */
    public static double[] empiricalExpectedValueAndVarianceSparse(int totalNumberOfExperiments, double[] observationsDifferentFromZero) {
        double[] result = new double[2];
        if (totalNumberOfExperiments == 1 && observationsDifferentFromZero.length == 1){
            return new double[]{observationsDifferentFromZero[0], 0};
        }
        if (observationsDifferentFromZero.length == 0){
            return new double[]{0, 0};
        }

        double empExpectedValue = empiricalExpectedValueSparse(totalNumberOfExperiments, observationsDifferentFromZero);
        result[0] = empExpectedValue;
        double[] poweredDifferences = new double[observationsDifferentFromZero.length];
        for (int i = 0; i < poweredDifferences.length; i++) {
            poweredDifferences[i] = Math.pow(observationsDifferentFromZero[i] - empExpectedValue,2);
        }
        int countZeros = totalNumberOfExperiments - observationsDifferentFromZero.length;
        result[1] = (  StatUtils.sum(poweredDifferences) + (countZeros * Math.pow(empExpectedValue,2))  )  /  (totalNumberOfExperiments - 1);
        return result;
    }
}

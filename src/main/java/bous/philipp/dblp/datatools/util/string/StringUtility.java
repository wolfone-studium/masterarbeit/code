package bous.philipp.dblp.datatools.util.string;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.tartarus.snowball.ext.EnglishStemmer;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Provides utility-methods for String-processing which are rather specific to the dblp.datatools library.
 */
public class StringUtility {

    private static final CharArraySet STOP_WORDS_ENGLISH = new CharArraySet(StopWordCollections.ENGLISH_STANFORD_CORE_NLP, true);

    /**
     * Based on: https://stackoverflow.com/a/23699630
     * Removes stopwords from a given String, intended for english language, the stop-words are defined in STOP_WORDS_ENGLISH, which is currently
     * the STANFORD_CORE_NLP-set found here: https://github.com/stanfordnlp/CoreNLP/blob/master/data/edu/stanford/nlp/patterns/surface/stopwords.txt.
     *
     * @param tokens text in which stop-words should be eliminated
     * @return the text as string without stop-words specified through STOP_WORDS_ENGLISH
     */
    public static List<String> removeStopWordsEnglish(List<String> tokens) {
        List<String> result = new ArrayList<>();
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < tokens.size(); i++) {
            String token = tokens.get(i);
            text.append(token).append(" ");
        }

        TokenStream tokenStream = new WhitespaceTokenizer(new StringReader(text.toString()));
        tokenStream = new StopFilter(tokenStream, STOP_WORDS_ENGLISH);

        CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);

        try {
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                String term = charTermAttribute.toString();
                result.add(term);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Applies english stemming using porter-stemmer to a given list of tokens.
     * @param tokens the tokens to be processed
     * @return a List containing the stemmed tokens
     */
    public static List<String> stemEnglish(List<String> tokens) {
        EnglishStemmer stemmer = new EnglishStemmer();

        List<String> result = new ArrayList<>();

        for (int i = 0; i < tokens.size(); i++) {
            String token =  tokens.get(i);
            stemmer.setCurrent(token);
            stemmer.stem();
            result.add(stemmer.getCurrent());
        }

        return result;
    }

    /**
     * Removes text inside "()" as well as inside "[]". Doesn't check for well-formed bracket-order!
     * Does not remove text in curly braces!
     * After an idea from: https://stackoverflow.com/a/25335225 .
     *
     * @param text the text to be stripped of content in brackets.
     * @return the input without the text in brackets
     */
    public static String removeBracketContent(String text) {
        StringBuilder builder = new StringBuilder();

        int parenthesisCounter = 0;

        for (char character : text.toCharArray()) {
            if (character == '(' || character == '[')
                parenthesisCounter++;
            if (character == ')' || character == ']')
                parenthesisCounter--;
            if (!(character == '(' || character == ')' || character == '[' || character == ']') && parenthesisCounter == 0)
                builder.append(character);
        }

        return builder.toString().trim();
    }


    /**
     * Removes tokens under minimum length from given list of strings, i.e. will manipulate the input list.
     * @param tokens the text to be processed
     * @param minLength minimum token length
     * @return the text without the tokens with length under [minLength] seperated by [separatorForResult]
     */
    public static List<String> removeTokensUnderMinimumLength(List<String> tokens, int minLength){
        tokens.removeIf(token->token.length() < minLength);
        return tokens;
    }

    /**
     * Wraps a given text with a prefix and a suffix.
     *
     * @param text   the text to be wrapped
     * @param prefix the prefix to be prepended
     * @param suffix the suffix to be appended
     * @return concatenated prefix text suffix
     */
    public static String wrap(String text, String prefix, String suffix) {
        return prefix + text + suffix;
    }

    /**
     * Wraps a given text with a prefix and a suffix.
     *
     * @param text         the text to be wrapped
     * @param prefixSuffix a String-array of length 2 where the first entry encodes the wanted prefix and the second the wanted suffix.
     * @return prefix + text + suffix
     */
    public static String wrap(String text, String[] prefixSuffix) {
        if (prefixSuffix.length != 2) {
            throw new IllegalArgumentException("Prefix/Suffix-Array has length != 2");
        }
        return prefixSuffix[0] + text + prefixSuffix[1];
    }

    /**
     * Processes a list of tokens and returns the resulting modified list. What processing is done is specified by the methods parameters (e.g. stop-word-removal).
     * @param tokens the list of tokens
     * @param applyLowerCasing if true every token will be entirely turned to lower casing
     * @param removeStopwords if true stop words will be removed; WARNING: english language is assumed
     * @param applyStemming if true stemming will be applied to all tokens; WARNING: english language is assumed
     * @return the modified list
     */
    public static List<String> processTokens(List<String> tokens, boolean applyLowerCasing, boolean removeStopwords, boolean applyStemming){
        //NOTE: should be generified? Ensures no ":" are in tokens as that interferes with separators in vocab files; it does so by dividing any token containing ":" into subtokens
        for (int i = 0; i < tokens.size(); i++) {
            String token = tokens.get(i);
            if (token.contains(":")){
                String[] subTokens = token.split(":");
                tokens.set(i, subTokens[0]);
                tokens.addAll(Arrays.asList(subTokens).subList(1, subTokens.length));
            }
        }
        if (applyLowerCasing) {
            for (int i = 0; i < tokens.size(); i++) {
                String token = tokens.get(i);
                tokens.set(i, token.toLowerCase());
            }
        }
        if (removeStopwords) {
            tokens = removeStopWordsEnglish(tokens);
        }
        if (applyStemming) {
            tokens = stemEnglish(tokens);
        }
        return tokens;
    }

    /**
     * Takes an array of single tokens and converts it into an array of n-grams, assuming that the input array
     * preserved the desired ordering of tokens, e.g. didn't change the word order if n-grams are to be obtained from
     * natural language input.
     * @param tokens the tokens to be processed
     * @param n denotes the window-size for the n-grams; set to values less than 2 will make the method simply return the input array
     * @param separator will be used as separator between the tokens of each n-gram
     * @return a new string array where each entry is a n-gram
     */
    public static String[] convertToNGrams(String[] tokens, int n, String separator){
        if (n < 2){
            return tokens;
        }

        List<String> ngrams = new ArrayList<>();
        for (int i = n-1; i < tokens.length; i++) {
            String token = tokens[i];
            StringBuilder ngram = new StringBuilder();
            for (int j = n-1; j > 0; j--){
                ngram.append(tokens[i - j]).append(separator);
            }
            ngram.append(token);
            ngrams.add(ngram.toString().trim());
        }
        return ngrams.toArray(new String[0]);
    }

    /**
     * Concatenates tokens given as list using a given separator.
     * @param tokens the list of tokens
     * @param separator the separator
     * @return the concatenated tokens
     */
    public static String concat(List<String> tokens, String separator) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < tokens.size(); i++) {
            String token = tokens.get(i);
            builder.append(token).append(separator);
        }
        return builder.toString().trim();
    }

    /**
     * Turns a vector given as String in svmlight format into an array of doubles.
     * Expects the input String to already have the target value to be truncated.
     * It will further build the smallest array possible (including 0 values) meaning
     * that the maximum index in the input will ne used to determine the array's length.
     * @param svmLightVectorWithoutTarget the svmlight-vector without the target information
     * @return an array representing the input vector
     */
    public static double[] svmLightToDoubleArray(String svmLightVectorWithoutTarget) {
        String[] vectorTokens = svmLightVectorWithoutTarget.split(" ");
        int maxIndex = Integer.parseInt(vectorTokens[vectorTokens.length-1].split(":")[0]);
        double[] returnArray = new double[maxIndex+1];
        for (int i = 0; i < vectorTokens.length; i++) {
            String[] indexValue = vectorTokens[i].split(":");
            returnArray[Integer.parseInt(indexValue[0])] = Integer.parseInt(indexValue[1]);
        }
        return returnArray;
    }

}

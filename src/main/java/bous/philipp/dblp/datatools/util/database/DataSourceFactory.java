package bous.philipp.dblp.datatools.util.database;

import bous.philipp.dblp.datatools.util.config.ExternalConfig;
import com.mysql.cj.jdbc.MysqlDataSource;

import java.util.Properties;


/**
 * Creates data-sources.
 */
public class DataSourceFactory {
    /**
     * Creates a MysqlDataSource based on configuration in a properties file
     * compliant to standard JDBC configuration structure, meaning it should contain:
     * <ul>
     *     <li>db.url= ..</li>
     *     <li>db.user=...</li>
     *     <li>db.password=...</li>
     * </ul>
     * @param dbPropertiesFilePath absolute path to the properties file for configuration
     * @return the constructed MysqlDataSource
     */
    public static MysqlDataSource getMySQLDataSource(String dbPropertiesFilePath) {
        MysqlDataSource dataSource = new MysqlDataSource();
        Properties properties = ExternalConfig.readProperties(dbPropertiesFilePath);

        dataSource.setURL(properties.getProperty("db.url"));
        dataSource.setUser(properties.getProperty("db.user"));
        dataSource.setPassword(properties.getProperty("db.password"));

        return dataSource;
    }
}

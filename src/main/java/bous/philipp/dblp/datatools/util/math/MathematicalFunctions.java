package bous.philipp.dblp.datatools.util.math;

/**
 * Provides mathematical functions not included in the java-standard-library.
 */
public class MathematicalFunctions {
    /**
     * "Standard"-Sigmoid function. See:  <a href="https://de.wikipedia.org/wiki/Sigmoidfunktion">Sigmoid-Function</a>.
     * @param value argument for which the sigmoid-function should be calculated
     * @return 1.0/(1.0 + Math.exp(-value))
     */
    public static double sigmoid(double value){
        return 1.0/(1.0 + Math.exp(-value));
    }
}

package bous.philipp.dblp.datatools.util.xml;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Provides helper methods for working with XML.
 */
public class XMLUtility {

    /**
     * Tests if a given element has a certain attribute whose value matches a given regex.
     * @param element element to test
     * @param attributeName name of the attribute for which should be tested
     * @param regex regex to be matched against the attribute-value
     * @return true if the attribute exists and its value matches the specified regex, false else
     */
    public static boolean hasAttributeMatchingRegex(Element element, String attributeName, String regex) {
        if (element.hasAttribute(attributeName)) {
            return element.getAttribute(attributeName).matches(regex);
        } else {
            return false;
        }
    }

    /**
     * Iterates over all nodes of a given document of type Node.ELEMENT_NODE.
     * @param nodes a NodeList
     * @return iterator for all element-type nodes in the document
     */
    public static Iterator<Element> elementIterator(NodeList nodes) {
        List<Element> elements = new ArrayList<>();
        for (int i = 0; i < nodes.getLength(); i++){
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                elements.add((Element) node);
            }
        }
        return new Iterator<>() {
            List<Element> eles = elements;
            int index = 0;
            @Override
            public boolean hasNext() {
                return index < (eles.size());
            }

            @Override
            public Element next() {
                return eles.get(index++);
            }
        };
    }

    private static Node getPreviousSibling(Node node){
        Node sibling = node.getPreviousSibling();
        while (sibling != null && !(sibling.getNodeType() == Node.ELEMENT_NODE)){
            sibling = sibling.getPreviousSibling();
        }
        return sibling;
    }

    private static String getSiblingPosition(Node node){
        int positionCounter = 1;
        Node tempNode = node;
        tempNode = getPreviousSibling(tempNode);
        while ( tempNode != null ){
            positionCounter++;
            tempNode = getPreviousSibling(tempNode);
        }
        return Integer.toString(positionCounter);
    }

    /**
     * Returns the position of a node in the form SIBLING_NUMBER.SIBLING_NUMBER.[...] where a "."
     * shows a parent-child relationship PARENT_LEVEL.CHILD_LEVEL.
     * Example:
     * <pre>
     * {@code
     * <thing>
     *     <property>prop1</property>
     *     <property>prop2</property>
     *     <property>prop3</property>
     * </thing>
     * }
     * </pre>
     * The node with text-content prop2 would have the position "1.2".
     * The function takes a number of levels to skip as a second argument.
     * If we knew for example that thing had a parent of which we know that it doesn't come with
     * siblings so it would always yield an additional "1." in front of the position-string, we can
     * omit that by specifying a cut-level of 1 to reduce for example "1.2.3" to "2.3".
     *
     * @param node the node of which we want to know the position
     * @param cutLevels levels to cut; "1" would cut "1.3.2" to "3.2"
     * @return the position as string
     */
    public static String getPosition(Node node, int cutLevels) {
        StringBuilder position = new StringBuilder();
        do {
            position.insert(0, getSiblingPosition(node));
            node = node.getParentNode();
            if (node!= null ){
                position.insert(0, ".");
            }
        }while (node != null);

        position.delete(0,2 + cutLevels * 2);

        if (position.length() < 1){
            position.append("<1");
        }
        return position.toString();
    }


    /**
     * Rectifies a given raw xml-string to decrease number of parsing errors.
     * @param rawXML the xml-text
     * @return the rectified xml
     */
    @NotNull
    public static String rectifyRawXML(String rawXML) {
        if (rawXML == null) {
            return "";
        }
        rawXML = rawXML.replaceAll("[\\000]*", "");
        return rawXML;
    }
}

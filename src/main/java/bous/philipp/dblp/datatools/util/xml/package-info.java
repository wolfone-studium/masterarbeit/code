/**
 * Provides utility to simplify working with XMLs.
 */
package bous.philipp.dblp.datatools.util.xml;
package bous.philipp.dblp.datatools.util.fileprocessing;

import bous.philipp.dblp.datatools.util.logging.LoggerFactory;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.lang3.time.StopWatch;
import org.xml.sax.SAXParseException;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Provides methods to iterate over tar-archives and process the contained files.
 */
public class TarProcessor {

    /**
     * Iterates a tar-archive of xml-files in a flat manner, meaning not descending into subdirectories. Every visited entry
     * is subject to processing through a {@link XMLRawProcessor} that has to be specified as parameter.
     * @param tarLocation location of the tar in question
     * @param maxNumber maximum number of entries to be processed; -1 for no max number. REMARK: this is with respect to entries that didn't throw any parsing errors!
     * @param startIndex a starting index to specify which file in the archive should be the first to be processed (useful if it is necessary to process a big tar in several runs)
     * @param entryProcessor a {@link XMLRawProcessor} whose processRawXML-method will be called on each entry
     * @param silentErrors if true SAXParseException-stacktraces will NOT be printed to standard.out
     */
    public static void iterateRawXMLTarFlat(String tarLocation, int maxNumber, int startIndex, XMLRawProcessor entryProcessor, boolean silentErrors){
        Logger logger = LoggerFactory.getLogger(TarProcessor.class.getName());

        try (TarArchiveInputStream tarArchiveInputStream =
                     new TarArchiveInputStream(
                             new GzipCompressorInputStream(
                                     new FileInputStream(tarLocation)));
             BufferedInputStream bufferedInputStream = new BufferedInputStream(tarArchiveInputStream)){

            StopWatch watch = new StopWatch();

            TarArchiveEntry entry;

            int processedTarEntries = 0;
            int numberOfExceptions = 0;
            int startCounter = 0;

            watch.start();
            logger.info("Started iterating.");

            while ((entry = tarArchiveInputStream.getNextTarEntry()) != null && (processedTarEntries - numberOfExceptions < maxNumber || maxNumber == -1)) {
                if (startCounter >= startIndex) {
                    if (entry.isDirectory()) {
                        continue;
                    }
                    byte[] content = new byte[(int)entry.getSize()];
                    bufferedInputStream.read(content, 0, content.length);

                    String unparsedXML = new String(content);
                    try {
                        entryProcessor.processRawXML(unparsedXML);
                    }catch (SAXParseException e){
                        numberOfExceptions++;
                        if (!silentErrors){
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        processedTarEntries++;
                    }
                } else {
                    startCounter++;
                }
            }

            watch.stop();

            logger.info("Finished iterating.");
            logger.info("Time elapsed: " + watch.getTime() + " ms");
            logger.info("Read entries: " + processedTarEntries);
            logger.info("Entries causing an exception: " + numberOfExceptions);
            logger.info("Percentage auf entries causing exceptions: " + ((double) numberOfExceptions / (double) processedTarEntries)*100);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package bous.philipp.dblp.datatools.util.logging;

import java.util.HashMap;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Logger;

/**
 * Helper that provides for better management of loggers throughout the project; instanciation and caching.
 */
public class LoggerFactory {
    private static final HashMap<String, Logger> loggers = new HashMap<>();

    /**
     * Helper to generate loggers. Includes caching of loggers.
     * @param loggerName name of the logger to be build
     * @return the logger
     */
    public static Logger getLogger(String loggerName) {
        Logger logger;
        logger = loggers.get(loggerName);
        if (logger == null) {
            logger = Logger.getLogger(loggerName);
            logger.setUseParentHandlers(false);

            ConsoleHandler handler = new ConsoleHandler();

            Formatter formatter = new LogFormatter();
            handler.setFormatter(formatter);

            logger.addHandler(handler);
            loggers.put(loggerName, logger);
        }
        return logger;
    }
}

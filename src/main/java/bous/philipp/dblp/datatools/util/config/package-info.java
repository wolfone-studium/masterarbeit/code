/**
 * Provides base classes/interfaces for a common interface for configurable experiment-steps as executed by
 * functions in bous.philipp.dblp.datatools.
 */
package bous.philipp.dblp.datatools.util.config;
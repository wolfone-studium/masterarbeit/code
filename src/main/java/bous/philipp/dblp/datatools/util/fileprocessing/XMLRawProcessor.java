package bous.philipp.dblp.datatools.util.fileprocessing;


/**
 * Functional interface providing a method to process a given xml in form of a String.
 */
@FunctionalInterface
public interface XMLRawProcessor {
    /**
     * Process an xml String.
     * @param rawXML xml to be processed
     * @throws Exception depends on implementation
     */
    void processRawXML(String rawXML) throws Exception;
}

/**
 * Provides utility to work on strings.
 */
package bous.philipp.dblp.datatools.util.string;
/**
 * Provides utility for mathematics like certain mathematic functions or statistic capabilities.
 */
package bous.philipp.dblp.datatools.util.math;
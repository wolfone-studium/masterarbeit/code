package bous.philipp.dblp.datatools.executables.vocabbuilding;

import bous.philipp.dblp.datatools.featureconstruction.vocabulary.VocabularyBuilder;

/**
 * Provides an executable to create vocabularies for Bag of Words vectorization using artificial balancing
 * when drawing the records from the database (will attempt to draw equal amount of positive/negative examples).
 */
public class BuildScoredVocabularyBalanced {
    /**
     * Executable to create vocabularies based on configuration in a properties file.
     * Entries to build the vocabulary are drawn in an artificially balanced manner.
     * @see VocabularyBuilder#buildAndWriteScoredVocabulariesBalancedWithConfigFile()
     * @param args irrelevant
     */
    public static void main(String[] args) {
        VocabularyBuilder.buildAndWriteScoredVocabulariesBalancedWithConfigFile();
    }
}

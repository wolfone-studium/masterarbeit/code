/**
 * Contains a set of executables as little scripts to explore or prepare
 * data or for the sake of debugging. Some of these may have underwent major changes. They are not part of the API
 * and, as they are not meant to be used outside the context of the corresponding thesis,
 * exist only for documentation purposes.
 * */
package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;
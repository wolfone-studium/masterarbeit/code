package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.featureconstruction.vectorization.BalancedVectorizationFoldPostHook;
import bous.philipp.dblp.datatools.featureconstruction.vectorization.SingleColumnCVSetProperties;
import bous.philipp.dblp.datatools.featureconstruction.vocabulary.ScoredVocabulary;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;
import org.apache.commons.collections4.ListUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Remains for documentation purposes; used to export time code parts from original arxiv-ids.
 */
public class ExportOriginalIdTimeCodeVectorizationPosthook implements BalancedVectorizationFoldPostHook {
    private final SingleColumnCVSetProperties props;
    private List<RecordDocument> positives;
    private List<RecordDocument> negatives;

    public ExportOriginalIdTimeCodeVectorizationPosthook(SingleColumnCVSetProperties props) {
        this.props = props;
    }

    @Override
    public void afterPositivesProcessed(int foldNumber, List<RecordDocument> positiveRecords, ScoredVocabulary vocabulary) {
        this.positives = positiveRecords;
    }

    @Override
    public void afterNegativesProcessed(int foldNumber, List<RecordDocument> negativeRecords, ScoredVocabulary vocabulary) {
        this.negatives = negativeRecords;
    }

    @Override
    public void finalize(int foldNumber, ScoredVocabulary vocabulary) {
        List<String> originalIds = new ArrayList<>();
        try (FileWriter fileWriter = new FileWriter(props.getFileOutputPath() + "/SetStats(" + foldNumber + ").txt"); BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            positives = ListUtils.union(positives, negatives);
            String[] outputSplit;
            String output;
            for (int i = 0; i < positives.size(); i++) {
                RecordDocument recordDocument = positives.get(i);
                outputSplit = recordDocument.getOriginalId().split("\\.");
                if (outputSplit.length > 0) {
                    output = outputSplit[0];
                } else {
                    output = recordDocument.getOriginalId();
                }
                originalIds.add(output);
            }
            originalIds.sort(String::compareTo);

            for (int i = 0; i < originalIds.size(); i++) {
                String id = originalIds.get(i);
                bufferedWriter.write(id + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

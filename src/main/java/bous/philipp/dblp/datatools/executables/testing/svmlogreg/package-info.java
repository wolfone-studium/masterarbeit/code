/**
 * Provides executables to test existing SVMs and LR models.
 */
package bous.philipp.dblp.datatools.executables.testing.svmlogreg;
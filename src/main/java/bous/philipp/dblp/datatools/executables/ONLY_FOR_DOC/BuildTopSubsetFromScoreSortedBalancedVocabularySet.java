package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.nio.file.Path;

public class BuildTopSubsetFromScoreSortedBalancedVocabularySet {
    /**
     * NO API PART
     * @param args irrelevant
     */
    public static void main(String[] args) {
        int number = 2000;
        String pathPrefixOriginal = "G:\\Entwicklung\\repositories\\masterarbeit\\code\\data\\experiments\\2019-06-25AdvRandomness\\vocabBase\\2019-06-25AdvRandomnessBaseVocab";
        String pathPrefixNew = "G:\\Entwicklung\\repositories\\masterarbeit\\code\\data\\experiments\\2019-06-25AdvRandomness\\" + number + "Vocab\\vocab\\2019-06-25AdvRandomnessVocab" + number;
        writeVocabSetTopVocabsSubset(pathPrefixOriginal, pathPrefixNew, 10, number);
    }

    private static void writeVocabSetTopVocabsSubset(String pathPrefixOriginalVocabs, String destinationPrefixOfNewVocabSet, int numberOfVocabFiles, int numberToKeep) {
        for (int i = 0; i < numberOfVocabFiles; i++) {
            String vocabPathOriginal = pathPrefixOriginalVocabs + "(" + i + ").fbowvoc";
            String vocabPathOutput = destinationPrefixOfNewVocabSet + "(" + i + ").fbowvoc";
            try (
                FileReader fr = new FileReader(vocabPathOriginal);
                BufferedReader vocabReader = new BufferedReader(fr);
                FileWriter fw = new FileWriter(vocabPathOutput);
                BufferedWriter newVocabWriter = new BufferedWriter(fw)
            ) {
                int counter = 0;
                String line = vocabReader.readLine();
                String[] firstLineTokens = line.split(" ");
                newVocabWriter.write(firstLineTokens[0] + " " + firstLineTokens[1] + " " + FilenameUtils.removeExtension(Path.of(vocabPathOutput).getFileName().toString()) + "\n");
                line = vocabReader.readLine();
                while (line != null && counter < numberToKeep) {
                    newVocabWriter.write(line + "\n");
                    counter++;
                    line = vocabReader.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

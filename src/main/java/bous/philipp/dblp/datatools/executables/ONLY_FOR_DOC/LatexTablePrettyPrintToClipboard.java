package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.tools.latex.Tabular;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.regex.Pattern;

public class LatexTablePrettyPrintToClipboard {
    public static void main(String[] args) {
        final String unformattedTabular =
                "";

        Tabular tabular = Tabular.fromTabularString(unformattedTabular);

        tabular.removeVerticalLines();
        tabular.setColumnDefinition(tabular.getColumnDefinition().replaceAll("r", "R{5cm}"));

        Pattern pattern = Pattern.compile("\\\\multicolumn\\{1\\}\\{l\\|\\}\\{(.*)\\}");
        tabular.reduceCellContentToCaptureGroup(pattern , 1);
        String prettyString = tabular.prettyTableString();

        System.out.println(prettyString);
        StringSelection stringSelection = new StringSelection(prettyString);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
        //System.out.println(tabular.cellMatrix()[1][1].getContent());
       /* System.out.println(tabular.getColumnDefinition());
        tabular.getCells().stream().forEach(System.out::println);
        System.out.println(tabular.getNumberOfRows());
        System.out.println(tabular.getNumberOfColumns());*/
    }

}

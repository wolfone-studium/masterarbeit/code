package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CountPositiveNegativeModelWeights {
    public static void main(String[] args) {
        String fileLocation = "G:\\Entwicklung\\repositories\\masterarbeit\\code\\data\\experiments\\2019-09-14PLoSErrorExport\\trainLiblinear(3u4)\\2019-09-14PLoSErrorExport-MODEL3u4";
        int positives = 0;
        int negatives = 1;
        try (FileReader fileReader = new FileReader(fileLocation); BufferedReader modelReader = new BufferedReader(fileReader)) {
            for (int i = 0; i < 7; i++) {
                modelReader.readLine();
            }
            String line = modelReader.readLine();
            while (line != null) {
                if (line.startsWith("-")) {
                    negatives++;
                } else {
                    positives++;
                }
                line = modelReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("positives: " + positives);
        System.out.println("negatives: " + negatives);
    }
}

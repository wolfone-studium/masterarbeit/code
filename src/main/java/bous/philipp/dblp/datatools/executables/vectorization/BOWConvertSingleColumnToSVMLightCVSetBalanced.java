package bous.philipp.dblp.datatools.executables.vectorization;

import bous.philipp.dblp.datatools.featureconstruction.vectorization.BagOfWordsVectorizationArchetypes;

/**
 * Provides an executable to convert database entries into artificially balanced cross validation sets in svmlight-file-format.
 * Bag of words approach is used for vectorization.
 */
public class BOWConvertSingleColumnToSVMLightCVSetBalanced {
    /**
     * Executable to perform conversion of database entries into cross validation sets in svmlight-file-format
     * using a bag of words approach and features of a single type to generate vectors from.
     * Will ask for the location of an appropriate config file.
     * @see BagOfWordsVectorizationArchetypes#convertSingleFeatureToSVMLightCrossValidationSetsBalanced()
     * @param args irrelevant
     */
    public static void main(String[] args) {
        BagOfWordsVectorizationArchetypes.convertSingleFeatureToSVMLightCrossValidationSetsBalanced();
    }
}

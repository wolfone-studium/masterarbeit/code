package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.featureconstruction.vectorization.BagOfWordsVectorizationArchetypes;
import bous.philipp.dblp.datatools.featureconstruction.vectorization.SingleColumnCVSetProperties;

import java.nio.file.Path;

public class ExportOriginalIds {
    /**
     * Exists only for documentation purposes; was used as quickly changeable executable to export original ids from records in cv-sets.
     * @param args irrelevant
     */
    public static void main(String[] args) {
        SingleColumnCVSetProperties props = new SingleColumnCVSetProperties();
        props.readProperties(Path.of("G:\\Entwicklung\\repositories\\masterarbeit\\code\\data\\debug2\\testForRandomness\\2019-06-30TestForTimeDependencyTestset\\vectors\\2019-06-27TestVectorsBalArxivTestset4000VocabAdvRandomness.properties"), "csctsvmlcvs-20.06.2019-1");
        ExportOriginalIdTimeCodeVectorizationPosthook posthook = new ExportOriginalIdTimeCodeVectorizationPosthook(props);
        BagOfWordsVectorizationArchetypes.convertSingleFeatureToSVMLightCrossValidationSetsBalanced(props, "CVSet", posthook);
    }
}

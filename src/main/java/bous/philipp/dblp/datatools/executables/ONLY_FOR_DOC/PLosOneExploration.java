package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.datamodel.records.archetypes.PlosOneRecord;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.factory.PlosOneRecordFactory;
import bous.philipp.dblp.datatools.util.fileprocessing.TarProcessor;
import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.concurrent.atomic.AtomicInteger;

public class PLosOneExploration {
    /**
     * Exists only for documentation purposes; was used as quickly changeable executable to expolore the PLoSOne Dataset.
     * @param args irrelevant
     */
    public static void main(String[] args) {
        AtomicInteger countNotComplete = new AtomicInteger(0);
        AtomicInteger countZero = new AtomicInteger(0);
        AtomicInteger countArticles = new AtomicInteger(0);
        AtomicInteger countCategorized = new AtomicInteger(0);
        AtomicInteger countProcessed = new AtomicInteger(0);
        AtomicInteger countNeg = new AtomicInteger(0);
        AtomicInteger countMayBeInteresting = new AtomicInteger(0);

        PlosOneRecordFactory recordFactory = new PlosOneRecordFactory();

        TarProcessor.iterateRawXMLTarFlat(
                "G:\\Entwicklung\\repositories\\masterarbeit\\NON_git\\PLosOne\\PLoS_One.tar.gz",
                100,
                0,
                rawXML -> {
                    countProcessed.incrementAndGet();
                    DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                    builderFactory.setValidating(false);
                    builderFactory.setNamespaceAware(true);
                    builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
                    builderFactory.setFeature("http://xml.org/sax/features/validation", false);
                    builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
                    builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
                    DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();

                    documentBuilder.setErrorHandler(new ErrorHandler() {
                        @Override
                        public void warning(SAXParseException exception) throws SAXException {

                        }

                        @Override
                        public void error(SAXParseException exception) throws SAXException {

                        }

                        @Override
                        public void fatalError(SAXParseException exception) throws SAXException {
                            if (exception.toString().contains("must start and end within") || exception.toString().contains("must be terminated by the matching end-tag")) {
                                countNotComplete.incrementAndGet();
                            }
                            if (exception.toString().contains("0x0")) {
                                countZero.incrementAndGet();
                            }

                        }
                    });

                    rawXML = rawXML.replaceAll("[\\000]*", "");

                    if (!rawXML.contains("article-type=\"correction\"") && !rawXML.contains("article-type=\"retraction\"")) {
                        Document document = documentBuilder.parse(new InputSource(new StringReader(rawXML)));
                        PlosOneRecord plosOneRecord = (PlosOneRecord) recordFactory.fromDocument(document, 0, 0);
/*                        if (plosOneRecord.getCategories().matches("(Computer and Information Sciences)|(Computer Science)")) {
                            try (FileWriter fw = new FileWriter("G:\\Daten\\PLosOne\\PosExamples\\pmc" + plosOneRecord.getOriginalId() + ".xml");
                                 BufferedWriter bw = new BufferedWriter(fw)){
                                bw.write(rawXML);
                            }
                        }*/
                        /*if (!plosOneRecord.getCategories().matches("(.*[Aa]lgorith.*)|(.*[Dd]ata.*)|(.*[Cc]omput.*)") && !plosOneRecord.getCategories().trim().equals("") && countNeg.get() < 300) {
                            try (FileWriter fw = new FileWriter("G:\\Daten\\PLosOne\\NegExamples\\pmc" + plosOneRecord.getOriginalId() + ".xml");
                                 BufferedWriter bw = new BufferedWriter(fw)){
                                bw.write(rawXML);
                                countNeg.incrementAndGet();
                            }
                        }*/
                        if (plosOneRecord.getCategories().matches("(.*[Cc]omputational [Bb]iology.*)|([Cc]omputer and [Ii]nformation [Ss]ciences)|([Cc]omputer [Ss]cience)")) {
                            countMayBeInteresting.incrementAndGet();
                        }
                        if (!plosOneRecord.getCategories().equals("")) {
                            countCategorized.incrementAndGet();
                        }
                        countArticles.incrementAndGet();
                    }
                    if (countProcessed.get() % 5000 == 0) {
                        System.out.println("5k processed");
                    }

                }
                , true);
        System.out.println("zero count: " + countZero.get());
        System.out.println("not complete count: " + countNotComplete.get());
        System.out.println("articles count: " + countArticles.get());
        System.out.println("categorized count: " + countCategorized.get());
        System.out.println("potentially interesting count: " + countMayBeInteresting.get());
    }
}

package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SummarizeTimeCodeIdParts {
    /**
     * Exists only for documentation purposes; was used to summarize those parts of original ids of arxiv records
     * (based on a previously createt statistics file for such a set) that represent a time-code to make sure that
     * no time dependency has leaked into the CV data-sets.
     * @param args irrelevant
     */
    public static void main(String[] args) {
        String fileLocation = "G:\\Entwicklung\\repositories\\masterarbeit\\code\\data\\debug2\\testForRandomness\\2019-06-30TestForTimeDependencyTestset\\vectors\\SetStats(0).txt";

        Map<String, Integer> timeslotsoccurrences = new HashMap<>();
        Map<String, Integer> yearsoccurrences = new HashMap<>();

        try (FileReader fr = new FileReader(fileLocation); BufferedReader reader = new BufferedReader(fr)) {
            String line = reader.readLine();
            while (line != null) {
                if (line.length() == 4) {
                    incrementOccurrence(yearsoccurrences, line.substring(0,2));
                }

                if (line.length() > 4) {
                    line = line.split("/")[0];
                    incrementOccurrence(yearsoccurrences, line);
                }
                incrementOccurrence(timeslotsoccurrences, line);


                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        yearsoccurrences.forEach((key, value) -> System.out.println("id-part: " + key + " occurrences: " + value));
    }

    private static void incrementOccurrence(Map<String, Integer> occurrenceMap, String line) {
        if (occurrenceMap.get(line) != null) {
            occurrenceMap.put(line, occurrenceMap.get(line) + 1);
        } else {
            occurrenceMap.put(line, 1);
        }
    }
}

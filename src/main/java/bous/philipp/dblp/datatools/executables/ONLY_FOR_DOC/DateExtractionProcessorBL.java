package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.util.fileprocessing.XMLRawProcessor;
import lombok.Getter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class DateExtractionProcessorBL implements XMLRawProcessor {

    @Getter private LocalDate earliest = LocalDate.of(2040, 12, 30);
    @Getter private LocalDate latest = LocalDate.of(1040, 12, 30);

    private String patternString = "<datestamp>(.*)</datestamp>";
    private Pattern pattern = Pattern.compile(patternString);
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);

    @Override
    public void processRawXML(String rawXML) throws Exception {
        Matcher matcher = pattern.matcher(rawXML);
        if (matcher.find()){
            String dateString = matcher.group(1).split("T")[0];
            LocalDate date = LocalDate.parse(dateString, dateTimeFormatter);
            if (date.isBefore(earliest)) {
                earliest = date;
            }
            if (date.isAfter(latest)) {
                latest = date;
            }
        }
    }
}


package bous.philipp.dblp.datatools.executables.testing.svmlogreg;

import bous.philipp.dblp.datatools.modelprocessing.ConfigurableLiblinearTaskArchetypes;

/**
 * Provides an executable to test a liblinear model.
 */
public class TestLiblinearModel {
    /**
     * Executable to test a liblinear model; will ask for an appropriate config file.
     * @see ConfigurableLiblinearTaskArchetypes#testLiblinearModel()
     * @param args irrelevant
     */
    public static void main(String[] args) {
        ConfigurableLiblinearTaskArchetypes.testLiblinearModel();
    }
}

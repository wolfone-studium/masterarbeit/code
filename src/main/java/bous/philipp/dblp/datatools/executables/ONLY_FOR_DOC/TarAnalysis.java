package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;


import bous.philipp.dblp.datatools.util.fileprocessing.TarProcessor;

public class TarAnalysis {
    public static void main(String[] args) {
        DateExtractionProcessorPlos dateExtractionProcessor = new DateExtractionProcessorPlos();
        TarProcessor.iterateRawXMLTarFlat(
                "G:\\Entwicklung\\repositories\\masterarbeit\\NON_git\\PLosOne\\PLoS_One.tar.gz",
                -1,
                -1,
                dateExtractionProcessor,
                false);
        System.out.println(dateExtractionProcessor.getEarliest());
        System.out.println(dateExtractionProcessor.getLatest());
    }
}
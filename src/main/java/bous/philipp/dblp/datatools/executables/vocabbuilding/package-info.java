/**
 * Provides an executable to build a {@link bous.philipp.dblp.datatools.featureconstruction.vocabulary.ScoredVocabulary}
 * from data drawn from a database/tables as described in the corresponding thesis.
 */
package bous.philipp.dblp.datatools.executables.vocabbuilding;
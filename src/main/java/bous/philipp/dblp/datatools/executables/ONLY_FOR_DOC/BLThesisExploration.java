package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.converter.heuristics.classification.BLStrictClassificationHeuristic;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.BLThesisRecord;
import bous.philipp.dblp.datatools.util.fileprocessing.TarProcessor;
import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.concurrent.atomic.AtomicInteger;

public class BLThesisExploration {
    /**
     * Exists only for documentation purposes; was used as quickly changeable executable to expolore the BlThesis Dataset.
     * @param args irrelevant
     */
    public static void main(String[] args) {
        AtomicInteger countNotComplete = new AtomicInteger(0);
        AtomicInteger countZero = new AtomicInteger(0);
        AtomicInteger countArticles = new AtomicInteger(0);
        AtomicInteger countCategorized = new AtomicInteger(0);
        AtomicInteger countProcessed = new AtomicInteger(0);
        AtomicInteger countNeg = new AtomicInteger(0);
        AtomicInteger countMayBeInteresting = new AtomicInteger(0);

        BLStrictClassificationHeuristic classificationHeuristic = new BLStrictClassificationHeuristic();

        TarProcessor.iterateRawXMLTarFlat(
                "G:\\Entwicklung\\repositories\\masterarbeit\\NON_git\\Theses\\ethos-all-2018-10-20.tar.gz",
                1000,
                0,
                rawXML -> {
                    countProcessed.incrementAndGet();
                    DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                    builderFactory.setValidating(false);
                    builderFactory.setNamespaceAware(true);
                    builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
                    builderFactory.setFeature("http://xml.org/sax/features/validation", false);
                    builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
                    builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
                    DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();

                    documentBuilder.setErrorHandler(new ErrorHandler() {
                        @Override
                        public void warning(SAXParseException exception) throws SAXException {

                        }

                        @Override
                        public void error(SAXParseException exception) throws SAXException {

                        }

                        @Override
                        public void fatalError(SAXParseException exception) throws SAXException {
                            if (exception.toString().contains("must start and end within") || exception.toString().contains("must be terminated by the matching end-tag")) {
                                countNotComplete.incrementAndGet();
                            }
                            if (exception.toString().contains("0x0")) {
                                countZero.incrementAndGet();
                            }

                        }
                    });

                    rawXML = rawXML.replaceAll("[\\000]*", "");

                    Document document = documentBuilder.parse(new InputSource(new StringReader(rawXML)));
                    BLThesisRecord record = BLThesisRecord.fromDocument(document, 1, 0);

                    RecordDocument recordDocument = record.toRecordDocument();
                    if (classificationHeuristic.isUseful(recordDocument)){
                        System.out.println(recordDocument);
                        System.out.println("has class: " + classificationHeuristic.guessClass(recordDocument));
                    }

                    //Arrays.stream(numberCategories).forEach(category -> System.out.println(category));
                    //System.out.println(record.toRecordDocument());
                }
                , false);
        System.out.println("zero count: " + countZero.get());
        System.out.println("not complete count: " + countNotComplete.get());
        System.out.println("articles count: " + countArticles.get());
        System.out.println("categorized count: " + countCategorized.get());
        System.out.println("potentially interesting count: " + countMayBeInteresting.get());
    }
}

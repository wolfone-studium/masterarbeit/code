package bous.philipp.dblp.datatools.executables.modelbuilding.svmlogreg;

import bous.philipp.dblp.datatools.modelprocessing.ConfigurableLiblinearTaskArchetypes;

public class TrainLiblinearAndDoCrossvalidation {
    /**
     * Provides executable to perform cross validation with liblinear on a svmlight-formatted
     * CV-appropriate set of sets. Will ask for an appropriate config-file.
     * @see ConfigurableLiblinearTaskArchetypes#trainLiblinearCrossValidationFromSvmLight()
     * @param args irrelevant
     */
    public static void main(String[] args) {
        ConfigurableLiblinearTaskArchetypes.trainLiblinearCrossValidationFromSvmLight();
    }
}

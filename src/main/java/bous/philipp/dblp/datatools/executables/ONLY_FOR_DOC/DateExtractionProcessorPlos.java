package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.util.fileprocessing.XMLRawProcessor;
import lombok.Getter;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class DateExtractionProcessorPlos implements XMLRawProcessor {

    @Getter private LocalDate earliest = LocalDate.of(2040, 12, 30);
    @Getter private LocalDate latest = LocalDate.of(1040, 12, 30);

    private String patternOuter = "<date date-type=\"accepted\">(.*)</date>";
    private Pattern pattern = Pattern.compile(patternOuter);

    @Override
    public void processRawXML(String rawXML) throws Exception {
        Matcher matcher = pattern.matcher(rawXML);
        if (matcher.find()){
            String dateString = matcher.group(1);
            dateString = dateString.replaceAll("<day>", "");
            dateString = dateString.replaceAll("</day><month>", "-");
            dateString = dateString.replaceAll("</month><year>", "-");
            dateString = dateString.replaceAll("</year>", "");
            String[] dayMonthYear = dateString.split( "-");
            if (dayMonthYear.length != 3) {
                return;
            }
            LocalDate date = LocalDate.of(
                    Integer.parseInt(dayMonthYear[2]),
                    Integer.parseInt(dayMonthYear[1]),
                    Integer.parseInt(dayMonthYear[0])
            );
            if (date.getYear() < 1980) {
                return;
            }
            if (date.isBefore(earliest)) {
                earliest = date;
            }
            if (date.isAfter(latest)) {
                latest = date;
            }
        }
    }
}


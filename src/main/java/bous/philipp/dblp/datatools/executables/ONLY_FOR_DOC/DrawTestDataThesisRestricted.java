package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.converter.CommonArchivesToDatabaseConverter;
import bous.philipp.dblp.datatools.util.database.DataSourceFactory;
import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Exists for documentation purposes; used to draw data from british library thesis tar using restricted drawing heuristic and write it to the database.
 */
public class DrawTestDataThesisRestricted {
    /**
     * Draw test data bl-theses restricted.
     * @param args irrelevant
     */
    public static void main(String[] args) {
        MysqlDataSource dataSource = DataSourceFactory.getMySQLDataSource("G:\\Entwicklung\\repositories\\masterarbeit\\code\\src\\main\\resources\\db.properties");
        try {
            Connection connection = dataSource.getConnection();
            CommonArchivesToDatabaseConverter.britishLibraryTarToDBRestricted(
                    connection,
                    "bl_theses_records_restricted",
                    "bl_theses_features_restricted",
                    "G:\\Entwicklung\\repositories\\masterarbeit\\NON_git\\Theses\\ethos-all-2018-10-20.tar.gz",
                    20000,
                    -1,
                    0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

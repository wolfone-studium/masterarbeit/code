package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.converter.CommonArchivesToDatabaseConverter;
import bous.philipp.dblp.datatools.util.database.DataSourceFactory;
import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Exists for documentation purposes; used to draw data from british library thesis tar using extended drawing heuristic and write it to the database.
 */
public class DrawTestDataThesisExtended {
    /**
     * Draw test data bl-theses extended.
     * @param args irrelevant
     */
    public static void main(String[] args) {
        MysqlDataSource dataSource = DataSourceFactory.getMySQLDataSource("G:\\Entwicklung\\repositories\\masterarbeit\\code\\src\\main\\resources\\db.properties");
        try {
            Connection connection = dataSource.getConnection();
            CommonArchivesToDatabaseConverter.britishLibraryTarToDBExtended(
                    connection,
                    "bl_theses_records_extended",
                    "bl_theses_features_extended",
                    "G:\\Entwicklung\\repositories\\masterarbeit\\NON_git\\Theses\\ethos-all-2018-10-20.tar.gz",
                    20000,
                    -1,
                    0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

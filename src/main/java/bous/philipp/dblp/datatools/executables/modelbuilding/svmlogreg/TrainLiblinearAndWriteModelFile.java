package bous.philipp.dblp.datatools.executables.modelbuilding.svmlogreg;

import bous.philipp.dblp.datatools.modelprocessing.ConfigurableLiblinearTaskArchetypes;

public class TrainLiblinearAndWriteModelFile {
    /**
     * Executable to trains a model with liblinear and write the resulting model to a file.
     * Will ask for an appropriate config file.
     * @see ConfigurableLiblinearTaskArchetypes#trainLiblinearAndWriteModelFile()
     * @param args irrelevant
     */
    public static void main(String[] args) {
        ConfigurableLiblinearTaskArchetypes.trainLiblinearAndWriteModelFile();
    }
}

package bous.philipp.dblp.datatools.executables.vectorization;

import bous.philipp.dblp.datatools.featureconstruction.vectorization.BagOfWordsVectorizationArchetypes;

/**
 * Provides an executable to convert database entries into cross validation sets in svmlight-file-format.
 * Bag of words approach is used for vectorization.
 * Entries are drawn as is without artificial balancing.
 */
public class BOWConvertSingleColumnToSVMLightCVSetUnbalanced {
    /**
     * Executable to build a set of svmlight files for cross-validation without artificial class balancing.
     * Will ask for an appropriate config file location.
     * @see BagOfWordsVectorizationArchetypes#convertSingleFeatureToSVMLightCrossValidationSetsUnbalanced()
     * @param args irrelevant
     */
    public static void main(String[] args) {
        BagOfWordsVectorizationArchetypes.convertSingleFeatureToSVMLightCrossValidationSetsUnbalanced();
    }
}

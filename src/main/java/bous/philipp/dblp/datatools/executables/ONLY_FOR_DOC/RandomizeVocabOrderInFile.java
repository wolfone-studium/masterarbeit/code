package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class RandomizeVocabOrderInFile {
    /**
     * NO API PART
     * @param args irrelevant
     */
    public static void main(String[] args) {
        Path vocabPath = askForVocabFilePath();
        long seed = askForSeed();

        String outputPath = FilenameUtils.removeExtension(vocabPath.toAbsolutePath().toString()) + "RAND-SEED" + seed + ".fbowvoc";

        ArrayList<String> terms = readVocabs(vocabPath);
        List<String> randomizedTerms = randomizeVocabOrder(terms, seed);

        writeNewRandomizedVocabFile(vocabPath, seed, outputPath, randomizedTerms);
    }

    private static void writeNewRandomizedVocabFile(Path vocabPath, long seed, String outputPath, List<String> randomizedTerms) {
        try (FileWriter fw = new FileWriter(outputPath); BufferedWriter newVocabWriter = new BufferedWriter(fw)) {

            String lineOut = "fvoc: RANDOMIZE " +  FilenameUtils.removeExtension(vocabPath.getFileName().toString())+ "RAND-SEED" + seed;
            newVocabWriter.write(lineOut + "\n");
            for (int i = 0; i < randomizedTerms.size(); i++) {
                String term = randomizedTerms.get(i);
                newVocabWriter.write(term + "\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    private static ArrayList<String> readVocabs(Path vocabPath) {

        ArrayList<String> terms = new ArrayList<>();
        try (FileReader fr = new FileReader(vocabPath.toString()); BufferedReader vocabReader = new BufferedReader(fr)){
            String lineIn = vocabReader.readLine();
            lineIn = vocabReader.readLine();
            while (lineIn != null) {
                terms.add(lineIn);
                lineIn = vocabReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return terms;
    }

    //Seeded randomize
    private static List<String> randomizeVocabOrder(List<String> terms, long seed) {
        Random random = new Random(seed);
        Collections.shuffle(terms, random);
        return terms;
    }

    private static Path askForVocabFilePath() {
        Scanner scanner = new Scanner(System.in);

        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current path is: " + s);
        System.out.println("Enter vocabulary-path: ");
        return Paths.get(scanner.next());
    }

    private static long askForSeed() {
        Scanner scanner = new Scanner(System.in);

        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current path is: " + s);
        System.out.println("Enter seed (long-type): ");
        return Long.parseLong(scanner.next());
    }
}

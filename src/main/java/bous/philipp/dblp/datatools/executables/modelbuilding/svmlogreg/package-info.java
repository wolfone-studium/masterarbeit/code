/**
 * Provides executables to perform cross-validation and standalone model-building processes using
 * SVMs and LR.
 */
package bous.philipp.dblp.datatools.executables.modelbuilding.svmlogreg;
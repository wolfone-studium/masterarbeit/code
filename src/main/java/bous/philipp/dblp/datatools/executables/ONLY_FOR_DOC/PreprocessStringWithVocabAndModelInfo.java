package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.tokenization.LuceneStandardTokenizerAdapter;
import bous.philipp.dblp.datatools.util.string.StringUtility;
import com.google.common.util.concurrent.AtomicDouble;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

public class PreprocessStringWithVocabAndModelInfo {
    public static void main(String[] args) {
        String rawInput = "Grounded cognition proposes that memory shares processing resources with sensorimotor systems. " +
                "The aim of the present study was to show that motor simulation participates in the conceptual representation " +
                "of manipulable objects in long-term memory. In two experiments, lists of manipulable and nonmanipulable " +
                "objects were presented. Participants were instructed to memorize the items while adopting different postures. " +
                "In the control condition, they had to keep their hands at rest in front of them. In the interference " +
                "condition, participants had to keep their hands crossed behind their back to make their hands less free for " +
                "action. After each list, participants had to perform first a distractive task, and then an oral free recall. " +
                "The results showed that the interfering posture produced a specific decrease in the recall of manipulable " +
                "objects, but not of nonmanipulable ones. This decrease was similar when the items were presented as pictures " +
                "(Experiment 1) or as words (Experiment 2), thus excluding a purely visual effect. These results provide strong " +
                "evidence that the motor simulation plays a role in the memory trace of the object.";
        String vocabLocation = "G:\\Entwicklung\\repositories\\masterarbeit\\code\\data\\experiments\\2019-09-14PLoSErrorExport\\vocabulary(3u4)\\2019-09-14PLoSErrorExportVocab3u4(0).fbowvoc";
        String modelLocation = "G:\\Entwicklung\\repositories\\masterarbeit\\code\\data\\experiments\\2019-09-14PLoSErrorExport\\trainLiblinear(3u4)\\2019-09-14PLoSErrorExport-MODEL3u4";
        HashMap<String, Integer> vocabsAndIndex = getVocabHashMap(vocabLocation);
        HashMap<String, Double> modelWeightsHashmap = getModelWeightsHashmap(modelLocation, vocabLocation);
        LuceneStandardTokenizerAdapter tokenizer = new LuceneStandardTokenizerAdapter();
        List<String> tokens = tokenizer.tokenize(rawInput);
        List<String> processedTokens = StringUtility.processTokens(
                tokens,
                true,
                true,
                true);
        Stream<String> distinctRelevantSortedTokens = processedTokens.stream()
                .distinct()
                .filter(term -> vocabsAndIndex.get(term) != null)
                .sorted(Comparator.comparingInt(vocabsAndIndex::get));
        AtomicDouble sum = new AtomicDouble(0);
        distinctRelevantSortedTokens.forEach(token -> {
            Double weight = modelWeightsHashmap.get(token);
            sum.addAndGet(weight);
            System.out.println(weight + "      " + token + " " + vocabsAndIndex.get(token));
        });
        System.out.println("Summe: " + sum.get());
    }

    private static HashMap<String, Integer> getVocabHashMap(String vocabLocation) {
        HashMap<String, Integer> vocabsAndIndex = new HashMap<>();
        try (FileReader fr = new FileReader(vocabLocation); BufferedReader vocabsReader = new BufferedReader(fr)){
            vocabsReader.readLine();
            String line = vocabsReader.readLine();
            int vocabIndex = 0;
            while (line != null) {
                vocabsAndIndex.put(line, vocabIndex);
                vocabIndex++;
                line = vocabsReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return vocabsAndIndex;
    }

    private static HashMap<String, Double> getModelWeightsHashmap(String modelLocation, String vocabLocation) {
        HashMap<String, Double> weightHashmap = new HashMap<>();
        try (
                FileReader fr = new FileReader(modelLocation);
                BufferedReader model = new BufferedReader(fr);
                FileReader fr2 = new FileReader(vocabLocation);
                BufferedReader vocab = new BufferedReader(fr2)) {
            int index = 0;
            String line = vocab.readLine();
            for (int i = 1; i < 7; i++) {
                model.readLine();
            }
            line = vocab.readLine();
            while (line != null) {
                weightHashmap.put(line, Double.parseDouble(model.readLine()));
                line = vocab.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return weightHashmap;
    }
}

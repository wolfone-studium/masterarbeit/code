package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import java.util.Arrays;
import java.util.Scanner;

public class VerticalizeLatexTableRow {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter row with ampersands to verticalize:");
        String line;
        while (true) {
            line = scanner.nextLine();
            if (line.equals("quit")) {
                break;
            }
            Arrays.stream(line.split("&")).forEach((token) -> {
                System.out.println(token.trim());
            });
            System.out.println("Enter next line: ");
        }
    }
}

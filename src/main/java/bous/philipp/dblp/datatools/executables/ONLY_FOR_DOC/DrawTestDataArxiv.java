package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.converter.CommonArchivesToDatabaseConverter;
import bous.philipp.dblp.datatools.util.database.DataSourceFactory;
import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Exists for documentation purposes; used to draw data from arxiv tar and write it to the database.
 */
public class DrawTestDataArxiv {

    /**
     * Draw test data arxiv.
     * @param args irrelevant
     */
    public static void main(String[] args) {
        MysqlDataSource dataSource = DataSourceFactory.getMySQLDataSource("G:\\Entwicklung\\repositories\\masterarbeit\\code\\src\\main\\resources\\db.properties");
        try {
            Connection connection = dataSource.getConnection();
            CommonArchivesToDatabaseConverter.arxivTarToDB(
                    connection,
                    "test_records",
                    "test_features",
                    "G:\\Entwicklung\\repositories\\masterarbeit\\NON_git\\Arxiv\\arxiv-all-raw-2018-10-19.tar.gz",
                    20000,
                    -1,
                    0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

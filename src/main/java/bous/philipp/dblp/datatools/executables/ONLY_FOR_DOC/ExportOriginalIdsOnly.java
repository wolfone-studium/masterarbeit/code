package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.featureconstruction.vectorization.BagOfWordsVectorizationArchetypes;
import bous.philipp.dblp.datatools.featureconstruction.vectorization.SingleColumnCVSetProperties;

import java.nio.file.Path;

public class ExportOriginalIdsOnly {
    /**
     * Exists only for documentation purposes; was used as quickly changeable executable to export original ids of records in cv sets.
     * @param args irrelevant
     */
    public static void main(String[] args) {
        SingleColumnCVSetProperties props = new SingleColumnCVSetProperties();
        props.readProperties(Path.of("G:\\Entwicklung\\repositories\\masterarbeit\\code\\data\\debug2\\compareSplitsBeforeAfter20-06\\before\\2019-06-06Explore-SmallVocabFisherVectors.properties"), "csctsvmlcvs-20.06.2019-1");
        ExportOriginalIdVectorizationPosthook posthook = new ExportOriginalIdVectorizationPosthook(props);
        BagOfWordsVectorizationArchetypes.convertSingleFeatureToSVMLightCrossValidationSetsBalanced(props, "CVSet", posthook);
    }
}

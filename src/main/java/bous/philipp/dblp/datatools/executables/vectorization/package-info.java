/**
 * Provides executables to draw data from a database/tables as described in the corresponding thesis
 * and vectorize the drawn data. Currently this means especially preparing sets of files of
 * data usable in CV-scenarios, vectorized using Bag of Words methods and derivations.
 */
package bous.philipp.dblp.datatools.executables.vectorization;
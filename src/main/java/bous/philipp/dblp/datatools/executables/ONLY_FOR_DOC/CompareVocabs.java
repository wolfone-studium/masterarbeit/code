package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.featureconstruction.vocabulary.ScoredVocabulary;
import bous.philipp.dblp.datatools.featureconstruction.vocabulary.TermStatistics;

import java.util.Iterator;

public class CompareVocabs {
    /**
     * Exists only for documentation purposes; was used as quickly changeable executable to compare vocabularies to each other.
     * @param args irrelevant
     */
    public static void main(String[] args) {
        String pathVocab1 = "G:\\Entwicklung\\repositories\\masterarbeit\\code\\data\\experiments\\2019-06-20FisherBigBaseVocab\\backup\\2019-06-06FisherBigBaseVocab(0).fbowvoc";
        String pathVocab2 = "G:\\Entwicklung\\repositories\\masterarbeit\\code\\data\\experiments\\2019-06-20FisherBigBaseVocab\\2019-06-06FisherBigBaseVocab(0).fbowvoc";

        ScoredVocabulary vocab1 = ScoredVocabulary.fromFile(pathVocab1);
        ScoredVocabulary vocab2 = ScoredVocabulary.fromFile(pathVocab2);

        int containedIn2 = 0;

        for (Iterator<TermStatistics> iterator = vocab1.iterator(); iterator.hasNext(); ) {
            TermStatistics termStatistics =  iterator.next();
            if (vocab2.get(termStatistics.getTerm()) != null) {
                containedIn2++;
            }
        }
        System.out.println("Of " + vocab1.size() + " vocabs in vocab1, vocab2 contained: " + containedIn2 + " which makes " + ((double) containedIn2 / (double) vocab1.size() * 100)+ " %");
    }
}

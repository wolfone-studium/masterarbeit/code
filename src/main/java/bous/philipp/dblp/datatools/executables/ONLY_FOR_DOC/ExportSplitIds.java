package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.featureconstruction.vectorization.BagOfWordsVectorizationArchetypes;
import bous.philipp.dblp.datatools.featureconstruction.vectorization.SingleColumnCVSetProperties;

import java.nio.file.Path;


public class ExportSplitIds {
    /**
     * Exists only for documentation purposes; was used as quickly changeable executable to export the ids of records in cv sets.
     * @param args irrelevant
     */
    public static void main(String[] args) {
        SingleColumnCVSetProperties props = new SingleColumnCVSetProperties();
        props.readProperties(Path.of("G:\\Entwicklung\\repositories\\masterarbeit\\code\\data\\debug2\\testForRandomness\\vectors2\\testForRandomnessVectors2.properties"), "csctsvmlcvs-20.06.2019-1");
        ExportSplitIdsVectorizationPosthook posthook = new ExportSplitIdsVectorizationPosthook(props);
        BagOfWordsVectorizationArchetypes.convertSingleFeatureToSVMLightCrossValidationSetsBalanced(props, "CVSet", posthook);
    }
}



package bous.philipp.dblp.datatools.executables.ONLY_FOR_DOC;

import bous.philipp.dblp.datatools.featureconstruction.vectorization.BalancedVectorizationFoldPostHook;
import bous.philipp.dblp.datatools.featureconstruction.vectorization.SingleColumnCVSetProperties;
import bous.philipp.dblp.datatools.featureconstruction.vocabulary.ScoredVocabulary;
import bous.philipp.dblp.datatools.datamodel.FeatureType;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;
import org.apache.commons.collections4.ListUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Remains for documentation purposes; used to export record ids for cv-sets.
 */
public class ExportSplitIdsVectorizationPosthook implements BalancedVectorizationFoldPostHook {
    private final SingleColumnCVSetProperties props;
    private List<RecordDocument> positives;
    private List<RecordDocument> negatives;

    public ExportSplitIdsVectorizationPosthook(SingleColumnCVSetProperties props) {
        this.props = props;
    }

    @Override
    public void afterPositivesProcessed(int foldNumber, List<RecordDocument> positiveRecords, ScoredVocabulary vocabulary) {
        this.positives = positiveRecords;
    }

    @Override
    public void afterNegativesProcessed(int foldNumber, List<RecordDocument> negativeRecords, ScoredVocabulary vocabulary) {
        this.negatives = negativeRecords;
    }

    @Override
    public void finalize(int foldNumber, ScoredVocabulary vocabulary) {
        try (FileWriter fileWriter = new FileWriter(props.getFileOutputPath() + "/SetStats(" + foldNumber + ").txt"); BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            positives = ListUtils.union(positives, negatives);
            for (int i = 0; i < positives.size(); i++) {
                RecordDocument recordDocument = positives.get(i);
                bufferedWriter.write(
                        "Original id: " + recordDocument.getOriginalId() + ", " +
                            "categories: " + recordDocument.getFeaturesByType(FeatureType.CATEGORIES).get(0).getValue() + ", " +
                            "class: " + recordDocument.getCsClassificationStatus() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

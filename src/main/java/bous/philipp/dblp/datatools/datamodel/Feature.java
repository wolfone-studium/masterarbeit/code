package bous.philipp.dblp.datatools.datamodel;

/**
 * Represents a feature of a record (as represented by {@link RecordDocument} where a fully populated feature would usually consist
 * of a valid "id", has a non-null {@link FeatureType}, a non-null/non-empty
 * "value"-String, a "recordId" which identifies the record to which this feature is tied.
 * Optimally from the source of the feature a "originalPosition" was derivable which
 * signals the features place in the hierarchy of structured data-format in the form
 * of a dot-separated hierarchy encoding (like "2.1.4" grandfather.father.child).
 */
public class Feature {
    private int id;
    private FeatureType type;
    private String value;
    private int recordId;
    private String originalPosition;

    /**
     * Constructs a feature.
     * @param id id of the feature
     * @param type feature-type
     * @param value feature-value
     * @param recordId id of the associated {@link RecordDocument}
     */
    public Feature(int id, FeatureType type, String value, int recordId){
        this.id = id;
        this.type = type;
        this.value = value;
        this.recordId = recordId;
        this.originalPosition = "0.0";
    }

    /**
     * Constructs a feature.
     * @param id id of the feature
     * @param type feature-type
     * @param value feature-value
     * @param recordId id of the associated {@link RecordDocument}
     * @param position position that this feature had in the original document; should have a structure like 1.2.3 where 1 represents the "oldest" level of hierarchy
     */
    public Feature(int id, FeatureType type, String value, int recordId, String position){
        this.id = id;
        this.type = type;
        this.value = value;
        this.recordId = recordId;
        this.originalPosition = position;
    }

    @Override
    public String toString() {
        return "Id: " + this.id + ", Type: " + this.type + ", Value: " + this.value + ", Associated record artificial id: " + this.recordId + ", Position: " + this.originalPosition;
    }

    public int getId(){
        return this.id;
    }

    public FeatureType getType(){
        return this.type;
    }

    public String getValue(){
        return this.value;
    }

    public int getRecordId(){
        return this.recordId;
    }

    public void setId(int id){
        this.id = id;
    }
    
    public void setType(FeatureType type){
        this.type = type;
    }

    public void setValue(String value){
        this.value = value;
    }

    public void setRecordId(int recordId){
        this.recordId = recordId;
    }

    public String getOriginalPosition() {
        return originalPosition;
    }

    public void setOriginalPosition(String originalPosition) {
        this.originalPosition = originalPosition;
    }
}
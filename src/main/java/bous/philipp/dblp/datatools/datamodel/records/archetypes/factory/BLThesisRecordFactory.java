package bous.philipp.dblp.datatools.datamodel.records.archetypes.factory;

import bous.philipp.dblp.datatools.datamodel.Feature;
import bous.philipp.dblp.datatools.datamodel.FeatureType;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.BLThesisRecord;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.RecordArchetype;
import bous.philipp.dblp.datatools.util.xml.XMLUtility;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Produces {@link BLThesisRecord}s from {@link Document}s.
 */
public class BLThesisRecordFactory implements RecordFactory {
    @Override
    public RecordArchetype fromDocument(Document document, int recordIdArtificial, int initialFeatureId) {
        BLThesisRecord thesisRecord = new BLThesisRecord();
        thesisRecord.setId(recordIdArtificial);
        thesisRecord.setFeatures(new ArrayList<>());

        NodeList recordNodes = document.getElementsByTagName("*");
        Iterator<Element> elementIterator = XMLUtility.elementIterator(recordNodes);

        StringBuilder contributorsBuilder = new StringBuilder();
        StringBuilder categoriesBuilder = new StringBuilder();
        //will just take the last recognized position!
        String categoriesPosition = "";
        //will take the position of the creator if existent, else stay empty
        String creatorPosition = "";
        while (elementIterator.hasNext()) {
            Element element = elementIterator.next();
            String tagName = element.getTagName();
            String tagContent = element.getTextContent().trim();

            if (StringUtils.isBlank(tagContent)) {
                continue;
            }

            switch (tagName) {
                //NOTE: timestamp aufnehmen?
                case "identifier":
                    thesisRecord.setOriginalId(tagContent);
                    thesisRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.ID,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(element, BLThesisRecord.POSITION_CUT_LEVEL)
                    ));
                    break;
                case "dc:title":
                    thesisRecord.setTitle(tagContent);
                    thesisRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.TITLE,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(element, BLThesisRecord.POSITION_CUT_LEVEL)
                    ));
                    break;
                case "dc:description":
                    thesisRecord.setAbstractOfPublication(tagContent);
                    thesisRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.ABSTRACT,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(element, BLThesisRecord.POSITION_CUT_LEVEL)
                    ));
                    break;
                case "dc:creator":
                    creatorPosition = XMLUtility.getPosition(element, BLThesisRecord.POSITION_CUT_LEVEL);
                    contributorsBuilder.append(tagContent).append(":::");
                    break;
                case "dc:contributor":
                    contributorsBuilder.append(tagContent).append(":::");
                    break;
                case "dc:subject":
                    categoriesBuilder.append(tagContent).append(":::");
                    categoriesPosition = XMLUtility.getPosition(element, BLThesisRecord.POSITION_CUT_LEVEL);
                    break;
                case "dc:type":
                    thesisRecord.setResourceType(tagContent);
                    thesisRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.RESOURCE_TYPE,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(element, BLThesisRecord.POSITION_CUT_LEVEL)
                    ));
            }
        }
        String categories = categoriesBuilder.toString();
        if (!categories.equals("") && categories.length() > 3) {
            String rectifiedCategories = categories.substring(0, categories.length() - 3).trim();
            thesisRecord.setCategories(rectifiedCategories);
            thesisRecord.addFeature(new Feature(
                    initialFeatureId++,
                    FeatureType.CATEGORIES,
                    rectifiedCategories,
                    recordIdArtificial,
                    categoriesPosition
            ));
        }

        String contributors = contributorsBuilder.toString();
        if (!contributors.equals("") && contributors.length() > 3){
            String rectifiedContributors = contributors.substring(0, contributors.length() - 3).trim();
            thesisRecord.setAuthors(rectifiedContributors);
            thesisRecord.addFeature(new Feature(
                    initialFeatureId++,
                    FeatureType.AUTHORS,
                    rectifiedContributors,
                    recordIdArtificial,
                    creatorPosition
            ));
        }

        return thesisRecord;
    }
}

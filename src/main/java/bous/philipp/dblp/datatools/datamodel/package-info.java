/**
 * Contains classes/interfaces/enums that build the base model of in-code data-representation of
 * documents and corresponding structures.
 */
package bous.philipp.dblp.datatools.datamodel;
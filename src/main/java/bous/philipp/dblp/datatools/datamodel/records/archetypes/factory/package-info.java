/**
 * Contains factories to build in-code representations of documents from specific sources (based on a common interface).
 * So for example there is {@link bous.philipp.dblp.datatools.datamodel.records.archetypes.factory.ArxivRecordFactory}
 * to build {@link bous.philipp.dblp.datatools.datamodel.records.archetypes.ArxivRecord}s and so on.
 */
package bous.philipp.dblp.datatools.datamodel.records.archetypes.factory;
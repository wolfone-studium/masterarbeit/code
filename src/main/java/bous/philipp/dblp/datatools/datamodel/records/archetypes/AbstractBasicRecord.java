package bous.philipp.dblp.datatools.datamodel.records.archetypes;

import bous.philipp.dblp.datatools.datamodel.Feature;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Basic implementation of {@link RecordArchetype} as some basic fields will/should be present
 * in nearly all implementations of RecordArchetype and some methods will barely ever change inside these implementations.
 */
public abstract class AbstractBasicRecord implements RecordArchetype{
    public static final int POSITION_CUT_LEVEL = 1;
    @Getter @Setter protected int id = -1;
    @Getter @Setter protected String categories = "NONE";
    @Getter @Setter protected String title = "NONE";
    @Getter @Setter protected String abstractOfPublication = "NONE";
    @Getter @Setter protected String authors = "NONE";
    @Getter @Setter protected String originalId = "NONE";

    @Setter protected List<Feature> features;

    /**
     * Converts the given AbstractBasicRecord-implementation to a {@link RecordDocument}.
     * ATTENTION: will always give the new RecordDocument a classification status of 0 (i.e. "UNKNOWN").
     * If a class is known it has to be set separately! This is due to the fact that, by design, an implementation
     * of AbstractBasicRecord or {@link RecordArchetype} should not care about its own hypothetical class.
     * @return the record-document
     */
    @Override
    public RecordDocument toRecordDocument() {
        RecordDocument document = new RecordDocument();
        document.setFeatures(this.getFeatureList());
        document.setCsClassificationStatus(0);
        document.setOriginalId(this.getOriginalId());
        document.setId(this.id);
        return document;
    }

    @Override
    public String toString() {
        return this.toRecordDocument().toString();
    }

    @Override
    public List<Feature> getFeatureList() {
        return this.features;
    }

    /**
     * Adds a feature.
     * @param feature the feature to be added
     */
    public void addFeature(Feature feature) {
        this.features.add(feature);
    }
}

package bous.philipp.dblp.datatools.datamodel.records.archetypes.factory;

import bous.philipp.dblp.datatools.datamodel.Feature;
import bous.philipp.dblp.datatools.datamodel.FeatureType;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.PlosOneRecord;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.RecordArchetype;
import bous.philipp.dblp.datatools.util.xml.XMLUtility;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Produces {@link PlosOneRecord}s from {@link Document}s.
 */
public class PlosOneRecordFactory implements RecordFactory {

    @Override
    public RecordArchetype fromDocument(Document document, int recordIdArtificial, int initialFeatureId) {
        PlosOneRecord record = new PlosOneRecord();
        record.setCurrentFeatureId(initialFeatureId);
        record.setId(recordIdArtificial);
        record.setFeatures(new ArrayList<>());

        NodeList front = document.getElementsByTagName("front");
        Node frontNode = front.item(0);
        NodeList nodeList = frontNode.getChildNodes();

        Iterator<Element> elementIterator = XMLUtility.elementIterator(nodeList);

        while (elementIterator.hasNext()) {
            Element element = elementIterator.next();
            String tagName = element.getTagName();

            if (tagName.equals("journal-meta")){
                NodeList journalMetaNodes = element.getChildNodes();
                Iterator<Element> journalNodes = XMLUtility.elementIterator(journalMetaNodes);
                while (journalNodes.hasNext()) {
                    Element journalNode = journalNodes.next();
                    this.processJournalMetaNode(journalNode, record);
                }
            }

            if (tagName.equals("article-meta")){
                NodeList articleMetaNodes = element.getChildNodes();
                Iterator<Element> articleNodes = XMLUtility.elementIterator(articleMetaNodes);
                while (articleNodes.hasNext()) {
                    Element articleNode = articleNodes.next();
                    this.processArticleMetaNode(articleNode, record);
                }
            }
        }

        return record;
    }

    private void processJournalMetaNode(Element metaNode, PlosOneRecord record) {
        String tagName = metaNode.getTagName();
        if (tagName.equals("journal-title-group")) {
            this.processJournalTitleGroupNode(metaNode, record);
        }
    }

    private void processJournalTitleGroupNode(Element node, PlosOneRecord record) {
        Iterator<Element> childNodes = XMLUtility.elementIterator(node.getChildNodes());
        while (childNodes.hasNext()) {
            Element element = childNodes.next();
            if (element.getTagName().equals("journal-title")) {
                record.setJournalRef(element.getTextContent().trim());
                if (!record.getJournalRef().equals("")) {
                    record.addFeature(
                            new Feature(
                                    record.getCurrentFeatureId(),
                                    FeatureType.JOURNAL_REF,
                                    record.getJournalRef(),
                                    record.getId(),
                                    XMLUtility.getPosition(node, PlosOneRecord.POSITION_CUT_LEVEL)
                            )
                    );
                    record.setCurrentFeatureId(record.getCurrentFeatureId() + 1);
                }
            }
        }
    }

    private void processArticleMetaNode(Element element, PlosOneRecord record) {
        String tagName = element.getTagName();
        switch (tagName) {
            case "article-id":
                this.processArticleIdNode(element, record);
                break;
            case "article-categories":
                this.processArticleCategoriesNode(element, record);
                break;
            case "title-group":
                this.processTitleGroupNode(element, record);
                break;
            case "contrib-group":
                this.processContribGroupNode(element, record);
                break;
            case "abstract":
                this.processAbstractNode(element, record);
                break;
        }
    }

    private void processAbstractNode(Element node, PlosOneRecord record) {
        record.setAbstractOfPublication(node.getTextContent().trim());
        if (!record.getAbstractOfPublication().equals("")) {
            record.addFeature(
                    new Feature(
                            record.getCurrentFeatureId(),
                            FeatureType.ABSTRACT,
                            record.getAbstractOfPublication(),
                            record.getId(),
                            XMLUtility.getPosition(node, PlosOneRecord.POSITION_CUT_LEVEL)
                    )
            );
            record.setCurrentFeatureId(record.getCurrentFeatureId() + 1);
        }
    }

    private void processContribGroupNode(Element node, PlosOneRecord record) {
        if (!record.getAuthors().equals("")){
            return;
        }
        StringBuilder contributors = new StringBuilder();
        Iterator<Element> childNodes = XMLUtility.elementIterator(node.getChildNodes());
        while (childNodes.hasNext()) {
            Element child = childNodes.next();
            if (XMLUtility.hasAttributeMatchingRegex(child, "contrib-type", "author")) {
                String authorName = getNameFromAuthorNode(child);
                contributors.append(authorName);
            }
            if (childNodes.hasNext()) {
                contributors.append("; ");
            }
        }
        record.setAuthors((record.getAuthors() + " " + contributors.toString()).trim());

        if (!record.getAuthors().equals("")){
            record.addFeature(
                    new Feature(
                            record.getCurrentFeatureId(),
                            FeatureType.AUTHORS,
                            record.getAuthors(),
                            record.getId(),
                            XMLUtility.getPosition(node, PlosOneRecord.POSITION_CUT_LEVEL)
                    )
            );
            record.setCurrentFeatureId(record.getCurrentFeatureId() + 1);
        }
    }

    private String getNameFromAuthorNode(Element node) {
        StringBuilder authorName = new StringBuilder();
        Iterator<Element> childNodes = XMLUtility.elementIterator(node.getChildNodes());
        while (childNodes.hasNext()) {
            Element element = childNodes.next();
            if (element.getTagName().equals("name")) {
                Iterator<Element> nameParts = XMLUtility.elementIterator(element.getChildNodes());
                while (nameParts.hasNext()) {
                    Element namePart = nameParts.next();
                    authorName.append(namePart.getTextContent()).append(" ");
                }
            }
        }
        return authorName.toString().trim();
    }

    private void processTitleGroupNode(Element node, PlosOneRecord record) {
        StringBuilder builder = new StringBuilder();
        Iterator<Element> childNodes = XMLUtility.elementIterator(node.getChildNodes());
        while (childNodes.hasNext()) {
            Element child = childNodes.next();
            builder.append(child.getTextContent());
            if (childNodes.hasNext()) {
                builder.append("; ");
            }
        }

        record.setTitle(builder.toString().trim());
        if (!record.getTitle().equals("")) {
            record.addFeature(
                    new Feature(
                            record.getCurrentFeatureId(),
                            FeatureType.TITLE,
                            record.getTitle(),
                            record.getId(),
                            XMLUtility.getPosition(node, PlosOneRecord.POSITION_CUT_LEVEL)
                    )
            );
            record.setCurrentFeatureId(record.getCurrentFeatureId() + 1);
        }
    }

    private void processArticleCategoriesNode(Element node, PlosOneRecord record) {
        Iterator<Element> subjectGroups = XMLUtility.elementIterator(node.getChildNodes());
        Iterator<Element> disciplines = null;

        HashSet<String> disciplineStrings = new HashSet<>();

        while (subjectGroups.hasNext()) {
            Element subjectGroup = subjectGroups.next();
            if (XMLUtility.hasAttributeMatchingRegex(subjectGroup, "subj-group-type", "[Dd]iscipline.*")) {
                disciplines = XMLUtility.elementIterator(subjectGroup.getChildNodes());
            }
            if (disciplines != null) {
                while (disciplines.hasNext()) {
                    Element discipline = disciplines.next();
                    if (discipline.getTagName().equals("subject")) {
                        //NOTE: "toLowerCase" because -yet again- that is not properly normalized in the plos one database as it seems so you may end up with the same category multiple times due to case-differences
                        disciplineStrings.add(discipline.getTextContent().trim().toLowerCase());
                    }
                }
            }
        }

        StringBuilder categoriesBuilder = new StringBuilder();
        for (Iterator<String> iterator = disciplineStrings.iterator(); iterator.hasNext(); ) {
            String discipline = iterator.next();
            categoriesBuilder.append(discipline);
            if (iterator.hasNext()) {
                categoriesBuilder.append("; ");
            }
        }

        record.setCategories(categoriesBuilder.toString());
        if (!record.getCategories().equals("")) {
            record.addFeature(
                    new Feature(
                            record.getCurrentFeatureId(),
                            FeatureType.CATEGORIES,
                            record.getCategories(),
                            record.getId(),
                            XMLUtility.getPosition(node, PlosOneRecord.POSITION_CUT_LEVEL)
                    )
            );
            record.setCurrentFeatureId(record.getCurrentFeatureId() + 1);
        }
    }

    private void processArticleIdNode(Element element, PlosOneRecord record){
        if (XMLUtility.hasAttributeMatchingRegex(element, "pub-id-type", "doi")) {
            record.setDoi(element.getTextContent().trim());
            if (!record.getDoi().equals("")) {
                record.addFeature(
                        new Feature(
                                record.getCurrentFeatureId(),
                                FeatureType.DOI,
                                record.getDoi(),
                                record.getId(),
                                XMLUtility.getPosition(element, PlosOneRecord.POSITION_CUT_LEVEL)
                        )
                );
                record.setCurrentFeatureId(record.getCurrentFeatureId() + 1);
            }
        }
        if (XMLUtility.hasAttributeMatchingRegex(element, "pub-id-type", "pmc")) {
            record.setOriginalId(element.getTextContent().trim());
            if (!record.getOriginalId().equals("")) {
                record.addFeature(
                        new Feature(
                                record.getCurrentFeatureId(),
                                FeatureType.ID,
                                record.getOriginalId(),
                                record.getId(),
                                XMLUtility.getPosition(element, PlosOneRecord.POSITION_CUT_LEVEL)
                        )
                );
                record.setCurrentFeatureId(record.getCurrentFeatureId() + 1);
            }
        }
    }
}

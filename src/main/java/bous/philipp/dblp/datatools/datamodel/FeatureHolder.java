package bous.philipp.dblp.datatools.datamodel;

import java.util.List;

/**
 * Signaling that the implementor has a list of {@link Feature}s that can be retrieved.
 */
public interface FeatureHolder {
    /**
     * Gets all features of a FeatureHolder as a List.
     * Any implementation should make sure that no features with a value equal to null will be included in the returned list!
     * Furthermore remark that depending on implementation/context the feature-ids
     * should be set to something meaningful (e.g. an incrementing index for a db).
     * @return a List of features
     */
    List<Feature> getFeatureList();
}

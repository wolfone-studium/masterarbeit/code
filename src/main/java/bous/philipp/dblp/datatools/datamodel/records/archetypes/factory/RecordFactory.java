package bous.philipp.dblp.datatools.datamodel.records.archetypes.factory;

import bous.philipp.dblp.datatools.datamodel.records.archetypes.RecordArchetype;
import org.w3c.dom.Document;

/**
 * Produces records from {@link Document}s.
 */
@FunctionalInterface
public interface RecordFactory {

    /**
     * Creates a {@link RecordArchetype} from a given document, based on the concrete implementation.
     * It is intended to introduce an additional layer of abstraction as documents may have to be processed
     * fundamentally different depending on the source of the xml that was used to instanciate the {@link Document}.
     * That means that the RecordFactory-implementations are the main anchors that have to know the specifics of the data-source necessary
     * to populate their corresponding RecordArchetypes with the correctly formatted data. Correctly formatted especially means
     * that the data has to be understood by the RecordArchetype's {@link RecordArchetype#toRecordDocument()}-method.
     * @param document the document to be converted
     * @param recordIdArtificial id for the new record
     * @param initialFeatureId id of the first feature; normally the method should give subsequent ids to all features added to the record archetype
     * @return a record-archetype-instance
     */
    RecordArchetype fromDocument(Document document, int recordIdArtificial, int initialFeatureId);
}

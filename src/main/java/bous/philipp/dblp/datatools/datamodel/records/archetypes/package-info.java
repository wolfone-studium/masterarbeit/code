/**
 * Contains classes/interfaces to represent documents from specific sources in-code with a minimum of a common interface.
 */
package bous.philipp.dblp.datatools.datamodel.records.archetypes;
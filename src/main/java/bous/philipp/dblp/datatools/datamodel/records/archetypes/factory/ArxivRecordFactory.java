package bous.philipp.dblp.datatools.datamodel.records.archetypes.factory;

import bous.philipp.dblp.datatools.datamodel.Feature;
import bous.philipp.dblp.datatools.datamodel.FeatureType;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.ArxivRecord;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.RecordArchetype;
import bous.philipp.dblp.datatools.util.xml.XMLUtility;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Produces {@link ArxivRecord}s from {@link Document}s.
 */
public class ArxivRecordFactory implements RecordFactory {

    @Override
    public RecordArchetype fromDocument(Document document, int recordIdArtificial, int initialFeatureId) {
        ArxivRecord arxivRecord = new ArxivRecord();
        arxivRecord.setId(recordIdArtificial);
        arxivRecord.setFeatures(new ArrayList<>());

        NodeList nodeList = document.getElementsByTagName("*");
        String tagName;
        String tagContent;

        for (int index = 0; index < nodeList.getLength(); index++) {
            Node node = nodeList.item(index);

            tagName = node.getNodeName();
            tagContent = StringUtils.trim(node.getTextContent());

            if (StringUtils.isBlank(tagContent)) {
                continue;
            }

            switch (tagName) {
                case "title":
                    arxivRecord.setTitle(tagContent);
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.TITLE,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
                case "abstract":
                    arxivRecord.setAbstractOfPublication(tagContent);
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.ABSTRACT,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
                case "authors":
                    arxivRecord.setAuthors(tagContent);
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.AUTHORS,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
                case "categories":
                    arxivRecord.setCategories(tagContent);
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.CATEGORIES,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
                case "submitter":
                    arxivRecord.setSubmitter(tagContent);
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.SUBMITTER,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
                case "datestamp":
                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    arxivRecord.setDateStamp(LocalDate.parse(nodeList.item(index).getTextContent(), dateTimeFormatter));
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.DATESTAMP,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
                case "comments":
                    arxivRecord.setComments(tagContent);
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.COMMENT,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
                case "doi":
                    arxivRecord.setDoi(tagContent);
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.DOI,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
                case "id":
                    arxivRecord.setOriginalId(tagContent);
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.ID,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
                case "journal-ref":
                    arxivRecord.setJournalRef(tagContent);
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.JOURNAL_REF,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
                case "msc-class":
                    //parsing msc-class turned out to be quite difficult due to the far from unified input
                    arxivRecord.setMscClass(tagContent);
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.MSC_CLASSIFICATION,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
                case "acm-class":
                    arxivRecord.setAcmClass(tagContent);
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.ACM_CLASSIFICATION,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
                case "proxy":
                    arxivRecord.setProxy(tagContent);
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.PROXY,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
                case "report-no":
                    arxivRecord.setReportNo(tagContent);
                    arxivRecord.addFeature(new Feature(
                            initialFeatureId++,
                            FeatureType.REPORT_NO,
                            tagContent,
                            recordIdArtificial,
                            XMLUtility.getPosition(node, ArxivRecord.POSITION_CUT_LEVEL)));
                    break;
            }
        }

        return arxivRecord;
    }
}

package bous.philipp.dblp.datatools.datamodel;

import bous.philipp.dblp.datatools.util.string.StringUtility;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Serves as a source-agnostic representation of a record.
 * It consists of an originalId that may be given to the record from
 * the original source, an "artificial" id (usually just some unique integer being incremented when inserting a
 * new record into a database-table), a "csClassificationStatus" that indicates if this record represents a
 * computer science document (1) or not (0) and a list of {@link Feature}s associated with this record.
 */
public class RecordDocument implements FeatureHolder {

    private final String[] PREFIX_SUFFIX = new String[]{"[","]."};

    private int id;
    private String originalId;
    private List<Feature> features;
    private int csClassificationStatus = 0;


    /**
     * Sorts the features of the RecordDocument-instance by feature-type-name.
     */
    void sortFeatures(){
        features.sort((feature1, feature2) -> feature1.getType().name().compareToIgnoreCase(feature2.getType().name()));
    }

    /**
     * Represents the record as a single-line-string which is basically a list of the features, wrapped by some prefix/suffix as defined in the PREFIX_SUFFIX constant.
     * E.g.: [CS_CLASS: 1].[AUTHORS: Philipp Bous].   [...].
     * @param stripClass if true, the classification-information will not be included in the resulting string-representation.
     * @param excludedFeatures features to be excluded from the string-representation
     * @param withPositions include original position information if true
     * @return a single-line-string representing this record
     */
    public String toSingleLine(boolean stripClass, EnumSet<FeatureType> excludedFeatures, boolean withPositions){
        StringBuilder recordLine = new StringBuilder();
        if (!stripClass){
            recordLine.append(StringUtility.wrap("CS_CLASS: " + csClassificationStatus, PREFIX_SUFFIX));
        }

        String currentFeature;

        for (Iterator<Feature> iterator = features.iterator(); iterator.hasNext(); ) {
            Feature feature = iterator.next();
            if (excludedFeatures == null || !excludedFeatures.contains(feature.getType())){
                if (withPositions){
                    currentFeature = feature.getType().name() + " " + feature.getValue() + " " + feature.getOriginalPosition();
                }else {
                    currentFeature = feature.getType().name() + " " + feature.getValue();
                }
                recordLine.append(StringUtility.wrap(currentFeature ,PREFIX_SUFFIX));
            }
        }
        return  recordLine.toString();
    }

    @Override
    public List<Feature> getFeatureList() {
        return this.features;
    }

    /**
     * Returns a list of only thos features that have a specific type.
     * @param type the type that is searched for
     * @return a list of only those features of the specified type
     */
    @Nullable
    public List<Feature> getFeaturesByType(FeatureType type){
        return this.features.stream()
                .filter(feature -> feature.getType() == type)
                .collect(Collectors.toList());
    }

    /**
     * Adds a feature to the RecordDocument.
     * @param feature the feature to be added
     */
    public void addFeature(Feature feature){
        if (features == null){
            this.features = new ArrayList<>();
        }
        this.features.add(feature);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("----------------------------------------RECORD--------------------------------------\n");
        stringBuilder.append(" record id:        ").append(id).append("\n");
        stringBuilder.append(" original id:      ").append(originalId).append("\n");
        stringBuilder.append(" List of features:\n");

        if (features != null){
            for (int i = 0; i < features.size(); i++) {
                Feature feature = features.get(i);
                stringBuilder.append("      ").append(feature.getType()).append(StringUtils.repeat(' ', 20 - feature.getType().name().length()));
                if (feature.getValue().length() > 45){
                    stringBuilder.append(feature.getValue(), 0, 42).append("...(> 45 chars)\n");
                }else{
                    stringBuilder.append(feature.getValue()).append("\n");
                }

            }
        }

        stringBuilder.append("------------------------------------------------------------------------------------\n\n");
        return stringBuilder.toString();
    }

    public int getCsClassificationStatus() {
        return csClassificationStatus;
    }

    public void setCsClassificationStatus(int csClassificationStatus) throws IllegalArgumentException{
        if (csClassificationStatus != -1 && csClassificationStatus != 0 && csClassificationStatus != 1){
            throw new IllegalArgumentException("Must be -1, 0 or 1.");
        }else{
            this.csClassificationStatus = csClassificationStatus;
        }
    }

    public List<Feature> getFeatures(){
        return this.features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public String getOriginalId() {
        return this.originalId;
    }

    public void setOriginalId(String originalId) {
        this.originalId = originalId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

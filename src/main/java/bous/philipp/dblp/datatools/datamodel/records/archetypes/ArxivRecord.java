package bous.philipp.dblp.datatools.datamodel.records.archetypes;

import bous.philipp.dblp.datatools.util.string.StringUtility;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a record from the arXiv-source.
 */
public class ArxivRecord extends AbstractBasicRecord {
    private LocalDate dateStamp;

    @Getter @Setter private String submitter;
    @Getter @Setter private String comments;
    @Getter @Setter private String reportNo;
    @Getter @Setter private String proxy;
    @Getter @Setter private String journalRef;
    @Getter @Setter private String doi;
    @Getter @Setter private String mscClass;
    @Getter @Setter private String acmClass;

    /**
     * Attempts to split a String containing a collection of acm-classes into a list of tokens each representing one class.
     * Due to various input-inconsistencies there cannot be a 100% correctness assumption, even if there do not exist known problems
     * with this in the context of acm-class-strings originating from arxiv-xml-files. Therefore this methods logic
     * is quite rudimentary.
     * @param acmClassification a String representing a collection of acm-classes
     * @return a list of Strings where each element is supposed to represent one category; returns null for empty or null input
     */
    //NOTE: untested
    public static List<String> splitAcmClassificationString(String acmClassification){
        if (acmClassification == null || acmClassification.equals("")){
            return null;
        }

        List<String> acmClasses = new ArrayList<>();

        acmClassification = StringUtility.removeBracketContent(acmClassification);
        String[] tokens = acmClassification.split(";|,|\\s");

        for (int i = 0; i < tokens.length; i++) {
            String token = StringUtils.trim(tokens[i]);

            if (StringUtils.isBlank(token) || !token.matches(".*\\d+.*")) {
                continue;
            }

            acmClasses.add(token);
        }
        return acmClasses;
    }

    /**
     * Attempts to split a String containing a collection of categories into a list of tokens each representing one category.
     * Due to various input-inconsistencies there cannot be a 100% correctness assumption, even if there do not exist known problems
     * with this in the context of category-strings originating from arxiv-xml-files.
     * @param categories a String representing a collection of categories
     * @return a list of Strings where each element is supposed to represent one category; returns null for empty or null input
     */
    //NOTE: untested
    public static List<String> splitCategoriesString(String categories){
        if (categories == null || categories.equals("")){
            return null;
        }

        List<String> categoriesList = new ArrayList<>();

        String[] tokens = categories.split("\\s");

        for (int i = 0; i < tokens.length; i++) {
            String token = StringUtils.trim(tokens[i]);

            if (StringUtils.isBlank(token)) {
                continue;
            }
            categoriesList.add(token);
        }

        return categoriesList;
    }


    /**
     * Attempts to split the current record's categories; uses splitCategoriesString internally.
     * @see #splitCategoriesString(String) 
     * @return a list where one Element represents one identified category-token; returns null if this record didn't have categories.
     */
    public List<String> getSplittedCategories(){
        if (this.categories != null && !this.categories.equals("")){
            return ArxivRecord.splitAuthorsString(this.categories);
        }else{
            return null;
        }
    }


    /**
     * Attempts to split a String containing a collection of authors into a list of tokens each representing one author.
     * Due to various input-inconsistencies there cannot be a 100% correctness assumption.
     * @param authors a String representing a colleciton of authors
     * @return a list of Strings where each element is supposed to represent one author; returns null for empty or null input
     */
    //NOTE: untested in this version; its necessity was not prominent any longer at a given point but it should be stated that "correctly" splitting the author strings as contained in arXic-records is a nightmare to say the least
    public static List<String> splitAuthorsString(String authors){
        String tempAuthors;
        if (authors == null || authors.equals("")){
            return null;
        }

        List<String> authorList = new ArrayList<>();

        tempAuthors = StringUtility.removeBracketContent(authors);

        String[] tokens = tempAuthors.split(",|\\sand\\s|&");

        for (int i = 0; i < tokens.length; i++) {
            String token = StringUtils.trim(tokens[i]);

            if (StringUtils.isBlank(token)) {
                continue;
            }
            authorList.add(token);
        }

        return authorList;
    }

    /**
     * Attempts to split the current record's authors; uses splitAuthorsString internally.
     * @return a list where one Element represents one identified author-token; returns null if this record didn't have authors.
     * @see #splitAuthorsString(String)
     */
    public List<String> getSplittedAuthors(){
        if (this.authors != null && !this.authors.equals("")){
            return ArxivRecord.splitAuthorsString(this.authors);
        }else{
            return null;
        }
    }


    /**
     * Attempts to split a String that represents a collection of msc-classes into tokens and returns those in a list.
     * Remark that there exist edge-cases that can not be uniquely resolved. The current implementation takes only
     * the identifiable main-classification and throws out the rest of the input String.
     *
     * @param mscClassification the msc-class String to be split
     * @return a list where one Element represents one identified class-token; returns null for empty or null input
     */
    //NOTE: untested
    public static List<String> splitMscClassString(String mscClassification) {
        if (mscClassification == null || mscClassification.equals("")){
            return  null;
        }
        List<String> mscClasses = new ArrayList<>();

        //parsing msc-class turned out to be quite difficult due to the far from unified input
        mscClassification = StringUtility.removeBracketContent(mscClassification);
        String[] tokens = mscClassification.split(";|,|\\s");

        for (int i = 0; i < tokens.length; i++) {
            String token = StringUtils.trim(tokens[i]);
            token = token.replaceAll("\\.", "");


            if (StringUtils.isBlank(token) || (!token.matches(".*\\d+.*") && token.length() > 1) || !token.matches("[0-9](.*)")) {
                continue;
            }

            //user input malformed with whitespaces; e.g. 34 A 43 45 -> only 34A can be safely parsed 43 can not be decided if single class or suffix of 34A;
            //in this case take what can be safely parsed and throw away the rest
            if (token.length() == 2 && token.matches("\\d\\d") && tokens.length > i + 1 && tokens[i + 1].length() == 1 && !tokens[i + 1].matches("\\d")) {
                token = token + tokens[i + 1];
                mscClasses.add(token);
                break;
            }

            mscClasses.add(token);
        }


        return mscClasses;
    }

    /**
     * Attempts to split the current record's msc-class; uses splitMscClassString internally
     * @return a list where one Element represents one identified class-token; returns null if this record didn't have a msc-classification.
     * @see #splitMscClassString(String)
     */
    public List<String> getSplittedMscClass(){
        if (this.mscClass != null && !this.mscClass.equals("")){
            return ArxivRecord.splitMscClassString(this.mscClass);
        }else{
            return null;
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("-------------------RECORD START----------------\n");
        stringBuilder.append("Artificial Id:  ").append(id).append("\n\n");
        stringBuilder.append("Categories:     ").append(categories).append("\n");
        stringBuilder.append("Title:          ").append(title).append("\n");
        stringBuilder.append("Abstract:       ").append(abstractOfPublication).append("\n");
        stringBuilder.append("Authors:        ").append(authors).append("\n");
        stringBuilder.append("Id:             ").append(originalId).append("\n");
        stringBuilder.append("Submitter:      ").append(submitter).append("\n");
        stringBuilder.append("Date-stamp:      ").append(dateStamp).append("\n\n");

        if (comments != null) {
            stringBuilder.append("Comments:       ").append(comments).append("\n");
        }
        if (reportNo != null) {
            stringBuilder.append("Report-No:      ").append(reportNo).append("\n");
        }
        if (proxy != null) {
            stringBuilder.append("Proxy:          ").append(proxy).append("\n");
        }
        if (journalRef != null) {
            stringBuilder.append("Journal-Ref.:   ").append(journalRef).append("\n");
        }
        if (doi != null) {
            stringBuilder.append("Doi:            ").append(doi).append("\n");
        }
        if (mscClass != null) {
            stringBuilder.append("MSC-Class:      ").append(mscClass).append("\n");
        }
        if (acmClass != null) {
            stringBuilder.append("ACM-Class:      ").append(acmClass).append("\n");
        }
        stringBuilder.append("-------------------RECORD END------------------");
        stringBuilder.append("\n\n");

        return stringBuilder.toString();
    }

    /**
     * Tries to extract the primary categories from a category-string as it is
     * contained in an arXiv-xml's categories-tag.
     * @param categoryString the tag-content to be processed
     * @return the categories in a set of Strings
     */
    public static Set<String> extractPrimaryCategories(String categoryString) {
        if (categoryString == null || categoryString.equals("")){
            return null;
        }

        HashSet<String> categories = new HashSet<>();
        String[] tokens = categoryString.split("\\s");
        String[] tokenParts;

        for (int i = 0; i < tokens.length; i++) {
            String token = tokens[i];
            tokenParts = token.split("\\.");
            if (isPhysics(tokenParts[0])) {
                categories.add("physics");
            } else {
                categories.add(tokenParts[0]);
            }
        }

        return categories;
    }

    /**
     * Returns a set of primary categories for this record.
     * Based on: http://arxitics.com/help/categories
     * @return the primary categories of this record
     */
    public Set<String> getPrimaryCategories() {
        return extractPrimaryCategories(this.categories);
    }

    /**
     * Convenience-method to check if a category string is part of the physics-category.
     * Based on: http://arxitics.com/help/categories
     * @param category the category
     * @return true if it is physics, false otherwise
     */
    public static boolean isPhysics(String category) {
        boolean isPhysics = false;
        switch (category) {
            case "physics":
            case "quant-ph":
            case "cond-mat":
            case "gr-qc":
            case "astro-ph":
            case "hep-ex":
            case "hep-lat":
            case "hep-ph":
            case "hep-th":
            case "math-ph":
            case "nlin":
            case "nucl-ex":
            case "nucl-th":
                isPhysics = true;
                break;
        }
        return isPhysics;
    }

    public LocalDate getDateStamp() {
        return dateStamp;
    }

    public void setDateStamp(LocalDate dateStamp) {
        this.dateStamp = dateStamp;
    }

    public void setDateStamp(String YMDString) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        this.setDateStamp(LocalDate.parse(YMDString, dateTimeFormatter));
    }
}

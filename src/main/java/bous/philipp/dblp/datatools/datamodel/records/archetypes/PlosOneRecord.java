package bous.philipp.dblp.datatools.datamodel.records.archetypes;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
/**
 * Represents a record from the PLoS-One-source.
 */
public class PlosOneRecord extends AbstractBasicRecord {
    @Getter @Setter private LocalDate dateStamp;
    @Getter @Setter private String comments;

    @Getter @Setter private String journalRef;
    @Getter @Setter private String doi;

    @Getter @Setter private int currentFeatureId;

    /**
     * This standard constructor will initialize all String fields to empty Strings.
     */
    public PlosOneRecord() {
       this.categories = "";
       this.title = "";
       this.abstractOfPublication = "";
       this.authors = "";
       this.originalId = "";
       this.comments = "";
       this.journalRef = "";
       this.doi = "";
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("-------------------RECORD START----------------\n");
        stringBuilder.append("Artificial Id:  ").append(id).append("\n\n");
        stringBuilder.append("Categories:     ").append(categories).append("\n");
        stringBuilder.append("Title:          ").append(title).append("\n");
        stringBuilder.append("Abstract:       ").append(abstractOfPublication).append("\n");
        stringBuilder.append("Authors:        ").append(authors).append("\n");
        stringBuilder.append("Id:             ").append(originalId).append("\n");
        stringBuilder.append("Date-Stamp:      ").append(dateStamp).append("\n\n");

        if (comments != null) {
            stringBuilder.append("Comments:       ").append(comments).append("\n");
        }
        if (journalRef != null) {
            stringBuilder.append("Journal-Ref.:   ").append(journalRef).append("\n");
        }
        if (doi != null) {
            stringBuilder.append("Doi:            ").append(doi).append("\n");
        }
        stringBuilder.append("-------------------RECORD END------------------");
        stringBuilder.append("\n\n");

        return stringBuilder.toString();
    }
}

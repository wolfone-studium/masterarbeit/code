package bous.philipp.dblp.datatools.datamodel.records.archetypes;

import bous.philipp.dblp.datatools.datamodel.RecordDocument;

/**
 * Functional interface signalling that the implementing class can be used
 * to produce a meaningful instance of {@link RecordDocument}.
 */
@FunctionalInterface
public interface ConvertableToRecDoc {
    /**
     * Converts "this" to an instance of {@link RecordDocument}
     * @return a RecordDocument-instance
     */
    RecordDocument toRecordDocument();
}

package bous.philipp.dblp.datatools.datamodel;

/**
 * Convenience-Enum primarily used when selecting records from a database to make the user-code somewhat more readable.
 */
public enum CsClassification {
    UNKNOWN,
    CS_ONLY,
    NON_CS_ONLY,
    CS_AND_NON_CS,
    ALL
}

package bous.philipp.dblp.datatools.datamodel;

/**
 * Lists the types of features of records that may be processed by this library.
 */
public enum FeatureType {
    TITLE,
    ABSTRACT,
    CATEGORIES,
    AUTHORS,
    ID,
    SUBMITTER,
    DATESTAMP,
    COMMENT,
    REPORT_NO,
    PROXY,
    JOURNAL_REF,
    DOI,
    MSC_CLASSIFICATION,
    ACM_CLASSIFICATION,
    RESOURCE_TYPE
}

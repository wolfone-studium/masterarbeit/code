package bous.philipp.dblp.datatools.datamodel.records.archetypes;

import bous.philipp.dblp.datatools.datamodel.FeatureHolder;

/**
 * Bundles main things a Record should be able to do.
 */
public interface RecordArchetype extends FeatureHolder, ConvertableToRecDoc{
}

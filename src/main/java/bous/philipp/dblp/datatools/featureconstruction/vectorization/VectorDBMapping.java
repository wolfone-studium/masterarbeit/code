package bous.philipp.dblp.datatools.featureconstruction.vectorization;

import java.io.*;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Represents a mapping of vectors in a file to entries in a table in the database.
 * Usually the index should correspond to the line of the vector in the file where each line represents
 * one vector.
 * REMARK: counting of vector indices should start with 1!
 * Allows advanced debugging and inspection.
 * A single vector-mapping is build as follows: VECTOR_INDEX:::TABLE_NAME_IN_DATABASE:::RECORD_ID_IN_DATABASE .
 */
public class VectorDBMapping {
    private Map<Integer, String> mappings;
    private final String forFile;

    /**
     * Returns this.mappingFile.
     * @return this.mappingFile
     */
    public File getMappingFile() {
        return this.mappingFile;
    }

    private final File mappingFile;

    /**
     * Creates a mapping-instance.
     * @param forFile the file for which this mapping is supposed to be
     * @param outputPath destination of the mapping file (including file name)
     */
    public VectorDBMapping(String forFile, Path outputPath) {
        this.mappingFile = outputPath.toFile();
        this.forFile = forFile;
        this.mappings = new HashMap<>();
    }

    /**
     * Returns the mapping for the vector of given index.
     * @param vectorIndex index of the vector
     * @return the mapping
     */
    public String get(int vectorIndex){
        return mappings.get(vectorIndex);
    }

    /**
     * Adds a mapping for a single vector to this instance.
     * @param vectorIndex index of the vector
     * @param recordTableName name of the table from which the vector was derived
     * @param recordId id of the record in the specified table that the vector for the given vectorIndex corresponds to
     */
    public void add(int vectorIndex, String recordTableName, String recordId){
        mappings.put(vectorIndex, recordTableName + ":::" + recordId);
    }

    /**
     * Writes this VectorDBMapping to the file given at time of creation of this instance.
     */
    public void write(){
        try(FileWriter fileWriter = new FileWriter(this.mappingFile); BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
            bufferedWriter.write("mappingFileFor: " + forFile + "\n");
            Iterator<Map.Entry<Integer, String>> iterator = this.mappings.entrySet().iterator();
            while(iterator.hasNext()) {
                Map.Entry<Integer, String> entry = iterator.next();
                bufferedWriter.write(entry.getKey() + ":::" + entry.getValue() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Rebuilds a VectorDBMapping instance from a file written by an instance of this class.
     * @param fileLocation absolute path to the mapping-file
     * @return a mapping-instance
     */
    public static VectorDBMapping fromFile(Path fileLocation){
        File file = fileLocation.toFile();
        try(FileReader fileReader = new FileReader(file); BufferedReader bufferedReader = new BufferedReader(fileReader)){
            String line = bufferedReader.readLine();
            String[] lineTokens = line.split("\\s");
            VectorDBMapping vDBmapping = new VectorDBMapping(lineTokens[1], fileLocation);
            for (line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine() ){
                lineTokens = line.split(":::");
                vDBmapping.add(Integer.parseInt(lineTokens[0]), lineTokens[1], lineTokens[2]);
            }
            return vDBmapping;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}

package bous.philipp.dblp.datatools.featureconstruction.vocabulary;

import bous.philipp.dblp.datatools.datamodel.CsClassification;
import bous.philipp.dblp.datatools.db.reader.DatabaseReader;
import bous.philipp.dblp.datatools.datamodel.Feature;
import bous.philipp.dblp.datatools.datamodel.FeatureType;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;
import bous.philipp.dblp.datatools.tokenization.LuceneStandardTokenizerAdapter;
import bous.philipp.dblp.datatools.util.string.StringUtility;
import bous.philipp.dblp.datatools.tokenization.Tokenizer;
import bous.philipp.dblp.datatools.util.database.DataSourceFactory;
import bous.philipp.dblp.datatools.util.logging.LoggerFactory;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.EnumSet;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Convenience class holding methods to build vocabularies.
 * Uses a {@link LuceneStandardTokenizerAdapter} for text-tokenization.
 */
public class VocabularyBuilder {

    private static Tokenizer tokenizer = new LuceneStandardTokenizerAdapter();

    /**
     * Builds a basic Vocabulary based on configuration in a java-properties-file; this version tries to draw a balanced set.
     * The properties-file should contain following information:
     * <ul>
     *     <li>METHOD_VERSION_ID - expected id of the this methods version (given as final string in the code)</li>
     *     <li>dbPropertiesFilePath - path properties-file with information about the jdbc database connection (only mysql supported atm)</li>
     *     <li>recordTableName - name of the record-table to be used</li>
     *     <li>featureTableName - name of the corresponding feature table</li>
     *     <li>featureType - feature to be extracted, allowed values are specified through the enum of the same name</li>
     *     <li>numberOfItemsPerClass - number of items that are attempted to be drawn from each class</li>
     *     <li>minWordLength - tokens under this length will be discarded</li>
     *     <li>windowSize - size for window in ngram-creation 1 or lower will just use unigrams</li>
     *     <li>additiveVocab - true or false; true will add unigrams AND ngrams to the vocab; false will only keep the ngrams</li>
     *     <li>countWords - may be true or false; true produces bow-vectors where the number of occurrences of a word is counted</li>
     *     <li>applyLowerCasing - true or false; if true lower-casing is applied to the data from the database before vectorization</li>
     *     <li>removeBracketContent - true or false; if true brackets and content are removed before vectorization</li>
     *     <li>removeStopwords - true or false; enables or disables stop-word-removal (english only atm) before vectorization</li>
     *     <li>applyStemming - true or false; true applies stemming (english only atm) before vectorization</li>
     *     <li>fileOutputPath - destination of the generated vocabulary-file</li>
     *     <li>additionalSteps - (optional) allows to specify usage of vocabulary methods by reflection; this has to be done in postfix-notation with separation by ";" for single method-calls; if you use methods based on fisher score make sure to use these last; this will furthermore trigger the output to be sorted by fisher scores</li>
     *     <li>byRID - (optional) if true, entries will be drawn according to ordering by values in column "random_id" otherwise they will be drawn by column "id"</li>
     * </ul>
     */
    public static void buildAndWriteScoredVocabulariesBalancedWithConfigFile() {
        Logger logger = LoggerFactory.getLogger(VocabularyBuilder.class.getName());
        ScoredVocabulary vocabulary;

        Path configPath = askForConfigPath();
        String propFileNameWithoutExtension = FilenameUtils.getBaseName(configPath.toString());
        VocabularyBuildingProperties props = setUpVocabularyBuildingProperties(configPath);
        copyConfigToVocabDestination(configPath, props);

        logger.info("number of folds: " + props.folds);
        if (props.folds <= 1) {
            vocabulary = buildScoredVocabularyBalanced(props);
            vocabulary.setId(propFileNameWithoutExtension);

            String destination = props.fileOutputPath + "\\" + propFileNameWithoutExtension + ".fbowvoc";
            if (vocabulary.termIndicesSortedByFisherScore != null) {
                vocabulary.writeToFileSortedByLastKnownFisherScore(destination);
            } else {
                vocabulary.writeToFile(destination);
            }
            logger.info("Finished writing vocabulary.");
        }else{
            buildAndWriteSetOfVocabulariesForCVBalanced(props, propFileNameWithoutExtension);
        }
    }

    private static void buildAndWriteSetOfVocabulariesForCVBalanced(VocabularyBuildingProperties props, String vocabFilenamePrefix) {
        final String METHOD_VERSION_ID = "bawsovfcb-05.06.2019-1";
        props.checkValidity(METHOD_VERSION_ID);
        Logger logger = LoggerFactory.getLogger(VocabularyBuilder.class.getName());
        int foldSize = props.numberOfItemsPerClass / props.folds;
        Connection connection = getConnectionFromBuildingProperties(props);
        final String recordId = props.recordTableName + ".id";
        final String csClass = props.recordTableName + ".cs_classification";
        final int lastIdPositives = getNextId(props.numberOfItemsPerClass-1, connection, CsClassification.CS_ONLY, props);
        int startIdLeaveOutPositives;
        int lastIdLeaveOutPositives;

        final int lastIdNegatives = getNextId(props.numberOfItemsPerClass-1, connection, CsClassification.NON_CS_ONLY, props);
        int startIdLeaveOutNegatives;
        int lastIdLeaveOutNegatives;

        EnumSet<FeatureType> excludedFeatures = EnumSet.complementOf(EnumSet.of(props.featureType));
        for (int i = 0; i < props.folds; i++) {
            startIdLeaveOutPositives = getNextId(foldSize*i, connection, CsClassification.CS_ONLY, props);
            lastIdLeaveOutPositives = getNextId(foldSize*(i+1)-1, connection, CsClassification.CS_ONLY, props);
            startIdLeaveOutNegatives = getNextId(foldSize*i, connection, CsClassification.NON_CS_ONLY, props);
            lastIdLeaveOutNegatives = getNextId(foldSize*(i+1)-1, connection, CsClassification.NON_CS_ONLY, props);

            List<RecordDocument> positives = DatabaseReader.selectRecordsFromDB(
                    connection,
                    excludedFeatures,
                    props.recordTableName,
                    props.featureTableName,
                    "WHERE " + csClass + "= 1 and ((" + recordId + "<" + startIdLeaveOutPositives + " or " + recordId + ">" + lastIdLeaveOutPositives + ") and " + recordId + " <= " + lastIdPositives + ")",
                    "",
                    props.isByRID()
            ).getRecords();

            List<RecordDocument> negatives = DatabaseReader.selectRecordsFromDB(
                    connection,
                    excludedFeatures,
                    props.recordTableName,
                    props.featureTableName,
                    "WHERE " + csClass + "= -1 and ((" + recordId + "<" + startIdLeaveOutNegatives + " or " + recordId + ">" + lastIdLeaveOutNegatives + ") and " + recordId + " <= " + lastIdNegatives + ")",
                    "",
                    props.isByRID()
            ).getRecords();

            ScoredVocabulary vocabulary = buildScoredVocabulary(positives, negatives, props);
            vocabulary.setId(vocabFilenamePrefix + "(" + i + ")");
            vocabulary.setCreatedBy(METHOD_VERSION_ID);

            logger.info("Start writing: " + vocabulary.getId());
            if (vocabulary.termIndicesSortedByFisherScore != null) {
                vocabulary.writeToFileSortedByLastKnownFisherScore(props.fileOutputPath + "\\" + vocabFilenamePrefix + "(" + i + ")" + ".fbowvoc");
            } else {
                vocabulary.writeToFile(props.fileOutputPath + "\\" + vocabFilenamePrefix + "(" + i + ")" + ".fbowvoc");
            }
            logger.info("Finished writing: " + vocabulary.getId());
        }
    }

    private static ScoredVocabulary buildScoredVocabulary(
            List<RecordDocument> positiveRecords,
            List<RecordDocument> negativeRecords,
            VocabularyBuildingProperties buildingProperties
    ) {
        Logger logger = LoggerFactory.getLogger(VocabularyBuilder.class.getName());

        ScoredVocabulary vocabulary = new ScoredVocabulary();
        String currentFeatureValue;

        logger.info("Start processing non-cs records");
        for (int i = 0; i < negativeRecords.size(); i++) {
            vocabulary.newDocument(-1);
            RecordDocument recordDocument = negativeRecords.get(i);
            List<Feature> features = recordDocument.getFeatureList();
            currentFeatureValue = features.get(0).getValue();
            if (buildingProperties.removeBracketContent) {
                currentFeatureValue = StringUtility.removeBracketContent(currentFeatureValue);
            }
            List<String> tokens = tokenizer.tokenize(currentFeatureValue);
            List<String> processedTokens = StringUtility.processTokens(tokens, buildingProperties.applyLowerCasing, buildingProperties.removeStopwords, buildingProperties.applyStemming);
            processedTokens = StringUtility.removeTokensUnderMinimumLength(processedTokens, buildingProperties.minWordLength);

            String[] tokensToAdd = processedTokens.toArray(new String[0]);
            vocabulary.addAll(tokensToAdd, buildingProperties.windowSize, buildingProperties.additiveVocab);
        }
        logger.info("Finished processing non-cs records.");

        negativeRecords = null;

        logger.info("Started processing cs-records.");
        for (int i = 0; i < positiveRecords.size(); i++) {
            vocabulary.newDocument(1);
            RecordDocument recordDocument = positiveRecords.get(i);
            List<Feature> features = recordDocument.getFeatureList();

            currentFeatureValue = features.get(0).getValue();
            if (buildingProperties.removeBracketContent) {
                currentFeatureValue = StringUtility.removeBracketContent(currentFeatureValue);
            }

            List<String> tokens = tokenizer.tokenize(currentFeatureValue);
            List<String> processedTokens = StringUtility.processTokens(tokens, buildingProperties.applyLowerCasing, buildingProperties.removeStopwords, buildingProperties.applyStemming);
            processedTokens = StringUtility.removeTokensUnderMinimumLength(processedTokens, buildingProperties.minWordLength);

            String[] tokensToAdd = processedTokens.toArray(new String[0]);
            vocabulary.addAll(tokensToAdd, buildingProperties.windowSize, buildingProperties.additiveVocab);
        }

        positiveRecords = null;

        logger.info("Vocabulary-size: " + vocabulary.size());
        System.out.println(buildingProperties.additionalSteps);
        if (buildingProperties.additionalSteps != null && !buildingProperties.additionalSteps.isBlank()) {
            logger.info("Start applying additional steps:");
            vocabulary.applyAdditionalSteps(buildingProperties.additionalSteps);
            logger.info("Finished applying additional steps.");
            logger.info("Steps performed:");
            System.out.println(vocabulary.getTransformationLog());
            logger.info("Vocabulary-size: " + vocabulary.size());
        }
        return vocabulary;
    }

    /**
     * Builds a ScoredVocabulary based an an artificially balanced set of
     * records from a database and a given configuration in form of
     * a {@link VocabularyBuildingProperties}-instance.
     * @param props the properties to configure the building process.
     * @return the vocabulary
     */
    public static ScoredVocabulary buildScoredVocabularyBalanced(VocabularyBuildingProperties props) {
        final String METHOD_VERSION_ID = "bbsv-05.06.2019-1";

        Logger logger = LoggerFactory.getLogger(VocabularyBuilder.class.getName());

        props.checkValidity(METHOD_VERSION_ID);

        Connection connection = getConnectionFromBuildingProperties(props);

        ScoredVocabulary vocabulary = new ScoredVocabulary();
        vocabulary.setCreatedBy(METHOD_VERSION_ID);

        logger.info("Start reading non-cs records.");
        EnumSet<FeatureType> excludedFeatures = EnumSet.complementOf(EnumSet.of(props.featureType));
        List<RecordDocument> recordsNonCs = DatabaseReader.selectRecordsFromDB(connection,
                excludedFeatures,
                props.recordTableName,
                props.featureTableName,
                props.startingRecordIdNonCs,
                props.numberOfItemsPerClass,
                CsClassification.NON_CS_ONLY,
                props.isByRID()).getRecords();
        logger.info("Finished reading non-cs records.");

        logger.info("Started reading cs-records.");
        List<RecordDocument> recordsCs = DatabaseReader.selectRecordsFromDB(connection,
                excludedFeatures,
                props.recordTableName,
                props.featureTableName,
                props.startingRecordIdCs,
                props.numberOfItemsPerClass,
                CsClassification.CS_ONLY,
                props.isByRID()).getRecords();
        logger.info("Finished reading cs-records.");

        return buildScoredVocabulary(recordsCs, recordsNonCs, props);
    }

    private static int getNextId(int numberToSkip, Connection connection, CsClassification csClassification, VocabularyBuildingProperties buildingProperties) {
        int nextId = -1;
        PreparedStatement st;
        int classification;
        if (csClassification.equals(CsClassification.CS_ONLY)) {
            classification = 1;
        } else if (csClassification.equals(CsClassification.NON_CS_ONLY)) {
            classification = -1;
        } else {
            throw new IllegalArgumentException("CS_ONLY and NON_CS_ONLY are the only permitted input values for csClassification");
        }

        try {
            st = connection.prepareStatement("select id from " + buildingProperties.getRecordTableName() + " where cs_classification=" + classification + " ORDER BY id ASC LIMIT " + numberToSkip + ",1");
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                nextId = rs.getInt(1);
            }else {
                st = connection.prepareStatement("select max(id) from " + buildingProperties.getRecordTableName() + " where cs_classification=" + classification + " ORDER BY id ASC");
                rs = st.executeQuery();
                rs.next();
                nextId = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException("Couldn't determine valid nextId for DB-access");
        }
        return nextId;
    }

    @Nullable
    private static Connection getConnectionFromBuildingProperties(VocabularyBuildingProperties buildingProperties) {
        MysqlDataSource dataSource = DataSourceFactory.getMySQLDataSource(buildingProperties.dbPropertiesFilePath);
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
        } catch (
                SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private static Path askForConfigPath() {
        Scanner scanner = new Scanner(System.in);

        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current path is: " + s);
        System.out.println("Enter vocabulary-building configuration location: ");
        return Paths.get(scanner.next());
    }

    private static void copyConfigToVocabDestination(Path configPath, VocabularyBuildingProperties props) {
        Path destination = Paths.get(props.fileOutputPath);
        if (!destination.equals(configPath)) {
            try {
                Files.copy(configPath, Paths.get(destination.toString() + "\\" + FilenameUtils.getName(configPath.toString())), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @NotNull
    private static VocabularyBuildingProperties setUpVocabularyBuildingProperties(Path configPath) {
        VocabularyBuildingProperties props = new VocabularyBuildingProperties();
        props.readProperties(configPath);
        return props;
    }

}

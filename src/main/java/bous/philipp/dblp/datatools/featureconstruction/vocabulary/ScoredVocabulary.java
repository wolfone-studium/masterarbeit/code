package bous.philipp.dblp.datatools.featureconstruction.vocabulary;

import bous.philipp.dblp.datatools.featurescoring.FisherScoreSimple;
import bous.philipp.dblp.datatools.util.string.StringUtility;
import bous.philipp.dblp.datatools.util.logging.LoggerFactory;
import bous.philipp.dblp.datatools.util.math.Statistics;
import com.google.common.primitives.Doubles;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;
import java.util.logging.Logger;

/**
 * Represents a vocabulary as usable in ML-classification-scenarios where text is vectorized based on
 * methods like Bag of Words. It provides extensive means to manipulate a vocabulary
 * once it is build, like cutting of parts of the vocabulary based on their fisher score.
 * For such manipulating functions that rely on statistics for every single term in the vocabulary it is
 * necessary for the user to tell the vocabulary when documents change and which class the new document has
 * during the "vocabulary-training-process" by calling {@link #newDocument(int)}.
 */
public class ScoredVocabulary implements Iterable<TermStatistics>{
    private static final Logger logger = LoggerFactory.getLogger(ScoredVocabulary.class.getName());
    private final StringBuilder transformationLog;
    private VocabularyBuildingProperties vocabProps;
    /**
     * Used to reduce the vocabulary by percentage constraints with respect to a fixed vocabulary-size (IMPORTANT:
     * supposed to count multiple occurrences! That means that calling {@link #fixatePercentageReference()} will set
     * this value to the current {@link #numberOfAdditions}) in contrary to the effect that occurs when for example
     * chaining {@link #keepOnlyBySmallerThanAbsoluteFrequency}
     * several times where each time the current {@link #numberOfAdditions} is used to calculate the percentages,
     * which changes with every chained call.
     */
    private int fixedSize;
    private String createdBy;
    private String id;
    private LinkedHashMap<String, TermStatistics> termStringToInfoMap;
    private LinkedHashMap<Integer, TermStatistics> termIndexToInfoMap;

    private int seenDocuments = 0;
    private int currentDocClass;
    private List<Integer> knownClasses;
    private LinkedHashMap<Integer, Integer> numberOfDocumentsPerClass;

    List<ImmutablePair<String, Double>> termIndicesSortedByFisherScore;


    /**
     * Builds a ScoredVocabulary and initializes this.createdBy to "UNKOWN"
     * and this.id to "NO-ID-EXISTENT".
     */
    public ScoredVocabulary(){
        this.knownClasses = new ArrayList<>();
        this.termIndexToInfoMap = new LinkedHashMap<>();
        this.termStringToInfoMap = new LinkedHashMap<>();
        this.numberOfDocumentsPerClass = new LinkedHashMap<>();
        this.transformationLog = new StringBuilder();
        this.createdBy = "UNKNOWN";
        this.id = "NO-ID-EXISTENT";
        this.fixedSize = 0;
        this.vocabProps = new VocabularyBuildingProperties();
    }

    /**
     * Will build a ScoredVocabulary from a *.fbowvoc file.
     * As those do not contain any class or document-information
     * this method will act as if all vocabs belong to a single document of class
     * -10. Most importantly that means that truncation based on statistics
     * of terms and documents will not work correctly (e.g. cutting by fisher-scores).
     * @param path absolute path to the vocabulary file
     * @return the vocabulary
     */
    public static ScoredVocabulary fromFile(String path){
        ScoredVocabulary vocabulary = new ScoredVocabulary();
        vocabulary.newDocument(-10);
        try (FileReader fileReader = new FileReader(path); BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line = bufferedReader.readLine();
            if (line.startsWith("fvoc:")) {
                String[] firstLineTokens = line.split(" ");
                if (firstLineTokens.length == 3){
                    vocabulary.setId(firstLineTokens[2]);
                }
                line = bufferedReader.readLine();
                int vocabIndex = 0;
                while(line != null){
                    vocabulary.add(line);
                    vocabIndex++;
                    line = bufferedReader.readLine();
                }
            }else{
                throw new IOException("Illegal file format!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return vocabulary;
    }

    /**
     * Can be used to fill the VocabularyProperties associated with this instance from a properties file as it is used
     * by the methods in {@link VocabularyBuilder}. This allows for some
     * tasks to be programmed more conveniently.
     * @param configPath path to the location of the config
     * @see VocabularyBuildingProperties#readProperties(Path)
     */
    public void readProperties(Path configPath){
        this.vocabProps.readProperties(configPath);
    }

    /**
     * Signals that all following terms belong to a new document.
     * @param docClass class of the new document
     */
    public void newDocument(int docClass){
        if (!this.knownClasses.contains(docClass)) {
            this.knownClasses.add(docClass);
            this.numberOfDocumentsPerClass.put(docClass, 1);
        }else {
            this.numberOfDocumentsPerClass.put(docClass, this.numberOfDocumentsPerClass.get(docClass) + 1);
        }
        this.currentDocClass = docClass;
        this.seenDocuments++;
    }

    public LinkedHashMap<String, TermStatistics> getTermStringToInfoMap() {
        return termStringToInfoMap;
    }

    public LinkedHashMap<Integer, TermStatistics> getTermIndexToInfoMap() {
        return termIndexToInfoMap;
    }

    /**
     * Removes a given term from the vocabulary.
     * @param term term to be removed
     */
    public void remove(String term){
        termIndexToInfoMap.remove(termStringToInfoMap.get(term).index);
        termStringToInfoMap.remove(term);
    }

    /**
     * Removes a term from the vocabulary by a given index.
     * @param index index of term to be removed
     */
    public void remove(int index){
        termStringToInfoMap.remove(termIndexToInfoMap.get(index).term);
        termIndexToInfoMap.remove(index);
    }

    /**
     * Adds a term to the vocabulary. This method will do all necessary statistics updates
     * and existence check-ups so the user does not have to worry about these making this
     * the preferred method to add terms to the vocabulary.
     * @param term term to be added
     */
    public void add(String term){
        if (this.seenDocuments == 0){
            throw new IllegalStateException("newDocument(int) has never been called; should be called at least once!");
        }
        TermStatistics termStats = termStringToInfoMap.get(term);
        if (termStats == null) {
            int index = termStringToInfoMap.size() + 1;
            termStats = new TermStatistics(term, index, seenDocuments, currentDocClass);
            termStringToInfoMap.put(term, termStats);
            termIndexToInfoMap.put(index, termStats);
        }else{
            termStats.addSighting(seenDocuments, currentDocClass);
        }
    }

    /**
     * Adds all given tokens to the vocabulary and if windowSize is greater than 1 will calculate n-grams and add those to the
     * vocabulary. It can be specified if only the n-grams should be added or if the underlying unigrams should also be included.
     * Remark that this does NOT add all *possible* n-grams that can be build with the single tokens but merely those
     * that really occur as given by the inherent ordering of the input-array!
     * @param tokens the tokens to be added to the vocabulary, it is assumed that they are given in their respective original order
     * @param windowSize if it's set to values greater than 1 [windowSize]-grams will be added to the vocabulary
     * @param additive if true, n-grans AND unigrams will be added, if false, only n-grams will be added
     */
    public void addAll(String[] tokens, int windowSize, boolean additive) {

        if (additive || (windowSize < 2)) {
            for (int i = 0; i < tokens.length; i++) {
                String token = tokens[i];
                this.add(token);
            }
            if (windowSize < 2){
                return;
            }
        }

        if(windowSize > 1){
            String[] nGrams = StringUtility.convertToNGrams(tokens, windowSize, " ");
            for (int i = 0; i < nGrams.length; i++) {
                String nGram = nGrams[i];
                this.add(nGram);
            }
        }
    }

    /**
     * Variation on {@link #addAll(String[], int, boolean)} that works on a single string but needs a regex, describing
     * the separators to use to split the given input into tokens.
     * @param input see base method
     * @param windowSize see base method
     * @param separatorRegex is used to calculate tokens as input.split(separatoregex)
     * @param additive see base method
     */
    public void addAll(String input, int windowSize, String separatorRegex, boolean additive){
        String[] tokens = input.split(separatorRegex);
        this.addAll(tokens, windowSize, additive);
    }

    /**
     * Reindexes the vocabulary.
     */
    public void reindex(){
        this.transformationLog.append("applied: reindex\n");
        int counter = 1;
        this.termIndexToInfoMap = new LinkedHashMap<>();
        for (TermStatistics termStatistics : this) {
            termStatistics.index = counter;
            this.termIndexToInfoMap.put(counter, termStatistics);
            counter++;
        }
    }

    /**
     * Sets {@link #fixedSize} to the current value of {@link #numberOfAdditions()}.
     * @return the modified Vocabulary instance itself to allow chaining of certain modification-methods.
     */
    public ScoredVocabulary fixatePercentageReference(){
        this.transformationLog.append("applied: fixatePercentageReference\n");
        this.fixedSize = numberOfAdditions();
        return this;
    }

    /**
     * Writes a text-representation of the vocabulary to a file at the given path.
     * The format is as follows:
     * <pre>
     * [line 0]fvoc: METHOD_ID this.id
     * [line 1]key
     * [line 2]key
     * ...
     * </pre>
     *
     * Example:
     * <pre>
     * fvoc: my_method_id my_vocabulary_id
     * do
     * not
     * go
     * gentle
     * into
     * that
     * good
     * night
     * </pre>
     * @param path the path where the file should be saved
     */
    public void writeToFile(String path){
        try (FileWriter fileWriter = new FileWriter(path); BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            Iterator<TermStatistics> iterator = this.iterator();
            writeVocabFileHeader(bufferedWriter);
            while(iterator.hasNext()) {
                TermStatistics termStatistics = iterator.next();
                bufferedWriter.write(termStatistics.term);
                if (iterator.hasNext()){
                    bufferedWriter.write("\n");
                }
            }
            logger.info("Wrote vocabulary at: " + path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the vocabulary's entries as a list.
     * @return the entries as sorted list
     */
    public List<Map.Entry<String, TermStatistics>> getEntryList(){
        Set<Map.Entry<String, TermStatistics>> entries = this.termStringToInfoMap.entrySet();
        return new ArrayList<>(entries);
    }

    /**
     * Returns the vocabulary's entries as a sorted list (descending by their absolute frequency).
     * @return the entries as sorted list
     */
    public List<Map.Entry<String, TermStatistics>> getEntryListSortedByAbsFrequencyDesc(){
        List<Map.Entry<String, TermStatistics>> entriesList = this.getEntryList();
        entriesList.sort(Comparator.comparingInt(item -> - item.getValue().totalNumberOfSightings()));

        return entriesList;
    }

    /**
     * Returns the vocabulary's entries as a sorted list (ascending by their absolute frequency).
     * @return the entries as sorted list
     */
    public List<Map.Entry<String, TermStatistics>> getEntryListSortedByAbsFrequencyAsc(){
        List<Map.Entry<String, TermStatistics>> entriesList = this.getEntryList();
        entriesList.sort(Comparator.comparingInt(item -> item.getValue().totalNumberOfSightings()));

        return entriesList;
    }

    /**
     * Deletes all entries from the vocabulary that have an absolute frequency value lower or equal
     * than the given minFrequency.
     * @param minFrequency the minFrequency that has to be surpassed for a word to be kept inside the vocabulary
     * @return the modified FrequencyBowVocabulary instance itself to allow chaining of certain modification-methods.
     */
    public ScoredVocabulary keepOnlyByBiggerThanAbsoluteFrequency(int minFrequency){
        this.transformationLog.append("applied: keepOnlyByBiggerThanAbsoluteFrequency(").append(minFrequency).append(")\n");
        Iterator<Map.Entry<String, TermStatistics>> iterator = this.iteratorTermStringToInfoMapEntrySet();
        while(iterator.hasNext()) {
            Map.Entry<String, TermStatistics> entry = iterator.next();
            if (entry.getValue().totalNumberOfSightings() <= minFrequency) {
                this.termIndexToInfoMap.remove(entry.getValue().index);
                //unfortunately due to LinkedHashMaps exception-model it is not possible to call the wrapper class'remove on an entry
                //see: https://stackoverflow.com/a/51205676 for additional info
                iterator.remove();
            }
        }
        this.reindex();
        return this;
    }

    /**
     * Deletes all entries from the vocabulary that have an absolute frequency value bigger or equal
     * than the given maxFrequency.
     * @param maxFrequency words with maxFrequency or higher will be deleted from the vocabulary
     * @return the modified FrequencyBowVocabulary instance itself to allow chaining of certain modification-methods.
     */
    public ScoredVocabulary keepOnlyBySmallerThanAbsoluteFrequency(int maxFrequency){
        this.transformationLog.append("applied: keepOnlyBySmallerThanAbsoluteFrequency(").append(maxFrequency).append(")\n");
        Iterator<Map.Entry<String, TermStatistics>> iterator = this.iteratorTermStringToInfoMapEntrySet();
        while(iterator.hasNext()) {
            Map.Entry<String, TermStatistics> entry = iterator.next();
            if (entry.getValue().totalNumberOfSightings() >= maxFrequency) {
                this.termIndexToInfoMap.remove(entry.getValue().index);
                iterator.remove();
            }
        }
        this.reindex();
        return this;
    }

    /**
     * Only keeps words in the vocabulary whose relative frequency/percentage of the current {@link #numberOfAdditions} is bigger than
     * minPercentage.
     * Attention: performing an "unfixed" method to reduce the vocabulary set reduces the total numberOfAdditions, so when chaining two
     * methods that reduce the vocabulary by some percentage constraint, the second method will only work on the subset created by the first one!
     * @param minPercentage this percentage has to be surpassed for a word to stay in the vocabulary
     * @return the modified FrequencyBowVocabulary instance itself to allow chaining of certain modification-methods.
     */
    public ScoredVocabulary keepOnlyByBiggerThanPercentage(double minPercentage){
        this.transformationLog.append("applied: keepOnlyByBiggerThanPercentage(").append(minPercentage).append(")\n");
        Iterator<Map.Entry<String, TermStatistics>> iterator = this.iteratorTermStringToInfoMapEntrySet();
        int totalNumberOfAdditions = numberOfAdditions();
        while(iterator.hasNext()) {
            Map.Entry<String, TermStatistics> entry = iterator.next();
            if ((double) entry.getValue().totalNumberOfSightings()*100/totalNumberOfAdditions <= minPercentage) {
                this.termIndexToInfoMap.remove(entry.getValue().index);
                iterator.remove();
            }
        }
        this.reindex();
        return this;
    }

    /*NOTE:unglücklicherweise kann man bei den folgenden methoden nicht ohne weiteres das remove der Klasse an sich
    nutzen, da wir sonst wunderbare concurrentmodificationexceptions erzeugen.
    Man könnte das umgehen, indem man während der iteration über die collection ne liste der rauszuwerfenden objects erzeugt und die danach rauswirft.
    da das aber in unserem fall potentiell viele sein können und das vocabulary eh schon den ram quält spar ich mir den arbeitsspeicher
    und mache es etwas hässlicher mit den iteratoren direkt.
    Siehe auch: https://www.baeldung.com/java-concurrentmodificationexception
     */

    /**
     * Only keeps words in the vocabulary whose relative frequency/percentage of the current {@link #fixedSize} is bigger than
     * minPercentage.
     * @param minPercentage this percentage has to be surpassed for a word to stay in the vocabulary
     * @return the modified FrequencyBowVocabulary instance itself to allow chaining of certain modification-methods.
     * @see ScoredVocabulary#fixedSize
     * @see ScoredVocabulary#fixatePercentageReference()
     */
    public ScoredVocabulary keepOnlyByBiggerThanPercentageFixated(double minPercentage){
        this.transformationLog.append("applied: keepOnlyByBiggerThanPercentageFixated(").append(minPercentage).append(")\n");
        Iterator<Map.Entry<String, TermStatistics>> iterator = this.iteratorTermStringToInfoMapEntrySet();
        while(iterator.hasNext()) {
            Map.Entry<String, TermStatistics> entry = iterator.next();
            if ((double) entry.getValue().totalNumberOfSightings()*100/this.fixedSize <= minPercentage) {
                this.termIndexToInfoMap.remove(entry.getValue().index);
                iterator.remove();
            }
        }
        this.reindex();
        return this;
    }

    /**
     * Only keeps words in the vocabulary whose relative frequency/percentage of the current {@link #numberOfAdditions} is smaller than
     * minPercentage.
     * Attention: performing an "unfixed" method to reduce the vocabulary set reduces the total numberOfAdditions, so when chaining two
     * methods that reduce the vocabulary by some percentage constraint, the second method will only work on the subset created by the first one!
     * @param maxPercentage only words with a relative frequency lower than maxPercentage are kept inside the vocabulary
     * @return the modified FrequencyBowVocabulary instance itself to allow chaining of certain modification-methods.
     */
    public ScoredVocabulary keepOnlyBySmallerThanPercentage(double maxPercentage){
        this.transformationLog.append("applied: keepOnlyBySmallerThanPercentage(").append(maxPercentage).append(")\n");
        int totalNumberOfAdditions = numberOfAdditions();
        Iterator<Map.Entry<String, TermStatistics>> iterator = this.iteratorTermStringToInfoMapEntrySet();
        while(iterator.hasNext()) {
            Map.Entry<String, TermStatistics> entry = iterator.next();
            if ((double) entry.getValue().totalNumberOfSightings()*100/totalNumberOfAdditions >= maxPercentage) {
                this.termIndexToInfoMap.remove(entry.getValue().index);
                iterator.remove();
            }
        }
        this.reindex();
        return this;
    }

    /**
     * Only keeps words in the vocabulary whose relative frequency/percentage of the current {@link #fixedSize} is smaller than
     * minPercentage.
     * @param maxPercentage only words with a relative frequency lower than maxPercentage are kept inside the vocabulary
     * @return the modified FrequencyBowVocabulary instance itself to allow chaining of certain modification-methods.
     * @see ScoredVocabulary#fixedSize
     * @see ScoredVocabulary#fixatePercentageReference()
     */
    public ScoredVocabulary keepOnlyBySmallerThanPercentageFixated(double maxPercentage){
        this.transformationLog.append("applied: keepOnlyBySmallerThanPercentageFixated(").append(maxPercentage).append(")\n");
        Iterator<Map.Entry<String, TermStatistics>> iterator = this.iteratorTermStringToInfoMapEntrySet();
        while(iterator.hasNext()) {
            Map.Entry<String, TermStatistics> entry = iterator.next();
            if ((double) entry.getValue().totalNumberOfSightings()*100/this.fixedSize >= maxPercentage) {
                this.termIndexToInfoMap.remove(entry.getValue().index);
                iterator.remove();
            }
        }
        this.reindex();
        return this;
    }

    /**
     * Removes all terms that make up greater or equal x% of the most occurring terms in the vocabulary. That DOES NOT mean x% of terms will be cut of!
     * It will sort the terms descending (with regards to their total number of occurrences) and then begin to throw away
     * terms starting with the most occurring one. It will proceed as long as the sum of the total number of occurrences of the
     * removed terms makes up less than x% of the total amount of terms.
     * Beware: uses {@link #fixedSize}
     * as reference for the total amount of terms in the vocabulary. So the correct setup lies in the users responsibility
     * as the user has to manually call the fitting methods to set the fixedSize correctly.
     * For that use method {@link #cutTopXPercent}.
     * @param percentage top percentage to be removed
     * @return the modified FrequencyBowVocabulary instance itself to allow chaining of certain modification-methods.
     */
    public ScoredVocabulary cutTopXPercentByoccurrencesFixated(double percentage){
        this.transformationLog.append("applied: cutTopXPercentByOccurrenceFixated(").append(percentage).append(")\n");
        if (this.size() == 0){
            return this;
        }
        List<Map.Entry<String, TermStatistics>> entries = this.getEntryListSortedByAbsFrequencyDesc();
        double currentPercentage;
        int currentSum = 0;
        for (int i = 0; i < entries.size(); i++) {
            Map.Entry<String, TermStatistics> stringNewTermStatisticsEntry = entries.get(i);
            currentSum += stringNewTermStatisticsEntry.getValue().totalNumberOfSightings();
            currentPercentage = (double) currentSum / (double) fixedSize;
            if (currentPercentage*100 <= percentage){
                this.termIndexToInfoMap.remove(stringNewTermStatisticsEntry.getValue().index);
                this.remove(stringNewTermStatisticsEntry.getKey());
            }
        }
        this.reindex();
        return this;
    }

    /**
     * Removes all terms that make up the top less or equal than x% of the most frequent terms in the vocabulary.
     * @param percentage top percentage to be removed
     * @return the modified FrequencyBowVocabulary instance itself to allow chaining of certain modification-methods.
     */
    public ScoredVocabulary cutTopXPercent(double percentage){
        this.transformationLog.append("applied: cutTopXPercent(").append(percentage).append(")\n");
        if (this.size() == 0){
            return this;
        }
        int referenceSize = this.size();
        List<Map.Entry<String, TermStatistics>> entries = this.getEntryListSortedByAbsFrequencyDesc();
        double currentPercentage;
        int numberOfEntries = 0;
        for (int i = 0; i < entries.size(); i++) {
            Map.Entry<String, TermStatistics> stringNewTermStatisticsEntry = entries.get(i);
            numberOfEntries ++;
            currentPercentage = (double) numberOfEntries/ (double) referenceSize;
            if (currentPercentage*100 <= percentage){
                this.termIndexToInfoMap.remove(stringNewTermStatisticsEntry.getValue().index);
                this.remove(stringNewTermStatisticsEntry.getKey());
            }
        }
        this.reindex();
        return this;
    }

    /**
     * Removes all terms that make up lesser or equal than x% of the least occurring terms in the vocabulary; beware: uses {@link #fixedSize}
     * as reference for the total amount of terms in the vocabulary.That DOES NOT mean x% of terms will be cut of!
     * For that use method {@link #cutBottomXPercent}.
     * @param percentage bottom percentage to be removed
     * @return the reduced vocabulary
     */
    public ScoredVocabulary cutBottomXPercentByOccurrencesFixated(double percentage){
        this.transformationLog.append("applied: cutBottomXPercentByOccurrenceFixated(").append(percentage).append(")\n");
        if (this.size() == 0){
            return this;
        }
        List<Map.Entry<String, TermStatistics>> entries = this.getEntryListSortedByAbsFrequencyAsc();
        double currentPercentage;
        int currentSum = 0;
        for (int i = 0; i < entries.size(); i++) {
            Map.Entry<String, TermStatistics> stringNewTermStatisticsEntry = entries.get(i);
            currentSum += stringNewTermStatisticsEntry.getValue().totalNumberOfSightings();
            currentPercentage = (double) currentSum / (double) this.fixedSize;
            if (currentPercentage*100 <= percentage){
                this.termIndexToInfoMap.remove(stringNewTermStatisticsEntry.getValue().index);
                this.remove(stringNewTermStatisticsEntry.getKey());
            }
        }
        this.reindex();
        return this;
    }

    /**
     * Removes all terms that make up the bottom less or equal than x% of the least frequent terms in the vocabulary.
     * @param percentage bottom percentage to be removed
     * @return the reduced vocabulary
     */
    public ScoredVocabulary cutBottomXPercent(double percentage){
        this.transformationLog.append("applied: cutBottomXPercent(").append(percentage).append(")\n");
        if (this.size() == 0){
            return this;
        }
        int referenceSize = this.size();
        List<Map.Entry<String, TermStatistics>> entries = this.getEntryListSortedByAbsFrequencyAsc();
        double currentPercentage;
        int numberOfEntries = 0;
        for (int i = 0; i < entries.size(); i++) {
            Map.Entry<String, TermStatistics> stringNewTermStatisticsEntry = entries.get(i);
            numberOfEntries ++;
            currentPercentage = (double) numberOfEntries / (double) referenceSize;
            if (currentPercentage*100 <= percentage){
                this.termIndexToInfoMap.remove(stringNewTermStatisticsEntry.getValue().index);
                this.remove(stringNewTermStatisticsEntry.getKey());
            }
        }
        this.reindex();
        return this;
    }

    /**
     * Cuts a specified top and bottom percentage of all terms from the vocabulary.
     * Top and bottom are referring to the total number of occurrences of each
     * term.
     * @param topPercentage percentage of terms to be cut of at the top
     * @param bottomPercentage percentage of terms to be cut of at the bottom
     * @return the reduced vocabulary
     */
    public ScoredVocabulary cutTopAndBottomXPercent(double topPercentage, double bottomPercentage){
        this.transformationLog.append("applied: cutTopAndBottomXPercent(").append(topPercentage).append(", ").append(bottomPercentage).append(")\n");
        if (this.size() == 0){
            return this;
        }
        int referenceSize = this.size();
        List<Map.Entry<String, TermStatistics>> entries = this.getEntryListSortedByAbsFrequencyAsc();
        double currentPercentage;
        int numberOfEntries = 0;
        for (int i = 0; i < entries.size(); i++) {
            Map.Entry<String, TermStatistics> stringNewTermStatisticsEntry = entries.get(i);
            numberOfEntries++;
            currentPercentage = (double) numberOfEntries / (double) referenceSize;
            if (currentPercentage*100 <= bottomPercentage){
                this.termIndexToInfoMap.remove(stringNewTermStatisticsEntry.getValue().index);
                this.remove(stringNewTermStatisticsEntry.getKey());
            }
        }

        entries = this.getEntryListSortedByAbsFrequencyDesc();
        numberOfEntries = 0;

        for (int i = 0; i < entries.size(); i++) {
            Map.Entry<String, TermStatistics> stringNewTermStatisticsEntry = entries.get(i);
            numberOfEntries++;
            currentPercentage = (double) numberOfEntries / (double) referenceSize;
            if (currentPercentage*100 <= topPercentage){
                this.termIndexToInfoMap.remove(stringNewTermStatisticsEntry.getValue().index);
                this.remove(stringNewTermStatisticsEntry.getKey());
            }
        }
        this.reindex();
        return this;
    }

    /**
     * Will calculate the fisher score for each term in the vocabulary and keep
     * only the top x sorted by fisher score (descending) in the vocabulary using the actual
     * number of occurrences of the individual terms while calculating the fisher scores (for example
     * for the estimated expected values).
     * @param numberToKeep the reduced vocabulary
     */
    public void keepTopXByFisherScoreCounted(int numberToKeep){
        this.transformationLog.append("applied: keepTopXByFisherScoreCounted(").append(numberToKeep).append(")\n");
        List<ImmutablePair<String, Double>> termIndicesByFScore = termsSortedByFisherScoreCounted();
        for (int i = numberToKeep; i < termIndicesByFScore.size(); i++) {
            ImmutablePair<String, Double> termScore = termIndicesByFScore.get(i);
            this.remove(termScore.getLeft());
        }
        this.termIndicesSortedByFisherScore = termIndicesByFScore.subList(0, numberToKeep);
        reindex();
    }

    /**
     * Will calculate the fisher score for each term in the vocabulary and keep
     * only the top x sorted by fisher score (descending) in the vocabulary using
     * binary counting to calculate the fisher score (for example for the estimated expected values).
     * @param numberToKeep the reduced vocabulary
     */
    public void keepTopXByFisherScoreBinary(int numberToKeep){
        this.transformationLog.append("applied: keepTopXByFisherScoreBinary(").append(numberToKeep).append(")\n");
        List<ImmutablePair<String, Double>> termIndicesByFScore = termsSortedByFisherScoreBinary();
        for (int i = numberToKeep; i < termIndicesByFScore.size(); i++) {
            ImmutablePair<String, Double> termScore = termIndicesByFScore.get(i);
            this.remove(termScore.getLeft());
        }
        this.termIndicesSortedByFisherScore = termIndicesByFScore.subList(0, numberToKeep);
        reindex();
    }

    //NOTE: die Methode muss richtig genutzt werden und ist nicht für jemanden gedacht, der nciht weiß, was er tut :D Zum Beispiel sollte derjenige wissen, dass sie nutzlos ist beim aufrufen überladener methoden

    /**
     * Will apply additional steps as specified in a String by using reflection; this allows for easily extensible external configuration but
     * should be used with caution. It requires knowledge on when it is necessary to call {@link #fixatePercentageReference}.
     * Furthermore it should not be used on overloaded methods. The input String uses postfix-notation and allows to chain
     * methods separated by ";". This method is mainly intended to apply methods to reduce the vocabulary.
     * @param stepsString steps to perform; example: "keepOnlyByBiggerThanAbsoluteFrequency 5; keepTopXByFisherScoreBinary 4000"
     */
    public void applyAdditionalSteps(String stepsString){
        if (stepsString.isBlank()){
            logger.warning("applyAdditionalSteps invoked but with empty argument!");
            return;
        }
        String[] steps = stepsString.split(";");
        Method[] methods = this.getClass().getMethods();
        Method method;
        for (int i = 0; i < steps.length; i++) {
            String step = steps[i].trim();
            String[] stepTokens = step.split("\\s+");
            String methodName = stepTokens[0];
            method = findMethods(methodName, methods);

            if (method == null) {
                logger.warning("Couldn't find method " + methodName + "! It will therefore not be applied!");
                continue;
            }

            Class[] parameterTypes = method.getParameterTypes();
            Object[] parameters = new Object[parameterTypes.length];

            for (int j = 0; j < parameterTypes.length; j++) {
                Class parameterType = parameterTypes[j];
                if (parameterType.getName().equals("double")){
                    parameters[j] = Double.parseDouble(stepTokens[j+1]);
                }
                if (parameterType.getName().equals("float")){
                    parameters[j] = Float.parseFloat(stepTokens[j+1]);
                }
                if (parameterType.getName().equals("int")){
                    parameters[j] = Integer.parseInt(stepTokens[j+1]);
                }
            }

            try {
                logger.info("apply " + methodName);
                method.invoke(this, parameters);
                logger.info("done applying " + methodName);
                logger.info("vocabulary size: " + this.size());
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Calculates the (simple) fisher scores for the terms in this vocabulary where empiric expected values and derived expected variances
     * are calculated based on the existence of the terms in the documents; that means independent on the actual number of
     * occurrences of a word in a text, for the calculation of the fisher score it will be counted as one occurrence. Methods based on absolute counting exist as well.
     * For a freely available paper featuring a short explanation of the F-Score see- in https://arxiv.org/ftp/arxiv/papers/1202/1202.3725.pdf
     * especially equation (4).
     * @return the scores as array; REMARK as term-indices start with 1 the result is shifted by one position i.e.
     * fisherScoresBinaryAsArray()[0] contains the score for the term with index 1;
     * fisherScoresBinaryAsArray()[1] contains the score for the term with index 2;
     */
    public double[] fisherScoresBinaryAsArray(){
        HashMap<Integer, Integer> classMappings = positiveClassMappings();

        int[] numberOfInstancesPerClass = numberOfDocumentsPerClassMappedAsArray(classMappings);

        double[] meansPerFeature = new double[this.size()];

        for (TermStatistics termStats : this) {
            List<MutablePair<Integer, Integer>> dococcurrencesPerClass = termStats.numberOfDocsoccurringInPerClass;
            int numberOfDocumentsAppearedIn = 0;
            for (int classIndex = 0; classIndex < dococcurrencesPerClass.size(); classIndex++) {
                MutablePair<Integer, Integer> classoccurrences = dococcurrencesPerClass.get(classIndex);
                numberOfDocumentsAppearedIn += classoccurrences.getRight();
            }
            meansPerFeature[termStats.index - 1] = (double) numberOfDocumentsAppearedIn / (double) this.seenDocuments;
        }

        double[][] meansPerFeaturePerClass = new double[this.size()][numberOfInstancesPerClass.length];
        double[][] variancesPerFeaturePerClass = new double[this.size()][numberOfInstancesPerClass.length];
        for(TermStatistics termStats : this){
            List<MutablePair<Integer, Integer>> classoccurrences = termStats.occurrencesPerClassAsList();
            int featureIndex = termStats.index;
            for (int j = 0; j < classoccurrences.size(); j++) {
                MutablePair<Integer, Integer> classAndFrequencyForTerm = classoccurrences.get(j);
                int originalClass = classAndFrequencyForTerm.getLeft();
                int classIndex = classMappings.get(classAndFrequencyForTerm.getLeft());

                List<Double> nonZeroFrequencies = new ArrayList<>();
                Map<Integer, TermDocAppearanceInfo> appearanceInfosru = termStats.appearanceInfo;
                Set<Map.Entry<Integer, TermDocAppearanceInfo>> appearanceInfs = appearanceInfosru.entrySet();
                for (Iterator<Map.Entry<Integer, TermDocAppearanceInfo>> iterator = appearanceInfs.iterator(); iterator.hasNext(); ) {
                    Map.Entry<Integer, TermDocAppearanceInfo> docNumAppearanceInfo = iterator.next();
                    if (docNumAppearanceInfo.getValue().documentClass == originalClass) {
                        nonZeroFrequencies.add(1d);
                    }
                }

                double[] nonZeroFreqs;
                double[] empExpectedValueAndVariance = new double[2];
                if (nonZeroFrequencies.size() > 0 ) {
                    nonZeroFreqs = Doubles.toArray(nonZeroFrequencies);
                    empExpectedValueAndVariance = Statistics.empiricalExpectedValueAndVarianceSparse(numberOfInstancesPerClass[classIndex], nonZeroFreqs);
                }else {
                    empExpectedValueAndVariance[1] = 0;
                }
                meansPerFeaturePerClass[featureIndex-1][classIndex] = empExpectedValueAndVariance[0];
                variancesPerFeaturePerClass[featureIndex-1][classIndex] = empExpectedValueAndVariance[1];
            }
        }

        return FisherScoreSimple.fisherScores(numberOfInstancesPerClass, meansPerFeature, meansPerFeaturePerClass, variancesPerFeaturePerClass);
    }

    /**
     * Similar to {@link #fisherScoresBinaryAsArray()} but returns a map instead of an array.
     * @return the scores as Map where the key is the term and the value the respective Fisher-Score
     * @see #fisherScoresBinaryAsArray()
     */
    public Map<String, Double> fisherScoresBinaryAsMap(){
        double[] scores = fisherScoresBinaryAsArray();
        Map<String, Double> scoresMap = new HashMap<>();
        for (int i = 0; i < scores.length; i++) {
            double score = scores[i];
            scoresMap.put(getTermByIndex(i+1), score);
        }
        return scoresMap;
    }

    /**
     * Calculates the (simple) fisher scores for the terms in this vocabulary where empiric expected values and derived expected variances
     * are calculated based on the absolute number of occurrences of the terms in the documents. Methods based on binary counting exist as well.
     * For a freely available paper featuring a short explanation of the F-Score see- in https://arxiv.org/ftp/arxiv/papers/1202/1202.3725.pdf
     * especially equation (4).
     * @return the scores as array; REMARK as term-indices start with 1 the result is shifted by one position i.e.
     * fisherScoresCountedAsArray()[0] contains the score for the term with index 1;
     * fisherScoresCountedAsArray()[1] contains the score for the term with index 2;
     * ...
     */
    public double[] fisherScoresCountedAsArray(){
        HashMap<Integer, Integer> classMappings = positiveClassMappings();

        int[] numberOfInstancesPerClass = numberOfDocumentsPerClassMappedAsArray(classMappings);

        double[] meansPerFeature = new double[this.size()];

        for (TermStatistics termStats : this) {
            List<MutablePair<Integer, Integer>> dococcurrencesPerClass = termStats.numberOfDocsoccurringInPerClass;
            int numberOfDocumentsAppearedIn = 0;
            for (int classIndex = 0; classIndex < dococcurrencesPerClass.size(); classIndex++) {
                MutablePair<Integer, Integer> classoccurrences = dococcurrencesPerClass.get(classIndex);
                numberOfDocumentsAppearedIn += classoccurrences.getRight();
            }
            meansPerFeature[termStats.index - 1] = (double) numberOfDocumentsAppearedIn / (double) this.seenDocuments;
        }

        double[][] meansPerFeaturePerClass = new double[this.size()][numberOfInstancesPerClass.length];
        double[][] variancesPerFeaturePerClass = new double[this.size()][numberOfInstancesPerClass.length];
        for(TermStatistics termStats : this){
            List<MutablePair<Integer, Integer>> classoccurrences = termStats.occurrencesPerClassAsList();
            int featureIndex = termStats.index;
            for (int j = 0; j < classoccurrences.size(); j++) {
                MutablePair<Integer, Integer> classAndFrequencyForTerm = classoccurrences.get(j);
                int originalClass = classAndFrequencyForTerm.getLeft();
                int classIndex = classMappings.get(classAndFrequencyForTerm.getLeft());

                List<Double> nonZeroFrequencies = new ArrayList<>();
                Map<Integer, TermDocAppearanceInfo> appearanceInfosru = termStats.appearanceInfo;
                Set<Map.Entry<Integer, TermDocAppearanceInfo>> appearanceInfs = appearanceInfosru.entrySet();
                for (Iterator<Map.Entry<Integer, TermDocAppearanceInfo>> iterator = appearanceInfs.iterator(); iterator.hasNext(); ) {
                    Map.Entry<Integer, TermDocAppearanceInfo> docNumAppearanceInfo = iterator.next();
                    if (docNumAppearanceInfo.getValue().documentClass == originalClass) {
                        nonZeroFrequencies.add((double)docNumAppearanceInfo.getValue().numberOfAppearances);
                    }
                }

                double[] nonZeroFreqs;
                double[] empExpectedValueAndVariance = new double[2];
                if (nonZeroFrequencies.size() > 0 ) {
                    nonZeroFreqs = Doubles.toArray(nonZeroFrequencies);
                    empExpectedValueAndVariance = Statistics.empiricalExpectedValueAndVarianceSparse(numberOfInstancesPerClass[classIndex], nonZeroFreqs);
                }else {
                    empExpectedValueAndVariance[1] = 0;
                }
                meansPerFeaturePerClass[featureIndex-1][classIndex] = empExpectedValueAndVariance[0];
                variancesPerFeaturePerClass[featureIndex-1][classIndex] = empExpectedValueAndVariance[1];
            }
        }

        return FisherScoreSimple.fisherScores(numberOfInstancesPerClass, meansPerFeature, meansPerFeaturePerClass, variancesPerFeaturePerClass);
    }

    /**
     * Similar to {@link #fisherScoresCountedAsArray()} but returns a map instead of an array.
     * @return the scores as Map where the key is the term and the value the respective Fisher-Score
     * @see #fisherScoresCountedAsArray()
     */
    public Map<String, Double> fisherScoresCountedAsMap(){
        double[] scores = fisherScoresCountedAsArray();
        Map<String, Double> scoresMap = new HashMap<>();
        for (int i = 0; i < scores.length; i++) {
            double score = scores[i];
            scoresMap.put(getTermByIndex(i+1), score);
        }
        return scoresMap;
    }

    /**
     * Returns a list term indices sorted by fisher score (counted version) in the form
     * of ImmutablePairs where left is the term-index and right is the associated fisher score
     * @return the list of fisher scores
     */
    public List<ImmutablePair<Integer, Double>> termIndicesSortedByFisherScoreCounted(){
        double[] scores = fisherScoresCountedAsArray();
        List<ImmutablePair<Integer, Double>> scoresPerTermIndex = new ArrayList<>();
        for (int i = 0; i < scores.length; i++) {
            double score = scores[i];
            scoresPerTermIndex.add(new ImmutablePair<>(i + 1, score));
        }
        scoresPerTermIndex.sort(Comparator.comparingDouble(item -> - item.getRight()));
        return scoresPerTermIndex;
    }

    /**
     * Returns a list term indices sorted by fisher score (binary version) in the form
     * of ImmutablePairs where left is the term-index and right is the associated fisher score
     * @return the list of fisher scores
     */
    public List<ImmutablePair<Integer, Double>> termIndicesSortedByFisherScoreBinary(){
        double[] scores = fisherScoresBinaryAsArray();
        List<ImmutablePair<Integer, Double>> scoresPerTermIndex = new ArrayList<>();
        for (int i = 0; i < scores.length; i++) {
            double score = scores[i];
            scoresPerTermIndex.add(new ImmutablePair<>(i + 1, score));
        }
        scoresPerTermIndex.sort(Comparator.comparingDouble(item -> - item.getRight()));
        return scoresPerTermIndex;
    }

    /**
     * Returns a list terms sorted by fisher score (counted version) in the form
     * of ImmutablePairs where left is the term and right is the associated fisher score
     * @return the list of fisher scores
     */
    public List<ImmutablePair<String, Double>> termsSortedByFisherScoreCounted(){
        List<ImmutablePair<Integer, Double>> scoresPerTermIndex = termIndicesSortedByFisherScoreCounted();
        List<ImmutablePair<String, Double>> sortedTerms = new ArrayList<>();
        for (int i = 0; i < scoresPerTermIndex.size(); i++) {
            ImmutablePair<Integer, Double> indexScore = scoresPerTermIndex.get(i);
            sortedTerms.add(new ImmutablePair<>(this.getTermByIndex(indexScore.getLeft()), indexScore.getRight()));
        }
        return sortedTerms;
    }

    /**
     * Returns a list terms sorted by fisher score (binary version) in the form
     * of ImmutablePairs where left is the term and right is the associated fisher score
     * @return the list of fisher scores
     */
    public List<ImmutablePair<String, Double>> termsSortedByFisherScoreBinary(){
        List<ImmutablePair<Integer, Double>> scoresPerTermIndex = termIndicesSortedByFisherScoreBinary();
        List<ImmutablePair<String, Double>> sortedTerms = new ArrayList<>();
        for (int i = 0; i < scoresPerTermIndex.size(); i++) {
            ImmutablePair<Integer, Double> indexScore = scoresPerTermIndex.get(i);
            sortedTerms.add(new ImmutablePair<>(this.getTermByIndex(indexScore.getLeft()), indexScore.getRight()));
        }
        return sortedTerms;
    }

    /**
     * Returns the {@link TermStatistics} for a given term.
     * @param term the term in question
     * @return the TermStatistics
     */
    public TermStatistics get(String term){
        return this.termStringToInfoMap.get(term);
    }

    /**
     * Returns the {@link TermStatistics} for a term by a given term-index.
     * @param index the index for the term in question
     * @return the TermStatistics
     */
    public TermStatistics get(int index){
        return this.termIndexToInfoMap.get(index);
    }

    /**
     * Returns the term associated with a given index.
     * @param index the index
     * @return the term
     */
    public String getTermByIndex(int index){
        return this.get(index).term;
    }

    /**
     * Returns the transformation log for this vocabulary containing all the steps
     * performed to reduce this vocabulary. REMARK that this will not work on
     * vocabularies that were read from a file as those do not persist this kind of information.
     * @return the transformation log as string
     */
    public String getTransformationLog(){
        return this.transformationLog.toString();
    }

    /**
     * Returns the number of different terms contained in this vocabulary.
     * @return the number of different terms in this vocabulary
     */
    public int size() {
        return this.termStringToInfoMap.size();
    }

    /**
     * Tests the vocabulary for emptiness
     * @return true if the vocabulary is empty, else false
     */
    public boolean isEmpty() {
        return this.termStringToInfoMap.isEmpty();
    }

    /**
     * Creates an iterator returning map entries where a term is associated with its corresponding
     * TermStatistics-object.
     * @return the iterator
     */
    public Iterator<Map.Entry<String, TermStatistics>> iteratorTermStringToInfoMapEntrySet(){
        return this.termStringToInfoMap.entrySet().iterator();
    }

    /**
     * Creates an iterator returning TermStatistic-objects for terms contained in this vocabulary.
     * @return the iterator
     */
    @NotNull
    @Override
    public Iterator<TermStatistics> iterator() {
        Set<Map.Entry<String, TermStatistics>> entries = termStringToInfoMap.entrySet();
        Iterator<Map.Entry<String, TermStatistics>> innerIterator = entries.iterator();
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return innerIterator.hasNext();
            }

            @Override
            public TermStatistics next() {
                return innerIterator.next().getValue();
            }
        };
    }

    @Override
    public void forEach(Consumer<? super TermStatistics> action) {
        for (TermStatistics termStatistics : this) {
            action.accept(termStatistics);
        }
    }

    /**
     * NOT IMPLEMENTED
     * @return null
     */
    @Override
    public Spliterator<TermStatistics> spliterator() {
        return null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.termStringToInfoMap.toString();
    }

    public boolean isApplyLowerCasing() {
        return this.vocabProps.applyLowerCasing;
    }

    public boolean isRemoveStopwords() {
        return this.vocabProps.removeStopwords;
    }

    public boolean isApplyStemming() {
        return this.vocabProps.applyStemming;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    int numberOfAdditions(){
        int additions = 0;
        for (TermStatistics termStatistics : this) {
            additions += termStatistics.totalNumberOfSightings();
        }
        return additions;
    }

    /**
     * Writes a text-representation of the vocabulary to a file at the given path ordered descending by fisher-score of individual terms
     * where the considered terms are {@link ScoredVocabulary#termIndicesSortedByFisherScore}; IMPORTANT it will NOT check if this fields value is
     * up to date! Problems with this can be avoided by calling any manipulations by fisher-scores as the last step before using this method
     * as they will update the field.
     * The format is as follows:
     * <pre>
     * [line 0]fvoc: METHOD_ID this.id
     * [line 1]key
     * [line 2]key
     * ...
     * </pre>
     *
     * Example:
     * <pre>
     * fvoc: my_method_id my_vocabulary_id
     * do
     * not
     * go
     * gentle
     * into
     * that
     * good
     * night
     * </pre>
     * @param path the path where the file should be saved
     */
    void writeToFileSortedByLastKnownFisherScore(String path){
        if (this.termIndicesSortedByFisherScore != null) {
            try (FileWriter fileWriter = new FileWriter(path); BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
                writeVocabFileHeader(bufferedWriter);
                for (int i = 0; i < this.termIndicesSortedByFisherScore.size(); i++) {
                    String term =  this.termIndicesSortedByFisherScore.get(i).getLeft();
                    bufferedWriter.write(term );

                    if (i != this.termIndicesSortedByFisherScore.size() - 1){
                        bufferedWriter.write("\n");
                    }
                }
                logger.info("Wrote vocabulary at: " + path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    List<Integer> knownClasses(){
        return this.knownClasses;
    }

    private void writeVocabFileHeader(BufferedWriter bufferedWriter) throws IOException {
        bufferedWriter.write("fvoc: " + createdBy + " " + id + "\n");
    }

    private Method findMethods(String methodName, Method[] methods){
        for (int i = 0; i < methods.length; i++) {
            Method method = methods[i];
            if (method.getName().equals(methodName)) {
                return method;
            }
        }
        return null;
    }

    /**
     * Generates a mapping af "true" class labels into the positive realm, meaning it iterates over all classes as found in
     * numberOfDocumentsPerClass and sequentially assigns positive integers as mapping starting with 0.
     * Consistent over multiple calls, as long as the vocabulary is not modified, as LinkedHashMap (used for numberOfDocumentsPerClass) preserves input order.
     * @return a Hashmap where the key is the original class and the value the mapped positive value.
     */
    private HashMap<Integer, Integer> positiveClassMappings(){
        HashMap<Integer, Integer> mappings = new HashMap<>();

        List<Integer> entriesList = knownClasses();
        for (int i = 0; i < entriesList.size(); i++) {
            mappings.put(entriesList.get(i), i);
        }
        return mappings;
    }

    private int[] numberOfDocumentsPerClassMappedAsArray(HashMap<Integer, Integer> positiveClassMappings){
        int[] numberOfInstancesPerClass = new int[this.numberOfDocumentsPerClass.size()];
        Set<Map.Entry<Integer, Integer>> docsPerClass = this.numberOfDocumentsPerClass.entrySet();
        List<Map.Entry<Integer, Integer>> docsPerClassList = new ArrayList<>(docsPerClass);
        for (int i = 0; i < docsPerClassList.size(); i++) {
            Map.Entry<Integer, Integer> classAndNumberOfDocuments = docsPerClassList.get(i);
            numberOfInstancesPerClass[positiveClassMappings.get(classAndNumberOfDocuments.getKey())] = classAndNumberOfDocuments.getValue();
        }
        return numberOfInstancesPerClass;
    }
}

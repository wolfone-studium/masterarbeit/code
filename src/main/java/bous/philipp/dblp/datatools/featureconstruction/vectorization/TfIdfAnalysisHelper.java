package bous.philipp.dblp.datatools.featureconstruction.vectorization;

import bous.philipp.dblp.datatools.datamodel.CsClassification;
import bous.philipp.dblp.datatools.db.reader.DatabaseReader;
import bous.philipp.dblp.datatools.featurescoring.TfIdfAnalyzer;
import bous.philipp.dblp.datatools.datamodel.FeatureType;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;
import bous.philipp.dblp.datatools.tokenization.LuceneStandardTokenizerAdapter;
import bous.philipp.dblp.datatools.tokenization.Tokenizer;
import bous.philipp.dblp.datatools.util.string.StringUtility;
import org.apache.commons.lang3.ArrayUtils;

import java.sql.Connection;
import java.util.EnumSet;
import java.util.List;

/**
 * Helper-class; helps simplify some actions in vectorization processes building
 * word based data-representations in cross-validation scenarios. It is not intended for
 * external usage but for sole usage in {@link BagOfWordsVectorizationArchetypes}.
 * It therefore underlies the same restrictions concerning tokenization.
 */
class TfIdfAnalysisHelper {

    private static Tokenizer getStandardTokenizer(){
        return new LuceneStandardTokenizerAdapter();
    }
    /**
     * Performs an unbalanced tf-idf-analysis (records are drawn from the database as-is).
     * @param connection the database connection
     * @param feature specifies the type of feature for which the analysis should be made
     * @param props properties file used to configure the associated CV-split procedure
     * @param itemsPerFold number of items to be drawn per fold in the associated CV-split-procedure
     * @param startId id from which to start
     * @param windowSize 1 - unigrams, 2 - bigrams ...
     * @return the analysis
     */
    static TfIdfAnalyzer tfidfAnalysisCVUnbalanced(
            Connection connection,
            FeatureType feature,
            SingleColumnCVSetProperties props,
            int itemsPerFold,
            int startId,
            int windowSize
    ) {
        TfIdfAnalyzer analyzer = new TfIdfAnalyzer();

        EnumSet<FeatureType> excludedFeatures = EnumSet.complementOf(EnumSet.of(feature));

        List<RecordDocument> records = DatabaseReader.selectRecordsFromDB(connection,
                excludedFeatures,
                props.getRecordTableName(),
                props.getFeatureTableName(),
                startId,
                itemsPerFold,
                CsClassification.CS_AND_NON_CS,
                props.isByRID()).getRecords();

        String currentFeatureValue;

        for (int j = 0; j < records.size(); j++) {
            RecordDocument recordDocument = records.get(j);
            currentFeatureValue = recordDocument.getFeatures().get(0).getValue();
            List<String> tokens = getStandardTokenizer().tokenize(currentFeatureValue);
            tokens = StringUtility.processTokens(tokens, props.isApplyLowerCasing(), props.isRemoveStopwords(), props.isApplyStemming());
            String[] tokenArray = tokens.toArray(new String[0]);
            if (windowSize > 1) {
                String[] ngrams = StringUtility.convertToNGrams(tokenArray, windowSize, " ");
                tokenArray = ArrayUtils.addAll(tokenArray, ngrams);
            }
            analyzer.updateForTerms(tokenArray);
            analyzer.nextDocument();
        }

        return  analyzer;
    }

    /**
     * Performs a balanced tf-idf-analysis (tries to draw records from the database in a balanced manner with regards to the class label).
     * @param connection the database connection
     * @param feature specifies the type of feature for which the analysis should be made
     * @param props properties file used to configure the associated CV-split procedure
     * @param itemsPerFoldAndClass number of items to be drawn per fold per class in the associated CV-split-procedure
     * @param startIdNonCs first id to draw from non-cs records
     * @param startIdCs first id to draw from cs records
     * @param windowSize 1 - unigrams, 2 - bigrams ...
     * @return the analysis
     */
    static TfIdfAnalyzer tfidfAnalysisBalanced(
            Connection connection,
            FeatureType feature,
            SingleColumnCVSetProperties props,
            int itemsPerFoldAndClass,
            int startIdNonCs,
            int startIdCs,
            int windowSize
    ) {
        TfIdfAnalyzer analyzer = new TfIdfAnalyzer();

        EnumSet<FeatureType> excludedFeatures = EnumSet.complementOf(EnumSet.of(feature));

        List<RecordDocument> recordsNonCs = DatabaseReader.selectRecordsFromDB(connection,
                excludedFeatures,
                props.getRecordTableName(),
                props.getFeatureTableName(),
                startIdNonCs,
                itemsPerFoldAndClass,
                CsClassification.NON_CS_ONLY,
                props.isByRID()).getRecords();

        String currentFeatureValue;

        for (int j = 0; j < recordsNonCs.size(); j++) {
            RecordDocument recordDocument = recordsNonCs.get(j);
            currentFeatureValue = recordDocument.getFeatures().get(0).getValue();
            List<String> tokens = getStandardTokenizer().tokenize(currentFeatureValue);
            tokens = StringUtility.processTokens(tokens, props.isApplyLowerCasing(), props.isRemoveStopwords(), props.isApplyStemming());
            String[] tokenArray = tokens.toArray(new String[0]);
            if (windowSize > 1) {
                String[] ngrams = StringUtility.convertToNGrams(tokenArray, windowSize, " ");
                tokenArray = ArrayUtils.addAll(tokenArray, ngrams);
            }
            analyzer.updateForTerms(tokenArray);
            analyzer.nextDocument();
        }

        recordsNonCs = null;

        List<RecordDocument> recordsCs = DatabaseReader.selectRecordsFromDB(connection,
                excludedFeatures,
                props.getRecordTableName(),
                props.getFeatureTableName(),
                startIdCs,
                itemsPerFoldAndClass,
                CsClassification.CS_ONLY,
                props.isByRID()).getRecords();

        for (int j = 0; j < recordsCs.size(); j++) {
            RecordDocument recordDocument = recordsCs.get(j);
            currentFeatureValue = recordDocument.getFeatures().get(0).getValue();
            List<String> tokens = getStandardTokenizer().tokenize(currentFeatureValue);
            tokens = StringUtility.processTokens(tokens, props.isApplyLowerCasing(), props.isRemoveStopwords(), props.isApplyStemming());
            String[] tokenArray = tokens.toArray(new String[0]);
            if (windowSize > 1) {
                String[] ngrams = StringUtility.convertToNGrams(tokenArray, windowSize, " ");
                tokenArray = ArrayUtils.addAll(tokenArray, ngrams);
            }
            analyzer.updateForTerms(tokenArray);
            analyzer.nextDocument();
        }

        return  analyzer;
    }
}

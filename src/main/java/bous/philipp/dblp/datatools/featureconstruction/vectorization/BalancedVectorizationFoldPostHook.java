package bous.philipp.dblp.datatools.featureconstruction.vectorization;

import bous.philipp.dblp.datatools.featureconstruction.vocabulary.ScoredVocabulary;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;

import java.util.List;

/**
 * Provides a means to hook into the (balanced version of the) vectorization process in bag of words based text-vectorization scenarios as for example found in
 * {@link BagOfWordsVectorizationArchetypes#convertSingleFeatureToSVMLightCrossValidationSetsBalanced()}. Its main purpose is
 * to allow extensible logging and inspection of the vectorization process.
 */
public interface BalancedVectorizationFoldPostHook {
    /**
     * Executed after positive entries have been processed.
     * @param foldNumber the number of the fold currently being processed
     * @param positiveRecords a list of the positive records that have just been processed
     * @param vocabulary the vocabulary that was used during the vectorization process
     */
    void afterPositivesProcessed(int foldNumber, List<RecordDocument> positiveRecords, ScoredVocabulary vocabulary);
    /**
     * Executed after negative entries have been processed.
     * @param foldNumber the number of the fold currently being processed
     * @param negativeRecords a list of the positive records that have just been processed
     * @param vocabulary the vocabulary that was used during the vectorization process
     */
    void afterNegativesProcessed(int foldNumber, List<RecordDocument> negativeRecords, ScoredVocabulary vocabulary);

    /**
     * Will be called before finishing the record-processing inside a fold.
     * @param foldNumber the number of the fold currently being processed
     * @param vocabulary the vocabulary used during the vectorization process
     */
    void finalize(int foldNumber, ScoredVocabulary vocabulary);
}

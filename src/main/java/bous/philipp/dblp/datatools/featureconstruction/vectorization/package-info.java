/**
 * Provides vectorization-logic/classes. This includes classes/interfaces to
 * augment or influence the vectorization-process of data from a database.
 */
package bous.philipp.dblp.datatools.featureconstruction.vectorization;
package bous.philipp.dblp.datatools.featureconstruction.vectorization;

import bous.philipp.dblp.datatools.datamodel.FeatureType;
import bous.philipp.dblp.datatools.util.config.DbDependentExecutionConfig;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Execution-config for construction of files for cross-validation-sets.
 */
public class SingleColumnCVSetProperties implements DbDependentExecutionConfig {
    @Getter String methodID;
    String dbPropertiesFilePath;
    @Getter @Setter String vocabularyLocation;
    @Getter @Setter String vocabularyPrefixPath;
    @Getter String recordTableName;
    @Getter String featureTableName;
    @Getter FeatureType featureType;
    @Getter int numberOfItemsPerClass;
    @Getter int numberOfFolds;
    @Getter int maxNumber;
    @Getter int windowSize;
    @Getter boolean countWords;
    @Getter boolean weightWithTfIdf;
    @Getter boolean applyLowerCasing;
    @Getter boolean removeBracketContent;
    @Getter boolean removeStopwords;
    @Getter boolean applyStemming;
    @Getter boolean printInfoFile;
    @Getter boolean writeAdditionalTar;
    @Getter boolean byRID;

    @Getter String fileOutputPath;

    /**
     * Reads parameters for creating a CV set for a single feature of record-documents from a properties file, with following
     * allowed keys:
     * <ul>
     *     <li>METHOD_VERSION_ID - id of the this methods version (given as final string in the code)</li>
     *     <li>dbPropertiesFilePath - path properties-file with information about the jdbc database connection (only mysql supported atm)</li>
     *     <li>vocabularyLocation - path to the vocabulary-file to be used for conversion; only for single vocabulary-CV; can not be used alongside vocabularyPrefixPath</li>
     *     <li>vocabularyPrefixPath - initializes usage of a set of vocabularies for cv should be the path to the folder where the vocabulary-set resides including the common prefixes of all vocabularies</li>
     *     <li>recordTableName - name of the record-table to be used</li>
     *     <li>featureTableName - name of the corresponding feature table</li>
     *     <li>featureType - feature to be extracted, allowed values are specified through the enum of the same name</li>
     *     <li>numberOfItemsPerClass (mutex maxNumber) - mutually exclusive with "maxNumber", in the balanced vectorization case this configures the number of items drawn per class</li>
     *     <li>maxNumber (mutex numberOfItemsPerClass) - max number of items to draw for vectorization from the db; less than 0 means no upper boundary</li>
     *     <li>numberOfFolds - number of folds to be made for CV; attempts to make equally sized folds</li>
     *     <li>windowSize - size of window for ngram-production; if 1 or lower it will just use unigrams</li>
     *     <li>countWords - may be true or false; true produces BoW-vectors where the number of occurrences of a word is counted</li>
     *     <li>weightWithTfIdf - multiplies feature-vector-values with respective tf-idf-weights calculated separately for each fold</li>
     *     <li>applyLowerCasing - true or false; if true lower-casing is applied to the data from the database before vectorization</li>
     *     <li>removeBracketContent - true or false; if true brackets and content are removed before vectorization</li>
     *     <li>removeStopwords - true or false; enables or disables stop-word-removal (english only atm) before vectorization</li>
     *     <li>applyStemming - true or false; true applies stemming (english only atm) before vectorization</li>
     *     <li>printInfoFile - true or false; true writes an additional file about the vectorization specification</li>
     *     <li>writeAdditionalTar - true or false; if true all generated files are packed inside an additional tar.gz-file</li>
     *     <li>fileOutputPath - destination of the generated vectorization files</li>
     *     <li>byRID - will draw records from DB ordered by random_id column; IMPORTANT if this is specified to be "true" it is absolutely necessary that the same is done in the config for building the used vocabulary!</li>
     * </ul>
     * @param configPath path of the properties file
     * @param methodVersionId should be methodVersionId of the calling method (security measure to prevent processing of input with old method versions)
     */
    public void readProperties(Path configPath, String methodVersionId){
        Properties properties = new Properties();
        File propertiesFile = configPath.toFile();

        try (FileInputStream in = new FileInputStream(propertiesFile)){
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.methodID = properties.getProperty("METHOD_VERSION_ID");
        if (!methodVersionId.equals(methodID)){
            throw new IllegalStateException("METHOD_VERSION_ID MISMATCH: the properties file specifies a different ID");
        }
        this.dbPropertiesFilePath = properties.getProperty("dbPropertiesFilePath");
        this.vocabularyLocation = properties.getProperty("vocabularyLocation");
        this.vocabularyPrefixPath = properties.getProperty("vocabularyPrefixPath");
        if (this.vocabularyPrefixPath != null && this.vocabularyLocation != null) {
            throw new IllegalStateException("It seems like you specified both a vocabularyPrefixPath AND a vocabularyLocation in your property-file. Only one of them is permitted. Prefix for CV-set of vocabs; location for single vocab-use.");
        }
        if (this.vocabularyPrefixPath == null && this.vocabularyLocation == null) {
            throw new IllegalStateException("Exactly one of the properties vocabularyPrefixPath or vocabularyLocation has to be defined.");
        }
        this.recordTableName = properties.getProperty("recordTableName");
        this.featureTableName = properties.getProperty("featureTableName");
        this.featureType = FeatureType.valueOf(properties.getProperty("featureType"));
        String items = properties.getProperty("numberOfItemsPerClass");
        if (!(items == null)){
            System.out.println("REMARK: if you are using balanced conversion numberOfItemsPerClass-property is necessary; if you are using unbalanced conversion maxNumber-property is necessary. If you receive NullPointerExceptions, check that.");
            this.numberOfItemsPerClass = Integer.valueOf(items);
        }

        String window = properties.getProperty("windowSize");
        if (!(window == null)){
            this.windowSize= Integer.valueOf(window);
        }else{
            System.out.println("WARNING: No windowsize given (for ngram-processing): will be set to 1");
            this.windowSize = 1;
        }

        this.numberOfFolds = Integer.valueOf(properties.getProperty("numberOfFolds"));
        String maxNum = properties.getProperty("maxNumber");
        if (!(maxNum == null)){
            System.out.println("REMARK: if you are using balanced conversion numberOfItemsPerClass-property is necessary; if you are using unbalanced conversion maxNumber-property is necessary. If you receive NullPointerExceptions, check that.");
            this.maxNumber = Integer.valueOf(maxNum);
        }
        this.countWords = Boolean.valueOf(properties.getProperty("countWords"));
        this.applyLowerCasing = Boolean.valueOf(properties.getProperty("applyLowerCasing"));
        this.removeBracketContent = Boolean.valueOf(properties.getProperty("removeBracketContent"));
        this.removeStopwords = Boolean.valueOf(properties.getProperty("removeStopwords"));
        this.applyStemming = Boolean.valueOf(properties.getProperty("applyStemming"));
        this.weightWithTfIdf = Boolean.valueOf(properties.getProperty("weightWithTfIdf"));
        this.printInfoFile = Boolean.valueOf(properties.getProperty("printInfoFile"));
        this.writeAdditionalTar = Boolean.valueOf(properties.getProperty("writeAdditionalTar"));
        String byRIDRaw = properties.getProperty("byRID");
        if (byRIDRaw == null) {
            this.byRID = false;
        }else{
            this.byRID = Boolean.valueOf(byRIDRaw);
        }

        this.fileOutputPath = properties.getProperty("fileOutputPath");
        if (this.fileOutputPath == null) {
            this.fileOutputPath = ".";
        }
    }

    @Override
    public String getDbPropertiesFilePath() {
        return this.dbPropertiesFilePath;
    }
}

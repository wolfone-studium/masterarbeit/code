package bous.philipp.dblp.datatools.featureconstruction.vocabulary;

import org.apache.commons.lang3.tuple.MutablePair;

import java.util.*;

/**
 * Holds statistical information about a term like the term itself, an associated index,
 * in which document it was last seen, how many times it occurred per class and so on.
 */
public class TermStatistics {

    final String term;
    int index;
    int lastSeenInDocument;

    /**
     * Key: class-label-index; value: total number of occurances in docs with this class
     * NOTE: Laufzeit erkauft durch Platz
     */
    HashMap<Integer, Integer> occurancesPerClass;

    /**
     * A pair represents following information: left, classification as Integer; right, number different documents this term occured in with this classification.
     * Remark that the term itself is not persisted inside this class (to save heap-space) as it is merely a helper for the enclosing class which saves the term inside
     * a hash-map.
     * NOTE: Laufzeit erkauft durch Platz
     */
    List<MutablePair<Integer, Integer>> numberOfDocsoccurringInPerClass;

    LinkedHashMap<Integer, TermDocAppearanceInfo> appearanceInfo;

    private TermDocAppearanceInfo currentAppearanceInfo;

    TermStatistics(String term, int index, int documentNumber, int documentClass) {
        this.term = term;
        this.index = index;
        this.lastSeenInDocument = documentNumber;
        this.currentAppearanceInfo = new TermDocAppearanceInfo(documentNumber, documentClass);
        this.appearanceInfo = new LinkedHashMap<>();
        this.appearanceInfo.put(documentNumber, this.currentAppearanceInfo);
        this.numberOfDocsoccurringInPerClass = new ArrayList<>();
        this.numberOfDocsoccurringInPerClass.add(new MutablePair<>(documentClass, 1));
        this.occurancesPerClass = new LinkedHashMap<>();
        this.occurancesPerClass.put(documentClass, 1);
    }

    @Override
    public String toString() {
        return "(" + index + "," + totalNumberOfSightings() + ")";
    }

    void addSighting(int documentNumber, int documentClass) {
        MutablePair<Integer, Integer> occurrenceInfo = alreadySeenInDocumentWithClass(documentClass);

        if (occurrenceInfo == null){
            this.numberOfDocsoccurringInPerClass.add(new MutablePair<>(documentClass, 1));
        }else if (this.lastSeenInDocument != documentNumber){
                Integer newRight = occurrenceInfo.getRight() + 1;
                occurrenceInfo.setRight(newRight);
        }

        /*NOTE: remember, remember: is the same as
            if (occurancesPerClass.get(documentClass) == null){
                occurancesPerClass.put(documentClass, 1);
            }else{
                occurancesPerClass.put(documentClass, occurancesPerClass.get(documentClass) + 1);
            }
         */
        occurancesPerClass.merge(documentClass, 1, Integer::sum);

        this.lastSeenInDocument = documentNumber;
        currentAppearanceInfo = appearanceInfo.get(documentNumber);
        if (currentAppearanceInfo == null) {
            currentAppearanceInfo = new TermDocAppearanceInfo(documentNumber, documentClass);
            appearanceInfo.put(documentNumber, currentAppearanceInfo);
        }else{
            if (currentAppearanceInfo.documentClass != documentClass) {
                throw new IllegalStateException("The same document number already occured with different assigned document class!");
            }
            currentAppearanceInfo.incrementAppearances();
        }
    }

    /**
     * Tests if the term has already been seen in a document with the specified class.
     * Attention: Returns the Pair describing the occurrence-information for this term in documents of the
     * specified class for convenience (null if the term has not been in a document of this class) - NOT A BOOLEAN.
     * @param documentClass class which should be tested for
     * @return the Pair<Integer, Integer></Integer,> describing the occurrence-information for this term in documents of the
     *         specified class, null if the term has not been in a document of this class
     */
    private MutablePair<Integer, Integer> alreadySeenInDocumentWithClass(int documentClass){
        for (int i = 0; i < numberOfDocsoccurringInPerClass.size(); i++) {
            MutablePair<Integer, Integer> occurrenceInfo = numberOfDocsoccurringInPerClass.get(i);
            if (occurrenceInfo.getLeft() == documentClass){
                return occurrenceInfo;
            }
        }
        return null;
    }

    /**
     * Returns the total number of sightings for this term (sum of all sightings per individual class).
     * @return total number of sightings
     */
    int totalNumberOfSightings(){
        int total = 0;
        for (Map.Entry<Integer, Integer> entry : occurancesPerClass.entrySet()) {
            total += entry.getValue();
        }
        return total;
    }

    List<MutablePair<Integer, Integer>> occurrencesPerClassAsList(){
        List<MutablePair<Integer, Integer>> result = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : occurancesPerClass.entrySet()) {
            result.add(new MutablePair<>(entry.getKey(), entry.getValue()));
        }
        return result;
    }

    Iterator<TermDocAppearanceInfo> termDocAppearanceInfoIterator(){
        Set<Map.Entry<Integer, TermDocAppearanceInfo>> entries = appearanceInfo.entrySet();
        Iterator<Map.Entry<Integer, TermDocAppearanceInfo>> innerIterator = entries.iterator();
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return innerIterator.hasNext();
            }

            @Override
            public TermDocAppearanceInfo next() {
                return innerIterator.next().getValue();
            }
        };
    }

    public String getTerm() {
        return term;
    }

    public int getIndex() {
        return index;
    }
}

package bous.philipp.dblp.datatools.featureconstruction.vectorization;

import bous.philipp.dblp.datatools.featureconstruction.vocabulary.ScoredVocabulary;
import bous.philipp.dblp.datatools.featureconstruction.vocabulary.TermStatistics;
import bous.philipp.dblp.datatools.util.string.StringUtility;
import org.apache.commons.lang3.ArrayUtils;

import java.util.List;
import java.util.Map;

/**
 * Provides methods for text-vectorization based on variations of Bag of Words-Approaches.
 */
public class BagOfWordsVectorizer {

    /**
     * This method is mainly intended to convert a new text for prediction.
     * Converts a given array of tokens into a BoW-Vector according to a given vocabulary; this
     * method needs the n-gram-window-size as input that was used to create the vocabulary; it should be 1
     * if only unigrams were considered.
     * @param tokens the tokens to be processed
     * @param windowSize the window-size that was used during vocabulary-creation
     * @param doCount if true, the vector will contain counts of word occurrences instead of just a binary encoding if a word was existent in the input-token-array
     * @param vocabulary the vocabulary that should be used for conversion
     * @return a numeric feature-vector for the given input
     */
    public static double[] convertToVector(String[] tokens, int windowSize, boolean doCount, ScoredVocabulary vocabulary){
        double[] bowVector = new double[vocabulary.size()];
        String[] tokensNgrams;

        if (windowSize > 1){
            String[] ngrams = StringUtility.convertToNGrams(tokens, windowSize, " ");
            tokensNgrams = ArrayUtils.addAll(tokens, ngrams);
        }else{
            tokensNgrams = tokens;
        }


        for (int i = 0; i < tokensNgrams.length; i++) {
            String token = tokensNgrams[i];
            TermStatistics vocabElement = vocabulary.get(token);
            if (vocabElement != null){
                if (doCount){
                    bowVector[vocabElement.getIndex()-1]++;
                }else{
                    bowVector[vocabElement.getIndex()-1] = 1;
                }
            }
        }

        return bowVector;
    }

    /**
     * This method is mainly intended to convert a new text for prediction.
     * Converts a given array of tokens into a BoW-Vector according to a given vocabulary; this
     * method needs the n-gram-window-size as input that was used to create the vocabulary; it should be 1
     * if only unigrams were considered.
     * @param tokenList the tokens to be processed
     * @param windowSize the window-size that was used during vocabulary-creation
     * @param doCount if true, the vector will contain counts of word occurrences instead of just a binary encoding if a word was existent in the input-token-array
     * @param vocabulary the vocabulary that should be used for conversion
     * @return a numeric feature-vector for the given input
     */
    public static double[] convertToVector(List<String> tokenList, int windowSize, boolean doCount, ScoredVocabulary vocabulary){
        double[] bowVector = new double[vocabulary.size()];
        String[] tokensNgrams;

        String[] tokens = tokenList.toArray(new String[0]);

        if (windowSize > 1){
            String[] ngrams = StringUtility.convertToNGrams(tokens, windowSize, " ");
            tokensNgrams = ArrayUtils.addAll(tokens, ngrams);
        }else{
            tokensNgrams = tokens;
        }


        for (int i = 0; i < tokensNgrams.length; i++) {
            String token = tokensNgrams[i];
            TermStatistics vocabElement = vocabulary.get(token);
            if (vocabElement != null){
                if (doCount){
                    bowVector[vocabElement.getIndex()-1]++;
                }else{
                    bowVector[vocabElement.getIndex()-1] = 1;
                }
            }
        }

        return bowVector;
    }

    /**
     * Converts a given target and text to a String conforming to the SvmLight-format with regards to
     * a given vocabulary using a bag of words approach. It can be specified if the existence of a word should just be
     * marked with a 1 or if the number of occurrences should be counted as well.
     * @param target the target value as required by the SvmLight-format
     * @param tokens the tokens to be converted
     * @param windowSize enables using n-grams with windowSize = n
     * @param doCount true if the number of occurrences should be saved to the resulting vector, false if 1 shall just encode "exists in the given text".
     * @param vocabulary the vocabulary in question
     * @return a String conforming to the SvmLight-format
     */
    public static String convertToLabeledSvmLightVectorString(int target, String[] tokens, int windowSize, boolean doCount, ScoredVocabulary vocabulary){
        StringBuilder svmLightVector = new StringBuilder();

        svmLightVector.append(target).append(" ");

        int[] bowVector = new int[vocabulary.size()];
        String[] tokensNGrams;

        if (windowSize > 1){
            String[] ngrams = StringUtility.convertToNGrams(tokens, windowSize, " ");
            tokensNGrams = ArrayUtils.addAll(tokens, ngrams);
        }else{
            tokensNGrams = tokens;
        }

        for (int i = 0; i < tokensNGrams.length; i++) {
            String token = tokensNGrams[i];
            TermStatistics vocabElement = vocabulary.get(token);
            if (vocabElement != null){
                if (doCount){
                    bowVector[vocabElement.getIndex()-1]++;
                }else{
                    bowVector[vocabElement.getIndex()-1] = 1;
                }
            }
        }

        for (int i = 0; i < bowVector.length; i++) {
            int occurrenceValue = bowVector[i];
            if (occurrenceValue > 0){
                svmLightVector.append(i + 1).append(":").append(occurrenceValue).append(" ");
            }
        }

        return svmLightVector.toString().trim();
    }

    /**
     * Converts a given target and text to a String conforming to the SvmLight-format with regards to
     * a given vocabulary. This version of the method accepts an additional map with term weightings that are applied to the final vector. It can be specified if the existence of a word should just be
     * marked with a 1 or if the number of occurrences should be counted as well.
     * @param target the target value as required by the SvmLight-format
     * @param tokens the tokens to be converted
     * @param windowSize enables using n-grams with windowSize = n
     * @param doCount true if the number of occurrences should be saved to the final String, false if 1 shall just encode "exists in the given text".
     * @param weights the weightings for terms in form of a Map "Term":"Weight"
     * @param vocabulary the vocabulary in question
     * @return a String conforming to the SvmLight-format
     */
    public static String convertToLabeledSvmLightVectorString(int target, String[] tokens, int windowSize, Map<String, Double> weights, boolean doCount, ScoredVocabulary vocabulary){
        StringBuilder svmLightVector = new StringBuilder();

        svmLightVector.append(target).append(" ");

        int[] bowVector = new int[vocabulary.size()];
        String[] tokensNGrams;

        if (windowSize > 1){
            String[] nGrams = StringUtility.convertToNGrams(tokens, windowSize, " ");
            tokensNGrams = ArrayUtils.addAll(tokens, nGrams);
        }else{
            tokensNGrams = tokens;
        }

        double[] tempWeights = new double[vocabulary.size()];

        for (int i = 0; i < tokensNGrams.length; i++) {
            String token = tokensNGrams[i];
            TermStatistics vocabElement = vocabulary.get(token);
            if (vocabElement != null){
                if (weights.get(token) != null){
                    tempWeights[vocabElement.getIndex() - 1] = weights.get(token);
                }else {
                    tempWeights[vocabElement.getIndex() - 1] = 0.0;
                }
                if (doCount){
                    bowVector[vocabElement.getIndex() - 1]++;
                }else{
                    bowVector[vocabElement.getIndex() - 1] = 1;
                }
            }
        }

        for (int i = 0; i < bowVector.length; i++) {
            double occurrenceValue = bowVector[i] * tempWeights[i];
            if (occurrenceValue > 0){
                svmLightVector.append(i + 1).append(":").append(occurrenceValue).append(" ");
            }
        }
        return svmLightVector.toString().trim();
    }


}

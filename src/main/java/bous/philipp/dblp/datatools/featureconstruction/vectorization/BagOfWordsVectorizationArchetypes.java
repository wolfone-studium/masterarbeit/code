package bous.philipp.dblp.datatools.featureconstruction.vectorization;

import bous.philipp.dblp.datatools.datamodel.CsClassification;
import bous.philipp.dblp.datatools.datamodel.Feature;
import bous.philipp.dblp.datatools.datamodel.FeatureType;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;
import bous.philipp.dblp.datatools.db.reader.DatabaseReader;
import bous.philipp.dblp.datatools.db.reader.SelectionResult;
import bous.philipp.dblp.datatools.featureconstruction.vocabulary.ScoredVocabulary;
import bous.philipp.dblp.datatools.featurescoring.TfIdfAnalyzer;
import bous.philipp.dblp.datatools.tokenization.LuceneStandardTokenizerAdapter;
import bous.philipp.dblp.datatools.tokenization.Tokenizer;
import bous.philipp.dblp.datatools.util.database.DataSourceFactory;
import bous.philipp.dblp.datatools.util.fileprocessing.Tar;
import bous.philipp.dblp.datatools.util.string.StringUtility;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Contains methods to simplify common tasks of vectorization for
 * the context of the thesis for which this code was written.
 * An external user should know that this class lacks the feature
 * to customize the tokenization process via config file as the need never
 * did arise during writing the thesis. Nevertheless this can be done
 * by hooking into the process earlier by directly using {@link BagOfWordsVectorizer}.
 */
public class BagOfWordsVectorizationArchetypes {

    private static Tokenizer getStandardTokenizer(){
        return new LuceneStandardTokenizerAdapter();
    }
    /**
     * #######
     * IMPORTANT REMARK: does not possess the ability to process CV with different vocabularies per fold as this necessity did never
     * arise until now; nonetheless this is rather the consequence of the still not needed implementation of a method
     * to build sets of vocabularies for CV in an unbalanced "as-is"-way. This is a result of this method being
     * primarily used to convert a whole table into a single svmlight-file.
     * #######
     * Reads data for a single feature from a database and converts it to svmlight
     * files usable for cross-validation.
     * It does so in an unbalanced fashion, meaning that data is drawn from the
     * specified database as is without the attempt to get a balanced sampling.
     * The necessary information is read from a configuration file.
     * This approach is chosen to have to make only minor changes in different tests and to
     * have a documentation of done tests in form of these files. The files have to be in the java-properties-file format
     * and expects following entries::
     * <ul>
     *      <li>METHOD_VERSION_ID - id of the this methods version (given as final string in the code)</li>
     *      <li>dbPropertiesFilePath - path properties-file with information about the jdbc database connection (only mysql supported atm)</li>
     *      <li>vocabularyLocation - path to the vocabulary-file to be used for conversion</li>
     *      <li>recordTableName - name of the record-table to be used</li>
     *      <li>featureTableName - name of the corresponding feature table</li>
     *      <li>featureType - feature to be extracted, allowed values are specified through the enum of the same name</li>
     *      <li>maxNumber - max number of items to draw for vectorization from the db; greater 0 means no upper boundary</li>
     *      <li>numberOfFolds - number of folds to be made for CV; attempts to make equally sized folds</li>
     *      <li>windowSize - size of window for ngram-production; if 1 or lower will just use unigrams</li>
     *      <li>countWords - may be true or false; true produces bow-vectors where the number of occurrences of a word is counted</li>
     *      <li>weightWithTfIdf - multiplies feature-vector-values with respective tf-idf-weights calculated separately for each fold</li>
     *      <li>applyLowerCasing - true or false; if true lower-casing is applied to the data from the database before vectorization</li>
     *      <li>removeBracketContent - true or false; if true brackets and content are removed before vectorization</li>
     *      <li>removeStopwords - true or false; enables or disables stop-word-removal (english only atm) before vectorization</li>
     *      <li>applyStemming - true or false; true applies stemming (english only atm) before vectorization</li>
     *      <li>printInfoFile - true or false; true writes an additional file about the vectorization specification</li>
     *      <li>writeAdditionalTar - true or false; if true all generated files are packed inside an additional tar.gz-file</li>
     *      <li>fileOutputPath - destination of the generated vectorization files</li>
     * </ul>
     */
    public static void convertSingleFeatureToSVMLightCrossValidationSetsUnbalanced() {
        final String METHOD_VERSION_ID = "csctsvmlcvsu-20.06.2019-1";

        Path configPath = askForConfigPath();
        String configFilenameWithoutExtension = FilenameUtils.getBaseName(configPath.toString());
        SingleColumnCVSetProperties props = getSingleColumnCVSetProperties(METHOD_VERSION_ID, configPath);

        copyConfigToOutputDestinationIfNecessary(configPath, props);

        convertSingleFeatureToSVMLightCrossValidationSetsUnbalanced(props, configFilenameWithoutExtension, null);
    }

    /**
     * #######
     * IMPORTANT REMARK: does not possess the ability to process CV with different vocabularies per fold as this necessity did never
     * arise until now; nonetheless this is rather the consequence of the still not needed implementation of a method
     * to build sets of vocabularies for CV in an unbalanced "as-is"-way. This is a result of this method being
     * primarily used to convert a whole table into a single svmlight-file.
     * #######
     * Reads data for a single feature from a database and converts it to svmlight
     * files usable for cross-validation.
     * It does so in an unbalanced fashion, meaning that data is drawn from the
     * specified database as is without the attempt to get a balanced sampling.
     * The necessary information is read from a configuration file.
     * This approach is chosen to have to make only minor changes in different tests and to
     * have a documentation of done tests in form of these files. The files have to be in the java-properties-file format
     * and expects following entries::
     * <ul>
     *      <li>METHOD_VERSION_ID - id of the this methods version (given as final string in the code)</li>
     *      <li>dbPropertiesFilePath - path properties-file with information about the jdbc database connection (only mysql supported atm)</li>
     *      <li>vocabularyLocation - path to the vocabulary-file to be used for conversion</li>
     *      <li>recordTableName - name of the record-table to be used</li>
     *      <li>featureTableName - name of the corresponding feature table</li>
     *      <li>featureType - feature to be extracted, allowed values are specified through the enum of the same name</li>
     *      <li>maxNumber - max number of items to draw for vectorization from the db; greater 0 means no upper boundary</li>
     *      <li>numberOfFolds - number of folds to be made for CV; attempts to make equally sized folds</li>
     *      <li>windowSize - size of window for ngram-production; if 1 or lower will just use unigrams</li>
     *      <li>countWords - may be true or false; true produces bow-vectors where the number of occurrences of a word is counted</li>
     *      <li>weightWithTfIdf - multiplies feature-vector-values with respective tf-idf-weights calculated separately for each fold</li>
     *      <li>applyLowerCasing - true or false; if true lower-casing is applied to the data from the database before vectorization</li>
     *      <li>removeBracketContent - true or false; if true brackets and content are removed before vectorization</li>
     *      <li>removeStopwords - true or false; enables or disables stop-word-removal (english only atm) before vectorization</li>
     *      <li>applyStemming - true or false; true applies stemming (english only atm) before vectorization</li>
     *      <li>printInfoFile - true or false; true writes an additional file about the vectorization specification</li>
     *      <li>writeAdditionalTar - true or false; if true all generated files are packed inside an additional tar.gz-file</li>
     *      <li>fileOutputPath - destination of the generated vectorization files</li>
     * </ul>
     * @param props the properties to be used for configuration
     * @param filenamePrefix prefix determining the first part of the filenames for files written by this method; do NOT include path to the files!
     * @param posthook a post-hook for the vectorization process. May be null.
     */
    public static void convertSingleFeatureToSVMLightCrossValidationSetsUnbalanced(SingleColumnCVSetProperties props, String filenamePrefix, UnbalancedVectorizationFoldPostHook posthook) {
        final String METHOD_VERSION_ID = "csctsvmlcvsu-20.06.2019-1";

        Connection connection = getMySqlConnection(props);

        String fileExtension = "svmlight";

        int numberOfItems;

        if (props.getMaxNumber() > 0){
            numberOfItems = props.getMaxNumber();
        }else{
            numberOfItems = getNumberOfItems(props, Objects.requireNonNull(connection));
        }

        if (posthook == null) {
            posthook = getDoNothingUnbalancedPosthook();
        }

        ScoredVocabulary vocabulary = ScoredVocabulary.fromFile(Objects.requireNonNull(props.getVocabularyLocation()));
        EnumSet<FeatureType> excludedFeatures = EnumSet.complementOf(EnumSet.of(props.getFeatureType()));
        int itemsPerFold = numberOfItems / props.getNumberOfFolds();
        int nextId = 0;

        List<File> allFiles = new ArrayList<>();
        List<File> vectorFiles = new ArrayList<>();

        System.out.println("Start processing records.");
        for (int i = 0; i < props.getNumberOfFolds(); i++){
            System.out.println("Processing records for fold " + (i+1));
            String basePathName = props.getFileOutputPath() + File.separator + filenamePrefix + "(" + i + ")";
            File currentFile = new File(basePathName + "." + fileExtension);
            VectorDBMapping mapping = new VectorDBMapping(FilenameUtils.getName(currentFile.getAbsolutePath()), Path.of(basePathName + "_MAPPING.vdbmap"));
            allFiles.add(mapping.getMappingFile());
            allFiles.add(currentFile);
            vectorFiles.add(currentFile);

            TfIdfAnalyzer analysis = null;
            if (props.isWeightWithTfIdf()) {
                analysis = TfIdfAnalysisHelper.tfidfAnalysisCVUnbalanced(connection, props.getFeatureType(), props, itemsPerFold, nextId, props.getWindowSize());
                analysis.finish();
            }
            int mappingIndex = 1;
            try (FileWriter foldWriter = new FileWriter(currentFile); BufferedWriter foldBufferedWriter = new BufferedWriter(foldWriter)){

                SelectionResult result = DatabaseReader.selectRecordsFromDB(connection,
                        excludedFeatures,
                        props.getRecordTableName(),
                        props.getFeatureTableName(),
                        nextId,
                        itemsPerFold,
                        CsClassification.CS_AND_NON_CS,
                        props.isByRID());
                List<RecordDocument> records = result.getRecords();
                nextId = result.getLastId() + 1;
                if (records.size() < itemsPerFold){
                    System.out.println("WARNING: fold " + i + " does only contain " + records.size() + " records but " + itemsPerFold + " were expected.");
                }

                String currentFeatureValue;

                for (int j = 0; j < records.size(); j++) {
                    RecordDocument recordDocument = records.get(j);
                    List<Feature> features = recordDocument.getFeatures();
                    if (features != null && features.size() == 1) {
                        currentFeatureValue = recordDocument.getFeatures().get(0).getValue();
                    }else{
                        continue;
                    }

                    List<String> tokens = getStandardTokenizer().tokenize(currentFeatureValue);
                    tokens = StringUtility.processTokens(tokens, props.isApplyLowerCasing(), props.isRemoveStopwords(), props.isApplyStemming());

                    int classification = recordDocument.getCsClassificationStatus();
                    if (classification == 0){
                        System.out.println("Warning: record with unknown classification found; this should not happen in Data intended for cross-validation; it will be assumed to be NON_CS but it is strongly encouraged to verify correctness of input!");
                    }
                    if (classification == -1){
                        classification = 0;
                    }
                    String[] tokenArray = tokens.toArray(new String[0]);

                    if (props.isWeightWithTfIdf()){
                        foldBufferedWriter.write(
                                BagOfWordsVectorizer.convertToLabeledSvmLightVectorString(
                                        classification,
                                        tokenArray,
                                        props.getWindowSize(),
                                        Objects.requireNonNull(analysis).tfIdfsForDocument(j+1, tokenArray),
                                        props.isCountWords(),
                                        vocabulary) + "\n");
                    }else{
                        foldBufferedWriter.write(BagOfWordsVectorizer.convertToLabeledSvmLightVectorString(classification, tokenArray, props.getWindowSize(), props.isCountWords(), vocabulary) + "\n");
                    }

                    mapping.add(mappingIndex++, props.getRecordTableName(), String.valueOf(recordDocument.getId()));
                }

                mapping.write();
                posthook.afterRecordsProcessed(i, records, vocabulary);

            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Finished processing records for fold " + (i+1));
        }
        System.out.println("Finished processing records.");

        System.out.println("Write combined file.");
        //Write combined version of vector-files.
        String combinedFileNameWithoutExtension = props.getFileOutputPath() + File.separator + filenamePrefix + "_ALL.";
        File combinedFile = new File(combinedFileNameWithoutExtension + fileExtension);
        try(FileWriter fwCombined = new FileWriter(combinedFile);
            BufferedWriter bwCombined = new BufferedWriter(fwCombined)
        ){

            for (int i = 0; i < vectorFiles.size(); i++) {
                File file = vectorFiles.get(i);
                try(FileReader fr = new FileReader(file);
                    BufferedReader brVectors = new BufferedReader(fr)){

                    String currentLineVectors = brVectors.readLine();
                    while (currentLineVectors != null) {
                        bwCombined.write(currentLineVectors + "\n");
                        currentLineVectors = brVectors.readLine();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Finished writing combined file.");
        if (props.isPrintInfoFile()){
            System.out.println("Write info-file.");
            File infoFile = new File(props.getFileOutputPath() + "\\" + filenamePrefix + "_INFO.txt");
            allFiles.add(infoFile);
            try (FileWriter infoWriter = new FileWriter(infoFile);
                 BufferedWriter infoBufferedWriter = new BufferedWriter(infoWriter)){
                infoBufferedWriter.write("-----Crossvalidation dataset metadata-----\n");
                infoBufferedWriter.write("METHOD_VERSION_ID:                     " + METHOD_VERSION_ID + "\n");
                infoBufferedWriter.write("Specified number of folds:             " + props.getNumberOfFolds() + "\n");
                infoBufferedWriter.write("Used vocabulary file name:             " + FilenameUtils.getName(props.getVocabularyLocation()) + "\n");
                infoBufferedWriter.write("Type of processed features:            " + props.getFeatureType().name() + "\n\n");
                writeBasicInputProcessingInfo(props, infoBufferedWriter);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Finished writing info-file.");
        }

        if (props.isWriteAdditionalTar()){
            System.out.println("Write archive.");
            try {
                Tar.compress(props.getFileOutputPath() + "\\" + filenamePrefix + ".tar.gz", allFiles);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Finished writing archive.");
        }
    }


    /**
     * Reads data for a single feature from a database and converts it to svmlight
     * files usable for cross-validation.
     * This method is similar to {@link #convertSingleFeatureToSVMLightCrossValidationSetsUnbalanced()} with
     * the difference that it takes no maxNumber of items but a number of items per class and
     * will try to draw this exact amount of examples from the database to get a balanced
     * data-set.
     * Note that this method is able to create CV-sets based on an infividual vocabulary for each set which is not
     * the case for the unbalanced versions.
     * The necessary information is again read from a configuration file which is quite similar
     * to the above mentioned method.
     * It expects the following entries:
     * <ul>
     *      <li>METHOD_VERSION_ID - id of the this methods version (given as final string in the code)</li>
     *      <li>dbPropertiesFilePath - path properties-file with information about the jdbc database connection (only mysql supported atm)</li>
     *      <li>vocabularyLocation - path to the vocabulary-file to be used for conversion</li>
     *      <li>vocabularyPrefixPath - mutually exclusive with vocabularyLocation; used in CV if a different vocabulary should be used for each fold; prefix path means absolute path including filename without numbering of the vocab-set and file extension
     *        . For example path/to/vocabulary if the vocabulary files are of form: path/to/vocabulary(0).fbowvoc.</li>
     *      <li>recordTableName - name of the record-table to be used</li>
     *      <li>featureTableName - name of the corresponding feature table</li>
     *      <li>featureType - feature to be extracted, allowed values are specified through the enum of the same name</li>
     *      <li>numberOfItemsPerClass - number of items that are attempted to be drawn from each class</li>
     *      <li>numberOfFolds - number of folds to be made for CV; attempts to make equally sized folds</li>
     *      <li>windowSize - size of window for ngram-production; if 1 or lower will just use unigrams</li>
     *      <li>countWords - may be true or false; true produces bow-vectors where the number of occurrences of a word is counted</li>
     *      <li>weightWithTfIdf - multiplies feature-vector-values with respective tf-idf-weights calculated separately for each fold</li>
     *      <li>applyLowerCasing - true or false; if true lower-casing is applied to the data from the database before vectorization</li>
     *      <li>removeBracketContent - true or false; if true brackets and content are removed before vectorization</li>
     *      <li>removeStopwords - true or false; enables or disables stop-word-removal (english only atm) before vectorization</li>
     *      <li>applyStemming - true or false; true applies stemming (english only atm) before vectorization</li>
     *      <li>printInfoFile - true or false; true writes an additional file about the vectorization specification</li>
     *      <li>writeAdditionalTar - true or false; if true all generated files are packed inside an additional tar.gz-file</li>
     *      <li>fileOutputPath - destination of the generated vectorization files</li>
     * </ul>
     * @param props the properties to be used for configuration
     * @param filenamePrefix prefix determining the first part of the filenames for files written by this method; do NOT include path to the files!
     * @param posthook a post-hook for the vectorization process. May be null.
     */
    //NOTE: das filenamePrefix ist das was ohne extension angegeben werden sollte um zu wissen, wie zu speichernde files benannt werden
    public static void convertSingleFeatureToSVMLightCrossValidationSetsBalanced(SingleColumnCVSetProperties props, String filenamePrefix, @Nullable BalancedVectorizationFoldPostHook posthook) {
        final String METHOD_VERSION_ID = "csctsvmlcvs-20.06.2019-1";

        Connection connection = getMySqlConnection(props);
        String fileExtension = "svmlight";
        ScoredVocabulary vocabulary = null;
        boolean newVocabularyEachFold = false;

        if (props.getVocabularyLocation() != null) {
            vocabulary = ScoredVocabulary.fromFile(props.getVocabularyLocation());
        } else {
            newVocabularyEachFold = true;
        }
        if (posthook == null) {
            posthook = getDoNothingBalancedPosthook();
        }

        EnumSet<FeatureType> excludedFeatures = EnumSet.complementOf(EnumSet.of(props.getFeatureType()));
        int itemsPerFoldAndClass = props.getNumberOfItemsPerClass() / props.getNumberOfFolds();
        int nextIdCs = 0;
        int nextIdNonCs = 0;

        List<File> allFiles = new ArrayList<>();
        List<File> vectorFiles = new ArrayList<>();
        System.out.println("Start processing records.");
        for (int i = 0; i < props.getNumberOfFolds(); i++){
            if (newVocabularyEachFold) {
                vocabulary = ScoredVocabulary.fromFile(props.getVocabularyPrefixPath() + "(" + i + ").fbowvoc");
            }
            System.out.println("Using vocabulary: " + vocabulary.getId());

            System.out.println("Processing records for fold " + (i+1));
            String basePathName = props.getFileOutputPath() + File.separator + filenamePrefix + "(" + i + ")";
            File currentFile = new File(basePathName + "." + fileExtension);
            VectorDBMapping mapping = new VectorDBMapping(FilenameUtils.getName(currentFile.getAbsolutePath()), Path.of(basePathName + "_MAPPING.vdbmap"));
            allFiles.add(mapping.getMappingFile());
            allFiles.add(currentFile);
            vectorFiles.add(currentFile);

            TfIdfAnalyzer analysis = null;

            if (props.isWeightWithTfIdf()) {
                analysis = TfIdfAnalysisHelper.tfidfAnalysisBalanced(
                        connection,
                        props.getFeatureType(),
                        props,
                        itemsPerFoldAndClass,
                        nextIdNonCs,
                        nextIdCs,
                        props.getWindowSize());
                analysis.finish();
            }

            try (FileWriter foldWriter = new FileWriter(currentFile);
                 BufferedWriter foldBufferedWriter = new BufferedWriter(foldWriter)){

                int writtenRecordsCounter = 1;
                SelectionResult result = DatabaseReader.selectRecordsFromDB(connection,
                        excludedFeatures,
                        props.getRecordTableName(),
                        props.getFeatureTableName(),
                        nextIdNonCs,
                        itemsPerFoldAndClass,
                        CsClassification.NON_CS_ONLY,
                        props.isByRID());
                List<RecordDocument> recordsNonCs = result.getRecords();
                nextIdNonCs = result.getLastId() + 1;
                result = null;

                if (recordsNonCs.size() < itemsPerFoldAndClass){
                    System.out.println("WARNING: fold " + i + " does only contain " + recordsNonCs.size() + " non-cs records but " + itemsPerFoldAndClass + " were expected.");
                }

                String currentFeatureValue;

                int numberOfNonCsRecords = recordsNonCs.size();

                for (int j = 0; j < numberOfNonCsRecords; j++) {
                    RecordDocument recordDocument = recordsNonCs.get(j);
                    List<Feature> features = recordDocument.getFeatures();
                    if (features != null && features.size() == 1) {
                        currentFeatureValue = recordDocument.getFeatures().get(0).getValue();
                    }else{
                        continue;
                    }
                    List<String> tokens = getStandardTokenizer().tokenize(currentFeatureValue);
                    tokens = StringUtility.processTokens(tokens, props.isApplyLowerCasing(), props.isRemoveStopwords(), props.isApplyStemming());
                    String[] tokenArray = tokens.toArray(new String[0]);
                    if (props.isWeightWithTfIdf()){
                        foldBufferedWriter.write(
                                BagOfWordsVectorizer.convertToLabeledSvmLightVectorString(
                                        0,
                                        tokenArray,
                                        props.getWindowSize(),
                                        Objects.requireNonNull(analysis).tfIdfsForDocument(j+1, tokenArray),
                                        props.isCountWords(),
                                        vocabulary)
                                        + "\n");
                    }else{
                        foldBufferedWriter.write(
                                BagOfWordsVectorizer.convertToLabeledSvmLightVectorString(
                                        0,
                                        tokenArray,
                                        props.getWindowSize(),
                                        props.isCountWords(),
                                        vocabulary)
                                        + "\n");
                    }
                    mapping.add(writtenRecordsCounter, props.getRecordTableName(), String.valueOf(recordDocument.getId()));
                    writtenRecordsCounter++;
                }

                posthook.afterNegativesProcessed(i, recordsNonCs, vocabulary);

                recordsNonCs = null;

                result = DatabaseReader.selectRecordsFromDB(connection,
                        excludedFeatures,
                        props.getRecordTableName(),
                        props.getFeatureTableName(),
                        nextIdCs,
                        itemsPerFoldAndClass,
                        CsClassification.CS_ONLY,
                        props.isByRID());
                List<RecordDocument> recordsCs = result.getRecords();
                nextIdCs = result.getLastId() + 1;

                if (recordsCs.size() < itemsPerFoldAndClass){
                    System.out.println("WARNING: fold " + i + " does only contain " + recordsCs.size() + " cs records but " + itemsPerFoldAndClass + " were expected.");
                }

                for (int j = 0; j < recordsCs.size(); j++) {
                    RecordDocument recordDocument = recordsCs.get(j);
                    List<Feature> features = recordDocument.getFeatures();
                    if (features != null && features.size() == 1) {
                        currentFeatureValue = recordDocument.getFeatures().get(0).getValue();
                    }else{
                        continue;
                    }
                    List<String> tokens = getStandardTokenizer().tokenize(currentFeatureValue);
                    tokens = StringUtility.processTokens(tokens, props.isApplyLowerCasing(), props.isRemoveStopwords(), props.isApplyStemming());
                    String[] tokenArray = tokens.toArray(new String[0]);
                    if (props.isWeightWithTfIdf()){
                        foldBufferedWriter.write(
                                BagOfWordsVectorizer.convertToLabeledSvmLightVectorString(
                                        1,
                                        tokenArray,
                                        props.getWindowSize(),
                                        Objects.requireNonNull(analysis).tfIdfsForDocument(j + 1 + numberOfNonCsRecords, tokenArray),
                                        props.isCountWords(), vocabulary)
                                        + "\n");
                    }else{
                        foldBufferedWriter.write(
                                BagOfWordsVectorizer.convertToLabeledSvmLightVectorString(
                                        1,
                                        tokenArray,
                                        props.getWindowSize(),
                                        props.isCountWords(),
                                        vocabulary)
                                        + "\n");
                    }
                    mapping.add(writtenRecordsCounter, props.getRecordTableName(), String.valueOf(recordDocument.getId()));
                    writtenRecordsCounter++;
                }

                mapping.write();

                posthook.afterPositivesProcessed(i, recordsCs, vocabulary);
                recordsCs = null;

                posthook.finalize(i, vocabulary);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Finished processing records for fold " + (i+1));
        }
        System.out.println("Finished processing records.");

        System.out.println("Write combined file.");
        //Write combined version of vector-files.
        String combinedFileNameWithoutExtension = props.getFileOutputPath() + File.separator + filenamePrefix + "_ALL.";
        File combinedFile = new File(combinedFileNameWithoutExtension + fileExtension);
        try(FileWriter fwCombined = new FileWriter(combinedFile);
            BufferedWriter bwCombined = new BufferedWriter(fwCombined)
        ){

            for (int i = 0; i < vectorFiles.size(); i++) {
                File file = vectorFiles.get(i);
                try(FileReader fr = new FileReader(file);
                    BufferedReader brVectors = new BufferedReader(fr)){

                    String currentLineVectors = brVectors.readLine();
                    while (currentLineVectors != null) {
                        bwCombined.write(currentLineVectors + "\n");
                        currentLineVectors = brVectors.readLine();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Finished writing combined file.");

        if (props.isPrintInfoFile()){
            System.out.println("Write info-file.");
            File infoFile = new File(props.getFileOutputPath() + "\\" + filenamePrefix + "_INFO.txt");
            allFiles.add(infoFile);
            try (FileWriter infoWriter = new FileWriter(infoFile);
                 BufferedWriter infoBufferedWriter = new BufferedWriter(infoWriter)){
                infoBufferedWriter.write("-----Crossvalidation dataset metadata-----\n");
                infoBufferedWriter.write("METHOD_VERSION_ID:                     " + METHOD_VERSION_ID + "\n");
                infoBufferedWriter.write("Specified number of folds:             " + props.getNumberOfFolds() + "\n");
                infoBufferedWriter.write("Specified number of items per class:   " + props.getNumberOfItemsPerClass() + "\n");
                infoBufferedWriter.write("Used vocabulary file name:             " + FilenameUtils.getName(props.getVocabularyLocation()) + "\n");
                infoBufferedWriter.write("Type of processed features:            " + props.getFeatureType().name() + "\n\n");
                writeBasicInputProcessingInfo(props, infoBufferedWriter);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Finished writing info-file.");
        }
        if (props.isWriteAdditionalTar()){
            System.out.println("Write archive.");
            try {
                Tar.compress(props.getFileOutputPath() + "\\" + filenamePrefix + ".tar.gz", allFiles);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Finished writing archive.");
        }

    }

    /**
     * Reads data for a single feature from a database and converts it to svmlight
     * files usable for cross-validation.
     * This method is similar to {@link #convertSingleFeatureToSVMLightCrossValidationSetsUnbalanced()} with
     * the difference that it takes no maxNumber of items but a number of items per class and
     * will try to draw this exact amount of examples from the database to get a balanced
     * data-set.
     * The necessary information is again read from a configuration file which is quite similar
     * to the above mentioned method.
     * It expects the following entries:
     * <ul>
     *      <li>METHOD_VERSION_ID - id of the this methods version (given as final string in the code)</li>
     *      <li>dbPropertiesFilePath - path properties-file with information about the jdbc database connection (only mysql supported atm)</li>
     *      <li>vocabularyLocation - path to the vocabulary-file to be used for conversion</li>
     *      <li>recordTableName - name of the record-table to be used</li>
     *      <li>featureTableName - name of the corresponding feature table</li>
     *      <li>featureType - feature to be extracted, allowed values are specified through the enum of the same name</li>
     *      <li>numberOfItemsPerClass - number of items that are attempted to be drawn from each class</li>
     *      <li>numberOfFolds - number of folds to be made for CV; attempts to make equally sized folds</li>
     *      <li>windowSize - size of window for ngram-production; if 1 or lower will just use unigrams</li>
     *      <li>countWords - may be true or false; true produces bow-vectors where the number of occurrences of a word is counted</li>
     *      <li>weightWithTfIdf - multiplies feature-vector-values with respective tf-idf-weights calculated separately for each fold</li>
     *      <li>applyLowerCasing - true or false; if true lower-casing is applied to the data from the database before vectorization</li>
     *      <li>removeBracketContent - true or false; if true brackets and content are removed before vectorization</li>
     *      <li>removeStopwords - true or false; enables or disables stop-word-removal (english only atm) before vectorization</li>
     *      <li>applyStemming - true or false; true applies stemming (english only atm) before vectorization</li>
     *      <li>printInfoFile - true or false; true writes an additional file about the vectorization specification</li>
     *      <li>writeAdditionalTar - true or false; if true all generated files are packed inside an additional tar.gz-file</li>
     *      <li>fileOutputPath - destination of the generated vectorization files</li>
     *     <li>byRID - will draw records from DB ordered by random_id column; IMPORTANT if this is specified to be "true" it is absolutely necessary that the same is done in the config for building the used vocabulary!</li>
     * </ul>
     */
    public static void convertSingleFeatureToSVMLightCrossValidationSetsBalanced() {
        final String METHOD_VERSION_ID = "csctsvmlcvs-20.06.2019-1";

        Path configPath = askForConfigPath();
        String configFilenameWithoutExtension = FilenameUtils.getBaseName(configPath.toString());
        SingleColumnCVSetProperties props = getSingleColumnCVSetProperties(METHOD_VERSION_ID, configPath);

        copyConfigToOutputDestinationIfNecessary(configPath, props);

        convertSingleFeatureToSVMLightCrossValidationSetsBalanced(props, configFilenameWithoutExtension, null);
    }

    private static void copyConfigToOutputDestinationIfNecessary(Path configPath, SingleColumnCVSetProperties props) {
        Path destination = Paths.get(props.getFileOutputPath());
        if (!destination.equals(configPath)) {
            try {
                Files.copy(configPath, Paths.get(destination.toString() + "\\" + FilenameUtils.getName(configPath.toString())), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static Path askForConfigPath() {
        Scanner scanner = new Scanner(System.in);

        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current relative path is: " + s);
        System.out.println("Enter conversion-building configuration location: ");
        return Paths.get(scanner.next());
    }

    private static UnbalancedVectorizationFoldPostHook getDoNothingUnbalancedPosthook(){
        return (foldNumber, records, vocabulary) -> {};
    }

    private static BalancedVectorizationFoldPostHook getDoNothingBalancedPosthook(){
        return new BalancedVectorizationFoldPostHook() {
            @Override
            public void afterPositivesProcessed(int foldNumber, List<RecordDocument> positiveRecords, ScoredVocabulary vocabulary) {

            }

            @Override
            public void afterNegativesProcessed(int foldNumber, List<RecordDocument> negativeRecords, ScoredVocabulary vocabulary) {

            }

            @Override
            public void finalize(int foldNumber, ScoredVocabulary vocabulary) {

            }
        };
    }

    @NotNull
    private static SingleColumnCVSetProperties getSingleColumnCVSetProperties(String METHOD_VERSION_ID, Path configPath) {
        SingleColumnCVSetProperties props = new SingleColumnCVSetProperties();
        props.readProperties(configPath, METHOD_VERSION_ID);
        return props;
    }


    private static int getNumberOfItems(SingleColumnCVSetProperties props, Connection connection) {
        int numberOfItems;
        PreparedStatement st;
        try {
            st = connection.prepareStatement("select count(*) from " + props.getRecordTableName() + " where cs_classification=1 or cs_classification=-1");
            ResultSet rs = st.executeQuery();
            rs.next();
            numberOfItems = rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
            numberOfItems = 100000;
            System.out.println("Failed to get count from record-table. Set number of drawn items to 100000.");
        }
        return numberOfItems;
    }

    private static void writeBasicInputProcessingInfo(SingleColumnCVSetProperties props, BufferedWriter infoBufferedWriter) throws IOException {
        infoBufferedWriter.write("-----Basic parameters for input-processing-----\n");
        infoBufferedWriter.write("Terms are counted:                     " + props.isCountWords() + "\n");
        infoBufferedWriter.write("Lower-casing applied:                  " + props.isApplyLowerCasing() + "\n");
        infoBufferedWriter.write("Bracket-removing applied:              " + props.isRemoveBracketContent() + "\n");
        infoBufferedWriter.write("Stopword-removing applied:             " + props.isRemoveStopwords() + "\n");
        infoBufferedWriter.write("Stemming applied:                      " + props.isApplyStemming() + "\n");
        infoBufferedWriter.write("!!!REMARK: this is independent from the basic processing that was applied while building the vocabulary!!!\n");
    }

    @Nullable
    private static Connection getMySqlConnection(SingleColumnCVSetProperties props) {
        MysqlDataSource dataSource = DataSourceFactory.getMySQLDataSource(props.getDbPropertiesFilePath());
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
        } catch (
                SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}

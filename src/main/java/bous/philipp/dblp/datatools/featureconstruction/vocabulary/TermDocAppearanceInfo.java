package bous.philipp.dblp.datatools.featureconstruction.vocabulary;

class TermDocAppearanceInfo {
    final int documentNumber;
    final int documentClass;
    int numberOfAppearances;

    TermDocAppearanceInfo(int documentNumber, int documentClass){
        this.documentNumber = documentNumber;
        this.documentClass = documentClass;
        this.numberOfAppearances = 1;
    }

    public void incrementAppearances(){
        this.numberOfAppearances++;
    }
}

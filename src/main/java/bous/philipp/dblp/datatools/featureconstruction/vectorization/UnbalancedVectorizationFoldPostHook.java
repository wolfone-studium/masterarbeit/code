package bous.philipp.dblp.datatools.featureconstruction.vectorization;

import bous.philipp.dblp.datatools.featureconstruction.vocabulary.ScoredVocabulary;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;

import java.util.List;

/**
 * Interface to allow hooking into the vectorization process in a scenario without
 * forced balancing of classes. Allows additional processing after each fold.
 */
public interface UnbalancedVectorizationFoldPostHook {
    /**
     * Executed at the end of processing of each fold during the vectorization process for unbalanced CV-set creation in
     * {@link BagOfWordsVectorizationArchetypes#convertSingleFeatureToSVMLightCrossValidationSetsUnbalanced()} (bag of words model).
     * @param foldNumber number of the current fold
     * @param records records processed in this fold
     * @param vocabulary the vocabulary used during processing
     */
    void afterRecordsProcessed(int foldNumber, List<RecordDocument> records, ScoredVocabulary vocabulary);
}

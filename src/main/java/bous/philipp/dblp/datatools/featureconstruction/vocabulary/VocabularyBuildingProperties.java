package bous.philipp.dblp.datatools.featureconstruction.vocabulary;

import bous.philipp.dblp.datatools.datamodel.FeatureType;
import bous.philipp.dblp.datatools.util.config.DbDependentExecutionConfig;
import bous.philipp.dblp.datatools.util.logging.LoggerFactory;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * This class's main purpose is to encapsulate the configuration for a run to build a vocabulary,
 * though it can be used to simplify reading information about a specific run that already happened
 * if this information is ever needed in code as it is mainly a container holding information
 * about the specifics of a vocabulary.
 */
class VocabularyBuildingProperties implements DbDependentExecutionConfig {

    @Getter @Setter String methodVersionId;
    @Setter String dbPropertiesFilePath;
    @Getter @Setter String recordTableName;
    @Getter @Setter String featureTableName;
    @Getter @Setter String additionalSteps;
    @Getter @Setter String fileOutputPath;
    @Getter @Setter FeatureType featureType;
    @Getter @Setter Integer numberOfItemsPerClass;
    @Getter @Setter Integer maxNumber;
    @Getter @Setter int minWordLength;
    @Getter @Setter int windowSize;
    @Getter @Setter int folds;
    @Getter @Setter int startingRecordIdCs;
    @Getter @Setter int startingRecordIdNonCs;
    @Getter @Setter boolean additiveVocab;
    @Getter @Setter boolean applyLowerCasing;
    @Getter @Setter boolean removeBracketContent;
    @Getter @Setter boolean removeStopwords;
    @Getter @Setter boolean applyStemming;
    @Getter boolean byRID;

    public VocabularyBuildingProperties(){
        this.startingRecordIdCs = 1;
        this.startingRecordIdNonCs = 1;
    }

    /**
     * Reads configuration for a vocabulary-building-process from a properties file.
     * Requires input of a method-version-id that will be checked against the one
     * present in the read properties file.
     * Such a file can contain the following properties:
     * <ul>
     *     <li>METHOD_VERSION_ID - expected id of the this methods version (given as final string in the code)</li>
     *     <li>dbPropertiesFilePath - path properties-file with information about the jdbc database connection (only mysql supported atm)</li>
     *     <li>recordTableName - name of the record-table to be used</li>
     *     <li>featureTableName - name of the corresponding feature table</li>
     *     <li>featureType - feature to be extracted, allowed values are specified through the enum of the same name</li>
     *     <li>numberOfItemsPerClass - number of items that are attempted to be drawn from each class</li>
     *     <li>minWordLength - tokens under this length will be discarded</li>
     *     <li>windowSize - size for window in ngram-creation 1 or lower will just use unigrams</li>
     *     <li>additiveVocab - true or false; true will add unigrams AND ngrams to the vocab; false will only keep the ngrams</li>
     *     <li>countWords - may be true or false; true produces bow-vectors where the number of occurrences of a word is counted</li>
     *     <li>applyLowerCasing - true or false; if true lowercasing is applied to the data from the database before vectorization</li>
     *     <li>removeBracketContent - true or false; if true brackets and content are removed before vectorization</li>
     *     <li>removeStopwords - true or false; enables or disables stopwordremoval (english only atm) before vectorization</li>
     *     <li>applyStemming - true or false; true applies stemming (english only atm) before vectorization</li>
     *     <li>fileOutputPath - destination of the generated vocabulary-file</li>
     *     <li>additionalSteps - allows to specify usage of vocabulary methods by reflection; this has to be done in postfix-notation with seperation by ";" for single method-calls</li>
     *     <li>byRID - will draw records from DB ordered by random_id column; IMPORTANT if this is specified to be "true" it is absolutely necessary that the same is done in the config for vectorization!</li>
     * </ul>
     * @param configPath path to the properties file
     * @param methodVersionId should be methodVersionId of the calling method (security measure to prevent processing of input with old method versions)
     */
    @Override
    public void readProperties(Path configPath, String methodVersionId) {
        this.readProperties(configPath);
        checkValidity(methodVersionId);
    }

    /**
     * Check if a given id is the same as the one that is set for this config and if one of maxNumber and numberOfItemsPerClass
     * is null.
     * This should kill the entire program in case of failure for security reasons
     * (with IllegalStateException).
     * @param methodVersionId the method-version-id to be checked against the one present in this instance
     */
    void checkValidity(String methodVersionId){
        if (!methodVersionId.equals(this.methodVersionId)){
            throw new IllegalStateException("METHOD_VERSION_ID MISMATCH: the properties file specifies a different ID; props contained *" + this.methodVersionId + "* value supplied to this method was: *"  + methodVersionId + "*.");
        }
        if (maxNumber != null && numberOfItemsPerClass != null){
            throw new IllegalStateException("NumberOfItemsPerClass and maxNumber were both not null! One has to be null.");
        }
    }

    /**
     * Reads properties from a file at a specified path; it is different from {@link VocabularyBuildingProperties#readProperties(Path, String)}
     * (see this for a list of allowed properties)
     * in that it doesn't check the method version ID so it is slightly unsafe but may be a little bit more convenient to use in some situations.
     * @param configPath path where the configuration file lies
     */
    public void readProperties(Path configPath) {
        Logger logger = LoggerFactory.getLogger(VocabularyBuildingProperties.class.getName());
        Properties properties = new Properties();
        File propertiesFile = configPath.toFile();

        try (FileInputStream in = new FileInputStream(propertiesFile)) {
            properties.load(in);
        } catch (FileNotFoundException e) {
            logger.severe("ERROR: File not found! Please check your input!");
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.dbPropertiesFilePath = properties.getProperty("dbPropertiesFilePath");
        this.recordTableName = properties.getProperty("recordTableName");
        this.featureTableName = properties.getProperty("featureTableName");
        this.additionalSteps = properties.getProperty("additionalSteps");
        this.featureType = FeatureType.valueOf(properties.getProperty("featureType"));
        this.methodVersionId = properties.getProperty("METHOD_VERSION_ID");

        String rawNumItPerClass = properties.getProperty("numberOfItemsPerClass");
        String rawMaxNumber = properties.getProperty("maxNumber");
        String rawFolds = properties.getProperty("folds");
        if (rawNumItPerClass != null) {
            numberOfItemsPerClass = Integer.valueOf(rawNumItPerClass);
        }
        if (rawMaxNumber != null) {
            maxNumber = Integer.valueOf(rawMaxNumber);
        }
        if (rawFolds != null) {
            folds = Integer.valueOf(rawFolds);
            if (folds <= 0) {
                System.out.println("No or invalid value for folds given; setting folds=1");
                this.folds = 1;
            }
        }
        if (rawMaxNumber != null && rawNumItPerClass != null) {
            throw new IllegalStateException("You specified a maxNumber and numberOfItemsPerClass, which should not happen. The first is for unbalanced vocab-creation, the second for balanced.");
        }

        minWordLength = Integer.valueOf(properties.getProperty("minWordLength"));
        windowSize = Integer.valueOf(properties.getProperty("windowSize"));
        additiveVocab = Boolean.valueOf(properties.getProperty("additiveVocab"));
        applyLowerCasing = Boolean.valueOf(properties.getProperty("applyLowerCasing"));
        removeBracketContent = Boolean.valueOf(properties.getProperty("removeBracketContent"));
        removeStopwords = Boolean.valueOf(properties.getProperty("removeStopwords"));
        applyStemming = Boolean.valueOf(properties.getProperty("applyStemming"));

        fileOutputPath = properties.getProperty("fileOutputPath");
        if (fileOutputPath == null) {
            fileOutputPath = ".";
        }
        String byRIDRaw = properties.getProperty("byRID");
        if (byRIDRaw == null) {
            byRID = false;
        }else{
            byRID = Boolean.valueOf(byRIDRaw);
        }
    }

    @Override
    public String getDbPropertiesFilePath() {
        return this.dbPropertiesFilePath;
    }
}

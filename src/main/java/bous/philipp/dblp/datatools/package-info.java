/**
 * Contains code developed for the master-thesis (Information Science) of Philipp Bous in 2019 at the University
 * of Trier.
 * First corrector: Prof. Dr. Ralf Schenkel
 * Second corrector: Prof. Dr. Ralph Bergmann
 */
package bous.philipp.dblp.datatools;
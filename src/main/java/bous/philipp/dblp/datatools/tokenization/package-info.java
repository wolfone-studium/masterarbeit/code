/**
 * Provides means of tokenization.
 */
package bous.philipp.dblp.datatools.tokenization;
package bous.philipp.dblp.datatools.tokenization;

import java.util.List;

/**
 * Interface for a tokenizer for Strings.
 */
public interface Tokenizer {
    /**
     * Tokenize the input string.
     * @param input String to be tokenized
     * @return extracted tokens
     */
    List<String> tokenize(String input);
}

package bous.philipp.dblp.datatools.tokenization;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Tokenizer utilizing the {@link StandardTokenizer} of the Apache Lucene Library.
 */
public class LuceneStandardTokenizerAdapter implements Tokenizer{

    @Override
    public List<String> tokenize(String input) {
        List<String> result = new ArrayList<>();

        TokenStream tokenStream = new StandardTokenizer(new StringReader(input));
        CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
        try {
            tokenStream.reset();
            while (tokenStream.incrementToken()){
                String term = charTermAttribute.toString();
                result.add(term);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}

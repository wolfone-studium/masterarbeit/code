package bous.philipp.dblp.datatools.converter.xmlchecks.raw;

import bous.philipp.dblp.datatools.converter.heuristics.classification.ClassificationHeuristic;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.factory.RecordFactory;

import java.sql.Connection;

/**
 * Has the main purpose of injecting custom checks on raw xml-strings in {@link bous.philipp.dblp.datatools.converter.ArchiveToDataBaseConverter#xmlTarArchiveToDataBase(Connection, String, String, String, int, int, int, RawXmlCheck, ClassificationHeuristic, RecordFactory)}.
 */
public interface RawXmlCheck {
    /**
     * Should return true if the given xml is deemed useful/ok for ingestion into a database, false otherwise.
     * @param xml some xml
     * @return true if the given xml is deemed useful/ok for ingestion into a database, false otherwise.
     */
     boolean checkXML(String xml);
}

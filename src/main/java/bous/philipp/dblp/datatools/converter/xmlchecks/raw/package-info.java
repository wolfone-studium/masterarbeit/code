/**
 * Contains classes supporting tar-archive by providing additional means of checking
 * raw input XMLs.
 */
package bous.philipp.dblp.datatools.converter.xmlchecks.raw;
package bous.philipp.dblp.datatools.converter.heuristics.classification;

import bous.philipp.dblp.datatools.datamodel.Feature;
import bous.philipp.dblp.datatools.datamodel.FeatureType;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

/**
 * Extended classification-heuristic for {@link RecordDocument}s that are derived from
 * xml-files for theses as provided by the british library. Will normally classify
 * more records as 1 than {@link BLStrictClassificationHeuristic}.
 * @see ClassificationHeuristic
 */
public class BLExtendedClassificationHeuristic implements ClassificationHeuristic {
    private RecordDocument cachedRecord;
    private String[] cachedNumberCategories;

    /**
     * Considers all thesis that have at least one number-category where the number
     * suffices the regex (00[456]).?(\d*) to be CS, all others are considered as non-cs.
     * @param record the record to be classified
     * @return 1 if CS, -1 if non-CS, 0 if unknown
     */
    @Override
    public int guessClass(RecordDocument record) {
        String[] numberCategories = getNumberCategories(record);
        if (Arrays.stream(numberCategories).anyMatch(category -> category.matches("(00[456]).?(\\d*)"))){
            return 1;
        }
        return -1;
    }

    /**
     * This heuristic will only consider such theses as useful that have a single number-format-category (several prosa-categories might exist).
     * @param record some record
     * @return true if only a single category is in number-format; else false.
     */
    @Override
    public boolean isUseful(@NotNull RecordDocument record) {
        String[] numberCategories = getNumberCategories(record);
        //we want only those entries which have a single number-category to have higher probability of our class estimations to be correct
        return numberCategories.length == 1;
    }

    /**
     * Extracts the categories from a record that are given in number-format.
     * Caches the result to prevent unnecessary stream-operations.
     * @param record the record in question
     * @return string-array of the number-categories.
     */
    private String[] getNumberCategories(RecordDocument record) {
        if (cachedNumberCategories != null && record == cachedRecord){
            return cachedNumberCategories;
        }else{
            cachedRecord = record;
            List<Feature> categoryFeatures = record.getFeaturesByType(FeatureType.CATEGORIES);
            if (categoryFeatures.size() < 1){
                return new String[0];
            }
            StringBuilder categoriesBuilder = new StringBuilder();
            for (int i = 0; i < categoryFeatures.size(); i++) {
                Feature feature =  categoryFeatures.get(i);
                categoriesBuilder.append(feature.getValue()).append(":::");
            }
            String[] categories = categoriesBuilder.toString()
                    .substring(0, categoriesBuilder.length() - 3)
                    .trim()
                    .split(":::");

            String[] numberedCategories = Arrays
                    .stream(categories)
                    .filter(
                            category -> category.matches("(\\d+).?(\\d*)")
                    )
                    .toArray(String[]::new);
            cachedNumberCategories = numberedCategories;
            return numberedCategories;
        }
    }


}

/**
 * Contains classes supporting tar-archive conversion with labeling-heuristic-strategies for
 * documents from different data-sources.
 */
package bous.philipp.dblp.datatools.converter.heuristics.classification;
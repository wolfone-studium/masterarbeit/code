package bous.philipp.dblp.datatools.converter.xmlchecks.raw;

/**
 * Checks a raw xml from the PLoS-One source.
 */
public class PlosOneRawXmlCheck implements RawXmlCheck {
    /**
     * Checks if the xml represents a correction- or retraction-type article. If
     * yes it is not deemed useful.
     * @param xml the xml
     * @return true if the xml represents neither a correction nor a retraction, false otherwise
     */
    @Override
    public boolean checkXML(String xml) {
        return !xml.contains("article-type=\"correction\"") && !xml.contains("article-type=\"retraction\"");
    }
}

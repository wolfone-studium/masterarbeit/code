/**
 * Contains classes that provide methods to convert and write tar-archives for different
 * data-sources to a database.
 */
package bous.philipp.dblp.datatools.converter;
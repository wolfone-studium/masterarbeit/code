package bous.philipp.dblp.datatools.converter.heuristics.classification;

import bous.philipp.dblp.datatools.datamodel.Feature;
import bous.philipp.dblp.datatools.datamodel.FeatureType;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.ArxivRecord;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Set;

/**
 * Classification-heuristic for {@link RecordDocument}s that are derived from
 * xml-files as provided by arXiv.org.
 * @see ClassificationHeuristic
 */
public class ArxivClassificationHeuristic implements ClassificationHeuristic {
    /**
     * Labels an arXiv-record based on the found primary categories.
     * @param record the record to be classified
     * @return 1 if primary categories contain "cs", 0 if no primary categories were found (this shouldn't happen however), -1 otherwise
     */
    @Override
    public int guessClass(RecordDocument record) {
        Set<String> primaryCategories = getPrimaryCategories(record);
        if (primaryCategories == null) {
            return 0;
        }
        if (primaryCategories.contains("cs")) {
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Considers a record useful only if it has a single primary-category.
     * @param record some record
     * @return true if number of distinct primary categories == 1, false otherwise
     */
    @Override
    public boolean isUseful(RecordDocument record) {
        Set<String> primaryCategories = getPrimaryCategories(record);
        return primaryCategories != null && primaryCategories.size() == 1;
    }

    /**
     * Returns the set of primary categories for a given record based on an arXiv-xml.
     * @param record some record
     * @return set of primary categories
     */
    @Nullable
    private Set<String> getPrimaryCategories(RecordDocument record) {
        List<Feature> categoryFeatures = record.getFeaturesByType(FeatureType.CATEGORIES);
        if (categoryFeatures == null || categoryFeatures.size() == 0) {
            return null;
        }
        //we can do the following as we know for archive records there can only be one category-feature (at least currently 25.5.2019)
        return ArxivRecord.extractPrimaryCategories(categoryFeatures.get(0).getValue());
    }
}

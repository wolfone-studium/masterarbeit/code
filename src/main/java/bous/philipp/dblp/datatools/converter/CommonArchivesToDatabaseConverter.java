package bous.philipp.dblp.datatools.converter;

import bous.philipp.dblp.datatools.converter.heuristics.classification.*;
import bous.philipp.dblp.datatools.converter.xmlchecks.raw.PlosOneRawXmlCheck;
import bous.philipp.dblp.datatools.converter.xmlchecks.raw.RawXmlCheck;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.factory.ArxivRecordFactory;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.factory.BLThesisRecordFactory;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.factory.PlosOneRecordFactory;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.factory.RecordFactory;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

/**
 * Convenience-class to provide archive-conversions for archives from common sources.
 */
public class CommonArchivesToDatabaseConverter extends ArchiveToDataBaseConverter {
    /**
     * Processes entries of a british library theses-tar.gz-file, converting it to be written to a database.
     * This is the restricted version, using a {@link BLStrictClassificationHeuristic} for initial labeling.
     * Database should contain tables for records and features of following structure:
     * Records:
     * <ul>
     *     <li>`id` int(11) NOT NULL</li>
     *     <li>`original_id` varchar(80) DEFAULT NULL</li>
     *     <li>`cs_classification` tinyint(4) NOT NULL</li>
     *     <li>`random_id` bigint(20) DEFAULT NULL</li>
     *     <li>PRIMARY KEY (`id`)</li>
     *     <li>UNIQUE KEY `id_artificial_UNIQUE` (`id`)</li>
     *     <li>UNIQUE KEY `id_UNIQUE` (`original_id`)</li>
     *     <li>UNIQUE KEY `random_id_UNIQUE` (`random_id`)</li>
     *     <li></li>
     * </ul>
     *
     * Features:
     * <ul>
     *     <li>`id` int(11) NOT NULL</li>
     *     <li>`type` varchar(45) NOT NULL</li>
     *     <li>`value` text NOT NULL</li>
     *     <li>`record_id` int(11) DEFAULT NULL</li>
     *     <li>`original_position` varchar(45) DEFAULT NULL</li>
     *     <li>PRIMARY KEY (`id`)</li>
     *     <li>UNIQUE KEY `id_UNIQUE` (`id`)</li>
     *     <li>KEY `arxiv_features_rand_balanced_records_rand_balanced` (`record_id`)</li>
     *     <li>CONSTRAINT `featurs_records_key` FOREIGN KEY (`record_id`) REFERENCES `RECORD_TABLE_NAME` (`id`)</li>
     * </ul>
     *
     * @see ArchiveToDataBaseConverter#xmlTarArchiveToDataBase(Connection, String, String, String, int, int, int, RawXmlCheck, ClassificationHeuristic, RecordFactory)
     * @param connection db-connection
     * @param recordTableName name of the record table
     * @param featureTableName name of the feature table
     * @param tarPath path of the tar to be processed
     * @param batchSize how many records should be prepared before writing a batch to the db
     * @param maxEntries maximum number of entries to be processed, -1 for all; maxEntries is NOT the number of entries that will be put into the DB but the maximum of tars without errors that we will process
     * @param startIndex index defining which file in the tar should be processed first (usually this should be 0 but this would allow splitting the processing of bigger archives).
     */
    public static void britishLibraryTarToDBRestricted(@NotNull Connection connection,
                                                       String recordTableName,
                                                       String featureTableName,
                                                       String tarPath,
                                                       int batchSize,
                                                       int maxEntries,
                                                       int startIndex) {

        xmlTarArchiveToDataBase(
                connection,
                recordTableName,
                featureTableName,
                tarPath,
                batchSize,
                maxEntries,
                startIndex,
                null,
                new BLStrictClassificationHeuristic(),
                new BLThesisRecordFactory()
        );
    }

    /**
     * Processes entries of a british library theses-tar.gz-file, converting it to be written to a database.
     * This is the extended version, using a {@link BLExtendedClassificationHeuristic} for initial labeling.
     * Database should contain tables for records and features of following structure:
     * Records:
     * <ul>
     *     <li>`id` int(11) NOT NULL</li>
     *     <li>`original_id` varchar(80) DEFAULT NULL</li>
     *     <li>`cs_classification` tinyint(4) NOT NULL</li>
     *     <li>`random_id` bigint(20) DEFAULT NULL</li>
     *     <li>PRIMARY KEY (`id`)</li>
     *     <li>UNIQUE KEY `id_artificial_UNIQUE` (`id`)</li>
     *     <li>UNIQUE KEY `id_UNIQUE` (`original_id`)</li>
     *     <li>UNIQUE KEY `random_id_UNIQUE` (`random_id`)</li>
     *     <li></li>
     * </ul>
     *
     * Features:
     * <ul>
     *     <li>`id` int(11) NOT NULL</li>
     *     <li>`type` varchar(45) NOT NULL</li>
     *     <li>`value` text NOT NULL</li>
     *     <li>`record_id` int(11) DEFAULT NULL</li>
     *     <li>`original_position` varchar(45) DEFAULT NULL</li>
     *     <li>PRIMARY KEY (`id`)</li>
     *     <li>UNIQUE KEY `id_UNIQUE` (`id`)</li>
     *     <li>KEY `arxiv_features_rand_balanced_records_rand_balanced` (`record_id`)</li>
     *     <li>CONSTRAINT `featurs_records_key` FOREIGN KEY (`record_id`) REFERENCES `RECORD_TABLE_NAME` (`id`)</li>
     * </ul>
     * @see ArchiveToDataBaseConverter#xmlTarArchiveToDataBase(Connection, String, String, String, int, int, int, RawXmlCheck, ClassificationHeuristic, RecordFactory)
     * @param connection db-connection
     * @param recordTableName name of the record table
     * @param featureTableName name of the feature table
     * @param tarPath path of the tar to be processed
     * @param batchSize how many records should be prepared before writing a batch to the db
     * @param maxEntries maximum number of entries to be processed, -1 for all; maxEntries is NOT the number of entries that will be put into the DB but the maximum of tars without errors that we will process
     * @param startIndex index defining which file in the tar should be processed first (usually this should be 0 but this would allow splitting the processing of bigger archives).
     */
    public static void britishLibraryTarToDBExtended(@NotNull Connection connection,
                                                     String recordTableName,
                                                     String featureTableName,
                                                     String tarPath,
                                                     int batchSize,
                                                     int maxEntries,
                                                     int startIndex) {
        xmlTarArchiveToDataBase(
                connection,
                recordTableName,
                featureTableName,
                tarPath,
                batchSize,
                maxEntries,
                startIndex,
                null,
                new BLExtendedClassificationHeuristic(),
                new BLThesisRecordFactory());
    }

    /**
     * Processes entries of a PLoS-One tar.gz-file, converting it to be written to a database.
     * This is the restricted version, using a {@link PlosOneStrictClassificationHeuristic} for initial labeling.
     * Database should contain tables for records and features of following structure:
     * Records:
     * <ul>
     *     <li>`id` int(11) NOT NULL</li>
     *     <li>`original_id` varchar(80) DEFAULT NULL</li>
     *     <li>`cs_classification` tinyint(4) NOT NULL</li>
     *     <li>`random_id` bigint(20) DEFAULT NULL</li>
     *     <li>PRIMARY KEY (`id`)</li>
     *     <li>UNIQUE KEY `id_artificial_UNIQUE` (`id`)</li>
     *     <li>UNIQUE KEY `id_UNIQUE` (`original_id`)</li>
     *     <li>UNIQUE KEY `random_id_UNIQUE` (`random_id`)</li>
     *     <li></li>
     * </ul>
     *
     * Features:
     * <ul>
     *     <li>`id` int(11) NOT NULL</li>
     *     <li>`type` varchar(45) NOT NULL</li>
     *     <li>`value` text NOT NULL</li>
     *     <li>`record_id` int(11) DEFAULT NULL</li>
     *     <li>`original_position` varchar(45) DEFAULT NULL</li>
     *     <li>PRIMARY KEY (`id`)</li>
     *     <li>UNIQUE KEY `id_UNIQUE` (`id`)</li>
     *     <li>KEY `arxiv_features_rand_balanced_records_rand_balanced` (`record_id`)</li>
     *     <li>CONSTRAINT `featurs_records_key` FOREIGN KEY (`record_id`) REFERENCES `RECORD_TABLE_NAME` (`id`)</li>
     * </ul>
     * @see ArchiveToDataBaseConverter#xmlTarArchiveToDataBase(Connection, String, String, String, int, int, int, RawXmlCheck, ClassificationHeuristic, RecordFactory)
     * @param connection db-connection
     * @param recordTableName name of the record table
     * @param featureTableName name of the feature table
     * @param tarPath path of the tar to be processed
     * @param batchSize how many records should be prepared before writing a batch to the db
     * @param maxEntries maximum number of entries to be processed, -1 for all; maxEntries is NOT the number of entries that will be put into the DB but the maximum of tars without errors that we will process
     * @param startIndex index defining which file in the tar should be processed first (usually this should be 0 but this would allow splitting the processing of bigger archives).
     */
    public static void plosOneTarToDBRestricted(@NotNull Connection connection,
                                                String recordTableName,
                                                String featureTableName,
                                                String tarPath,
                                                int batchSize,
                                                int maxEntries,
                                                int startIndex) {
        xmlTarArchiveToDataBase(
                connection,
                recordTableName,
                featureTableName,
                tarPath,
                batchSize,
                maxEntries,
                startIndex,
                new PlosOneRawXmlCheck(),
                new PlosOneStrictClassificationHeuristic(),
                new PlosOneRecordFactory()
        );
    }

    /**
     * Processes entries of a PLoS-One tar.gz-file, converting it to be written to a database.
     * This is the extended version, using a {@link PlosOneCBIncludingClassificationHeuristic} for initial labeling.
     * Database should contain tables for records and features of following structure:
     * Records:
     * <ul>
     *     <li>`id` int(11) NOT NULL</li>
     *     <li>`original_id` varchar(80) DEFAULT NULL</li>
     *     <li>`cs_classification` tinyint(4) NOT NULL</li>
     *     <li>`random_id` bigint(20) DEFAULT NULL</li>
     *     <li>PRIMARY KEY (`id`)</li>
     *     <li>UNIQUE KEY `id_artificial_UNIQUE` (`id`)</li>
     *     <li>UNIQUE KEY `id_UNIQUE` (`original_id`)</li>
     *     <li>UNIQUE KEY `random_id_UNIQUE` (`random_id`)</li>
     *     <li></li>
     * </ul>
     *
     * Features:
     * <ul>
     *     <li>`id` int(11) NOT NULL</li>
     *     <li>`type` varchar(45) NOT NULL</li>
     *     <li>`value` text NOT NULL</li>
     *     <li>`record_id` int(11) DEFAULT NULL</li>
     *     <li>`original_position` varchar(45) DEFAULT NULL</li>
     *     <li>PRIMARY KEY (`id`)</li>
     *     <li>UNIQUE KEY `id_UNIQUE` (`id`)</li>
     *     <li>KEY `arxiv_features_rand_balanced_records_rand_balanced` (`record_id`)</li>
     *     <li>CONSTRAINT `featurs_records_key` FOREIGN KEY (`record_id`) REFERENCES `RECORD_TABLE_NAME` (`id`)</li>
     * </ul>
     * @see ArchiveToDataBaseConverter#xmlTarArchiveToDataBase(Connection, String, String, String, int, int, int, RawXmlCheck, ClassificationHeuristic, RecordFactory)
     * @param connection db-connection
     * @param recordTableName name of the record table
     * @param featureTableName name of the feature table
     * @param tarPath path of the tar to be processed
     * @param batchSize how many records should be prepared before writing a batch to the db
     * @param maxEntries maximum number of entries to be processed, -1 for all; maxEntries is NOT the number of entries that will be put into the DB but the maximum of tars without errors that we will process
     * @param startIndex index defining which file in the tar should be processed first (usually this should be 0 but this would allow splitting the processing of bigger archives).
     */
    public static void plosOneTarToDBExtended(@NotNull Connection connection,
                                              String recordTableName,
                                              String featureTableName,
                                              String tarPath,
                                              int batchSize,
                                              int maxEntries,
                                              int startIndex) {
        xmlTarArchiveToDataBase(
                connection,
                recordTableName,
                featureTableName,
                tarPath,
                batchSize,
                maxEntries,
                startIndex,
                new PlosOneRawXmlCheck(),
                new PlosOneCBIncludingClassificationHeuristic(),
                new PlosOneRecordFactory()
        );
    }

    /**
     * Processes entries of a arXiv tar.gz-file, converting it to be written to a database using a {@link ArxivClassificationHeuristic} for initial labeling.
     * Database should contain tables for records and features of following structure:
     * Records:
     * <ul>
     *     <li>`id` int(11) NOT NULL</li>
     *     <li>`original_id` varchar(80) DEFAULT NULL</li>
     *     <li>`cs_classification` tinyint(4) NOT NULL</li>
     *     <li>`random_id` bigint(20) DEFAULT NULL</li>
     *     <li>PRIMARY KEY (`id`)</li>
     *     <li>UNIQUE KEY `id_artificial_UNIQUE` (`id`)</li>
     *     <li>UNIQUE KEY `id_UNIQUE` (`original_id`)</li>
     *     <li>UNIQUE KEY `random_id_UNIQUE` (`random_id`)</li>
     *     <li></li>
     * </ul>
     *
     * Features:
     * <ul>
     *     <li>`id` int(11) NOT NULL</li>
     *     <li>`type` varchar(45) NOT NULL</li>
     *     <li>`value` text NOT NULL</li>
     *     <li>`record_id` int(11) DEFAULT NULL</li>
     *     <li>`original_position` varchar(45) DEFAULT NULL</li>
     *     <li>PRIMARY KEY (`id`)</li>
     *     <li>UNIQUE KEY `id_UNIQUE` (`id`)</li>
     *     <li>KEY `arxiv_features_rand_balanced_records_rand_balanced` (`record_id`)</li>
     *     <li>CONSTRAINT `featurs_records_key` FOREIGN KEY (`record_id`) REFERENCES `RECORD_TABLE_NAME` (`id`)</li>
     * </ul>
     * @see ArchiveToDataBaseConverter#xmlTarArchiveToDataBase(Connection, String, String, String, int, int, int, RawXmlCheck, ClassificationHeuristic, RecordFactory)
     * @param connection db-connection
     * @param recordTableName name of the record table
     * @param featureTableName name of the feature table
     * @param tarPath path of the tar to be processed
     * @param batchSize how many records should be prepared before writing a batch to the db
     * @param maxEntries maximum number of entries to be processed, -1 for all; maxEntries is NOT the number of entries that will be put into the DB but the maximum of tars without errors that we will process
     * @param startIndex index defining which file in the tar should be processed first (usually this should be 0 but this would allow splitting the processing of bigger archives).
     */
    public static void arxivTarToDB(@NotNull Connection connection,
                                    String recordTableName,
                                    String featureTableName,
                                    String tarPath,
                                    int batchSize,
                                    int maxEntries,
                                    int startIndex) {
        xmlTarArchiveToDataBase(
                connection,
                recordTableName,
                featureTableName,
                tarPath,
                batchSize,
                maxEntries,
                startIndex,
                null,
                new ArxivClassificationHeuristic(),
                new ArxivRecordFactory()
        );
    }
}

package bous.philipp.dblp.datatools.converter.heuristics.classification;

import bous.philipp.dblp.datatools.converter.xmlchecks.raw.RawXmlCheck;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.factory.RecordFactory;

import java.sql.Connection;

/**
 * Interface meant to provide some heuristics for classifying given {@link RecordDocument}s
 * in the process of ingesting them into a database.
 * Implementing classes will usually differ, depending on the source of the record-document.
 * The heuristic consists of 2 parts: deciding if a record-document is useful at all and
 * guessing a class for a record-document. Both parts could be used seperately but are mainly intended
 * to be used together.
 * More precisely this interface is meant to allow injection of custom behaviour into
 * the ingestion process in {@link bous.philipp.dblp.datatools.converter.ArchiveToDataBaseConverter#xmlTarArchiveToDataBase(Connection, String, String, String, int, int, int, RawXmlCheck, ClassificationHeuristic, RecordFactory)}.
 * @see bous.philipp.dblp.datatools.converter.ArchiveToDataBaseConverter#xmlTarArchiveToDataBase(Connection, String, String, String, int, int, int, RawXmlCheck, ClassificationHeuristic, RecordFactory)
 */
public interface ClassificationHeuristic {
    /**
     * Guesses the class of a given record based on some heuristic
     * @param record the record to be classified
     * @return -1 for non CS, 0 for unknown, 1 for CS
     */
    int guessClass(RecordDocument record);

    /**
     * Tests if a given record is useful to be taken into the database.
     * Can be used to filter records that are noise.
     * @param record some record
     * @return true if the record is useful, false if not
     */
    boolean isUseful(RecordDocument record);
}

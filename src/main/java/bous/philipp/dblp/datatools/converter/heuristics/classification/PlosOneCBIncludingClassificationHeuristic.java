package bous.philipp.dblp.datatools.converter.heuristics.classification;

import bous.philipp.dblp.datatools.datamodel.RecordDocument;

/**
 * Strict classification-heuristic for {@link RecordDocument}s that are derived from
 * xml-files for theses as provided by PLoS-One. Will normally classify
 * more records as 1 than {@link PlosOneStrictClassificationHeuristic}.
 * @see ClassificationHeuristic
 */
public class PlosOneCBIncludingClassificationHeuristic extends PlosOneAbstractClassificationHeuristic{
    /**
     * Guessing heuristic that includes computational biology in the CS-set without further inspection for experimental reasons.
     * @see ClassificationHeuristic#guessClass(RecordDocument)
     * @param record the record to be classified
     * @return <ul>
     *     <li>1 if categories match "(.*[Cc]omputational [Bb]iology.*)|([Cc]omputer and [Ii]nformation [Ss]ciences)|([Cc]omputer [Ss]cience)"</li>
     *     <li>-1 if categories are not empty and do NOT match "(.*[Aa]lgorith.*)|(.*[Dd]ata.*)|(.*[Cc]omput.*)"</li>
     *     <li>0 otherwise</li>
     * </ul>
     */
    @Override
    public int guessClass(RecordDocument record) {
        String categories = getCategories(record);
        if (categories == null) {
            return 0;
        }
        if (categories
                .matches("(.*[Cc]omputational [Bb]iology.*)|([Cc]omputer and [Ii]nformation [Ss]ciences)|([Cc]omputer [Ss]cience)")) {
            return  1;
        } else if (!categories.matches("(.*[Aa]lgorith.*)|(.*[Dd]ata.*)|(.*[Cc]omput.*)") && !categories.trim().equals("")) {
            return -1;
        } else {
            return 0;
        }
    }
}

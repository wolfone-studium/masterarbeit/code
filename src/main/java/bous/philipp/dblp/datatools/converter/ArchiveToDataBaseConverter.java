package bous.philipp.dblp.datatools.converter;

import bous.philipp.dblp.datatools.converter.heuristics.classification.ClassificationHeuristic;
import bous.philipp.dblp.datatools.converter.xmlchecks.raw.RawXmlCheck;
import bous.philipp.dblp.datatools.datamodel.Feature;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.RecordArchetype;
import bous.philipp.dblp.datatools.datamodel.records.archetypes.factory.RecordFactory;
import bous.philipp.dblp.datatools.util.xml.XMLUtility;
import bous.philipp.dblp.datatools.util.fileprocessing.TarProcessor;
import bous.philipp.dblp.datatools.util.logging.LoggerFactory;
import org.apache.commons.lang3.time.StopWatch;
import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

/**
 * Has the main purpose to provide a standardized but sufficiently configurable way to convert records in
 * arbitrary tar.gz xml-archives into data inside a database.
 * This is achieved through the {@link #xmlTarArchiveToDataBase(Connection, String, String, String, int, int, int, RawXmlCheck, ClassificationHeuristic, RecordFactory)}-method
 */
public class ArchiveToDataBaseConverter {

    /**
     * Processes entries of a tar.gz-file, converting them in an appropriate format to be written to a database.
     * Its most prominent parameters are {@link RawXmlCheck},{@link ClassificationHeuristic} and a {@link RecordFactory}.
     * The first two help in determining usefulness of a record as well as a class label that is guessed without any machine learning involved (i.e. "purely" human-heuristic).
     * The latter is used to instantiate the correct implementations of {@link RecordArchetype} from {@link Document}s that are parsed from the raw xml-files.
     * Ids for records and features are given incrementally upwards from the current corresponding maximum ids that are present in the database.
     * Database should contain tables for records and features of following structure:
     * Records:
     * <ul>
     *     <li>`id` int(11) NOT NULL</li>
     *     <li>`original_id` varchar(80) DEFAULT NULL</li>
     *     <li>`cs_classification` tinyint(4) NOT NULL</li>
     *     <li>`random_id` bigint(20) DEFAULT NULL</li>
     *     <li>PRIMARY KEY (`id`)</li>
     *     <li>UNIQUE KEY `id_artificial_UNIQUE` (`id`)</li>
     *     <li>UNIQUE KEY `id_UNIQUE` (`original_id`)</li>
     *     <li>UNIQUE KEY `random_id_UNIQUE` (`random_id`)</li>
     *     <li></li>
     * </ul>
     *
     * Features:
     * <ul>
     *     <li>`id` int(11) NOT NULL</li>
     *     <li>`type` varchar(45) NOT NULL</li>
     *     <li>`value` text NOT NULL</li>
     *     <li>`record_id` int(11) DEFAULT NULL</li>
     *     <li>`original_position` varchar(45) DEFAULT NULL</li>
     *     <li>PRIMARY KEY (`id`)</li>
     *     <li>UNIQUE KEY `id_UNIQUE` (`id`)</li>
     *     <li>KEY `arxiv_features_rand_balanced_records_rand_balanced` (`record_id`)</li>
     *     <li>CONSTRAINT `featurs_records_key` FOREIGN KEY (`record_id`) REFERENCES `RECORD_TABLE_NAME` (`id`)</li>
     * </ul>
     * @param connection db-connection
     * @param recordTableName name of the record table
     * @param featureTableName name of the feature table
     * @param tarPath path of the tar to be processed
     * @param batchSize how many records should be prepared before writing a batch to the db
     * @param maxEntries maximum number of entries to be processed, -1 for all; maxEntries is NOT the number of entries that will be put into the DB but the maximum of tars without errors that we will process
     * @param startIndex index defining which file in the tar should be processed first (usually this should be 0 but this would allow splitting the processing of bigger archives).
     * @param xmlCheck the check will pe performed as one part of the decision if a given xml-file is of interest; useful to filter corrupted documents or documents that are of a wrong type
     * @param classificationHeuristic its method {@link ClassificationHeuristic#isUseful(RecordDocument)} is the second test that determines if a document will be represented in the DB; before that, it is initially labeled by the heuristic's {@link ClassificationHeuristic#guessClass(RecordDocument)} method.
     * @param recordFactory a factory to build appropriate {@link RecordArchetype}-Implementations from a {@link Document}, build from the given xml-files
     */
    public static void xmlTarArchiveToDataBase(@NotNull Connection connection,
                                               @NotNull String recordTableName,
                                               @NotNull String featureTableName,
                                               @NotNull String tarPath,
                                               int batchSize,
                                               int maxEntries,
                                               int startIndex,
                                               RawXmlCheck xmlCheck,
                                               @NotNull ClassificationHeuristic classificationHeuristic,
                                               @NotNull RecordFactory recordFactory
    ) {
        Logger logger = LoggerFactory.getLogger(ArchiveToDataBaseConverter.class.getName());

        StopWatch watch = new StopWatch();

        try {
            PreparedStatement recordInsertStatement = Objects.requireNonNull(recordDocDBInsertStatement(connection, recordTableName));
            PreparedStatement featureInsertStatement = Objects.requireNonNull(featureDBInsertStatement(connection, featureTableName));

            connection.setAutoCommit(false);

            try {
                int currentMax = getCurrentMaxId(connection, recordTableName);
                logger.info("current max index for records: " + currentMax);

                int currentMaxFeatureId = getCurrentMaxId(connection, featureTableName);
                logger.info("current max index for features: " + currentMaxFeatureId);

                AtomicInteger batchCounter = new AtomicInteger(0);
                AtomicInteger idArtificialRecord = new AtomicInteger(currentMax);
                AtomicInteger idFeatures = new AtomicInteger(currentMaxFeatureId);

                watch.start();
                TarProcessor.iterateRawXMLTarFlat(
                        tarPath,
                        maxEntries,
                        startIndex,
                        rawXML -> {
                            DocumentBuilder documentBuilder = getDocumentBuilder();
                            rawXML = XMLUtility.rectifyRawXML(rawXML);
                            Document document = documentBuilder.parse(new InputSource(new StringReader(rawXML)));

                            RecordArchetype record = recordFactory.fromDocument(document, idArtificialRecord.incrementAndGet(), idFeatures.incrementAndGet());
                            RecordDocument recordDocument = record.toRecordDocument();

                            boolean rawXmlIsOk;
                            if (xmlCheck != null){
                                rawXmlIsOk = xmlCheck.checkXML(rawXML);
                            }else {
                                rawXmlIsOk = true;
                            }

                            if (rawXmlIsOk && classificationHeuristic.isUseful(recordDocument)) {
                                configureRecordInsertStatement(recordInsertStatement, classificationHeuristic, recordDocument);
                                addFeatureStatementsForBatchExecution(featureInsertStatement, record.getFeatureList());
                                //necessary as most probably more than one feature is added if we keep the record
                                idFeatures.addAndGet(record.getFeatureList().size() - 1);
                            } else {
                                //to avoid gaps in IDs if we decided not to keep the record
                                idArtificialRecord.decrementAndGet();
                                idFeatures.decrementAndGet();
                            }

                            batchCounter.incrementAndGet();

                            if (batchCounter.get() == batchSize) {
                                finalizeBatch(connection, logger, watch, recordInsertStatement, featureInsertStatement, batchCounter);
                            }
                        }
                        , true);

                logger.info("EXECUTE BATCH");
                Objects.requireNonNull(recordInsertStatement).executeBatch();
                Objects.requireNonNull(featureInsertStatement).executeBatch();
                connection.commit();
                logger.info("COMMITTED SUCCESSFULLY");

            } catch (SQLException e) {
                connection.rollback();
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private static int getCurrentMaxId(@NotNull Connection connection, String recordTableName) throws SQLException {
        ResultSet currentMaxIdRecords = connection.createStatement().executeQuery("SELECT MAX(id) FROM " + recordTableName);
        currentMaxIdRecords.next();
        return currentMaxIdRecords.getInt(1);
    }

    private static void addFeatureStatementsForBatchExecution(PreparedStatement featureInsertStatement, List<Feature> featureList) throws SQLException {
        for (Feature feature : featureList) {
            populateFeaturePreparedStatement(featureInsertStatement, feature);
            featureInsertStatement.addBatch();
        }
    }

    private static void configureRecordInsertStatement(PreparedStatement recordInsertStatement, ClassificationHeuristic classificationHeuristic, RecordDocument recordDocument) throws SQLException {
        recordInsertStatement.setInt(1, recordDocument.getId());
        recordInsertStatement.setString(2, recordDocument.getOriginalId());

        int recordClassification = classificationHeuristic.guessClass(recordDocument);
        recordInsertStatement.setInt(3, recordClassification);
        recordInsertStatement.addBatch();
    }

    private static void populateFeaturePreparedStatement(PreparedStatement featureInsertStatement, Feature feature) throws SQLException {
        featureInsertStatement.setInt(1, feature.getId());
        featureInsertStatement.setString(2, feature.getType().toString());
        featureInsertStatement.setString(3, feature.getValue());
        featureInsertStatement.setInt(4, feature.getRecordId());
        featureInsertStatement.setString(5, feature.getOriginalPosition());
    }

    @NotNull
    private static DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setValidating(false);
        builderFactory.setNamespaceAware(true);
        builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
        builderFactory.setFeature("http://xml.org/sax/features/validation", false);
        builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
        builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
        documentBuilder.setErrorHandler(getSaxErrorHandler());
        return documentBuilder;
    }

    private static void finalizeBatch(@NotNull Connection connection, Logger logger, StopWatch watch, PreparedStatement recordInsertStatement, PreparedStatement featureInsertStatement, AtomicInteger batchCounter) throws SQLException {
        batchCounter.set(0);
        logger.info("EXECUTE BATCH");
        recordInsertStatement.executeBatch();
        featureInsertStatement.executeBatch();
        connection.commit();
        logger.info("COMMITTED SUCCESSFULLY");
        watch.stop();
        logger.info("Elapsed time: " + watch.getTime());
        watch.reset();
        watch.start();
    }

    private static PreparedStatement recordDocDBInsertStatement(Connection connection, String tableName) {
        String sql = "INSERT INTO " + tableName + "(id, original_id, cs_classification) VALUES(?,?,?)";
        try {
            return connection.prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static PreparedStatement featureDBInsertStatement(Connection connection, String tableName) {
        String sql = "INSERT INTO " + tableName + "(id, type, value, record_id, original_position) VALUES(?,?,?,?,?)";
        try {
            return connection.prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //NOTE: check ob das sinnvoll wäre da tatsächlich was zu implementieren; ist aber zweitrangig
    private static ErrorHandler getSaxErrorHandler() {
        return new ErrorHandler() {
            @Override
            public void warning(SAXParseException exception) throws SAXException {

            }

            @Override
            public void error(SAXParseException exception) throws SAXException {

            }

            @Override
            public void fatalError(SAXParseException exception) throws SAXException {

            }
        };
    }
}

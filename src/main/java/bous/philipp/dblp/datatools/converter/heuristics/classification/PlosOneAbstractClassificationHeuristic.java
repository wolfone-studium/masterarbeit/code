package bous.philipp.dblp.datatools.converter.heuristics.classification;

import bous.philipp.dblp.datatools.datamodel.Feature;
import bous.philipp.dblp.datatools.datamodel.FeatureType;
import bous.philipp.dblp.datatools.datamodel.RecordDocument;

import java.util.List;

/**
 * Base class for classification-heuristics for {@link RecordDocument}s that are derived from
 * xml-files for theses as provided by PlosOne.
 * @see ClassificationHeuristic
 */
public abstract class PlosOneAbstractClassificationHeuristic implements ClassificationHeuristic {

    /**
     * Extracts the categories string from a record based on a plos-one-xml.
     * Note that this can be done this way as we know that the way plos one xmls are processed contain at most a single CATEGORIES-feature.
     * @param record some record
     * @return the categories
     */
    protected String getCategories(RecordDocument record) {
        List<Feature> featuresByType = record.getFeaturesByType(FeatureType.CATEGORIES);
        if (featuresByType.size() > 0) {
            return featuresByType.get(0).getValue();
        }
        return "";
    }

    /**
     * Tests a record based on a plos-one-xml for usefulness.
     * @param record some record
     * @return true if it has a usable categories-feature (not empty, not null), false otherwise
     */
    @Override
    public boolean isUseful(RecordDocument record) {
        String categories = getCategories(record);
        return categories != null && !categories.equals("");
    }
}

package bous.philipp.dblp.datatools.converter.heuristics.classification;

import bous.philipp.dblp.datatools.datamodel.RecordDocument;

/**
 * Strict classification-heuristic for {@link RecordDocument}s that are derived from
 * xml-files for theses as provided by PLoS-One. Will normally classify
 * less records as 1 than {@link PlosOneCBIncludingClassificationHeuristic}.
 * @see ClassificationHeuristic
 */
public class PlosOneStrictClassificationHeuristic extends PlosOneAbstractClassificationHeuristic{

    /**
     * Strict guessing heuristic for plos-one-based records.
     * @see ClassificationHeuristic#guessClass(RecordDocument)
     * @param record the record to be classified
     * @return <ul>
     *     <li>1 if categories match "([Cc]omputer and [Ii]nformation [Ss]ciences)|([Cc]omputer [Ss]cience)"</li>
     *     <li>-1 if categories are not empty and do NOT match "(.*[Aa]lgorith.*)|(.*[Dd]ata.*)|(.*[Cc]omput.*)"</li>
     *     <li>0 otherwise</li>
     * </ul>
     */
    @Override
    public int guessClass(RecordDocument record) {
        String categories = getCategories(record);
        if (categories == null) {
            return 0;
        }
        if (categories
                .matches("([Cc]omputer and [Ii]nformation [Ss]ciences)|([Cc]omputer [Ss]cience)")) {
            return  1;
        } else if (!categories.matches("(.*[Aa]lgorith.*)|(.*[Dd]ata.*)|(.*[Cc]omput.*)") && !categories.trim().equals("")) {
            return -1;
        } else {
            return 0;
        }
    }
}

/**
 * Provides classes/interfaces used in classifier-evaluation.
 */
package bous.philipp.dblp.datatools.evaluation;
package bous.philipp.dblp.datatools.evaluation;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.ArrayUtils;
import smile.validation.AUC;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Primarily used by {@link BinaryClassificationEvaluator} to suit more complex
 * evaluation-scenarios like cross-validation but could be used standalone for
 * evaluation of a single set with printing of a performance report.
 */
public class BinaryEvaluationSet implements Evaluator {
    /**
     * Digits after the comma to be printed when printing evaluation-statistics. Defaults to 4 after using parameterless constructor.
     */
    private int valueOutputPrecision;

    private double classificationThreshold;

    @Getter @Setter private double truePositives;
    @Getter @Setter private double falsePositives;
    @Getter @Setter private double trueNegatives;
    @Getter @Setter private double falseNegatives;

    private double auc;

    private List<Double> weightedGainValues;

    private List<Double> predictedScores;
    private List<Integer> actualClasses;

    private List<WeightedGainWeightingInfo> weightingInfos;

    /**
     * This standard-constructor will instanciate the BinaryEvaluationSet with
     * a default classificationThreshold of 0.5 and a value-output-precision of
     * 4 digits behind the comma.
     */
    public BinaryEvaluationSet() {
        this.classificationThreshold = 0.5;

        this.valueOutputPrecision = 4;

        this.truePositives = 0;
        this.falsePositives = 0;
        this.trueNegatives = 0;
        this.falseNegatives = 0;

        this.auc = 0;

    }

    /**
     * Allows specifying a custom classification-threshold.
     * Value-output-precision will be set to 4 digits behind the comma.
     * @param classificationThreshold custom classification threshold
     */
    public BinaryEvaluationSet(double classificationThreshold) {
        this.classificationThreshold = classificationThreshold;

        this.valueOutputPrecision = 4;

        this.truePositives = 0;
        this.falsePositives = 0;
        this.trueNegatives = 0;
        this.falseNegatives = 0;

        this.auc = 0;
    }


    /**
     * If this instance contains scores and actual classes this method writes one dat-file containing all scores for
     * actual positive examples and one file for actual negative examples indicated by suffix "_SCORES_POSITIVES"/"_SCORES_NEGATIVES".
     * WARNING: this class doesn't prevent inserting arbitrary values as actual classes; in this method everything
     * that does not have actual class value 1 will be treated as negative example while actual class value 1 is
     * treated as positive example.
     * @param destinationFolder the folder in which the files should be saved
     * @param filenamePrefix prefix that determines the main filename-part before "_SCORES_POSITIVES.dat"/"_SCORES_NEGATIVES.dat"
     */
    public void writeScoreFiles(String destinationFolder, String filenamePrefix){
        File positivesFile = new File(destinationFolder + File.separator + filenamePrefix + "_SCORES_POSITIVES.dat");
        File negativesFile = new File(destinationFolder + File.separator + filenamePrefix + "_SCORES_NEGATIVES.dat");
        try (FileWriter posFileWriter = new FileWriter(positivesFile); BufferedWriter posBuffWriter = new BufferedWriter(posFileWriter);
             FileWriter negFileWriter = new FileWriter(negativesFile); BufferedWriter negBuffWriter = new BufferedWriter(negFileWriter)) {

            for (int i = 0; i < predictedScores.size(); i++) {
                Double score = predictedScores.get(i);
                if (actualClasses.get(i) == 1){
                    posBuffWriter.write(score.toString());
                    if (i != predictedScores.size() - 1){
                        posBuffWriter.write("\n");
                    }
                }else{
                    negBuffWriter.write(score.toString());
                    if (i != predictedScores.size() - 1){
                        negBuffWriter.write("\n");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Labels a value based on the classificationThreshold of the associated BinaryClassificationEvaluator instance.
     * @param value the value
     * @return 1 if value >= threshold, 0 otherwise
     */
    private double label(double value, double threshold){
        if (value >= threshold){
            return 1;
        }else{
            return 0;
        }
    }

    /**
     * Compares a given actual class and a given score and determines with the given classification threshold
     * for the associated BinaryClassificationEvaluator instance, if the resulting classification would be correct
     * or false and does the necessary incrementation and saving of values for the final evaluation of this test-set.
     * Returns true if classification was correct, false if not for convenience of user-code.
     * @param actualClass the actual class
     * @param score the score
     * @return true if classification was correct, false if not
     */
    public boolean evaluate(double actualClass, double score){

        double prediction = label(score, this.classificationThreshold);

        if (this.actualClasses == null || this.predictedScores == null){
            if (this.actualClasses != null || this.predictedScores != null){
                throw new IllegalStateException("actualClasses or predictedScores was null; either both have to be null or different from null");
            }
            this.actualClasses = new ArrayList<>();
            this.predictedScores = new ArrayList<>();
        }

        actualClasses.add((int) actualClass);
        predictedScores.add(score);

        if (prediction == actualClass){
            if (prediction == 0){
                this.incrementTrueNegatives();
            }else {
                this.incrementTruePositives();
            }
            return true;
        }else{
            if (prediction == 0){
                this.incrementFalseNegatives();
            }else {
                this.incrementFalsePositives();
            }
            return false;
        }
    }

    /**
     * Updates the AUC-value of this EvaluationSet based on the currently given scores and actual classes. The user is responsible
     * to keep these in a correct state.
     */
    void updateAUC(){
        if (this.actualClasses == null || this.predictedScores == null){
            if (this.actualClasses != null || this.predictedScores != null){
                throw new IllegalStateException("actualClasses or predictedScores was null; either both have to be null or different from null");
            }
            System.out.println("No actualClasses and predictedScores given; not able to calculate new auc; auc will not be changed.");
        }

        Double[] scores = this.predictedScores.toArray(new Double[0]);
        Integer[] classes = this.actualClasses.toArray(new Integer[0]);
        this.auc = AUC.measure(ArrayUtils.toPrimitive(classes), ArrayUtils.toPrimitive(scores));
    }

    /**
     * Checks if this instance holds a minimum of 1 score. (Doesn't perform integrity checks with number of actual classes given.
     * @return true if this.predictedScores != null and has at least 1 item; false else
     */
    public boolean hasScores(){
        return this.predictedScores != null && this.predictedScores.size() > 0;
    }

    /**
     * Checks if all 4 main-fields of the confusion-matrix for this instance are currently set to 0.
     * @return true if all 4 main-fields are 0; false else
     */
    public boolean hasOnlyZeroCounts() {
        return this.truePositives == 0 && this.falsePositives == 0 && this.trueNegatives == 0 && this.falseNegatives == 0;
    }


    /**
     * Adds a new WeightedGainConfig to this instance. Calling one of the performanceReport methods will include
     * estimated WeightedGain values for each configuration that is present in this class.
     * Adding additional configs is especially useful to estimate a weighted gain on populations that have a different distribution than
     * validation-sets.
     * @param tprWeight weight that should be applied to the TPR
     * @param tnrWeight weight that should be applied to the TNR
     * @param probPos an estimation of the probability to encounter a positive example
     * @param probNeg an estimation of the probability to encounter a negative example
     */
    public void addWeightedGainConfig(double tprWeight, double tnrWeight, double probPos, double probNeg){
        if (weightingInfos == null){
            weightingInfos = new ArrayList<>();
        }
        weightingInfos.add(new WeightedGainWeightingInfo(tprWeight, tnrWeight, probPos, probNeg));
    }

    /**
     * Updates the weighted gain value for each existing weighted gain config, which means basically all gain values are recalculated on the currently given TPR and TNR for this instance.
     */
    public void updateWeightedGainValues(){
        this.weightedGainValues = new ArrayList<>();
        if (this.weightingInfos == null) {
            System.out.println("No Weighting-Infos given!");
            return;
        }
        for (int i = 0; i < this.weightingInfos.size(); i++) {
            WeightedGainWeightingInfo weightingInfo = this.weightingInfos.get(i);
            this.weightedGainValues.add(weightingInfo.probPos*weightingInfo.tprWeight*this.getTpr() + weightingInfo.probNeg*weightingInfo.tnrWeight*this.getTnr());
        }
    }

    /**
     * Sets the classification-threshold of this EvaluationSet to a new given value and recalculates the confusion-matrix for this instance based on the new threshold.
     * @param threshold the new threshold.
     */
    public void recalculateConfusionMatrixForNewThreshold(double threshold){
        this.classificationThreshold = threshold;
        if (!this.hasScores()){
            throw new IllegalStateException("Could not perform confusion matrix recalculation as no scorings are present in this instance.");
        }

        this.resetConfusionMatrix();

        List<Double> tempPredictedScores = this.predictedScores;
        List<Integer> tempActualClasses = this.actualClasses;

        this.predictedScores = null;
        this.actualClasses = null;

        for (int i = 0; i < tempPredictedScores.size(); i++) {
            Double score = tempPredictedScores.get(i);
            int actualClass = tempActualClasses.get(i);
            this.evaluate(actualClass, score);
        }
    }

    void incrementTruePositives() {
        this.truePositives++;
    }

    void incrementFalsePositives() {
        this.falsePositives++;
    }

    void incrementTrueNegatives() {
        this.trueNegatives++;
    }

    void incrementFalseNegatives() {
        this.falseNegatives++;
    }

    /**
     * Sets all 4 main fields of the confusion-matrix represented by this instance back to 0.
     */
    public void resetConfusionMatrix(){
        this.truePositives = 0;
        this.trueNegatives = 0;
        this.falsePositives = 0;
        this.falseNegatives = 0;
    }

    public double getMatthewsCorCof() {
        return ((this.truePositives*this.trueNegatives) - (this.falsePositives*this.falseNegatives))/Math.sqrt((this.truePositives+this.falsePositives)*(this.truePositives+this.falseNegatives)*(this.trueNegatives+this.falsePositives)*(this.trueNegatives+this.falseNegatives));
    }

    /**
     * Returns a String containing a performance report that features:
     *
     * * tpr
     * * tnr
     * * precision
     * * negative predictive value
     * * accuracy
     * * F1-score
     * * Matthews Correlation Coefficient
     *
     * beside basic information about the EvaluationSet.
     * @return the performance-report-String
     */
    public String performanceReport() {
        return performanceReport(true, true);
    }

    /**
     * Prints a performance report of given values; primarily intended as helper method for reporting averaged values.
     * It will not calculate anything but merely construct a string that contains a formated display of the given values.
     * @param printTestSetMetadata if true, will print some basic data about the testset (#positives, #negatives, ...)
     * @param printBasicTestResults if true, prints values that would be contained in a confusion matrix
     * @param valueOutputPrecision how many digits behind the comma will be shown for values
     * @param truePositives number of true positives
     * @param trueNegatives number of true negatives
     * @param falsePositives number of false positives
     * @param falseNegatives number of false negatives
     * @param positives number of positives
     * @param negatives number of negatives
     * @param tpr true positives rate
     * @param tnr true negatives rate
     * @param ppv positive predictive value
     * @param npv negative predictive value
     * @param acc accuracy
     * @param f1score f1-score
     * @param auc area under (roc)-curve
     * @param mcc matthews correlation coefficient
     * @param weightedGainValues list of calculated weighting gain values
     * @param weightingInfos weighting info corresponding to the gain infos; user is responsible to provide correct ordering
     * @return the performance report as string
     */
    static String performanceReport(boolean printTestSetMetadata,
                                    boolean printBasicTestResults,
                                    int valueOutputPrecision,
                                    double truePositives,
                                    double trueNegatives,
                                    double falsePositives,
                                    double falseNegatives,
                                    double positives,
                                    double negatives,
                                    double tpr,
                                    double fpr,
                                    double tnr,
                                    double fnr,
                                    double ppv,
                                    double npv,
                                    double acc,
                                    double f1score,
                                    double auc,
                                    double mcc,
                                    List<Double> weightedGainValues,
                                    List<WeightedGainWeightingInfo> weightingInfos) {

        StringBuilder builder = new StringBuilder();

        String numberFormatString = "%.0" + valueOutputPrecision + "f";

        String numberOfItems = String.valueOf((int)(positives + negatives));

        if (printTestSetMetadata){
            int secondColLength = numberOfItems.length()+4;
            builder.append("\n----- Test-Set-Metadata ---------------").append("-".repeat(secondColLength)).append("\n| ").append("Total number of items          |    ").append((int) (positives + negatives)).append("    |\n");
            builder.append("| Number of positives items      |    ").append((int) positives).append(" ".repeat(secondColLength - String.valueOf((int) positives).length())).append("|\n");
            builder.append("| Number of negative items       |    ").append((int) negatives).append(" ".repeat(secondColLength - String.valueOf((int) negatives).length())).append("|\n");
            builder.append("| Percentage of positives (~)    |    ").append(String.format(Locale.ENGLISH, "%.02f", (positives / (positives + negatives)))).append(" ".repeat(secondColLength - 4)).append("|\n");
            builder.append("---------------------------------------").append("-".repeat(secondColLength)).append("\n\n");
        }

        if (printBasicTestResults) {
            int maxLength = String.valueOf((int) Math.max(Math.max(Math.max(truePositives, trueNegatives), falsePositives), falseNegatives)).length() + 4;
            builder.append("----- Basic test-results -").append("-".repeat(maxLength)).append("\n");
            builder.append("| True positives    |    ").append((int) truePositives).append(" ".repeat(maxLength - String.valueOf((int) truePositives).length())).append("|\n");
            builder.append("| False positives   |    ").append((int) falsePositives).append(" ".repeat(maxLength - String.valueOf((int) falsePositives).length())).append("|\n");
            builder.append("| True negatives    |    ").append((int) trueNegatives).append(" ".repeat(maxLength - String.valueOf((int) trueNegatives).length())).append("|\n");
            builder.append("| False negatives   |    ").append((int) falseNegatives).append(" ".repeat(maxLength - String.valueOf((int) falseNegatives).length())).append("|\n");
            builder.append("--------------------------").append("-".repeat(maxLength)).append("\n\n");
        }

        builder.append("!!! The following values are formatted to end ").append(valueOutputPrecision).append(" digits behind the comma. !!!\n");
        builder.append("----- Derived measurements -----------------------------\n");
        builder.append("| Sensitivity (TPR/recall)              |    ").append(String.format(Locale.ENGLISH, numberFormatString, tpr)).append("    |\n");
        builder.append("| False Positive Rate                   |    ").append(String.format(Locale.ENGLISH, numberFormatString, fpr)).append("    |\n");
        builder.append("| Specificity (TNR)                     |    ").append(String.format(Locale.ENGLISH, numberFormatString, tnr)).append("    |\n");
        builder.append("| False Negative Rate                   |    ").append(String.format(Locale.ENGLISH, numberFormatString, fnr)).append("    |\n");
        builder.append("| Precision (PPV)                       |    ").append(String.format(Locale.ENGLISH, numberFormatString, ppv)).append("    |\n");
        builder.append("| Negative predictive value             |    ").append(String.format(Locale.ENGLISH, numberFormatString, npv)).append("    |\n");
        builder.append("|-  -  -  -  -  -  -  -  -  -  -  -  -  | -  -  -  -  -|\n");
        builder.append("| Accuracy                              |    ").append(String.format(Locale.ENGLISH, numberFormatString, acc)).append("    |\n");
        builder.append("| F1-Score                              |    ").append(String.format(Locale.ENGLISH, numberFormatString, f1score)).append("    |\n");
        builder.append("| AUC                                   |    ").append(String.format(Locale.ENGLISH, numberFormatString, auc)).append("    |\n");
        builder.append("| Matthews Correlation Coefficient      |    ").append(String.format(Locale.ENGLISH, numberFormatString, mcc)).append("    |\n");
        builder.append("--------------------------------------------------------\n\n");

        if (weightedGainValues != null && weightingInfos != null) {
            builder.append("--------Weighted Gain information-----------------\n");

            if (weightedGainValues.size() != weightingInfos.size()){
                throw new IllegalStateException("Number of weighting infos and weightedTprTnr values in at least one EvaluationSet don't match!");
            }
            for (int i = 0; i < weightedGainValues.size(); i++) {
                Double weightedTprTnr = weightedGainValues.get(i);
                WeightedGainWeightingInfo weightingInfo = weightingInfos.get(i);
                builder.append("| Given class encounter probability estimates:   |\n");
                builder.append("|          prob(positive)    |      ").append(String.format(Locale.ENGLISH, numberFormatString, weightingInfo.probPos)).append("       |\n");
                builder.append("|          prob(negative)    |      ").append(String.format(Locale.ENGLISH, numberFormatString, weightingInfo.probNeg)).append("       |\n");
                builder.append("| Weights:                   |                   |\n");
                builder.append("|          weight(TPR)       |      ").append(String.format(Locale.ENGLISH, numberFormatString, weightingInfo.tprWeight)).append("       |\n");
                builder.append("|          weight(TNR)       |      ").append(String.format(Locale.ENGLISH, numberFormatString, weightingInfo.tnrWeight)).append("       |\n");
                double maxValue = weightingInfo.probPos * weightingInfo.tprWeight + weightingInfo.probNeg * weightingInfo.tnrWeight;
                builder.append("| Resulting max-value:       |      ").append(String.format(Locale.ENGLISH, numberFormatString, maxValue)).append("       |\n");
                builder.append("| WeightedScore:             |      ").append(String.format(Locale.ENGLISH, numberFormatString, weightedTprTnr)).append("       |\n");
                builder.append("| weightedScore/maxValue:    |      ").append(String.format(Locale.ENGLISH, numberFormatString, (weightedTprTnr / maxValue))).append("       |\n");
                if (i != weightedGainValues.size() - 1){
                    builder.append("|-  -  -  -  -  -  -  -  -  -|-  -  -  -  -  -  -|\n");
                }
            }
            builder.append("--------------------------------------------------\n\n");
        }

        return builder.toString();
    }

    /**
     * Prints a performance report similar to the parameterless version of the method but lets the user specify if
     * metadata and basic test results should be printed.
     * @param printTestSetMetadata if true additional information about the test-set will be printed
     * @param printBasicTestResults if true basic test results will be printed (like total number of true positives and so on)
     * @return the performance report
     */
    public String performanceReport(boolean printTestSetMetadata, boolean printBasicTestResults) {
        if (this.auc == 0){
            updateAUC();
        }

        StringBuilder builder = new StringBuilder();

        String numberFormatString = "%.0" + this.valueOutputPrecision + "f";

        String numberOfItems = String.valueOf((int)(this.getPositives() + this.getNegatives()));

        if (printTestSetMetadata){
            int secondColLength = numberOfItems.length()+4;
            builder.append("\n----- Test-Set-Metadata ---------------").append("-".repeat(secondColLength)).append("\n| ").append("Total number of items          |    ").append((int) (this.getPositives() + this.getNegatives())).append("    |\n");
            builder.append("| Number of positives items      |    ").append((int) this.getPositives()).append(" ".repeat(secondColLength - String.valueOf((int) this.getPositives()).length())).append("|\n");
            builder.append("| Number of negative items       |    ").append((int) this.getNegatives()).append(" ".repeat(secondColLength - String.valueOf((int) this.getNegatives()).length())).append("|\n");
            builder.append("| Percentage of positives (~)    |    ").append(String.format(Locale.ENGLISH, "%.02f", (this.getPositives() / (this.getPositives() + this.getNegatives())))).append(" ".repeat(secondColLength - 4)).append("|\n");
            builder.append("---------------------------------------").append("-".repeat(secondColLength)).append("\n\n");
        }

        if (printBasicTestResults) {
            int maxLength = String.valueOf((int) Math.max(Math.max(Math.max(this.truePositives, this.trueNegatives), this.falsePositives), this.falseNegatives)).length() + 4;
            builder.append("----- Basic test-results -").append("-".repeat(maxLength)).append("\n");
            builder.append("| True positives    |    ").append((int) this.truePositives).append(" ".repeat(maxLength - String.valueOf((int) this.truePositives).length())).append("|\n");
            builder.append("| False positives   |    ").append((int) this.falsePositives).append(" ".repeat(maxLength - String.valueOf((int) this.falsePositives).length())).append("|\n");
            builder.append("| True negatives    |    ").append((int) this.trueNegatives).append(" ".repeat(maxLength - String.valueOf((int) this.trueNegatives).length())).append("|\n");
            builder.append("| False negatives   |    ").append((int) this.falseNegatives).append(" ".repeat(maxLength - String.valueOf((int) this.falseNegatives).length())).append("|\n");
            builder.append("--------------------------").append("-".repeat(maxLength)).append("\n\n");
        }

        builder.append("!!! The following values are formatted to end ").append(this.valueOutputPrecision).append(" digits behind the comma. !!!\n");
        builder.append("----- Derived measurements -----------------------------\n");
        builder.append("| Sensitivity (TPR/recall)              |    ").append(String.format(Locale.ENGLISH, numberFormatString, this.getTpr())).append("    |\n");
        builder.append("| False Positive Rate                   |    ").append(String.format(Locale.ENGLISH, numberFormatString, this.getFpr())).append("    |\n");
        builder.append("| Specificity (TNR)                     |    ").append(String.format(Locale.ENGLISH, numberFormatString, this.getTnr())).append("    |\n");
        builder.append("| False Negative Rate                   |    ").append(String.format(Locale.ENGLISH, numberFormatString, this.getFnr())).append("    |\n");
        builder.append("| Precision (PPV)                       |    ").append(String.format(Locale.ENGLISH, numberFormatString, this.getPrecision())).append("    |\n");
        builder.append("| Negative predictive value             |    ").append(String.format(Locale.ENGLISH, numberFormatString, this.getNegativePredictiveValue())).append("    |\n");
        builder.append("|-  -  -  -  -  -  -  -  -  -  -  -  -  | -  -  -  -  -|\n");
        builder.append("| Accuracy                              |    ").append(String.format(Locale.ENGLISH, numberFormatString, this.getAccuracy())).append("    |\n");
        builder.append("| F1-Score                              |    ").append(String.format(Locale.ENGLISH, numberFormatString, this.getF1Score())).append("    |\n");
        builder.append("| AUC                                   |    ").append(String.format(Locale.ENGLISH, numberFormatString, this.auc)).append("    |\n");
        builder.append("| Matthews Correlation Coefficient      |    ").append(String.format(Locale.ENGLISH, numberFormatString, this.getMatthewsCorCof())).append("    |\n");
        builder.append("--------------------------------------------------------\n\n");


        updateWeightedGainValues();

        if (this.weightedGainValues != null && this.weightingInfos != null) {
            builder.append("--------Weighted Gain information-----------------\n");

            if (weightedGainValues.size() != weightingInfos.size()){
                throw new IllegalStateException("Number of weighting infos and weightedTprTnr values in at least one EvaluationSet don't match!");
            }
            for (int i = 0; i < weightedGainValues.size(); i++) {
                Double weightedTprTnr = weightedGainValues.get(i);
                WeightedGainWeightingInfo weightingInfo = this.weightingInfos.get(i);
                builder.append("| Given class encounter probability estimates:   |\n");
                builder.append("|          prob(positive)    |      ").append(String.format(Locale.ENGLISH, numberFormatString, weightingInfo.probPos)).append("       |\n");
                builder.append("|          prob(negative)    |      ").append(String.format(Locale.ENGLISH, numberFormatString, weightingInfo.probNeg)).append("       |\n");
                builder.append("| Weights:                   |                   |\n");
                builder.append("|          weight(TPR)       |      ").append(String.format(Locale.ENGLISH, numberFormatString, weightingInfo.tprWeight)).append("       |\n");
                builder.append("|          weight(TNR)       |      ").append(String.format(Locale.ENGLISH, numberFormatString, weightingInfo.tnrWeight)).append("       |\n");
                double maxValue = weightingInfo.probPos * weightingInfo.tprWeight + weightingInfo.probNeg * weightingInfo.tnrWeight;
                builder.append("| Resulting max-value:       |      ").append(String.format(Locale.ENGLISH, numberFormatString, maxValue)).append("       |\n");
                builder.append("| WeightedScore:             |      ").append(String.format(Locale.ENGLISH, numberFormatString, weightedTprTnr)).append("       |\n");
                builder.append("| weightedScore/maxValue:    |      ").append(String.format(Locale.ENGLISH, numberFormatString, (weightedTprTnr / maxValue))).append("       |\n");
                if (i != weightedGainValues.size() - 1){
                    builder.append("|-  -  -  -  -  -  -  -  -  -|-  -  -  -  -  -  -|\n");
                }
            }
            builder.append("--------------------------------------------------\n\n");
        }

        return builder.toString();
    }

    @Override
    public String toString() {
        return performanceReport();
    }

    public double getPositives() {
        return this.getTruePositives() + this.getFalseNegatives();
    }

    public double getNegatives() {
        return this.getTrueNegatives() + this.getFalsePositives();
    }

    public double getTpr() {
        return this.truePositives / this.getPositives();
    }

    public double getFpr() { return this.falsePositives / (this.falsePositives + this.trueNegatives); }

    public double getTnr() {
        return this.trueNegatives / this.getNegatives();
    }

    public double getFnr() { return this.falseNegatives / (this.truePositives + this.falseNegatives); }

    public double getPrecision() {
        return this.truePositives / (this.truePositives + this.falsePositives);
    }

    public double getNegativePredictiveValue() {
        return this.trueNegatives / (this.trueNegatives + this.falseNegatives);
    }

    public double getAccuracy() {
        return (this.truePositives + this.trueNegatives) / (this.getPositives() + this.getNegatives());
    }

    public double getF1Score() {
        return 2 * (this.getPrecision() * this.getTpr()) / (this.getPrecision() + this.getTpr());
    }

    public double getAuc() {
        updateAUC();
        return auc;
    }

    public List<Double> getPredictedScores() {
        return predictedScores;
    }

    public List<Integer> getActualClasses() {
        return actualClasses;
    }

    public void setAuc(double auc) {
        this.auc = auc;
    }

    public List<Double> getWeightedGainValues() {
        return weightedGainValues;
    }

    public void setWeightedGainValues(List<Double> weightedGainValues) {
        this.weightedGainValues = weightedGainValues;
    }

    public List<WeightedGainWeightingInfo> getWeightingInfos() {
        return weightingInfos;
    }

    public void setWeightingInfos(List<WeightedGainWeightingInfo> weightingInfos) {
        this.weightingInfos = weightingInfos;
    }

    public void setPredictedScores(List<Double> predictedScores) {
        this.predictedScores = predictedScores;
    }

    public void setActualClasses(List<Integer> actualClasses) {
        this.actualClasses = actualClasses;
    }

}

package bous.philipp.dblp.datatools.evaluation;

import java.util.ArrayList;
import java.util.List;

/**
 * Used in a scenario where only two classes exist. See the evaluate-methods
 * documentation for more information. Can be used for more advanced evaluation-purposes
 * in a cross-validation-scenario by holding a list of evaluation sets.
 * After evaluating a set of predictions, it can be used to provide detailed performance
 * reports which is the main use-case for this class.
 */
public class BinaryClassificationEvaluator implements Evaluator {
    private List<BinaryEvaluationSet> evaluationSets;

    private BinaryEvaluationSet currentEvaluationSet;

    private List<WeightedGainWeightingInfo> weightingInfos;

    private double classificationThreshold;

    /**
     * This standard-constructor will set the classification-threshold to a default of 0.5 and
     * will create and set a first {@link BinaryEvaluationSet}.
     */
    public BinaryClassificationEvaluator() {
        //threshold is set to 0.5 if not specified otherwise
        this.classificationThreshold = 0.5;
        this.evaluationSets = new ArrayList<>();
        BinaryEvaluationSet newSet = new BinaryEvaluationSet(this.classificationThreshold);
        evaluationSets.add(newSet);
        currentEvaluationSet = newSet;
    }

    /**
     * Creates a BinaryClassificationEvaluator taking a classification-threshold to be
     * used when labeling. Remark that this already creates a first evaluation set,
     * so if you want to just evaluate one test-run (i.e. not doing cross-validation),
     * you are already set. If you want to do crossvalidation you have to call
     * {@link BinaryClassificationEvaluator#newEvaluationSet()} after each individual run
     * to receive correctly averaged performance reports.
     * @param classificationThreshold the classification threshold used in labeling.
     */
    public BinaryClassificationEvaluator(double classificationThreshold) {
        this.classificationThreshold = classificationThreshold;
        this.evaluationSets = new ArrayList<>();
        BinaryEvaluationSet newSet = new BinaryEvaluationSet(this.classificationThreshold);
        evaluationSets.add(newSet);
        currentEvaluationSet = newSet;
    }


    /**
     * Adds a mew BinaryEvaluationSet to this instance which is then set as the currentEvaluationSet.
     */
    public void newEvaluationSet() {
        if (currentEvaluationSet.hasOnlyZeroCounts()) {
            return;
        }
        currentEvaluationSet.updateAUC();
        BinaryEvaluationSet newSet = new BinaryEvaluationSet(this.classificationThreshold);
        configureEvaluationSetWeightingInfos(newSet);
        this.evaluationSets.add(newSet);
        currentEvaluationSet = newSet;
    }


    /**
     * Adds a new WeightedGainWeightingInfo to this instance. Calling one of the performanceReport methods will include
     * estimated WeightedGain values for each configuration that is present in this class.
     * The new weighted-gain-config will be added to each EvaluationSet currently associated with this instance.
     *
     * Adding additional configs is especially useful to estimate a weighted gain on populations that have a different distribution than
     * validation-sets.
     * @param tprWeight weight that will be given to TPR
     * @param tnrWeight weight that will be given to TNR
     * @param probPos estimation of the probability to encounter a positive example
     * @param probNeg estimation of the probability to encounter a negative example
     */
    public void addWeightedGainConfig(double tprWeight, double tnrWeight, double probPos, double probNeg){
        if (this.weightingInfos == null) {
            this.weightingInfos = new ArrayList<>();
        }

        this.weightingInfos.add(new WeightedGainWeightingInfo(tprWeight, tnrWeight, probPos, probNeg));
        if (this.evaluationSets.size() == 1){
            this.evaluationSets.get(0).addWeightedGainConfig(tprWeight, tnrWeight, probPos, probNeg);
        }
    }

    /**
     * Configures the WeightingInfos for a given BinaryEvaluationSet which means that the weightingInfos of this
     * BinaryClassificationEvaluator will be mirrored to the the EvaluationSet.
     * @param binaryEvaluationSet the BinaryEvaluationSet to be configured
     */
    private void configureEvaluationSetWeightingInfos(BinaryEvaluationSet binaryEvaluationSet){
        if (weightingInfos == null){
            return;
        }
        for (int j = 0; j < weightingInfos.size(); j++) {
            WeightedGainWeightingInfo weightedGainWeightingInfo = weightingInfos.get(j);
            binaryEvaluationSet.addWeightedGainConfig(weightedGainWeightingInfo.tprWeight, weightedGainWeightingInfo.tnrWeight, weightedGainWeightingInfo.probPos, weightedGainWeightingInfo.probNeg);
        }
    }

    /**
     * Evaluates a given score and actual class; that means that the score is compared with the classification threshold of the current EvaluationSet
     * and according to the given actual class TN/TP/FN/FP values are incremented as necessary.
     * All calls of this method are delegated to the currentEvaluationSet. It is highly recommended to use this method whenever
     * a ML algorithm is used that is able to produce scores as it will save scoring and actual class data to the evaluation set to enable advanced
     * features like fast recalculation of measures for shifting thresholds! It is basically an intelligent wrapper around the basic incrementXY-methods.
     * @param actualClass actual class of the example from which the score was derived (1 for positive examples, else negative)
     * @param score the score that was calculated by a ML-model for the example in question
     * @return true if classification was correct, false if not (for convenience)
     */
    public boolean evaluate(double actualClass, double score){
        return currentEvaluationSet.evaluate(actualClass, score);
    }

    /**
     * Sets the classification thresholds for all contained BinaryEvaluationSets to a new value and recalculates their respective confusion matrices
     * based on the new threshold; this requires the EvaluationSets to have scoring and actual-class information!
     * This function can be used to quickly calculate differing measures for shifting thresholds without having to repeat the learning process to
     * gather the scores.
     * @param newThreshold the new threshold
     */
    public void recalculateConfusionMatricesForNewThreshold(double newThreshold) {
        for (int i = 0; i < evaluationSets.size(); i++) {
            BinaryEvaluationSet binaryEvaluationSet = evaluationSets.get(i);
            binaryEvaluationSet.recalculateConfusionMatrixForNewThreshold(newThreshold);
        }
    }

    /**
     * Increments current evaluation set's true positives count.
     */
    public void incrementTruePositives() {
        currentEvaluationSet.incrementTruePositives();
    }

    /**
     * Increments current evaluation set's false positives count.
     */
    public void incrementFalsePositives() {
        currentEvaluationSet.incrementFalsePositives();
    }

    /**
     * Increments current evaluation set's true negatives count.
     */
    public void incrementTrueNegatives() {
        currentEvaluationSet.incrementTrueNegatives();
    }

    /**
     * Increments current evaluation set's false negatives count.
     */
    public void incrementFalseNegatives() {
        currentEvaluationSet.incrementFalseNegatives();
    }


    /**
     * Returns a list of this instances evaluation-sets.
     * @return a list of this instances evaluation-sets
     */
    public List<BinaryEvaluationSet> getEvaluationSets() {
        return evaluationSets;
    }

    /**
     * Returns the current evaluation set.
     * @return the current evaluation set
     */
    public BinaryEvaluationSet getCurrentEvaluationSet() {
        return currentEvaluationSet;
    }

    /**
     * Returns a String containing a performance report that features:
     * <p>
     * * tpr
     * * tnr
     * * precision
     * * negative predictive value
     * * accuracy
     * * F1-score
     * <p>
     * averaged over all EvaluationSets that are present in this BinaryClassificationEvaluator-object.
     *
     * @return the performance-report string
     */
    public String performanceReportAveraged() {

        System.out.println("PRINTING AVERAGED REPORT\n");

        double avTruePositives = 0;
        double avTrueNegatives = 0;
        double avFalsePositives = 0;
        double avFalseNegatives = 0;

        double avPositives = 0;
        double avNegatives = 0;

        double avTpr = 0;
        double avFpr = 0;
        double avTnr = 0;
        double avFnr = 0;
        double avPrecision = 0;
        double avNpv = 0;

        double avAccuracy = 0;
        double avF1Score = 0;

        double avAuc = 0;
        double avMcc = 0;

        double[] averagedWeightedGainScores = null;
        boolean weightingInfoExistent = false;
        if (this.weightingInfos != null) {
            averagedWeightedGainScores = new double[this.weightingInfos.size()];
            weightingInfoExistent = true;
        }
        List<WeightedGainWeightingInfo> weightingInfos = null;

        for (int i = 0; i < evaluationSets.size(); i++) {
            BinaryEvaluationSet evaluationSet = evaluationSets.get(i);
            if (evaluationSet.getWeightingInfos() != null){
                weightingInfos = evaluationSet.getWeightingInfos();
                evaluationSet.updateWeightedGainValues();
            }
            if (evaluationSet.getAuc() == 0){
                evaluationSet.updateAUC();
            }
            if (weightingInfoExistent){
                for (int j = 0; j < averagedWeightedGainScores.length; j++) {
                    averagedWeightedGainScores[j] = averagedWeightedGainScores[j] + evaluationSet.getWeightedGainValues().get(j);
                }
            }
            avTruePositives += evaluationSet.getTruePositives();
            avFalsePositives += evaluationSet.getFalsePositives();
            avTrueNegatives += evaluationSet.getTrueNegatives();
            avFalseNegatives += evaluationSet.getFalseNegatives();

            avPositives += evaluationSet.getPositives();
            avNegatives += evaluationSet.getNegatives();

            avTpr += evaluationSet.getTpr();
            avFpr += evaluationSet.getFpr();
            avTnr += evaluationSet.getTnr();
            avFnr += evaluationSet.getFnr();
            avPrecision += evaluationSet.getPrecision();
            avNpv += evaluationSet.getNegativePredictiveValue();

            avAccuracy += evaluationSet.getAccuracy();
            avF1Score += evaluationSet.getF1Score();

            avAuc += evaluationSet.getAuc();

            avMcc += evaluationSet.getMatthewsCorCof();
        }

        int countEvalSets = this.evaluationSets.size();

        avTruePositives = avTruePositives / countEvalSets;
        avFalsePositives = avFalsePositives / countEvalSets;
        avTrueNegatives = avTrueNegatives / countEvalSets;
        avFalseNegatives = avFalseNegatives / countEvalSets;

        avPositives = avPositives / countEvalSets;
        avNegatives = avNegatives / countEvalSets;

        avTpr = avTpr / countEvalSets;
        avFpr = avFpr / countEvalSets;
        avTnr = avTnr / countEvalSets;
        avFnr = avFnr / countEvalSets;
        avPrecision = avPrecision / countEvalSets;
        avNpv = avNpv / countEvalSets;

        avAccuracy = avAccuracy / countEvalSets;
        avF1Score = avF1Score / countEvalSets;

        avAuc = avAuc / countEvalSets;
        avMcc = avMcc / countEvalSets;

        List<Double> avWeightedGainScores;
        if (weightingInfoExistent) {
            avWeightedGainScores = new ArrayList<>();
            for (int i = 0; i < averagedWeightedGainScores.length; i++) {
                double averagedWeightedGainScore = averagedWeightedGainScores[i];
                avWeightedGainScores.add(averagedWeightedGainScore / countEvalSets);
            }
        }else {
            avWeightedGainScores = null;
        }

        return BinaryEvaluationSet.performanceReport(
                false,
                false,
                4,
                avTruePositives,
                avTrueNegatives,
                avFalsePositives,
                avFalseNegatives,
                avPositives,
                avNegatives,
                avTpr,
                avFpr,
                avTnr,
                avFnr,
                avPrecision,
                avNpv,
                avAccuracy,
                avF1Score,
                avAuc,
                avMcc,
                avWeightedGainScores,
                weightingInfos
        );
    }

}

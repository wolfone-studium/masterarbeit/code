package bous.philipp.dblp.datatools.evaluation;

import lombok.Getter;

/**
 * Encapsulates values used to calculated a weighted-gain value in the evaluation of a
 * binary classifier.
 */
public class WeightedGainWeightingInfo {
    @Getter double tprWeight;
    @Getter double tnrWeight;
    @Getter double probPos;
    @Getter double probNeg;

    /**
     * Instanciates a weighted gain info object.
     * @param tprWeight weight for the true positive rate
     * @param tnrWeight weight for the true negative rate
     * @param probPos estimated probability to encounter a positive example
     * @param probNeg estimated probability to encounter a negative example
     */
    public WeightedGainWeightingInfo(double tprWeight, double tnrWeight, double probPos, double probNeg) {
        this.tprWeight = tprWeight;
        this.tnrWeight = tnrWeight;
        this.probPos = probPos;
        this.probNeg = probNeg;
    }
}

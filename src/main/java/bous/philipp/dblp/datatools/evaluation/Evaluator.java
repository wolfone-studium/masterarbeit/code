package bous.philipp.dblp.datatools.evaluation;

/**
 * Provides a contract for methods like evaluation of a given score to a value that represents a classification and
 * compare it to an actual class.
 */
interface Evaluator {
    /**
     * Evaluate a given score to a value that represents a classification and
     * compare it to an actual class.
     * @param actualClass the actual class of the example for which the score is supplied
     * @param score score of the example to be evaluated
     * @return should be true if actual class and estimated class are equal, false else
     */
    boolean evaluate(double actualClass, double score);
}

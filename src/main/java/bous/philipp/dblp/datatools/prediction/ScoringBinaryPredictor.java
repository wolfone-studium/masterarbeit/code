package bous.philipp.dblp.datatools.prediction;

import java.io.InputStream;

/**
 * A binary predictor is used to predict a given input in form of a vector (or stream of vectors)
 * to belong to one of two possible classes and return scores for the input vectors alongside the prediction.
 */
interface ScoringBinaryPredictor extends BinaryPredictor{
    /**
     * Takes a vector of doubles as input and returns a double[2], where the first value is the predicted class and the second
     * value the score that was returned by the ML-Algorithm; Implementers should make sure that the score is between 0 and 1.
     * @param vector some feature-vector
     * @return double[2]; first value:=predicted class; second value:=score given by the ML algorithm
     */
    double[] predictAndScore(double[] vector);

    /**
     * Takes data from an input-stream (i.e. potentially multiple vectors) and
     * returns an array of predictions in order of input-entries as well as their corresponding scores; implementers should make
     * sure that the score is between 0 and 1.
     * Can potentially be used to implement more efficient predicting for multiple vectors.
     * @param data a stream of feature vectors
     * @return array of predicted labels
     */
    double[][] predictAndScore(InputStream data);
}

package bous.philipp.dblp.datatools.prediction;

import bous.philipp.dblp.datatools.util.math.MathematicalFunctions;
import de.bwaldvogel.liblinear.*;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * A {@link ScoringBinaryPredictor} that utilizes the liblinear library.
 */
public class LiblinearPredictor implements ScoringBinaryPredictor {
    private double predictionThreshold;
    private Model model;

    /**
     * Constructs a liblinear-predictor using a given prediction-threshold
     * @param predictionThreshold prediction threshold to be used in labeling
     * @param model model to be used
     */
    public LiblinearPredictor(double predictionThreshold, Model model){
        this.predictionThreshold = predictionThreshold;
        this.model = model;
    }

    /**
     * Expects a vector WITHOUT one of the coordinates representing an unknown target value!
     * @param vector some feature-vector
     * @return a predicted class for this vector
     */
    @Override
    public double predict(double[] vector) {
        return predictAndScore(vector)[0];
    }

    @Override
    public double[] predict(InputStream data) {
        return predictAndScore(data)[0];
    }

    /**
     * Expects a vector WITHOUT one of the coordinates representing an unknown target value!
     * @param vector some feature-vector
     * @return an array containing the predicted target-value as first and the score that was calculated for this vector as second coordinates
     */
    @Override
    public double[] predictAndScore(double[] vector) {
        StringBuilder vectorAsString = new StringBuilder();
        vectorAsString.append(2 + " ");
        for (int i = 0; i < vector.length; i++) {
            double value = vector[i];
            if (value != 0) {
                //it seems as it would be necessary to use i+1 but in reality this will introduce a wrong input-shift in the testdata; readProblem in Liblinear is probably already accounting for this necessary shift so we would be doubling that
                vectorAsString.append(i).append(":").append(value).append(" ");
            }
        }
        vector = null;
        try {
            InputStream vectorAsInputStream = IOUtils.toInputStream(vectorAsString.toString().trim(), "UTF-8");
            Problem vectorAsProblem = Train.readProblem(vectorAsInputStream, model.getBias());
            Feature[] instance = vectorAsProblem.x[0];

            double[] scores = new double[2];
            double[] values = new double[1];

            if (model.isProbabilityModel() && !model.getSolverType().isSupportVectorRegression()){
                Linear.predictProbability(model, instance , scores);
            }else{
                if (model.getSolverType().isSupportVectorRegression()){
                    throw new IllegalStateException("Currently no support-vector-regression solvers are supported; please choose a logistic regression or standard SVC solver.");
                }
            /* there is no scoring available for SVM solvers in liblinear; this mirrors the scoring for logistic regression solvers; it applies a sigmoid-function for mapping to [0,1]
               Remark that no calibration is happening!!!
             */
                Linear.predictValues(model, instance, values);
                scores[1] = MathematicalFunctions.sigmoid(-values[0]);
                scores[0] = 1 - scores[1];
            }
            return new double[]{label(scores[1]), scores[1]};
        } catch (IOException | InvalidInputDataException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Expects every line to be in svmlight format, including a target value at the FIRST position (which should just be some integer
     * value to signal "unknown"). This constraint exists to leverage liblinear's very fast problem-reading process which is significantly
     * faster than reading every vector that should be classified one by one.
     * @param data a stream of feature vectors
     * @return a nested array containing where the first index corresponds to the vector and the second to prediction/score i.e. [0][0] is the prediction for first vector; [0][1] is the score for first vector.
     */
    @Override
    public double[][] predictAndScore(InputStream data) {
        List<Double> predictions = new ArrayList<>();
        List<Double> scoreList = new ArrayList<>();
        try {
            Problem vectors = Train.readProblem(data, model.getBias());

            for (int i = 0; i < vectors.x.length; i++) {
                Feature[] instance = vectors.x[i];

                if (vectors.y[i] != 0 && vectors.y[i] != 1){
                    throw new IllegalArgumentException("Unexpected target value: " + vectors.y[i]);
                }
                    /*  double[2] because we have 2 classes, the doc is not very precise about this method predictProbability but from looking into the code I
                        was able to derive that we are supposed
                        to give predictProbability an additional double array so it can place a score for each class into the respective array-coordinate. so scores[0]
                        is the score for a prediction value of 0 and scores[1] for 1. It seems that the sum of the array-values is normed to be 1.
                        Nonetheless the predicted class is what is returned by predictProbability, thus making
                        the method's name somewhat confusing. Works only with logistic regression models.
                     */
                double[] scores = new double[2];
                double[] values = new double[1];
                if (model.isProbabilityModel() && !model.getSolverType().isSupportVectorRegression()){
                    Linear.predictProbability(model, instance , scores);
                }else{
                    if (model.getSolverType().isSupportVectorRegression()){
                        throw new IllegalStateException("Currently no support-vector-regression solvers are supported; please choose a logistic regression or standard SVC solver.");
                    }
                    /* there is no scoring available for SVM solvers in liblinear; this mirrors the scoring for logistic regression solvers; it applies a sigmoid-function for mapping to [0,1]
                       Remark that no calibration is happening!!!
                     */
                    Linear.predictValues(model, instance, values);
                    scores[1] = MathematicalFunctions.sigmoid(-values[0]);
                    scores[0] = 1 - scores[1];
                    predictions.add(label(scores[1]));
                    scoreList.add(scores[1]);
                }
            }
        } catch (IOException | InvalidInputDataException e) {
            e.printStackTrace();
        }
        double[][] predictionsScores = new double[predictions.size()][2];
        for (int i = 0; i < predictions.size(); i++) {
            predictionsScores[i][0] =  predictions.get(i);
            predictionsScores[i][1] = scoreList.get(i);
        }
        return predictionsScores;
    }

    private double label(double value){
        if (value >= this.predictionThreshold){
            return 1;
        }else{
            return 0;
        }
    }
}

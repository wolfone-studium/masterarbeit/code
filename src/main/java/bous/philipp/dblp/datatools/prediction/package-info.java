/**
 * Provides predictors and means of predictor-representations on a common interface.
 */
package bous.philipp.dblp.datatools.prediction;
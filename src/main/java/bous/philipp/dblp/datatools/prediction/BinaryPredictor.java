package bous.philipp.dblp.datatools.prediction;

import java.io.InputStream;

/**
 * A binary predictor is used to predict a given input in form of a vector (or stream of vectors)
 * to belong to one of two possible classes.
 */
interface BinaryPredictor {
    /**
     * Takes a vector of doubles as input and returns a predicted class in form of a double value.
     * @param vector some feature-vector
     * @return the predicted class
     */
    double predict(double[] vector);

    /**
     * Takes data from an input-stream (i.e. potentially multiple vectors) and
     * returns an array of predictions in order of input-entries.
     * Can potentially be used to implement more efficient predicting for multiple vectors.
     * @param data a stream of feature vectors
     * @return array of predicted labels
     */
    double[] predict(InputStream data);
}

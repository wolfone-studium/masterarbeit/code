package bous.philipp.dblp.datatools.modelprocessing;

import bous.philipp.dblp.datatools.evaluation.BinaryClassificationEvaluator;
import bous.philipp.dblp.datatools.util.math.MathematicalFunctions;
import de.bwaldvogel.liblinear.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides methods to test SVM and LR models.
 */
public class TestingSvmLogReg {

    /**
     * Tests a given model on a given stream of test-data for a given {@link BinaryClassificationEvaluator} which is used to evaluate
     * the results returned by the model. Returns a list of indices of vectors from the test-data for which the prediction turned out wrong
     * for convenience.
     * @param testDataStream the test-data
     * @param model the model
     * @param evaluation evaluation instance
     * @return list of indices of vectors from the test-data for which the prediction turned out wrong
     */
    public static List<Integer> testModelLiblinear(InputStream testDataStream, Model model, BinaryClassificationEvaluator evaluation){
        List<Integer> errorIndices = new ArrayList<>();
        try {
            Problem testData = Train.readProblem(testDataStream, model.getBias());

            for (int i = 0; i < testData.x.length; i++) {
                Feature[] instance = testData.x[i];

                if (testData.y[i] != 0 && testData.y[i] != 1){
                    throw new IllegalArgumentException("Unexpected target value: " + testData.y[i]);
                }
                    /*  double[2] because we have 2 classes, the doc is not very precise about this method predictProbability but from looking into the code I
                        was able to derive that we are supposed
                        to give predictProbability an additional double array so it can place a score for each class into the respective array-coordinate. so scores[0]
                        is the score for a prediction value of 0 and scores[1] for 1. It seems that the sum of the array-values is normed to be 1.
                        Nonetheless the predicted class is what is returned by predictProbability, thus making
                        the method's name somewhat confusing. Works only with logistic regression models.
                     */
                double[] scores = new double[2];
                double[] values = new double[1];
                if (model.isProbabilityModel() && !model.getSolverType().isSupportVectorRegression()){
                    Linear.predictProbability(model, instance , scores);
                }else{
                    if (model.getSolverType().isSupportVectorRegression()){
                        throw new IllegalStateException("Currently no support-vector-regression solvers are supported; please choose a logistic regression or standard SVC solver.");
                    }
                    /* there is no scoring available for SVM solvers in liblinear; this mirrors the scoring for logistic regression solvers; it applies a sigmoid-function for mapping to [0,1]
                       Remark that no calibration is happening!!!
                     */
                    Linear.predictValues(model, instance, values);
                    scores[1] = MathematicalFunctions.sigmoid(-values[0]);
                    scores[0] = 1 - scores[1];
                }

                boolean predictionWasCorrect = evaluation.evaluate(testData.y[i], scores[1]);

                if (!predictionWasCorrect) {
                    errorIndices.add(i + 1);
                }

            }
        } catch (IOException | InvalidInputDataException e) {
            e.printStackTrace();
        }
        return errorIndices;
    }

}

/**
 * Contains classes/interfaces providing logic to do training/testing of SVM/LR models as well as
 * some archetype functions to initiate those processes.
 */
package bous.philipp.dblp.datatools.modelprocessing;
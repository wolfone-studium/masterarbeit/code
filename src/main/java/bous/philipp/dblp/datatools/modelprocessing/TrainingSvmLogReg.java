package bous.philipp.dblp.datatools.modelprocessing;

import bous.philipp.dblp.datatools.util.logging.LoggerFactory;
import de.bwaldvogel.liblinear.*;

import java.io.*;
import java.util.logging.Logger;

/**
 * Provides methods to train SVM and LR models.
 */
public class TrainingSvmLogReg {

    /**
     * Trains a model using the liblinear-library.
     * @param trainingDataStream the data on which the training should be performed as inputstream with data being in libsvm-format
     * @param bias a bias value
     * @param solverType the solver-type
     * @param constraintViolationCost the constraint-violation-cost
     * @param stoppingEpsilon the stopping-epsilon
     * @return the model created through the training process
     */
    static Model trainLiblinear(InputStream trainingDataStream, double bias, SolverType solverType, double constraintViolationCost, double stoppingEpsilon){
        try {
            Problem trainingData = Train.readProblem(trainingDataStream, bias);
            Parameter parameter = new Parameter(solverType, constraintViolationCost, stoppingEpsilon);
            return Linear.train(trainingData, parameter);
        } catch (IOException | InvalidInputDataException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Trains a model using the liblinear-library. This overloaded version takes an additional model-name and output-path and writes a model file.
     * @param trainingDataStream the data on which the training should be performed as inputstream with data being in libsvm-format
     * @param bias a bias value
     * @param solverType the solver-type
     * @param constraintViolationCost the constraint-violation-cost
     * @param stoppingEpsilon the stopping-epsilon
     * @param modelName the name under which the model should be saved
     * @param outPath folder to which the model should be written
     * @return the model created through the training process
     */
    static Model trainLiblinear(InputStream trainingDataStream, double bias, SolverType solverType, double constraintViolationCost, double stoppingEpsilon, String modelName, String outPath){
        try (FileWriter writer = new FileWriter(outPath + File.separator + modelName)) {
            Model model = trainLiblinear(trainingDataStream, bias, solverType, constraintViolationCost, stoppingEpsilon);
            model.save(writer);
            return model;
        } catch (FileNotFoundException e) {
            Logger logger = LoggerFactory.getLogger(TrainingSvmLogReg.class.getCanonicalName());
            logger.severe("FileNotFoundException; may occur due to the 'modelOutputPath'-property not representing a valid absolute file path.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}

package bous.philipp.dblp.datatools.modelprocessing;

import bous.philipp.dblp.datatools.util.config.ExecutionConfigProperties;
import bous.philipp.dblp.datatools.evaluation.WeightedGainWeightingInfo;
import de.bwaldvogel.liblinear.SolverType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Holds configuration-properties for configuration of a cross-validated training using the liblinear-library.
 */
class LiblinearCVProperties implements ExecutionConfigProperties {
    String datasetLocation;
    String datasetPrefix;
    SolverType solverType;
    double predictionThreshold;
    double[] averagedReportForAdditionalThresholds;
    double constraintViolationCost;
    double stoppingEpsilon;
    double bias;
    boolean writeModel;
    boolean printAllReports;
    int numberOfExportedErrors;
    List<WeightedGainWeightingInfo> weightingInfos;

    /**
     * Reads properties for a CV run using the liblinear-library and a CV-Trainingset created by one of the
     * respective methods in {@link bous.philipp.dblp.datatools.featureconstruction.vectorization.BagOfWordsVectorizer} from
     * a properties file at a given path. Such a properties file may contain following values:
     * A configuration-properties-file can contain following values:
     * <ul>
     *  <li>svmLightFilePath : path to the folder containing a set of svmlight-files (as created by {@link bous.philipp.dblp.datatools.featureconstruction.vectorization.BagOfWordsVectorizationArchetypes#convertSingleFeatureToSVMLightCrossValidationSetsBalanced} or similar methods)</li>
     *  <li>datasetPrefix : conversion methods in {@link bous.philipp.dblp.datatools.featureconstruction.vectorization.BagOfWordsVectorizer} will add "(NUMBER).svmlight" as a suffix to the individual created vector-sets. The prefix of the dataset in question has to be specified here.</li>
     *  <li>solverType : a string-representation of one of the solverTypes allowed in liblinear as defined by the respective Enumeration-Class. Please refer to Liblinear to see possible values.</li>
     *  <li>predictionThreshold (optional, default = 0.5): the scores calculated during validation will be tested against a threshold to decide which class will be assigned to a validation example. equal or greater than threshold -> 1; smaller than threshold ->0</li>
     *  <li>averagedReportForAdditionalThresholds (optional): comma separated list of additional thresholds for which an averaged performance report will be calculated</li>
     *  <li>constraintViolationCost : C in https://www.csie.ntu.edu.tw/~cjlin/papers/liblinear.pdf ; please refer to this paper to see how and for which solvers it is relevant</li>
     *  <li>stoppingEpsilon : Epsilon in https://www.csie.ntu.edu.tw/~cjlin/papers/liblinear.pdf ; please refer to this paper to see how and for which solvers it is relevant</li>
     *  <li>bias : b in https://www.csie.ntu.edu.tw/~cjlin/papers/liblinear.pdf ; please refer to this paper to see how and for which solvers it is relevant</li>
     *  <li>writeModel (optional): if "true" model files for the models resulting from the individual CV-folds will be written to the directory where the configuration file resides</li>
     *  <li>printAllReports (optional): if "true" will print evaluation reports for every single fold; otherwise will only print an averaged evaluation-report in the end</li>
     *  <li>numberOfExportedErrors (optional): if this is specified with a value bigger than 0, the method will attempt to export this number of wrongly classified records (the respective feature) into a separate file; the errors will be chosen randomly from all
     *         existing ones; it is possible to export all errors by using -1 as a value; REMARK it is necessary to have a valid DB online so the algorithm can extract the relevant records and features from
     *         the db, using the mapping that was generated for the vectors by one of the methods in {@link bous.philipp.dblp.datatools.featureconstruction.vectorization.BagOfWordsVectorizer}; if any part of this setup is corrupted, this method will fail to write an
     *         error export</li>
     *  <li>gainWeightings (optional): specification of one or more semicolon-seperated configs allows calculation of weighted gain as evaluation metric; values inside a single config should be separated by commas
     *         expected format: TPR_WEIGHT,TNR_WEIGHT,POSITIVE_CLASS_PROBABILITY_ESTIMATION,NEGATIVE_CLASS_PROBABILITY_ESTIMATION;
     *         example: gainWeightings=1.0,0.8,0.5,0.5;1.0,0.8,0.08,0.92
     *         (please refer to the evaluation-section of the thesis for more conceptual information)</li>
     * </ul>
     * @param configPath path to the properties file
     * @param methodVersionId should be methodVersionId of the calling method (security measure to prevent processing of input with old method versions)
     */
    public void readProperties(Path configPath, String methodVersionId){
        Properties properties = new Properties();
        File propertiesFile = configPath.toFile();

        try (FileInputStream in = new FileInputStream(propertiesFile)){
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String methodID = properties.getProperty("METHOD_VERSION_ID");
        if (!methodVersionId.equals(methodID)){
            throw new IllegalStateException("METHOD_VERSION_ID MISMATCH: the properties file specifies a different ID");
        }
        this.datasetLocation = properties.getProperty("datasetLocation");
        this.datasetPrefix = properties.getProperty("datasetPrefix");
        this.solverType = SolverType.valueOf(properties.getProperty("solverType"));

        if (properties.getProperty("predictionThreshold") != null){
            this.predictionThreshold = Double.valueOf(properties.getProperty("predictionThreshold"));
        }else{
            this.predictionThreshold = 0.5;
        }

        if (properties.getProperty("averagedReportForAdditionalThresholds") != null){
            String additionalThresholds = properties.getProperty("averagedReportForAdditionalThresholds");
            String[] tokens = additionalThresholds.split(",\\s*");
            double[] thresholds = new double[tokens.length];
            for (int i = 0; i < tokens.length; i++) {
                String token = tokens[i];
                thresholds[i] = Double.valueOf(token);
            }
            this.averagedReportForAdditionalThresholds = thresholds;
        }

        String weightingInfoString = properties.getProperty("gainWeightings");
        if (weightingInfoString != null){
            this.weightingInfos = new ArrayList<>();
            String[] weightings = weightingInfoString.split(";\\s*");
            for (int i = 0; i < weightings.length; i++) {
                String weighting = weightings[i];
                String[] valueString = weighting.split(",\\s*");
                this.weightingInfos.add(new WeightedGainWeightingInfo(
                        Double.parseDouble(valueString[0]),
                        Double.parseDouble(valueString[1]),
                        Double.parseDouble(valueString[2]),
                        Double.parseDouble(valueString[3])
                ));
            }
        }

        if (properties.getProperty("numberOfExportedErrors") != null){
            this.numberOfExportedErrors = Integer.valueOf(properties.getProperty("numberOfExportedErrors"));
        }else{
            this.numberOfExportedErrors = 0;
        }

        this.constraintViolationCost = Double.valueOf(properties.getProperty("constraintViolationCost"));
        this.stoppingEpsilon = Double.valueOf(properties.getProperty("stoppingEpsilon"));
        this.bias = Double.valueOf(properties.getProperty("bias"));
        if (properties.getProperty("writeModel") != null){
            this.writeModel = Boolean.valueOf(properties.getProperty("writeModel"));
        }else {
            this.writeModel = false;
        }

        if (properties.getProperty("printAllReports") != null){
            this.printAllReports = Boolean.valueOf(properties.getProperty("printAllReports"));
        }else {
            this.printAllReports = false;
        }

    }
}

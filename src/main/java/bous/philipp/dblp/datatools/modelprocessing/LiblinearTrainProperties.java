package bous.philipp.dblp.datatools.modelprocessing;

import bous.philipp.dblp.datatools.util.config.ExecutionConfigProperties;
import de.bwaldvogel.liblinear.SolverType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Holds configuration-properties for configuration to train a liblinear-library-model.
 */
public class LiblinearTrainProperties implements ExecutionConfigProperties {
    String svmLightFileLocation;
    SolverType solverType;
    double constraintViolationCost;
    double stoppingEpsilon;
    double bias;
    String modelOutputPath;

    /**
     * Reads properties to configure a training-run with liblinear.
     * A configuration-file should contain following values:
     * <ul>
     *     <li>METHOD_VERSION_ID - id of the method to be called (for security)</li>
     *     <li>svmLightFilePath - complete path to the datafile</li>
     *     <li>modelOutputPath - complete path of the model output-file</li>
     *     <li>solverType - see the respectiv enum class in the liblinear library for allowed values</li>
     *     <li>constraintViolationCost</li>
     *     <li>stoppingEpsilon</li>
     *     <li>bias</li>
     * </ul>
     * @param configPath absolute path to the properties file
     * @param methodVersionId a method version id that will be checked against the method version id specified in the properties file
     */
    public void readProperties(Path configPath, String methodVersionId){
        Properties properties = new Properties();
        File propertiesFile = configPath.toFile();

        try (FileInputStream in = new FileInputStream(propertiesFile)){
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String methodID = properties.getProperty("METHOD_VERSION_ID");
        if (!methodVersionId.equals(methodID)){
            throw new IllegalStateException("METHOD_VERSION_ID MISMATCH: the properties file specifies a different ID");
        }
        this.svmLightFileLocation = properties.getProperty("svmLightFileLocation");
        if (this.svmLightFileLocation == null || this.svmLightFileLocation.equals("")) {
            throw new IllegalStateException("No valid svmLightFilePath-property value given.");
        }
        this.modelOutputPath = properties.getProperty("modelOutputPath");
        if (this.modelOutputPath == null || this.modelOutputPath.equals("")) {
            throw new IllegalStateException("No valid modelOutputPath-property value given.");
        }
        this.solverType = SolverType.valueOf(properties.getProperty("solverType"));

        this.constraintViolationCost = Double.valueOf(properties.getProperty("constraintViolationCost"));
        this.stoppingEpsilon = Double.valueOf(properties.getProperty("stoppingEpsilon"));
        this.bias = Double.valueOf(properties.getProperty("bias"));


    }
}

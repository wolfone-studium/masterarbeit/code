package bous.philipp.dblp.datatools.modelprocessing;

import bous.philipp.dblp.datatools.featureconstruction.vectorization.BagOfWordsVectorizationArchetypes;
import bous.philipp.dblp.datatools.featureconstruction.vectorization.BagOfWordsVectorizer;
import bous.philipp.dblp.datatools.featureconstruction.vectorization.VectorDBMapping;
import bous.philipp.dblp.datatools.util.database.DataSourceFactory;
import bous.philipp.dblp.datatools.util.logging.LoggerFactory;
import bous.philipp.dblp.datatools.evaluation.BinaryClassificationEvaluator;
import bous.philipp.dblp.datatools.evaluation.WeightedGainWeightingInfo;
import com.mysql.cj.jdbc.MysqlDataSource;
import de.bwaldvogel.liblinear.Model;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Provides high level methods to perform tasks using the liblinear-library.
 */
public class ConfigurableLiblinearTaskArchetypes {


    /**
     * Tests a model on labeled data given in a svmlight-file.
     * Will ask for an appropriate properties-file location on execution; this
     * file should contain following properties:
     * <ul>
     *     <li>METHOD_VERSION_ID - id of the method to be called (for security)</li>
     *     <li>svmLightFilePath - complete path to the datafile</li>
     *     <li>modelFilePath - complete path to the model-file</li>
     *     <li>predictionThreshold</li>
     *     <li>weightingInfos</li>
     * </ul>
     */
    public static void testLiblinearModel() {
        final String METHOD_VERSION_ID = "tllm-05.05.2019-1";

        Scanner scanner = new Scanner(System.in);

        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current relative path is: " + s);
        System.out.println("Enter configuration location for testing liblinear-model on svmlight labeled data: ");
        Path configPath = Paths.get(scanner.next());
        LiblinearModelTestProperties props = new LiblinearModelTestProperties();
        props.readProperties(configPath, METHOD_VERSION_ID);

        BinaryClassificationEvaluator evaluation = new BinaryClassificationEvaluator(props.predictionThreshold);

        if (props.weightingInfos != null){
            for (int i = 0; i < props.weightingInfos.size(); i++) {
                WeightedGainWeightingInfo weightingInfo = props.weightingInfos.get(i);
                evaluation.addWeightedGainConfig(
                        weightingInfo.getTprWeight(),
                        weightingInfo.getTnrWeight(),
                        weightingInfo.getProbPos(),
                        weightingInfo.getProbNeg());
            }
        }

        try {
            InputStream data = new FileInputStream(props.svmLightFilePath);
            Model model = Model.load(new File(props.modelFilePath));
            List<Integer> errorIndices = TestingSvmLogReg.testModelLiblinear(data, model, evaluation);
            if (props.numberOfExportedErrors > 0 || props.numberOfExportedErrors == -1){
                //System.out.println(errorIndices);
                writeErrorFileForSingleColumnTestRun(errorIndices, props, false);
            }
            System.out.println(evaluation.performanceReportAveraged());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Trains a model and writes it to a file based on a single set of labeled vectors
     * in a single svmlight-file.
     * Will ask for an appropriate properties-file location on execution; this
     * file should contain following properties:
     * <ul>
     *     <li>METHOD_VERSION_ID - id of the method to be called (for security)</li>
     *     <li>svmLightFilePath - complete path to the datafile</li>
     *     <li>modelOutputPath - complete path of the model output-file</li>
     *     <li>solverType - see the respectiv enum class in the liblinear library for allowed values</li>
     *     <li>constraintViolationCost</li>
     *     <li>stoppingEpsilon</li>
     *     <li>bias</li>
     * </ul>
     */
    public static void trainLiblinearAndWriteModelFile(){
        final String METHOD_VERSION_ID = "tllawmf-04.05.2019-1";

        Scanner scanner = new Scanner(System.in);

        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current relative path is: " + s);
        System.out.println("Enter configuration location for training liblinear on svmlight labeled data: ");
        Path configPath = Paths.get(scanner.next());
        LiblinearTrainProperties props = new LiblinearTrainProperties();
        props.readProperties(configPath, METHOD_VERSION_ID);

        try {
            InputStream data = new FileInputStream(props.svmLightFileLocation);

            TrainingSvmLogReg.trainLiblinear(
                    data,
                    props.bias,
                    props.solverType,
                    props.constraintViolationCost,
                    props.stoppingEpsilon,
                    FilenameUtils.getName(props.modelOutputPath),
                    FilenameUtils.getFullPath(props.modelOutputPath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Performs a crossvalidation training using liblinear on vectors in the svmlight-format.
     * Asks for a location of a configuration file that contains the necessary information to
     * configure the details for the run. This approach is taken to reduce input time for subsequent runs
     * as well as having the config-files as documentation of the exact parameters of the run.
     *
     * A configuration-properties-file can contain following values:
     * <ul>
     *  <li>svmLightFilePath : path to the folder containing a set of svmlight-files (as created by {@link BagOfWordsVectorizationArchetypes#convertSingleFeatureToSVMLightCrossValidationSetsBalanced} or similar methods)</li>
     *  <li>datasetPrefix : conversion methods in {@link BagOfWordsVectorizer} will add "(NUMBER).svmlight" as a suffix to the individual created vector-sets. The prefix of the dataset in question has to be specified here.</li>
     *  <li>solverType : a string-representation of one of the solverTypes allowed in liblinear as defined by the respective Enumeration-Class. Please refer to Liblinear to see possible values.</li>
     *  <li>predictionThreshold : the scores calculated during validation will be tested against a threshold to decide which class will be assigned to a validation example. Equal or greater than threshold evaluates to 1; smaller than threshold evaluates to 0</li>
     *  <li>averagedReportForAdditionalThresholds : comma separated list of additional thresholds for which an averaged performance report will be calculated</li>
     *  <li>constraintViolationCost : C in https://www.csie.ntu.edu.tw/~cjlin/papers/liblinear.pdf ; please refer to this paper to see how and for which solvers it is relevant</li>
     *  <li>stoppingEpsilon : Epsilon in https://www.csie.ntu.edu.tw/~cjlin/papers/liblinear.pdf ; please refer to this paper to see how and for which solvers it is relevant</li>
     *  <li>bias : b in https://www.csie.ntu.edu.tw/~cjlin/papers/liblinear.pdf ; please refer to this paper to see how and for which solvers it is relevant</li>
     *  <li>writeModel : if "true" model files for the models resulting from the individual CV-folds will be written to the directory where the configuration file resides</li>
     *  <li>printAllReports : if "true" will print evaluation reports for every single fold; otherwise will only print an averaged evaluation-report in the end</li>
     *  <li>numberOfExportedErrors : if this is specified with a value bigger than 0, the method will attempt to export this number of wrongly classified records (the respective feature) into a separate file; the errors will be chosen randomly from all
     *         existing ones; it is possible to export all errors by using -1 as a value; REMARK it is necessary to have a valid DB online so the algorithm can extract the relevant records and features from
     *         the db, using the mapping that was generated for the vectors by one of the methods in {@link BagOfWordsVectorizer}; if any part of this setup is corrupted, this method will fail to write an
     *         error export</li>
     *  <li>gainWeightings : specification of one or more semicolon-seperated configs allows calculation of weighted gain as evaluation metric; values inside a single config should be separated by commas
     *         expected format: TPR_WEIGHT,TNR_WEIGHT,POSITIVE_CLASS_PROBABILITY_ESTIMATION,NEGATIVE_CLASS_PROBABILITY_ESTIMATION;
     *         example: gainWeightings=1.0,0.8,0.5,0.5;1.0,0.8,0.08,0.92
     *         (please refer to the evaluation-section of the thesis for more conceptual information)</li>
     * </ul>
     */
    public static void trainLiblinearCrossValidationFromSvmLight(){

        final String METHOD_VERSION_ID = "tllcvfsvml-05.03.2019-1";

        Scanner scanner = new Scanner(System.in);

        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current relative path is: " + s);
        System.out.println("Enter configuration location for training liblinear on crossvalidation dataset: ");
        Path configPath = Paths.get(scanner.next());
        LiblinearCVProperties props = new LiblinearCVProperties();
        props.readProperties(configPath, METHOD_VERSION_ID);

        AtomicInteger numberOfFiles = new AtomicInteger(0);
        try (Stream<Path> paths = Files.walk(Paths.get(props.datasetLocation))){
            paths
                    .filter(path -> FilenameUtils.getBaseName(path.toString()).startsWith(props.datasetPrefix))
                    .filter(path -> FilenameUtils.isExtension(path.toString(), "svmlight"))
                    .filter(path -> !path.toString().contains("_ALL"))
                    .forEach((path -> numberOfFiles.incrementAndGet()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        SequenceInputStream inputStream;
        BinaryClassificationEvaluator evaluation = new BinaryClassificationEvaluator(props.predictionThreshold);

        if (props.weightingInfos != null){
            for (int i = 0; i < props.weightingInfos.size(); i++) {
                WeightedGainWeightingInfo weightingInfo = props.weightingInfos.get(i);
                evaluation.addWeightedGainConfig(
                        weightingInfo.getTprWeight(),
                        weightingInfo.getTnrWeight(),
                        weightingInfo.getProbPos(),
                        weightingInfo.getProbNeg());
            }
        }

        for (int i = 0; i < numberOfFiles.get(); i++){
            evaluation.newEvaluationSet();
            inputStream = buildSequenceInputStreamFromCrossValidationDataFilesSvmLight(props.datasetLocation, props.datasetPrefix, i);
            String testDataLocationPrefix = props.datasetLocation + File.separator + props.datasetPrefix + "(" + i + ")";
            try (FileInputStream testData = new FileInputStream(testDataLocationPrefix + ".svmlight");
                 BufferedInputStream testDataBuffered = new BufferedInputStream(testData)){

                final Model model;
                if (props.writeModel){
                    model = TrainingSvmLogReg.trainLiblinear(
                            inputStream,
                            props.bias,
                            props.solverType,
                            props.constraintViolationCost,
                            props.stoppingEpsilon,
                            props.datasetPrefix + "-MODEL(" + i + ")",
                            configPath.getParent().toString());
                }else{
                    model = TrainingSvmLogReg.trainLiblinear(inputStream, props.bias, props.solverType, props.constraintViolationCost, props.stoppingEpsilon);
                }

                List<Integer> errorIndices = TestingSvmLogReg.testModelLiblinear(testDataBuffered, Objects.requireNonNull(model), evaluation);

                if (props.numberOfExportedErrors > 0 || props.numberOfExportedErrors == -1){
                    writeErrorFileForCV(errorIndices, testDataLocationPrefix, props, true);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (props.printAllReports){
                System.out.println(evaluation.getCurrentEvaluationSet().performanceReport(false, false));
            }

        }
        System.out.println(evaluation.performanceReportAveraged());
        if(props.averagedReportForAdditionalThresholds != null && props.averagedReportForAdditionalThresholds.length > 0){
            for (int i = 0; i < props.averagedReportForAdditionalThresholds.length; i++) {
                double threshold = props.averagedReportForAdditionalThresholds[i];
                System.out.println("Recalculating metrics for threshold: " + threshold);
                evaluation.recalculateConfusionMatricesForNewThreshold(threshold);
                System.out.println("Resulting averaged report: ");
                System.out.println(evaluation.performanceReportAveraged() + "\n");
            }
        }
    }

    private static void writeErrorFileForSingleColumnTestRun(List<Integer> errorIndices, LiblinearModelTestProperties props, boolean randomize){
        if (randomize){
            Collections.shuffle(errorIndices);
        }
        VectorDBMapping mapping = VectorDBMapping.fromFile(Path.of(props.vectorMappingFilePath));
        String errorFilePath = props.errorFileOutputPath;
        File errorExport = new File(errorFilePath);

        Properties properties = new Properties();

        try (FileInputStream in = new FileInputStream(props.testVectorsPropertiesFilePath)) {
            properties.load(in);
        } catch (IOException ex) {
            Logger lgr = LoggerFactory.getLogger(ConfigurableLiblinearTaskArchetypes.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }

        MysqlDataSource dataSource = DataSourceFactory.getMySQLDataSource(properties.getProperty("dbPropertiesFilePath"));
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
        } catch (
                SQLException e) {
            e.printStackTrace();
        }

        try (FileWriter fileWriter = new FileWriter(errorExport); BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
            for (int j = 0; j < props.numberOfExportedErrors; j++){
                if (errorIndices.size() > j && mapping != null){
                    writeError(errorIndices.get(j), mapping, properties, connection, bufferedWriter);
                }else{
                    break;
                }
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    //testDataLocationPrefix is the path to the file without "_MAPPING.vdmap"
    private static void writeErrorFileForCV(List<Integer> errorIndices, String testDataLocationPrefix, LiblinearCVProperties props, boolean randomize){
        if (randomize){
            Collections.shuffle(errorIndices);
        }
        VectorDBMapping mapping = VectorDBMapping.fromFile(Path.of(testDataLocationPrefix + "_MAPPING.vdbmap"));
        String errorFilePath = testDataLocationPrefix + "-ERROR_EXPORTS.errors";
        File errorExport = new File(errorFilePath);

        Properties properties = new Properties();

        try (FileInputStream in = new FileInputStream(props.datasetLocation + File.separator + props.datasetPrefix + ".properties")) {
            properties.load(in);
        } catch (IOException ex) {
            Logger lgr = LoggerFactory.getLogger(ConfigurableLiblinearTaskArchetypes.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }

        MysqlDataSource dataSource = DataSourceFactory.getMySQLDataSource(properties.getProperty("dbPropertiesFilePath"));
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
        } catch (
                SQLException e) {
            e.printStackTrace();
        }

        try (FileWriter fileWriter = new FileWriter(errorExport); BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
            for (int j = 0; j < props.numberOfExportedErrors; j++){
                if (errorIndices.size() > j && mapping != null){
                    writeError(errorIndices.get(j), mapping, properties, connection, bufferedWriter);
                }else{
                    break;
                }
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeError(
            int index,
            VectorDBMapping mapping,
            Properties properties,
            Connection connection,
            BufferedWriter bufferedWriter
    ) throws SQLException, IOException {

        String mappedValue = mapping.get(index);
        if (mappedValue == null) {
            return;
        }
        String[] tableNameId = mappedValue.split(":::");
        int recordId = Integer.valueOf(tableNameId[1]);

        Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        Statement statement2 = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

        statement.setFetchSize(Integer.MIN_VALUE);

        ResultSet resultSet2 = statement2.executeQuery("SELECT cs_classification FROM " + properties.getProperty("recordTableName") + " WHERE id=" + recordId);
        resultSet2.next();
        bufferedWriter.write("\n--------------------------------------------------------------\n");
        bufferedWriter.write("tablename + artificial id: " + mappedValue + "\n");
        bufferedWriter.write("real class would have been: " + resultSet2.getString(1) + "\n\n");

        statement2.setFetchSize(Integer.MIN_VALUE);
        ResultSet resultSet = statement.executeQuery("SELECT type, value FROM " + properties.getProperty("featureTableName") + " WHERE record_id=" + recordId);
        while (resultSet.next()) {
            String type = resultSet.getString(1);
            bufferedWriter.write(type + ":" + " ".repeat(20 - type.length()) + resultSet.getString(2) + "\n");
        }
    }

    /**
     * Helper to allow convenient building of SequenceInputStreams from cross validation data files in the svmlight format as needed by
     * {@link ConfigurableLiblinearTaskArchetypes#trainLiblinearCrossValidationFromSvmLight()}
     * @param dataSetLocation location of the data-set
     * @param dataSetPrefix cross-validation-file-set-name without the numbering suffix and file extension. For example: ABSTRACT_CV-SET_6-2-2019_15-0-5 instead of ABSTRACT_CV-SET_6-2-2019_15-0-5(0).svmlight
     * @param leaveOutIndex specifies which file in the vs-data-set will be not included in the returned input stream so it can be used for testing.
     * @return a SequenceInputStream that consists of all sets except the one defined by the leaveOutIndex
     */
    private static SequenceInputStream buildSequenceInputStreamFromCrossValidationDataFilesSvmLight(String dataSetLocation, String dataSetPrefix, int leaveOutIndex){

        Vector<InputStream> streams = new Vector<>();

        try (Stream<Path> paths = Files.walk(Paths.get(dataSetLocation))) {
            paths
                    .filter(path -> FilenameUtils.getBaseName(path.toString()).startsWith(dataSetPrefix))
                    .filter(path -> FilenameUtils.isExtension(path.toString(), "svmlight"))
                    .filter(path -> !path.toString().contains("_ALL"))
                    .map(path -> FilenameUtils.removeExtension(path.toString()))
                    .forEach((path) ->{
                        if (!StringUtils.endsWith(path, "(" + leaveOutIndex + ")")){
                            try {
                                FileInputStream inputStream = new FileInputStream(path + ".svmlight");
                                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                                streams.add(bufferedInputStream);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new SequenceInputStream(streams.elements());
    }


}

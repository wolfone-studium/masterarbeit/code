package bous.philipp.dblp.datatools.modelprocessing;

import bous.philipp.dblp.datatools.util.config.ExecutionConfigProperties;
import bous.philipp.dblp.datatools.evaluation.WeightedGainWeightingInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Holds configuration-properties for configuration of a test-run in testing a liblinear-library-model.
 */
public class LiblinearModelTestProperties implements ExecutionConfigProperties {
    String errorFileOutputPath;
    String svmLightFilePath;
    String modelFilePath;
    String vectorMappingFilePath;
    String testVectorsPropertiesFilePath;
    double predictionThreshold;
    int numberOfExportedErrors;
    List<WeightedGainWeightingInfo> weightingInfos;

    /**
     * Reads configuration for a liblinear-test-run from a config-file, which should
     * contain the following properties:
     * <ul>
     *     <li>METHOD_VERSION_ID - id of the method to be called (for security)</li>
     *     <li>svmLightFileLocation - complete path to the datafile</li>
     *     <li>modelFileLocation - complete path to the model-file</li>
     *     <li>vectorMappingFilePath - only necessary when errors should be exported</li>
     *     <li>errorFileOutputPath - error-file to be printed to, only necessary, when errors should be exported</li>
     *     <li>testVectorsPropertiesFilePath - path to the properties file that was used to configure the creation of the used vectors</li>
     *     <li>predictionThreshold - threshold that shall be used for prediction</li>
     *     <li>gainWeightings - optional; will trigger evaluation of weighted gain. Example (gainWeightings=1.0,0.6,0.025,0.975) first two values are the class weights third and forth value represent the respective portion of the class in the distribution. Multiple weightings allowed seperated by ";"</li>
     *     <li>numberOfExportedErrors - number of misclassified examples that should be exported; if this is specified, the following values must also be set</li>
     *     <li>vectorMappingFilePath - path to the mapping file for the vectors that connects them to their respective DB entries</li>
     *     <li>errorFileOutputPath - path to where to put the error report</li>
     *     <li>testVectorsPropertiesFilePath - path to the properties-file of the used test-vector-set</li>
     * </ul>
     * @param configPath path to the properties file
     * @param methodVersionId should be methodVersionId of the calling method (security measure to prevent processing of input with old method versions)
     */
    public void readProperties(Path configPath, String methodVersionId){
        Properties properties = new Properties();
        File propertiesFile = configPath.toFile();

        try (FileInputStream in = new FileInputStream(propertiesFile)){
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String methodID = properties.getProperty("METHOD_VERSION_ID");
        if (!methodVersionId.equals(methodID)){
            throw new IllegalStateException("METHOD_VERSION_ID MISMATCH: the properties file specifies a different ID");
        }

        if (
                properties.getProperty("numberOfExportedErrors") != null &&
                properties.getProperty("vectorMappingFilePath") != null &&
                properties.getProperty("errorFileOutputPath") != null &&
                properties.getProperty("testVectorsPropertiesFilePath") != null
        ){

            this.numberOfExportedErrors = Integer.valueOf(properties.getProperty("numberOfExportedErrors"));
            this.vectorMappingFilePath = properties.getProperty("vectorMappingFilePath");
            this.errorFileOutputPath = properties.getProperty("errorFileOutputPath");
            this.testVectorsPropertiesFilePath = properties.getProperty("testVectorsPropertiesFilePath");
        }else{
            this.numberOfExportedErrors = 0;
        }

        this.svmLightFilePath = properties.getProperty("svmLightFileLocation");
        if (this.svmLightFilePath == null || this.svmLightFilePath.equals("")) {
            throw new IllegalStateException("No valid svmLightFilePath-property value given.");
        }

        this.modelFilePath = properties.getProperty("modelFileLocation");
        if (this.modelFilePath == null || this.modelFilePath.equals("")) {
            throw new IllegalStateException("No valid modelFilePath-property value given.");
        }

        if (properties.getProperty("predictionThreshold") != null){
            this.predictionThreshold = Double.valueOf(properties.getProperty("predictionThreshold"));
        }else{
            this.predictionThreshold = 0.5;
        }

        String weightingInfoString = properties.getProperty("gainWeightings");
        if (weightingInfoString != null){
            this.weightingInfos = new ArrayList<>();
            String[] weightings = weightingInfoString.split(";\\s*");
            for (int i = 0; i < weightings.length; i++) {
                String weighting = weightings[i];
                String[] valueString = weighting.split(",\\s*");
                this.weightingInfos.add(new WeightedGainWeightingInfo(
                        Double.parseDouble(valueString[0]),
                        Double.parseDouble(valueString[1]),
                        Double.parseDouble(valueString[2]),
                        Double.parseDouble(valueString[3])
                ));
            }
        }
    }
}

/**
 * Base package to provide utility to simplify working with Latex Tables.
 */
package bous.philipp.tools.latex;
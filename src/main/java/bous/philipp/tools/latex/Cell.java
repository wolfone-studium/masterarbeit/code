package bous.philipp.tools.latex;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Cell {
    private int row;
    private int column;
    private String content;
}

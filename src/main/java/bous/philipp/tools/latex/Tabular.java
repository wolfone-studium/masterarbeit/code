package bous.philipp.tools.latex;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tabular {
    @Getter @Setter
    int numberOfRows;
    @Getter @Setter
    int numberOfColumns;
    @Getter @Setter
    private String columnDefinition;
    @Getter @Setter
    private List<Cell> cells;

    public Tabular() {
        this.cells = new ArrayList<>();
        this.numberOfRows = 0;
        this.numberOfColumns = 0;
        this.columnDefinition = "";
    }

    public static Tabular fromTabularString(String tabular) {
        Tabular resultTabular = new Tabular();
        resultTabular.columnDefinition = extractColumnDefinition(tabular);
        String[] rows = extractRows(tabular);
        resultTabular.parseCells(rows);
        resultTabular.numberOfRows = rows.length;
        List<Cell> cellList = resultTabular.cells;
        for (int i = 0; i < cellList.size(); i++) {
            Cell cell = cellList.get(i);
            if (resultTabular.numberOfColumns < cell.getColumn() + 1) {
                resultTabular.numberOfColumns = cell.getColumn() + 1;
            }
        }
        return resultTabular;
    }

    public Cell[][] cellMatrix(){
        Cell[][] tableMatrix = new Cell[numberOfRows][numberOfColumns];
        for (int i = 0; i < cells.size(); i++) {
            Cell cell = cells.get(i);
            tableMatrix[cell.getRow()][cell.getColumn()] = cell;
        }
        return tableMatrix;
    }

    public static String[] extractRows(String tabular) {
        String[] lines = tabular.split("\\R");
        ArrayList<String> tempLines = new ArrayList<>();
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            if (line.contains("&")) {
                tempLines.add(line);
            }
        }
        return tempLines.toArray(new String[tempLines.size()]);
    }

    public void parseCells(String[] rows) {
        for (int i = 0; i < rows.length; i++) {
            String row = rows[i];
            parseRow(row, i);
        }
    }

    //will fail if the row contains "&" that are not column seperators; very barebone after all
    private void parseRow(String row, int rowNumber) {
        String tempRow = stripHlines(row).trim();
        tempRow = tempRow.substring(0, tempRow.length() - 2);
        String[] cellContents = tempRow.split("&");
        for (int i = 0; i < cellContents.length; i++) {
            String cellContent = cellContents[i];
            this.cells.add(new Cell(rowNumber, i, cellContent));
        }
    }

    private static String stripHlines(String table) {
        return table.replaceAll("\\\\hline", "");
    }

    public static String extractColumnDefinition(String tabular) {
        Pattern pattern = Pattern.compile("\\\\begin\\{tabular\\}\\{(.*)\\}");
        Matcher matcher = pattern.matcher(tabular);

        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new IllegalArgumentException("Input seems to be malformatted. Check for example for unnecessary whitespaces.");
        }
    }

    public void removeVerticalLines(){
        this.columnDefinition = this.columnDefinition.replaceAll("\\|", "");
    }

    public String prettyTableString(){
        Cell[][] cellMatrix =cellMatrix();
        int[] columnWidths = getColumnWidthsForPrettyString(cellMatrix);

        StringBuilder prettyTable = new StringBuilder();
        prettyTable.append("\\begin{tabular}{").append(this.columnDefinition).append("}\n");
        prettyTable.append("\\toprule\n");
        for (int row = 0; row < numberOfRows; row++) {
            for (int column = 0; column < numberOfColumns; column++) {
                String cellContent = cellMatrix[row][column].getContent();
                prettyTable.append("    ").append(" ".repeat(columnWidths[column] - cellContent.length())).append(cellContent);
                if (!(column == numberOfColumns - 1)) {
                    prettyTable.append(" & ");
                }else{
                    prettyTable.append("\\\\ \n");
                }
            }
        }
        prettyTable.append("\\bottomrule\n");
        prettyTable.append("\\end{tabular}");
        return prettyTable.toString();
    }

    public void reduceCellContentToCaptureGroup(Pattern pattern, int captureGroup){

        for (int i = 0; i < cells.size(); i++) {
            Cell cell = cells.get(i);
            Matcher matcher = pattern.matcher(cell.getContent());
            if (matcher.find()) {
                cell.setContent(matcher.group(captureGroup));
            }
        }
    }

    private int[] getColumnWidthsForPrettyString(Cell[][] cellMatrix) {
        int[] columnWidths = new int[numberOfColumns];
        for (int column = 0; column < numberOfColumns; column++) {
            for (int row = 0; row < numberOfRows; row++) {
                int contentLength = cellMatrix[row][column].getContent().length();
                if (columnWidths[column] < contentLength) {
                    columnWidths[column] = contentLength;
                }
            }
        }
        return columnWidths;
    }
}

SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE table records_arxiv; 
TRUNCATE table features_arxiv;
SET FOREIGN_KEY_CHECKS = 1;
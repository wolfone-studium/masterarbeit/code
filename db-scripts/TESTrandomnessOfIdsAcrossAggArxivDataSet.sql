SELECT arxiv_records_agg.id, arxiv_records_agg.random_id, arxiv_records_agg.cs_classification , arxiv_features_agg.type, arxiv_features_agg.value from masterarbeit.arxiv_records_agg 
LEFT JOIN arxiv_features_agg ON arxiv_records_agg.id = arxiv_features_agg.record_id where arxiv_features_agg.type = "CATEGORIES"
order by random_id asc;
DROP PROCEDURE IF EXISTS copyFeatures;
DELIMITER ;;

CREATE PROCEDURE copyFeatures()
BEGIN
DECLARE currentId INT DEFAULT 0;
DECLARE n INT DEFAULT 0;
DECLARE i INT DEFAULT 0;
SELECT COUNT(*) FROM arxiv_records_rand_originaldist_test INTO n;
SET i=0;
WHILE i<n DO 
  SET currentId = (SELECT id FROM arxiv_records_rand_originaldist_test LIMIT i,1);	
  INSERT INTO arxiv_features_rand_originaldist_test SELECT * FROM arxiv_features_agg WHERE currentId=record_id;
  SET i = i + 1;
END WHILE;
End;
;;

DELIMITER ;

CALL copyFeatures();